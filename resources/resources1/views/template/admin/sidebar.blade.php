<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::to('admin/images/profile/avatar_1.png')}}" class="img-circle" alt="User">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ \App\Utils::checkRoute('admin') ? 'active': '' }}">
                <a href="{{ route('admin.home') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php
            $get_permissions = \App\Utils::getSideMenuData();
            foreach($get_permissions as $main){
            if($main->menu_parent == 0){
            $parent = \App\Utils::getParentRoute();

            if( $parent == $main->id){?>
            <li class="treeview active ">
            <?php }else{ ?>
            <li class="treeview ">
                <?php  } ?>

                <a href="#"><i class="{{$main -> menu_icon}}"></i> <span>{{$main -> menu_name}}</span>

                    <span class="pull-right-container">

                      <i class="fa fa-angle-left pull-right"></i>

                    </span>

                </a>

                <ul class="treeview-menu">

                    <?php

                    foreach ($get_permissions as $sub) {

                    if($sub->menu_parent == $main->id)

                    {
                    if($sub->show_menu){

                    ?>

                    <li class="{{ \App\Utils::checkRoute($sub -> role_name) ? 'active': '' }}"><a
                            href="{{ url(\App\Utils::getUrlRoute().'/'.$sub -> menu_path) }}" class="active"><i
                                class="fa fa-circle-o"></i>{{$sub -> menu_name}}</a></li>

                    <?php } }

                    }?>

                </ul>

            </li>

            <?php

            }

            }

            ?>

        </ul>


    </section>
    <!-- /.sidebar -->
</aside>
