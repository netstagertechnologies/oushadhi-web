<footer class="{{$footer_class or ''}}">
    <div class="wid">
        <div class="fotdrs">
            <a href="{{route('/')}}" class="footerlogo" title="Oushadhi"><img
                    src="{{ asset('ui/images/footerlogo.png')}}" title="Oushadhi"></a>
            <p>{!! $site_settings['config_address'] or '' !!} </p>
            <ul class="phnemail">
                <li class="pHne"><a href="tel:{{$site_settings['config_contact_number1'] or ''}}"
                                    title="{{$site_settings['config_contact_number1'] or ''}}">{{$site_settings['config_contact_number1'] or ''}}</a>
                </li>
                <li><a href="tel:{{$site_settings['config_contact_number2'] or ''}}"
                       title="{{$site_settings['config_contact_number2'] or ''}}">{{$site_settings['config_contact_number2'] or ''}}</a>
                </li>
                <li class="maIl"><a href="mailto:{{$site_settings['config_contact_email'] or ''}}"
                                    title="{{$site_settings['config_contact_email'] or ''}}">{{$site_settings['config_contact_email'] or ''}}</a>
                </li>
            </ul>
        </div>
        <div class="qklinks">
            <span class="ftrhed">Quick Links</span>
            <ul>
                <li><a href="{{route('/')}}" title="Home">Home</a></li>
                <li><a href="{{route('media')}}" title="Media">Media</a></li>
                <li><a href="{{route('about-us')}}" title="About us">About us</a></li>
                <li><a href="{{route('tenders')}}" title="Tenders">Tenders</a></li>
                <li><a href="{{route('products')}}" title="Products">Products</a></li>
                <li><a href="{{URL::to('/').'/careers'}}" title="Careers">Careers</a></li>
                <li><a href="{{route('activities')}}" title="Activities">Activities</a></li>
                <li><a href="{{route('tenders')}}" title="Downloads">Downloads</a></li>
                <li><a href="{{ URL::to('/').'/hospital'}}" title="Hospital">Hospital</a></li>
                <li><a href="{{ URL::to('/').'/online-purchase'}}" title="Online purchase">Online purchase</a></li>
                <li><a href="{{route('contact-us')}}" title="Downloads">Contact</a></li>
            </ul>
        </div>

        <div class="prdctinks">
            <span class="ftrhed">Products</span>
            <ul>
                <?php $footer_products = \App\Utils::footerProducts();?>
                @if(isset($footer_products))
                    @foreach($footer_products as $footer_product)
                        <li><a href="{{ URL::to('/').'/product/'.$footer_product->slug}}"
                               title="{{$footer_product->title}}">{{$footer_product->title}}</a></li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="copyrgt">
            <p>© <?php echo date("Y")?> Oushadhi.org All Rights Reserved</p>
            <div class="social">
                <span>Follow us :</span>
                <ul>
                    <li><a class="fb" target="_blank" href="{{ $site_settings['config_facebook_page'] or '#' }}"
                           title="facebook"></a></li>
                    <li><a class="tw" target="_blank" href="{{ $site_settings['config_twitter_page'] or '#' }}"
                           title="twitter"></a></li>
                    <li><a class="ig" target="_blank" href="{{ $site_settings['config_instagram_page'] or '#' }}"
                           title="instagram"></a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>
