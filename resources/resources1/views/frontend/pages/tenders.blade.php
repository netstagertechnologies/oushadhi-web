@extends('layouts.frontend_template',['page_title'=>'Tenders'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Activities</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($tenders))
        <section class="innercontentarea">
            <div class="wid">
                <div class="tenters-main">
                    @foreach($tenders as $tender)
                        <div class="tenters-full">
                            <div class="stenter-left">
                                <p>{{$tender['title']}}
                                    @if($tender['from_date']) for the period
                                    <strong>{{date('d.m.Y', strtotime($tender['from_date']))}}
                                        to {{date('d.m.Y', strtotime($tender['to_date']))}}</strong>
                                    @endif. Last date & Time of receipt of tender
                                    forms<strong> {{date('d.m.Y', strtotime($tender['last_date']))}}
                                        , {{$tender['last_time']}}</strong>
                                </p>
                                <p>{{$tender['dept'] or ''}} , Email Id : <strong>{{$tender['contact'] or ''}}</strong>
                                </p>
                            </div>
                            @if(isset($tender['tender_files'])&&$tender['tender_files'])
                                <div class="stenter-right">
                                    <ul>
                                        @php $i=1;@endphp
                                        @foreach($tender['tender_files'] as $tender_file)
                                            <li>{{$i}}.{{$tender_file->title or ''}} <a href="{{asset('uploads/tenders/'.$tender_file->file_link)}}" target="_blank">
                                                    <img src="{{asset('ui/images/downloadbr.png')}}" alt="{{$tender_file->title or ''}}"></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

@endsection
