@extends('layouts.frontend_template',['page_title'=>'About Us'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Activities</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            <div class="abtsec">
                <h2 class="innersubhead">Largest producer of ayurveda medcines in public sector in india</h2>
                <p>Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector
                    companies, consistently <br> making profit and paying dividend to Government of Kerala since 1999.
                    Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.</p>
            </div>
            <div class="abtsec1">
                <div class="abtsec1-left">
                    <p>Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector
                        companies, consistently making profit and paying dividend to Government of Kerala since 1999.
                        <br>
                        Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.
                        Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector
                        companies, consistently making profit and paying dividend to Government of Kerala since 1999.
                        Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.
                    </p>
                </div>
                <div class="abtsec1-right">
                    <img src="{{asset('ui/images/about-us.jpg')}}" alt="" title="" class="fullwidth">
                </div>
            </div>
        </div>
    </section>
    <section class="innercontentarea2">
        <div class="wid">
            <div class="incont2-left">
                <img src="{{asset('ui/images/visionmision.jpg')}}" alt=" title" class="fullwidth">
            </div>
            <div class="incont2-right">
                <div class="inc-heading">Our Vision</div>
                <p>A leading world class Ayurveda Industry in the country by 2020. A leading world class Ayurveda
                    Industry in the country by 2020. A leading world class Ayurveda Industry in the country by 2020. A
                    leading world class Ayurveda Industry in the country by 2020. A leading world class Ayurveda
                    Industry in the country by 2020.</p>
                <div class="inc-heading">Mission</div>
                <p>Production and supply quality medicine at reasonable price</p>
            </div>
        </div>
    </section>
    <section class="innercontentarea3">
        <div class="incont2-left">
            <div>
                <div class="inc-heading">Milestones</div>
                <p>Commenced by His Highness, the Maharaja of Cochin as " Sree Kerala Varma Ayurveda Pharmacy". In 1959
                    , converted into co-operative society viz. Sree Kerala Varma Ayurveda Pharmacy and Stores Ltd.View
                    More</p>
            </div>
        </div>
        <div class="incont2-right">
            <img src="{{asset('ui/images/milestone.jpg')}}" alt=" title" class="fullwidth">
        </div>
    </section>
    <section class="abt-bottom">
        <div class="wid">
            <div class="abtbottom-right">
                <div class="abtbt-head">Strengths</div>
                <ul>
                    <li> Established brand image</li>
                    <li>Continuous Government Support</li>
                    <li>Huge market demand</li>
                    <li>Dedicated work force</li>
                </ul>
                <div class="abtbt-head">CORE VALUES</div>
                <ul>
                    <li>Mutual trust and respect</li>
                    <li>Customer satisfaction</li>
                    <li> Quality control</li>
                    <li>Professional ethics</li>
                    <li> March with time</li>
                </ul>
            </div>
        </div>
    </section>

@endsection
