@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_details->title or 'Oushadhi'}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="abtsec">
                <h1 class="main_heading">{{$page_title or ''}}</h1>
               <p>{!! $page_details->page_content or '' !!}</p>
            </div>

        </div>
    </section>


@endsection
