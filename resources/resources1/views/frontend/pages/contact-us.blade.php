@extends('layouts.frontend_template',['page_title'=>'Contact Us'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Contact Us</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="contact-main">
                @include('template.frontend.alert')
                <div class="contact-form">

                    <h1>Enquire Now</h1>
                    <form action="{{route('contact-us.formSubmit')}}" method="post">
                        <div class="formdiv-split {{($errors->has('name'))?'has-error':''}}">
                            <input type="text" placeholder="Your Name" name="name" class="cont-input" required>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv-split last {{($errors->has('email'))?'has-error':''}}">
                            <input type="text" placeholder="Your Email" name="email" class="cont-input " required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv {{($errors->has('mob_no'))?'has-error':''}}">
                            <input type="text" placeholder="Mobile No" name="mob_no" class="cont-input" required>
                            @if ($errors->has('mob_no'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mob_no') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="formdiv {{($errors->has('message'))?'has-error':''}}">
                            <textarea class="contacttextariea" name="message" placeholder="Your Message"
                                      required></textarea>
                            @if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input type="submit" value="Submit" class="Cont-btn">
                        {{ csrf_field() }}
                    </form>
                </div>
                @if($contacts)
                    <div class="contact-pers">
                        <ul>
                            @foreach($contacts as $contact)
                                <?php $image = ($contact->image) ? $contact->image : 'placeholder.png';?>

                                <li><img src="{{ asset('uploads/contacts/'.$image)}}" alt="" title="">
                                    <div class="conta-details">
                                        {{$contact->name or ''}}
                                        <span>{{$contact->designation or ''}}</span>
                                        <a href="tel:{{$contact->mob_no or ''}}"> {{$contact->mob_no or ''}}</a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </section>

@endsection
