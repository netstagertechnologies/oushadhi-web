@extends('layouts.email_template',['title'=>'Quotation created'])

@section('content')

    <tr>
        <td height="75" align="center" valign="middle" bgcolor="#009209">
            <table width="650" border="0" cellspacing="0" cellpadding="0">
               <tr>
                    <td align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif">
                            Quotaion #{{$data['quotation_id'] or ''}} has been created</font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="35" align="center" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table width="95%" align="center" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" bgcolor="#f9f9f9" cellpadding="6"
                               style="border:1px #06b51d solid;padding: 8px 0;margin-top: 10px;">
                            <tr>
                                <td align="left" valign="middle" style="font-size:13px;"><font
                                        face="Montserrat, sans-serif">User {{ucfirst($data['user_name'])}} created a new quotation
                                        #{{$data['quotation_id'] or ''}}.</font></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-size:16px;"><font face="Montserrat, sans-serif">Quotation
                            Details</font></td>
                </tr>

                <tr>
                    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="6" style="margin-top: 10px;">
                            @if(isset($data['quotation']['quotation_details']))
                                <tr>
                                    <td align="left" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif">Product Name</font>
                                    </td>
                                    <td align="left" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif">Quantity</font>
                                    </td>
                                    <td align="left" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif">Price</font>
                                    </td>
                                    <td align="left" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif">Total</font>
                                    </td>
                                </tr>
                                @foreach($data['quotation']['quotation_details'] as $details)
                                    <tr>
                                        <td align="left" valign="middle" style="font-size:13px;"><font
                                                face="Montserrat, sans-serif">{{$details->product_name}}<br/>
                                                Unit: {{$details->unit_value}}
                                            </font></td>
                                        <td align="left" valign="middle" style="font-size:13px;"><font
                                                face="Montserrat, sans-serif">{{$details->qty}}</font>
                                        </td>
                                        <td align="left" valign="middle" style="font-size:13px;"><font
                                                face="Montserrat, sans-serif">Rs {{$details->price}}</font>
                                        </td>
                                        <td align="left" valign="middle" style="font-size:13px;"><font
                                                face="Montserrat, sans-serif">Rs {{$details->total}}</font>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td align="left"  colspan="3" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif"><b>Total</b>
                                        </font></td>
                                    <td align="left" valign="middle" style="font-size:13px;"><font
                                            face="Montserrat, sans-serif">Rs {{$data['quotation']['total']}}</font>
                                    </td>
                                </tr>
                            @endif
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
@endsection
