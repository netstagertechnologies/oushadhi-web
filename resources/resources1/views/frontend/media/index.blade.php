@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title or ''}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="gallery-tab">

                <div class="tab-wrap">
                    <div class="tabBlock">

                        <ul class="tabBlock-tabs">

                            <li class="tabBlock-tab is-active">Photo GAllery</li>
                            <li class="tabBlock-tab"> video gallery</li>


                        </ul>
                        <!--tabBlock-content -->
                        <div class="tabBlock-content">
                            <!--tab01 -->
                            <div class="tabBlock-pane">

                                <div class="demo-gallery">

                                    <ul id="lightgallery2" class="list-unstyled row">
                                        @if(isset($medias))
                                            @foreach($medias as $medi)
                                                <?php $image = ($medi->cover) ? $medi->cover : 'placeholder.jpg';?>

                                                <li data-responsive="{{ asset('uploads/media/'.$image)}}"
                                                    data-src="{{ asset('uploads/media/'.$image)}}"
                                                    data-sub-html="">
                                                    <a href="{{ URL::to('/').'/media/'.$medi->slug}}">
                                                        <div class="hover-effect">
                                                            <i class="linkico" aria-hidden="true"></i>
                                                            <img src="{{ asset('uploads/media/'.$image)}}" alt="">
                                                        </div>
                                                    </a>
                                                    <h3>{{$medi->title}}</h3>
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <!--/tab01 -->
                            <!--tab02 -->
                            <div class="tabBlock-pane">
                                <ul class="vediolist">
                                    @if(isset($media_videos))
                                        @foreach($media_videos as $media_video)
                                            <li>
                                                <iframe width="100%" height="215"
                                                        src="{{$media_video->link}}" frameborder="0"
                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen></iframe>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <!--tab02 -->
                        </div>
                        <!--/tabBlock-content -->
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
