@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_title}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="product-detailsin">
                <div class="productdet-left">
                    <div>

                        <input type="hidden" name="ctl00$hfMenu" id="hfMenu" value="0.0"/>

                        <input type="hidden" name="ctl00$cphBody$__VIEWxSTATE" id="__VIEWxSTATE" value="MTE2Njs2Ozk="/>

                    </div>


                    <div class="container">
                        <div class="row">

                            <ul id='girlstop1' class='gc-start'>
                                <?php $image = ($product->image) ? $product->image : 'placeholder.jpg';?>

                                <li><img src="{{asset('uploads/product/'.$image)}}" alt='image1'
                                         data-gc-caption="{{$product->title or ''}}"/></li>
                                @if(isset($product_images))
                                    @foreach($product_images as $i=>$product_image)
                                        <?php $image = ($product->image) ? $product->image : 'placeholder.jpg';?>

                                        <li><img src="{{asset('uploads/product/'.$image)}}" alt='image{{$i+2}}'
                                                 data-gc-caption="{{$product->title.'-'.($i+2) }}"/></li>
                                    @endforeach
                                @endif
                            </ul>
                            <div class="pInstructions">
                                Roll over image to zoom in
                            </div>


                        </div>
                    </div>
                </div>
                <div class="productdet-right">
                    <div class="pro-rigt-top">
                        <div class="prodet-head">{{$product->title or ''}}</div>
                        @if(!empty($product->total_ingradients))
                            <p><strong>Total Ingradients</strong>: {{$product->total_ingradients}} </p>
                        @endif
                        @if(!empty($product->main_ingradients))
                            <p><strong>Main Ingradients</strong>: {{$product->main_ingradients}} </p>
                        @endif
                        @if(!empty($product->indication))
                            <p><strong>Indication</strong>{{$product->indication}} </p>
                        @endif
                        @if(!empty($product->special_indication))
                            <p><strong>Special Indication</strong>: {{$product->special_indication}} </p>
                        @endif
                        @if(!empty($product->dosage))
                            <p><strong>Dosage</strong>{{$product->dosage}} </p>
                        @endif
                        @if(!empty($product->presentation))
                            <p><strong>Presentation</strong>: {{$product->presentation}} </p>
                        @endif
                    </div>
                    <div class="pro-rigt-bottom">
                        <span class="somedia">Share : <a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=295465317810903&display=popup&caption=Oushadhi&name={{$product->title or '' }}&link={{ URL::to('/').'/product/'.$product->slug}}&picture={{asset('uploads/product/'.$image)}}&description=Click to view more details&redirect_uri={{ URL::to('/').'/product/'.$product->slug}}"><img src="{{asset('ui/images/fbp.png')}}" alt=""> </a>
                            <a target="_blank" href="https://www.tumblr.com/share"><img src="{{asset('ui/images/tp.png')}}" alt=""></a>
                            <a target="_blank" href="http://twitter.com/share?text=$product->title or '' Visit : &url={{ URL::to('/').'/product/'.$product->slug}}"><img src="{{asset('ui/images/twp.png')}}" alt=""></a>
                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{ URL::to('/').'/product/'.$product->slug}}&title={{$product->title or '' }}&source=LinkedIn"><img src="{{asset('ui/images/inp.png')}}" alt=""></a>
                            <a target="_blank"href="https://plus.google.com/share?url={{ URL::to('/').'/product/'.$product->slug}}"> <img src="{{asset('ui/images/gpp.png')}}" alt=""></a>

                        </span>
                        <span class="star"> Review :<ul>
                                @for($rt=0;$rt<5;$rt++)
                                    @if($rt<(int)$product->rating)
                                        <li class="active"></li>
                                    @else
                                        <li></li>
                                    @endif
                                @endfor
                            </ul> </span>

                        @if(isset($product_prices))
                            <table border="1">
                                <tr>
                                    <th>Unit</th>
                                    <th>Price</th>
                                </tr>
                                @foreach($product_prices as $product_price)
                                    <tr>
                                        <td>{{$product_price->unit_value or ''}}</td>
                                        <td>{{\App\Utils::formatPrice($product_price->price)}}</td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>

                <div class="product-tab">
                    <div class="tab-wrap">
                        <div class="tabBlock">
                            <ul class="tabBlock-tabs">
                                <li class="tabBlock-tab is-active">Description</li>
                                <li class="tabBlock-tab"> Additional information</li>
                            </ul>
                            <!--tabBlock-content -->
                            <div class="tabBlock-content">
                                <!--tab01 -->
                                <div class="tabBlock-pane">
                                    {!! $product->description or '' !!}
                                </div>
                                <!--/tab01 -->
                                <!--tab02 -->
                                <div class="tabBlock-pane">
                                    bb{!! $product->description or '' !!}
                                </div>
                                <!--tab02 -->
                            </div>
                            <!--/tabBlock-content -->
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <!--Slider Ends-->
    <script src="{{ asset('ui/js/jquery-1.12.4.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function (event) {
            $('.pInstructions').hide();
            //ZOOM
            $("#girlstop1").glassCase({
                'widthDisplay': 430,
                'heightDisplay': 300,
                'isSlowZoom': true,
                'isSlowLens': true,
                'capZType': 'in',
                'thumbsPosition': 'bottom',
                'isHoverShowThumbs': true,
                'colorIcons': '#F15129',
                'colorActiveThumb': 'rgb(255, 216, 0)',
                'mouseEnterDisplayCB': function () {
                    $('.pInstructions').text('Click to open expanded view');
                },
                'mouseLeaveDisplayCB': function () {
                    $('.pInstructions').text('Roll over image to zoom in');
                }
            });
            setTimeout(function () {
                $('.pInstructions').css({
                    'width': $('.gc-display-area').outerWidth(),
                    'left': parseFloat($('.gc-display-area').css('left'))
                });
                $('.pInstructions').fadeIn();
            }, 1000);

            $('#btnFeatures').on('click', function () {
                $('html, body').animate({
                    scrollTop: $('.tc-all-features').offset().top - 50 + 'px'
                }, 800);
            });
        });
    </script>

    <script id="tumblr-js" async src="https://assets.tumblr.com/share-button.js"></script>

@endsection
