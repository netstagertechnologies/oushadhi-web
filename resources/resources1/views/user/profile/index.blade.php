@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    @include('template.user.alert')
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUserUrlRoute().'/profile/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name" placeholder="Name"
                                                   value="{{$page_data->name or ''}}">
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Mobile Number</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="mobile" placeholder="Mobile"
                                                   value="{{$page_data->mobile or ''}}">
                                            {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Phone</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone"
                                                   value="{{$page_data->phone or ''}}">
                                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('district') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">District</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="district"
                                                   placeholder="District"
                                                   value="{{$page_data->district or ''}}">
                                            {!! $errors->first('district', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('state') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">State</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="state" placeholder="State"
                                                   value="{{$page_data->state or ''}}">
                                            {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Address</label>
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <textarea class="form-control" name="address" placeholder="Enter address"
                                                      rows="3"
                                            >{{$page_data->address or ''}}</textarea>
                                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUserUrlRoute().'/profile') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

@endsection
