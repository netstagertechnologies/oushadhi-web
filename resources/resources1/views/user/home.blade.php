@extends('layouts.user_template',$tabs)

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Dashboard
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        @include('template.user.alert')
        <!-- Small boxes (Stat box) -->

            <div class="row">

            </div>

        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

@endsection
