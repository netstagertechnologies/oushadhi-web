@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>


        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('template.user.alert')
                            <div class="pull-right">
                                @if($page_data->quotation_status!=5)
                                    <button type="button" class="btn btn-danger" data-toggle="modal"
                                            data-target="#modal-default">
                                        Cancel Quotation
                                    </button>
                                @endif
                                <a
                                    href="{{ url(\App\Utils::getUserUrlRoute().'/quotations/') }}"
                                    class="btn btn-warning btn-sm" title="Back">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back
                                </a>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-general" data-toggle="tab">{{ 'Overview' }}</a>
                                </li>
                                <li><a href="#tab-status" data-toggle="tab">{{ 'Status' }}</a></li>
                            </ul>
                            <div class="tab-content">
                                <br/>
                                <div class="tab-pane active" id="tab-general">
                                    <div class="pull-right">
                                        <a href="#" class="btn btn-info btn-sm" onclick="exportToPDF()"><i
                                                class="fa fa-file-pdf-o" aria-hidden="true"></i> Export to PDF</a>
                                        <br/><br/>
                                    </div>
                                    <div id="quotatation">
                                        <table class="table table-striped">
                                            <colgroup>
                                                <col width="40%">
                                                <col width="15%">
                                                <col width="15%">
                                                <col width="15%">
                                                <col width="15%">
                                            </colgroup>
                                            <thead>
                                            <tr class='warning'>
                                                <th>Product Name</th>
                                                <th>Unit</th>
                                                <th>Price</th>
                                                <th>Qty</th>
                                                <th>Total</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @php $total=0; $products=array();@endphp
                                            @if(isset($quotation_details))
                                                @foreach($quotation_details as  $product)
                                                    <tr>
                                                        <td>{{$product->product_name}}</td>
                                                        <td>{{$product->unit_value}}</td>
                                                        <td>{{\App\Utils::formatPrice($product->price)}}</td>
                                                        <td>{{$product->qty}}</td>
                                                        <td>{{\App\Utils::formatPrice($product->total)}}</td>
                                                    </tr>
                                                    @php $total+=$product->total;@endphp
                                                @endforeach
                                            @endif
                                            </tbody>

                                            <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><b>Total</b></td>
                                                <td>{{\App\Utils::formatPrice($total)}}</td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-status">
                                    <br/>
                                    <!-- The time line -->
                                    <ul class="timeline">
                                        <!-- timeline time label -->
                                        <li class="time-label">
                                            <span class="bg-red"> Quotation status</span>
                                        </li>
                                        <!-- /.timeline-label -->
                                        @if($quotation_histories)
                                            @foreach($quotation_histories as $history)
                                                <li>
                                                    <i class="fa fa-check bg-green"></i>

                                                    <div class="timeline-item">
                                                        <span class="time"><i
                                                                class="fa fa-clock-o"></i> {{$history->date_added}}</span>

                                                        <h3 class="timeline-header no-border"><a
                                                                href="#">{{$history->name}}</a>
                                                            {{$history->comment}}</h3>
                                                    </div>
                                                </li><!-- timeline item -->
                                        @endforeach
                                    @endif

                                    <!-- END timeline item -->
                                        <li>
                                            <i class="fa fa-clock-o bg-gray"></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Cancel Quotation</h4>
                                    </div>
                                    <form class="form-horizontal" method="POST"
                                          action="{{url(\App\Utils::getUserUrlRoute().'/cancel-quotation')}}"
                                          accept-charset="UTF-8" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="modal-body">
                                            <p>
                                            <div class="form-group {{ $errors->has('budget') ? 'has-error' : ''}}">
                                                <label class="col-sm-3 control-label required">Reason</label>

                                                <div class="col-sm-7">
                                                        <textarea rows="5" class="form-control" name="reason"
                                                                  placeholder="Enter cancel reason"
                                                        ></textarea>
                                                </div>
                                                <input type="hidden" class="form-control" name="quotation_id"
                                                       value="{{(int)$page_data->quotation_id}}">


                                            </div>


                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left"
                                                    data-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Cancel Quotation</button>
                                        </div>

                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <script type="text/javascript" src="{{ asset('exprt/jspdf.debug.js')}}"></script>
    <script>
        function exportToPDF() {


            var pdf = new jsPDF('p', 'pt', 'a4');
            // source can be HTML-formatted string, or a reference
            // to an actual DOM element from which the text will be scraped.
            source = $('#quotatation')[0];

            // we support special element handlers. Register them with jQuery-style
            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
            // There is no support for any other type of selectors
            // (class, of compound) at this time.
            specialElementHandlers = {};
            margins = {
                top: 20,
                bottom: 20,
                left: 20,
                right: 20,
                width: 992,
                setFontSize: 12
            };
            // all coords and widths are in jsPDF instance's declared units
            // 'inches' in this case

            pdf.setFontSize(12);
            pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, { // y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },

                function (dispose) {
                    // dispose: object with X, Y of the last line add to the PDF
                    //          this allow the insertion of new lines after html
                    pdf.save('Quotation-{{$page_data->quotation_id or '0'}}');
                }, margins);
        }

    </script>
@endsection
