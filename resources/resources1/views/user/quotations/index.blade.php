@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">

                        <div class="panel-body"><a
                                href="{{ url(\App\Utils::getUserUrlRoute().'/quotations/create') }}"
                                class="btn btn-success btn-sm" title="Add New Media">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.user.alert')

                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th style="width:50%">Quotation</th>
                                        <th>Amount</th>
                                        <th>Quotation Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td> Quotation #{{$d->quotation_id}}</td>
                                                <td>{{ \App\Utils::formatPrice($d->total) }}</td>
                                                <td>{{ \App\models\Common::getQuotationStatusName($d->quotation_status) }}</td>
                                                <td>

                                                    <a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id ) }}"
                                                       class="btn btn-success"><i class="fa fa-eye"></i></a>

                                                    @if($d->quotation_status==1)
                                                        <a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id . '/edit') }}"
                                                           class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                                    @endif
                                                    <form method="POST"
                                                          action="{{ url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id) }}"
                                                          accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger" title="Delete"
                                                                onclick="return confirm('Do you want to delete quotation?')">
                                                            <i
                                                                class="fa  fa-trash-o" aria-hidden="true"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
