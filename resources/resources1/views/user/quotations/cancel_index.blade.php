@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">

                        <div class="panel-body">
                            <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.user.alert')

                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th style="width:20%">Quotation</th>
                                        <th>Amount</th>
                                        <th style="width:40%">Cancel Reason</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td><a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/'.$d->quotation_id)}}"> Quotation #{{$d->quotation_id}}</a></td>
                                                <td>{{ \App\Utils::formatPrice($d->total) }}</td>
                                                <td>{{ $d->reason }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
