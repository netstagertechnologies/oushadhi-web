@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUserUrlRoute().'/quotations/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    @csrf
                                    <div class="form-group {{ $errors->has('budget') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Budget</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="budget" placeholder="Budget"
                                                   value="{{$page_data->total or ''}}">
                                            {!! $errors->first('budget', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>


                                    <table id="products"
                                           class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <td class="text-left" style="width: 45%">{{ 'Product Name' }}</td>
                                            <td class="text-left" style="width: 45%">{{ 'Qty, Price, Total' }}</td>
                                            <td class="text-left" style="width: 10%"></td>
                                        </tr>
                                        </thead>
                                        <tbody id="products-tbody">

                                        @php $product_row = 0;$total=0; $products=array();@endphp
                                        @if(isset($quotation_details))
                                            @foreach($quotation_details as  $product)

                                                <tr id="product-row{{ $product_row }}">
                                                    <td class="text-left" style="width: 40%;">
                                                        <input type="text" name="products[{{ $product_row }}][name]"
                                                               value="{{$product->product_name}}" placeholder="{{ 'Product Name' }}"
                                                               class="form-control"/>
                                                        <input type="hidden"
                                                               name="products[{{ $product_row }}][product_id]"
                                                               value="{{$product->product_id}}"/>
                                                        <input type="hidden"
                                                               name="products[{{ $product_row }}][details_id]"
                                                               value="{{$product->details_id}}"/>
                                                    </td>
                                                    <td class="text-left" id="product-row-unit{{ $product_row }}">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <th style="width:20%">UNIT</th>
                                                            <th style="width:20%">PRICE</th>
                                                            <th style="width:30%">QTY</th>
                                                            <th style="width:30%">TOTAL</th>
                                                            </thead>
                                                            <tr>

                                                                <td>{{$product->unit_value}}
                                                                    <input type="hidden"
                                                                           name="products[{{ $product_row }}][desc][0][unit_value]"
                                                                           value="{{$product->unit_value}}"/>
                                                                </td>
                                                                <td>{{\App\Utils::formatPrice($product->price)}}
                                                                    <input type="hidden"
                                                                           name="products[{{ $product_row }}][desc][0][price]"
                                                                           value="{{$product->price}}"
                                                                           id="hidden-price{{ $product_row }}-0"/>
                                                                </td>
                                                                <td>
                                                                    <input type="number" value="{{$product->qty}}"
                                                                           name="products[{{ $product_row }}][desc][0][qty]"
                                                                           class="form-control" min="0"
                                                                           onchange="calculatePrice(this,'{{ $product_row }}',0)">
                                                                    <input type="hidden" value="{{$product->qty_original}}"
                                                                           name="products[{{ $product_row }}][desc][0][qty_original]"
                                                                           class="form-control"/>
                                                                </td>
                                                                <td>
                                                                    <input type="text"
                                                                           name="products[{{ $product_row }}][0][desc][total]"
                                                                           value="{{ $product->total }}" readonly
                                                                           id="product-row-total{{ $product_row }}-0"
                                                                           placeholder="{{ 'Total' }}"
                                                                           class="form-control quot-tot-row"/>
                                                                </td>
                                                                <input type="hidden"
                                                                       name="products[{{ $product_row }}][status]"
                                                                       value="0"
                                                                       id="product-row-status{{ $product_row }}"
                                                                       class="form-control"/>
                                                                <input type="hidden"
                                                                       name="products[{{ $product_row }}][date_added]"
                                                                       value="{{ $product->date_added }}"
                                                                       class="form-control"/>
                                                            </tr>
                                                            @php $total+=$product->total;@endphp
                                                        </table>
                                                    </td>

                                                    <td class="text-left">
                                                        <button type="button"
                                                                onclick="removeRow({{ $product_row }});calcTotal();"
                                                                data-toggle="tooltip" title="{{ 'Remove' }}"
                                                                class="btn btn-danger">
                                                            <i class="fa fa-minus-circle"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @php $product_row = $product_row + 1; @endphp
                                            @endforeach
                                        @endif
                                        </tbody>

                                        <tfoot>

                                        <tr>
                                            <td colspan="2"></td>
                                            <td class="text-left">
                                                <button type="button" onclick="addProductRowCheck();"
                                                        data-toggle="tooltip" title="Add Product"
                                                        class="btn btn-primary"><i
                                                        class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        <tr class="dt">
                                            <td colspan="2" class="text-right"><b>Products Total</b></td>
                                            <td class="text-left" id="quot-total">
                                                <b>{{\App\Utils::formatPrice($total)}}</b>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>


                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"></label>
                                    <div class="col-sm-10">

                                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                                        <a href="{{ url(\App\Utils::getUserUrlRoute().'/quotations') }}"
                                           class="btn btn-danger pull-left"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


    <script>
        var product_row = '{{ $product_row }}';

        function addProductRowCheck() {
            budget = parseInt($('input[name=\'budget\']').val());
            if (budget > 0) {
                totl = calcTotal();
                if (tot > budget) {
                    var r = confirm("Total amount exceeds your budget amount. Do you want to continue??");
                    if (r) {
                        addProductRow();
                        return true;
                    } else {
                        return false;
                    }
                } else
                    addProductRow();
            } else {
                alert("Enter valid budget amount");
            }
        }

        function addProductRow() {
            html = '<tr id="product-row' + product_row + '">';
            html += '  <td class="text-left" style="width: 40%;"><input type="text" name="products[' + product_row + '][name]" value="" placeholder="{{ 'Product Name' }}" class="form-control" /><input type="hidden" name="products[' + product_row + '][product_id]" value="" /></td>';
            html += '  <td class="text-left" id="product-row-unit' + product_row + '"></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#product-row' + product_row + '\').remove();calcTotal();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#products-tbody').append(html);

            productautocomplete(product_row);

            product_row++;
        }

        function productautocomplete(product_row) {
            $('input[name=\'products[' + product_row + '][name]\']').autocomplete({
                'source': function (request, response) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    var query = $('input[name=\'products[' + product_row + '][name]\']').val();

                    $.ajax({
                        url: '{{ route('user.product.fetch') }}',
                        dataType: 'json',
                        method: "POST",
                        data: {query: query, _token: CSRF_TOKEN},
                        success: function (json) {
                            response($.map(json, function (item) {
                                return {
                                    value: item['product_id'],
                                    label: item['name'],
                                    //category: item['attribute_group']
                                }
                            }));
                        }
                    });
                },
                'select': function (items) {
                    $('input[name=\'products[' + product_row + '][name]\']').val(items['label']);
                    $('input[name=\'products[' + product_row + '][product_id]\']').val(items['value']);
                    modifyUnitPriceColumn(product_row, items['value'])

                }
            });
        }

        function removeRow(row_id) {

            $('#product-row'+row_id).addClass('hidden');
            $('#product-row-status'+row_id).val('1');
            $('#product-row-total'+row_id+'-0').removeClass('quot-tot-row')
        }
        function modifyUnitPriceColumn(product_rows, product_id) {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '{{ route('user.product.price') }}',
                dataType: 'json',
                method: "POST",
                data: {product_id: product_id, _token: CSRF_TOKEN},
                success: function (json) {
                    html1 = '<table class="table table-bordered">';
                    html1 += '<thead><th style="width:20%">UNIT</th><th style="width:20%">PRICE</th>';
                    html1 += '<th style="width:30%">QTY</th><th style="width:30%">TOTAL</th></thead>';
                    col_cnt = 0;
                    $.each(json, function (i, item) {
                        html1 += '<tr><td>' + this.unit_value + '<input type="hidden" name="products[' + product_rows + '][desc][' + col_cnt + '][unit_value]"  value="' + this.unit_value + '"/></td>';
                        html1 += '<td>Rs ' + this.price + '<input type="hidden"  name="products[' + product_rows + '][desc][' + col_cnt + '][price]" value="' + this.price + '" id="hidden-price' + product_rows + '-' + col_cnt + '" /></td>';
                        html1 += '<td><input type="number" value="0" name="products[' + product_rows + '][desc][' + col_cnt + '][qty]"  min="0" class="form-control" onchange="calculatePrice(this,' + product_rows + ',' + col_cnt + ')" ></td>';
                        html1 += '<td><input type="text" name="products[' + product_row + '][' + col_cnt + '][desc][total]" value="0" readonly id="product-row-total' + product_rows + '-' + col_cnt + '" placeholder="{{ 'Total' }}"  value="0" class="form-control quot-tot-row" /></td></tr>'
                        col_cnt = col_cnt + 1;
                    });
                    html1 += '</table>';

                    $('#product-row-unit' + product_rows).html(html1);
                    $('#product-row-' + product_rows).val(0);
                }
            });

        }

        function calculatePrice(col, p_row, price_row) {
            qty = parseInt($(col).val());
            total = parseInt(($('#product-row-total' + p_row).val()));
            unit_price = $('#hidden-price' + p_row + '-' + price_row).val();
            $('#product-row-total' + p_row + '-' + price_row).val((qty * unit_price));
            tot = calcTotal();

            budget = parseInt($('input[name=\'budget\']').val());
            if (tot > budget)
                alert('Total amount exceeded your budget amount.');

        }

        function calcTotal() {
            tot = 0;
            $('input.quot-tot-row').each(function () {
                tot += parseInt($(this).val());
            })
            $('#quot-total').html('<b> Rs ' + tot + '</b>')

            return tot;
        }

        $('#products tbody tr').each(function (index, element) {
            productautocomplete(index);
        });


    </script>
@endsection
