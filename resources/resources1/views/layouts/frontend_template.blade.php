<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{{ $page_title or config('app.name', 'Oushadhi') }}</title>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('ui/images/favicon32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('ui/images/favicon96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('ui/images/favicon16.png')}}">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">


    <link href="{{ asset('ui/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('ui/sass/glasscase.min.css')}}" rel="stylesheet"/>
</head>
<body>



@include('template.frontend.header')


<!-- Start Page Content -->
@yield('content')
@php $footer_class=(isset($footer_class))?'':'innerfoot';@endphp
@include('template.frontend.footer',['footer'=>$footer_class])

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('ui/js/jquery.min.js')}}"></script>
<script src="{{ asset('ui/js/toggle-nav.js')}}" type="text/javascript"></script>
<script>
    document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
        this.classList.toggle( ".active" );
    });
</script>
<!--Menu Ends-->
<script type="text/javascript">
    $(window).scroll(function() {
        $("header").toggleClass("aniPos", $(this).scrollTop() > 0);
    });
</script>
<!--Slider Ends-->

<script src="{{ asset('ui/js/SimpleTabs.js')}}" type="text/javascript"></script>
<script src="{{ asset('ui/js/modernizr.custom.js')}}" type="text/javascript"></script>


<script src="{{ asset('ui/js/jquery.glasscase.min.js')}}"></script>

@yield('page-script')
</body>
</html>
