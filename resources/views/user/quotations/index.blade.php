@extends('layouts.user_template',$tabs)

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">

                        <div class="panel-body"><a
                                href="{{ url(\App\Utils::getUserUrlRoute().'/quotations/create') }}"
                                class="btn btn-success btn-sm" title="Add New Media">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            <br/>
                            <br/>


                            <div class="col-md-12">
                                <div class="row">
                                <form method="GET" action="{{ url(\App\Utils::getUserUrlRoute().'/quotations') }}"
                                      accept-charset="UTF-8" class="navbar-form filter-form" role="search">

                                        <div class="form-group ">
                                            <input type="text" class="form-control"
                                                   value="{{Request::get('quotation_id')}}" name="quotation_id"
                                                   placeholder="Perfoma Invoice Id">
                                        </div>
                                        <div class="form-group ">
                                            <select name="status" id="status" class="form-control">
                                                <option value="">Select Status</option>
                                                <?php $categorys = (Request::get('status') != '') ? Request::get('status') : 0;?>
                                                @foreach($quotation_status as $qt)
                                                    <option value="{{ $qt->status_id }}"
                                                            @if($categorys ==$qt->status_id) selected="" @endif>{{ $qt->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <div class="input-group date">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="from_date"
                                                       class="form-control pull-right"
                                                       placeholder="From" value="{{Request::get('from_date')}}"
                                                       id="datepicker">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="input-group date">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="to_date"
                                                       class="form-control pull-right"
                                                       placeholder="To" value="{{Request::get('to_date')}}"
                                                       id="datepicker1">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <div class="form-group ">
                                            <a href="{{ url(\App\Utils::getUserUrlRoute().'/quotations') }}"
                                               class="btn  btn-warning"><i class="fa fa-close"></i>Clear</a>

                                        </div>
                                </form>
                                </div>
                            </div>

                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.user.alert')

                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th style="width:50%">Perfoma Invoice</th>
                                        <th>Amount</th>
                                        <th>Perfoma Invoice Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td> Perfoma Invoice #{{$d->quotation_id}}</td>
                                                <td>{{ \App\Utils::formatPrice($d->total) }}</td>
                                                <td>{{ \App\models\Common::getQuotationStatusName($d->quotation_status) }}</td>
                                                <td>

                                                    <a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id ) }}"
                                                       class="btn btn-success"><i class="fa fa-eye"></i></a>

                                                    @if($d->quotation_status==1)
                                                        <a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id . '/edit') }}"
                                                           class="btn btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                                    @endif
                                                    <form method="POST"
                                                          action="{{ url(\App\Utils::getUserUrlRoute().'/quotations/' . $d->quotation_id) }}"
                                                          accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger" title="Delete"
                                                                onclick="return confirm('Do you want to delete Perfoma Invoice?')">
                                                            <i
                                                                class="fa  fa-trash-o" aria-hidden="true"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
