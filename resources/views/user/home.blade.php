@extends('layouts.user_template',$tabs)

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User Dashboard
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">
        @include('template.user.alert')
        <!-- Small boxes (Stat box) -->

            <div class="row">
                <div class="col-md-4">
                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Pending Perfoma Invoice</span>
                            <span class="info-box-number">{{$pending_count or 0}}</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <div class="info-box bg-blue">
                        <span class="info-box-icon"><i class="fa fa-registered"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Reviewed Perfoma Invoice</span>
                            <span class="info-box-number">{{$review_count or 0}}</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-spinner"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Processing Perfoma Invoice</span>
                            <span class="info-box-number">{{$processing_count or 0}}</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-check-circle-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Delivered Perfoma Invoice</span>
                            <span class="info-box-number">{{$delivered_count or 0}}</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-times-circle-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Cancelled Perfoma Invoice</span>
                            <span class="info-box-number">{{$cancelled_count or 0}}</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->

                </div>
                <!-- /.col -->

                <div class="col-md-8">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Perfoma Invoice</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Perfoma Invoice</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!$latest_quotations->isEmpty())
                                        @foreach($latest_quotations as $d)
                                            <tr>
                                                <td>
                                                    <a href="{{ url(\App\Utils::getUserUrlRoute().'/quotations/'.$d->quotation_id) }}">Quotation
                                                        #{{$d->quotation_id}}</a></td>
                                               <td>{{\App\Utils::formatPrice($d->total)}}
                                                </td>
                                                <td>
                                                    @php $quot_stat=\App\models\Common::getQuotationStatusName($d->quotation_status);@endphp
                                                    <span
                                                        class="label
 @switch($d->quotation_status)
                                                        @case(1) label-info
                                            @break
                                                        @case(2)
                                                            label-primary
@break
                                                        @case(3)
                                                            label-warning
@break
                                                        @case(4)
                                                            label-success
@break
                                                        @case(5)
                                                            label-danger
@break
                                                        @default
                                                            label-info
@break
                                                        @endswitch
                                                            ">
                                                        {{$quot_stat}}</span>
                                                </td>
                                            </tr>
                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="5">No Perfoma Invoice found</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <a href="{{url(\App\Utils::getUserUrlRoute().'/quotations/')}}" class="btn btn-sm btn-default btn-flat pull-right">View All
                                Perfoma Invoice</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->


                </div>
            </div>

        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

@endsection
