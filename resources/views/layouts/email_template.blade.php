<!DOCTYPE HTML>
<html>
<head></head>

<body>

@include('template.email.header',['title'=>$title])
@yield('content')
@include('template.email.footer')
</body>
</html>
