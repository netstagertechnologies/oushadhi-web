@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_details->title or 'Oushadhi'}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="abtsec-cms hosp">
                <img class="hosp-ctr-img" src="{{asset('ui/images/hospitals/hospital.jpg')}}">
                <p>
                <p>God’s own country ‘Kerala’ is known all over the world for many unique things and the traditional
                    Ayurveda is one among them. There are more than 12,000 traditional healers and 800 Ayurveda medicine
                    manufacturing units in the state. Oushadhi is the largest manufacturer of Ayurvedic Medicine in the
                    government sector in India. Oushadhi Panchakarma Hospital & Research Institute located in the heart
                    of Thrissur, the cultural capital of Kerala, offers varieties of Ayurveda treatments at affordable
                    price using quality medicines produced by ‘Oushadhi’. There is always heavy rush for the treatment
                    in this government run Panchakarma Hospital and bookings are done well in advance.</p>

                <p><strong>Our Specialties </strong></p>

                <p><strong>PANCHAKARMA THERAPY </strong></p>

                <p> Panchakarma therapy (penta fold purificatory measures) play a vital role in Ayurvedic therapies and
                    as such, they occupy an important place in Ayurvedic system of Medicine. The classical Ayurveda
                    Panchakarma therapy is the comprehensive method of internal purification of the body by VAMANA,
                    VIRECHANA, VASTHI, NASYA & VAMANA</p>

                <div class="fsectionContainer">
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/virechana.jpg')}}">
                        <h4>VIRECHANA</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/vasthi.jpg')}}">
                        <h4>VASTHI</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/nasya.jpg')}}">
                        <h4>NASYA</h4>
                    </div>
                    <div class="boxContainerHosp">
                        <img src="{{asset('ui/images/hospitals/rekthamoksha.jpg')}}">
                        <h4>RAKTHAMOKSHAM</h4>
                    </div>
                </div>
                <p> Process by which vitiated humours are eliminated through oral route, for the purification of the
                    upper half of the body.(Induced emesis-specific therapy for kaphaja disorders)</p>

                <ol>
                    <li>
                        <p><strong>VIRECHANA : </strong>Process by which vitiated humours are eliminated through anal
                            route, for the purification of the lower half of the body. (Induced
                            Purgation/Laxation-specific therapy for paithika Disorders)</p>
                    </li>
                    <li>
                        <p><strong>VASTHI : </strong>Purificatory measure which consists of suitable medicaments in the
                            form of enemata through rectal, vaginal or urethral routes (Oleus and Unoleus
                            Enemata-specified therapy for Vathika Disorders)</p>
                    </li>
                    <li>
                        <p><strong>NASYA: </strong>Purificatory measures by which suitable medicaments are introduced
                            through nasal passage in liquid or in powder form. (Errhines/Snuffing-therapy for diseases
                            related with head)</p>
                    </li>
                    <li>RAKTHAMOKSHAM<strong> : </strong>Purificatory measures in diseases related with impure blood by
                        using external means like, instruments (Prachanam), leeches(Jalookavacharanam), horns etc,
                        (Bloodletting-specific therapy for diseases Related to Blood).
                        <p></p>
                    </li>
                </ol>

                <p><strong>RECREATION OF BODY</strong></p>

                <div class="activity-main">
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">DHARA</div>
                                <p>Uninterrupted application of medicated oil (Thailadhara), buttermilk (Thakradhara),
                                    milk
                                    (Ksheeradhara), fermented whey (Dhanyamladhara) on the head, throughout body or both
                                    for
                                    Insomnia, Rheumatic complaints etc.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">PIZHICHIL</div>
                                <p>Pouring comfortable warm medicated oil liberally over the body
                                    followed by gentle massage for skin disease and rheumatic complaints.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">NAVARAKKIZHI</div>
                                <p>Massage done with small linen bags filled with medicated and
                                    cooked Navara rice, after a liberal application of medicated oils for rheumatic
                                    complaints
                                    &rejuvenation.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">THAKRADHARA</div>
                                <p>Uninterrupted application of medicated butter
                                    milk on the head for sleeplessness, skin diseases, mental disorders etc.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">THALAPOTHICHIL</div>
                                <p>Procedure in which herbal medicines are made into paste and
                                    applied over the head 9in a special manner for mental disorders etc.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">UDHWARTHANAM</div>
                                <p>Massage with medicated powders specially meant for reducing
                                    body fat, improvement of complexion land functional improvement of vital organs.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">NAVARATHEPPU</div>
                                <p>Application of medicated cooked navara rice on whole or
                                    affected parts of the body for rheumatic complaints &rejuvenation.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">SNEHANAM</div>
                                <p>Oleation therapy-Internal as well as external done prior of
                                    Panchakarma Therapy for rheumatic complaints, skin diseases, mental disorders
                                    etc.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">SNEHAPANAM</div>
                                <p>Intake of medicated unctuous Dravya like Ghee, Oil etc. for a
                                    specific duration especially for chronic diseases.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">ABHYANGAM</div>
                                <p>Application of medicated oil all over the body. It relieves
                                    fatigue and provides stamina and perfect sleep..</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">PALPPUKA</div>
                                <p>Sudation Technique with medicated milk for arthritic problems.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>

                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">UPANAHAM</div>
                                <p>Wrapping specific areas for a specific time interval with
                                    medicated pastes intended to subside inflammation or pain.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">SWEDANAM</div>
                                <p>Sudation technique done prior to Panchakaram. Therapy for Thirteen
                                    methods of thermal Sudation like Mixed Fomentation, Hot bed Sudation, Steam kettle
                                    Sudation,
                                    Affusion Sudation, Bath Sudation, Sudatorium Sudation, Stone bed Sudation, Trench
                                    Sudation,
                                    Cabin Sudation, Ground red Sudation, Picher bed Sudation, Pit Sudation, Under bed
                                    Sudation,
                                    are followed in this method.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">KIZHI</div>
                                <p>Fomentation technique by using boluses of herbal leaves (Elakkizhi)
                                    or medicated powders (Choornakkizhi) or warm sand (Manalkkizhi) or roasted salt
                                    (Uppukkizhi)
                                    according to the conditions of disease</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-full">
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">PICHYU</div>
                                <p>Applying oil soaked cloth over affected body parts intended to
                                    reduce local pain or stiffness.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">LEPAM</div>
                                <p>Applying medicated pastes on the body parts for skin diseases, local
                                    pain & swelling.</p>
                            </div>
                            <div class="activiti-right">

                                <img src="{{asset('ui/images/hospitals/nasya.jpg')}}" class="fullwidth">
                            </div>
                        </div>
                    </div>
                    <div class="act-cont-bottom">
                        <div class="act-bot-left">
                            <h4>REJUVENATION</h4>
                            <p>
                                Free meditation and yoga classes are arranged for all inmates
                                to rejuvenate the body & mind.
                            </p>
                            <h4>RESEARCH</h4>
                            <p>Panchakarma Hospital has a clinical trial cell intended to carry
                                out research on chronic diseases and con ditions like Arthritis, Diabetes, Rheumatic
                                Complaints, Skin Diseases, Head ache, Calculi, Cholesterol etc. using newly
                                developed medicines by the R&D wing of Oushadhi.</p>
                        </div>
                        <div class="act-bot-right">
                            <h4>LOCATION</h4>
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.6090424229365!2d76.20977401428539!3d10.531427066549016!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7ee5a8a6ae3bd%3A0xb42d7bf1c365c2cd!2sOushadhi+Panchakarma+Ayurvefa+Hospital!5e0!3m2!1sen!2sin!4v1564831280579!5m2!1sen!2sin"
                                width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>


                </div>
                <h4>FACILITIES</h4>
                <p>HOSPITAL : Well furnished 75 bedded hospital which is always full to the capacity.Bookings are done well in advance
                </p>

                </p>


            </div>

        </div>
    </section>


@endsection
