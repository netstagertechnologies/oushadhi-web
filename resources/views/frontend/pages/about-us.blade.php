@extends('layouts.frontend_template',['page_title'=>'About Us'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">About Oushadhi</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            {{--<div class="abtsec">
                <h2 class="innersubhead">Largest producer of ayurveda medcines in public sector in india</h2>
                <p>Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector
                    companies, consistently <br> making profit and paying dividend to Government of Kerala since 1999.
                    Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.</p>
            </div>--}}
            <div class="abtsec1">
                <div class="abtsec1-left">
                    <p>Largest producer of Ayurveda medicines in public sector in India.
                        One among the few public sector companies, consistently making profit and paying dividend to government of Kerala.
                        Supplies medicines to dispensaries under ISM Department of state and these medicines reach lakhs of patients at free of cost.
                        <br/>A GMP and ISO 9001 :2015 Certified company.<br/>
                        Produces 498 Ayurveda formulations –both classical and proprietary.
                        Sole supplier of medicines to Government Ayurveda Hospital and Dispensaries in Kerala.
                        Supplier of Ayurveda Medicines to Government Hospitals and Dispensaries of 19 other states, like Madhya Pradesh, Andhra Pradesh, Karnataka, Himachal Pradesh, Punjab, Chhattisgarh, Sikkim etc.
                        Caters to the need of public through a vast network over 650 dealers spread all over the nation.
                        Reimbursement facility available for Central Govt.& State Govt. employees on purchase of Oushadhi Medicines.
                        Equipped with AYUSH accredited laboratory facility

                    </p>
                </div>
                <div class="abtsec1-right">
                    <img src="{{asset('ui/images/about-us.jpg')}}" alt="" title="" class="fullwidth">
                </div>
            </div>
        </div>
    </section>
    <section class="innercontentarea2">
        <div class="wid">
            <div class="incont2-left">
                <img src="{{asset('ui/images/visionmision.jpg')}}" alt=" title" class="fullwidth">
            </div>
            <div class="incont2-right">
                <div class="inc-heading">Our Vision</div>
                <p>A leading world class Ayush medicine manufacturing organization by 2025. </p>
                <div class="inc-heading">Mission</div>
                <p>Production and supply quality medicine at reasonable price</p>
            </div>
        </div>
    </section>
    <section class="innercontentarea3">
        <div class="incont2-left">
            <div>
                <div class="inc-heading txt-left">Milestones</div>
                <ul class="com-list">
                    <li><span>1941:</span> Commenced by His Highness, the Maharaja of Cochin as " Sree Kerala Varma
                        Ayurveda Pharmacy"
                    </li>
                    <li><span>1959:</span> Converted into co-operative society viz. Sree Kerala Varma Ayurveda Pharmacy
                        and Stores Ltd.
                    </li>
                    <li><span>1975:</span> Registered as a Company under Indian Companies Act 1956 and renamed as "The
                        Pharmaceutical Corporation (Indian Medicines) Kerala Ltd" Thrissur.
                    </li>
                    <li><span>1991:</span> Commissioned modern manufacturing unit at Kuttanellur and shifted the factory
                        to the new premises.
                    </li>
                    <li><span>2004:</span> Started a new Panachakarma Hospital and Research Institute at Thrissur.</li>
                    <li><span>2007:</span> The entire office shifted to the factory premises at Kuttanellur.</li>
                    <li><span>2008:</span> Commenced full-fledged R&D Centre at Kuttanellur and Regional Distribution
                        Unit at Kannur
                    </li>
                    <li><span>2014:</span> Commissioned Center of Excellence for Manufacturing Asavarishta at
                        Kuttanellur.
                    </li>
                    <li><span>2014:</span> Started Regional Distribution center at Pathanapuram.</li>
                    <li><span>2017:</span> Commissioned Modern Packing Section complying WHO GMP standard.</li>
                    <li><span>2018:</span> Established Medicinal Plant Extension Center at Paryaram.</li>
                    <li><span>2018:</span> Established modern manufacturing unit for Proprietary Medicines at
                        Muttathara.
                    </li>
                    <li><span>2019:</span> Inaugurated New 50 bed Oushadhi Panachakarma Hospital Block at Thrissur.</li>
                    <li><span>2019:</span> Established Medicinal Plant Extension Center at Kuttanellur.</li>


                </ul>
            </div>
        </div>
        <div class="incont2-right">
            <img src="{{asset('ui/images/milestone.jpg')}}" alt=" title" class="fullwidth">
        </div>
    </section>
    <section class="abt-bottom">
        <div class="wid">
            <div class="abtbottom-right">
                <div class="abtbt-head">Strengths</div>
                <ul>
                    <li> Established brand image</li>
                    <li>Continuous Government Support</li>
                    <li>Huge market demand</li>
                    <li>Dedicated work force</li>
                </ul>
                <div class="abtbt-head">CORE VALUES</div>
                <ul>
                    <li>Mutual trust and respect</li>
                    <li>Customer satisfaction</li>
                    <li> Quality control</li>
                    <li>Professional ethics</li>
                    <li> March with time</li>
                </ul>
            </div>
        </div>
    </section>

@endsection
