@extends('layouts.frontend_template',['page_title'=>'Careers'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Contact Us</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">

            @include('template.frontend.alert')
            <div class="tenters-main">

                @foreach($careers as $career)
                    <div class="tenters-full">
                            <div class="stenter-left">
                                <p>{{$career['title']}}. Last date & Time of receipt of application
                                    forms<strong> {{date('d.m.Y', strtotime($career['last_date']))}}</strong>
                                </p>
                                <p>{{$career['dept'] or ''}}
                                    ,@if(!empty($career['contact'])&&$career['contact']!=null) Contact :
                                    <strong>{{$career['contact'] or ''}}</strong>
                                    @endif
                                    @if(!empty($career['contact_email'])&&$career['contact_email']!=null) Email :
                                    <strong>{{$career['contact_email'] or ''}}</strong>
                                    @endif
                                </p>
                            </div>
                            @if(isset($career['career_files'])&&$career['career_files'])
                                <div class="stenter-right">
                                    <ul>
                                        @php $i=1;@endphp
                                        @foreach($career['career_files'] as $files)
                                            <li>{{$i}}.{{$files->title or ''}} <a
                                                    href="{{asset('uploads/careers/'.$files->file_link)}}"
                                                    target="_blank">
                                                    <img src="{{asset('ui/images/downloadbr.png')}}"
                                                         alt="{{$files->title or ''}}"></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                    </div>
                @endforeach


                <div class="tenters-full new">

                    <div class="stenter-right">
                        <ul>

                            @foreach($careers as $career)
                                <li><p>
                                        <span>{{$career['title']}}</span>
                                    </p>
                                    <span class="smalldownld">
                                        @if($career['career_files'])
                                            @foreach($career['career_files'] as $files)
                                                <a href="{{asset('uploads/careers/'.$files->file_link)}}" target="_blank"><img src="{{asset('ui/images/downsm.png')}}"
                                                                 alt="">{{$files->title or ''}}</a>
                                            @endforeach
                                        @endif
                                    </span>
                                </li>

                            @endforeach
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection
