@extends('layouts.frontend_template',['page_title'=>'Officers'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Officers</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="contact-main">
                @if($contacts)
                    <div class="contact-pers">
                        <ul>
                            @foreach($contacts as $contact)
                                <?php $image = 'placeholder.png';//($contact->image) ? $contact->image : 'placeholder.png';
                                ?>

                                <li><img src="{{ asset('uploads/contacts/'.$image)}}" alt="" title="">
                                    <div class="conta-details">
                                        {{$contact->name or ''}}
                                        <span>{{$contact->designation or ''}}</span>
                                        <a href="tel:{{$contact->mob_no or ''}}"> {{$contact->mob_no or ''}}</a>
                                        @if(!empty($contact->email))
                                            <span>{{$contact->email or ''}}</span>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </section>

@endsection
