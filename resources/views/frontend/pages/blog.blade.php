@extends('layouts.frontend_template',['page_title'=>$page_title])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">{{$page_details->title or 'Oushadhi'}}</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>


    <section class="innercontentarea">
        <div class="wid">
            <div class="blog-main">
                <div class="blog-img">

                    <img src="{{ asset('uploads/blog/'.$page_data->image)}}" alt="" >
                </div>
                <div class="blog-details">
                    <h3 class="smallhead">{{$page_data->title or ''}}</h3>
                    <p>{!! $page_data->description or '' !!}</p>
                </div>
            </div>

        </div>

    </section>


@endsection
