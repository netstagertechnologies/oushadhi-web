@extends('layouts.frontend_template',['page_title'=>'Tenders'])

@section('content')
    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Tenders</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($tenders))
        <section class="innercontentarea">
            <div class="wid">
                <div class="tenters-main">
                    @foreach($tenders as $tender)
                        <div class="tenters-full">
                            <div class="stenter-left">
                                <p>{{$tender['title']}}
                                    @if(!empty($tender['last_date'])) Last date & Time :
                                    <strong> {{date('d.m.Y', strtotime($tender['last_date']))}}
                                        , {{$tender['last_time']}}</strong>@endif
                                </p>
                                @if(!empty($tender['dept'])) Department : <strong>{{$tender['dept'] or ''}}</strong><br/>@endif
                                @if(!empty($tender['contact'])&&$tender['contact']!=null)
                                    Contact No. :<strong>{{$tender['contact'] or ''}}</strong><br/>
                                @endif
                                @if(!empty($tender['contact_email'])&&$tender['contact_email']!=null)
                                            Contact Email :<strong>{{$tender['contact_email'] or ''}}</strong><br/>
                                @endif
                                @if(!empty($tender['tender_link']))
                                            <strong><a href="{{$tender['tender_link']}}" target="_blank">
                                                @if($tender['tender_link']==1) Tender Link @else e-Tender Link @endif</a>
                                        </strong><br/>

                                @endif
                            </div>
                                <div class="stenter-right">
                                    @if(isset($tender['tender_files'])&&$tender['tender_files'])
                                    <ul>
                                        @php $i=1;@endphp
                                        @foreach($tender['tender_files'] as $tender_file)
                                            <li>{{$tender_file->title or ''}} <a
                                                    href="{{asset('uploads/tenders/'.$tender_file->file_link)}}"
                                                    target="_blank">
                                                    <img src="{{asset('ui/images/downloadbr.png')}}"
                                                         alt="{{$tender_file->title or ''}}"></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>


                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

@endsection
