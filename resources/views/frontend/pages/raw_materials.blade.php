@extends('layouts.frontend_template',['page_title'=>'Raw Materials'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Raw Materials Required</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
            <ul class="phnemail rwl">
                <li class="pHne"><a href="tel:04872459841" title="0487 2459841">0487 2459841</a>
                </li>
                <li><a href="tel:04872459807" title="0487 2459807">0487 2459807</a>
                </li>
                <li class="maIl"><a href="mailto:purchase@oushadhi.org" title="purchase@oushadhi.org">purchase@oushadhi.org</a>
                </li>
            </ul>
        </div>
    </section>
    @if(isset($raw_materials))
        <section class="innercontentarea">
            <div class="wid">
                @include('template.frontend.alert')
                <div class="activity-main">

                    <a href="#" class="scltop">TOP</a>
                    <div class="raw-mat-left">
                        <div class="product-lefthead">Raw Material Type</div>
                        @if(isset($raw_types)&&!empty($raw_types))
                            <ul class="rw-list">
                                @foreach($raw_types as $raw_type)
                                    <li class="rw-itm rw-item{{$raw_type->id}}"><a
                                            href="rwitm{{$raw_type->id}}"> {{$raw_type->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="raw-mat-right">
                        <div class="activiti-full tb-cnt">
                            <table>
                                <tr class="st-rw-f">
                                    <th style="width:5%">&nbsp;</th>
                                    <th colspan="4" style="width:76%">പേരുവിവരം</th>
                                    <th style="width:14%">ഉദ്ദേശം പരിമാണം<br/><span>Approximate Quantity Kgs</span></th>
                                    <th style="width:5%">Image</th>
                                </tr>
                                <tr class="st-rw-s">
                                    <th style="width:5%">&nbsp;</th>
                                    <th style="width:19%">മലയാളം</th>
                                    <th style="width:19%">English</th>
                                    <th style="width:19%">Hindi</th>
                                    <th style="width:19%">Botanical Name</th>
                                    <th style="width:14%">&nbsp;</th>
                                    <th style="width:5%">&nbsp;</th>
                                </tr>@php $i=0;@endphp
                                @foreach($raw_materials as $raw_type=>$materials)
                                    <tr style="width:100%" class="title-hd">
                                        <td colspan="7" id="rwitm{{$raw_type}}"><strong>{{$materials['title']}}</strong></td>
                                    </tr>
                                    @foreach($materials['materials'] as $raw_material)
                                        @php $i++;@endphp
                                        <tr>
                                            <td style="width:5%">{{$i}}</td>
                                            <td style="width:19%">{{$raw_material->title}}</td>
                                            <td style="width:19%">{{$raw_material->title_en}}</td>
                                            <td style="width:19%">{{$raw_material->title_hn}}</td>
                                            <td style="width:19%"><i>{{$raw_material->botanical_name}}</i></td>
                                            <td style="width:14%">{{$raw_material->quantity}}</td>
                                            <td style="width:5%"><?php $image = ($raw_material->image) ? $raw_material->image : 'placeholder.jpg';?>
                                                <img src="{{ asset('uploads/raw-materials/'.$image)}}"
                                                     data-rawmaterial='{{ asset('uploads/raw-materials/'.$image)}}'
                                                     data-toggle="modal" data-target="#myModalRawMaterial"
                                                     height="75" alt="" class="thumrmr">
                                            </td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </table>

                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div id="myModalRawMaterial" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                            </div>
                            <div class="modal-body">
                                <img src="" id="rawMatId" class="bigImg"/>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    @endif

    <script>
        $('#myModalRawMaterial').on('show.bs.modal', function (event) {
            var filtre = $(event.relatedTarget).attr('data-rawmaterial');
            $('#rawMatId').attr('src', filtre);
        });

        /* scrolling bit */

        var scrollTime = 1000;
        var navHeight = $('.innerbanner').offset().top;

        var tablerowf = $('.st-rw-f').outerHeight();
        var tablerows = $('.st-rw-s').outerHeight();

        totalHeight = navHeight + tablerowf + tablerows+200;
        $('.rw-list li').click(function () {
            var divId = $(this).find('a').attr("href").toLowerCase();
           // alert($("#" + divId).offset().top);
            $('html, body').animate({
                scrollTop: $("#" + divId).offset().top - (totalHeight)
            }, scrollTime);
            return false;
        });

        $('.scltop').click(function () {
            $('html, body').animate({
                scrollTop: 0
            }, scrollTime);
        });

        $(window).scroll(function () {
            $(".st-rw-f,.st-rw-s").toggleClass("aniPos", $(this).scrollTop() > 405);
        });
    </script>
@endsection


