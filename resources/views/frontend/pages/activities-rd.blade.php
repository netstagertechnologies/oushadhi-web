@extends('layouts.frontend_template',['page_title'=>'Activities of R&D'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Activities of R&D</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <section class="innercontentarea">
        <div class="wid">
            <ul class="com-list">
                <li>New product development</li>
                <li>Process modification</li>
                <li>Product modification</li>
                <li>Standardization at Raw Material in process and product levels.</li>
                <li>Inter disciplinary work pattern</li>
                <li>Corrective &preventive actions</li>
                <li>Extension activities- Training programme for Ayurveda pharmacy MD students, Biotechnology
                    scholars, Microbiology graduates, Biochemistry scholars etc.
                </li>
                <li>Demonstration Garden & Drug Gallery.</li>
                <li>Collaborative projects.</li>

            </ul>
        </div>
    </section>
@endsection
