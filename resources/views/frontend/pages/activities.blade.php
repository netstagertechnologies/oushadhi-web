@extends('layouts.frontend_template',['page_title'=>'Activities'])

@section('content')

    <section class="innerbanner" style="background-image: url({{asset('ui/images/aboutus-banner.jpg')}}">
        <div class="wid">
            <h1 class="innerheading">Activities</h1>
            <ul class="brdcrum">
                @foreach($breadcrumbs as $breadcrumb)
                    <li><a href="{{$breadcrumb['link']}}" title="{{$breadcrumb['name']}}">{{$breadcrumb['name']}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    @if(isset($activities))
        <section class="innercontentarea">
            <div class="wid">
                <div class="activity-main">
                    @foreach($activities as $activity)
                        <div class="activiti-full">
                            <div class="activiti-left">
                                <div class="activity-head">{{$activity->title or ''}}</div>
                                <p>{!! $activity->description or '' !!}</p>
                            </div>
                            <?php $image = ($activity->image) ? $activity->image : 'placeholder.jpg';?>

                            <div class="activiti-right">
                                <img src="{{ asset('uploads/activity/'.$image)}}" alt="" class="fullwidth">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif
@endsection
