@extends('layouts.email_template',['title'=>'Oushadhi: Account Rejected'])

@section('content')

    <tr>
        <td height="197" align="center" valign="middle" bgcolor="#009209">
            <table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="22" align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif"><strong
                                style="color:#fff;font-size:13px;font-weight:600;"><font face="Montserrat, sans-serif">Dear {{ucfirst($name)}}
                                    ,</font></strong></font></td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="color:#fff;font-size:13px;font-weight:300;"><font
                            face="Montserrat, sans-serif">Your account has been rejected due to official reasons. Please contact Oushadhi for more details.</font></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">&nbsp;</td>
    </tr>
@endsection
