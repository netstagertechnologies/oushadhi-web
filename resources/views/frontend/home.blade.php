@extends('layouts.frontend_template',['page_title'=>'Oushadhi','footer_class'=>''])

@section('content')

    @if(isset($banners))

        <section class="banner">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        @foreach($banners as $banner)
                            <li class="bcount1" data-transition="fade" data-slotamount="6" data-masterspeed="1500"
                                data-thumb="images/oushadhi-banner1.jpg" data-delay="7000">

                                <img src="{{ asset('uploads/banner/'.$banner->image)}}" alt="laptopmockup_sliderdy"
                                     data-bgposition="center center" data-kenburns="on" data-duration="16000"
                                     data-ease="Linear1.easeNone" data-bgfit="100" data-bgfitend="110"
                                     data-bgpositionend="center center" data-bgrepeat="no-repeat">
                                @if(!empty($banner->title))
                                    <div class="tp-caption lft customout rs-parallaxlevel-0" data-x="0" data-y="0"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="700" data-start="1400" data-easing="Power3.easeInOut"
                                         data-elementdelay="0.1" data-endelementdelay="0.1" style="z-index: 3;">
                                        <div class="bannercaptions">
                                            <p class="bannerhead">{!! $banner->title or '' !!}</p>
                                            <a href="{{$banner->link or ''}}" class="readmore" title="Know more">Know
                                                more</a>
                                        </div>
                                    </div>
                                @endif
                            </li>

                        @endforeach

                    </ul>

                </div>
            </div>
        </section>
    @endif


    @if(isset($latest_news))
        <div class="latestnews">
            <div class="wid">
                <span class="latestnewshed">Latest News</span>
                <marquee onMouseOver="this.stop()" onMouseOut="this.start()">
                    <ul class="newslist">
                        @foreach($latest_news as $news)
                            <li>{{$news->title or ''}}</li>
                        @endforeach
                    </ul>
                </marquee>
            </div>
        </div>

    @endif
    @if(isset($home_cms[1]))
        <section class="sec1">
            <div class="wid">
                <h1 class="heading">{{$home_cms[1]['caption']}}</h1>
                <h2 class="subheading">{{$home_cms[1]['title']}}</h2>
                <p class="homp">{{$home_cms[1]['description']}}</p>
                {{--<a href="{{$home_cms[1]['link'] or '#'}}" class="readmore" title="Know more">Know more</a>--}}
            </div>
        </section>
    @endif
    @if(isset($featured_products))
        <section class="sec2">
            <div class="wid">
                <h2 class="heading">Featured Products</h2>
                <ul class="productslist">
                    @foreach($featured_products as $featured_product)
                        <li>
                            <a href="{{ URL::to('/').'/product/'.$featured_product->slug}}"
                               title="{{$featured_product->title or ''}}">
                                <div class="cvrprdts">
                                    <?php $image = ($featured_product->image) ? $featured_product->image : 'placeholder.jpg';?>
                                    <div class="prdtimg"><img src="{{ asset('uploads/product/'.$image)}}"
                                                              title="{{$featured_product->title or ''}}"
                                                              alt="{{$featured_product->title or ''}}"></div>
                                    <div class="prdtname">{{$featured_product->title or ''}}</div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <a href="{{ URL::to('/').'/products/featured'}}" class="readmore" title="View all">View all</a>
            </div>
        </section>
    @endif

    <section class="sec3">
        <div class="wid">
            @if(isset($home_cms[2]))
                <div class="healthtip"
                     style="background-image: url({{ asset('uploads/home-cms/'.$home_cms[2]['image'])}});">
                    <div class="healthytitle">
                        <div class="healthycvr">
                            <h3 class="smallhead">{{$home_cms[2]['title'] or ''}}</h3>
                            <p>{{$home_cms[2]['description'] or ''}}</p>
                        </div>
                        <a href="{{$home_cms[2]['link'] or ''}}" class="readmore" title="Know More">Know More</a>
                    </div>
                </div>
            @endif
            @if(isset($latest_news))
                <div class="nEwsents">
                    <h3 class="heading">News & Events</h3>
                    <marquee behavior="scroll" direction="up" scrolldelay="200" onMouseOver="this.stop()"
                             onMouseOut="this.start()">
                        <ul>
                            @foreach($latest_news as $news)

                                <li>
                                    <div class="newsimg"><img src="{{ asset('uploads/news/'.$news->image)}}"></div>
                                    <div class="newscontent">
                                        <span>{{date('d F Y ', strtotime($news->date_added))}}</span>
                                        <p>{!! $news->title !!}</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </marquee>
                    <a href="{{ URL::to('/').'/news'}}" class="readmore" title="Know More">Know More</a>
                </div>
            @endif
        </div>
    </section>


    @if(isset($factory_images))
        <section class="sec6">
            <div class="wid">
                <h3 class="heading">Our Factory</h3>
                <div class="factorycvr">
                    <ul id="factorySlider" class="content-slider">

                        @foreach($factory_images as $factory_image)
                            <li>
                                <img src="{{ asset('uploads/factory-images/'.$factory_image->image)}}">
                                <span> {{$factory_image->title or ''}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
    @endif



    @if(isset($latest_blog))

        <section class="sec4">
            <div class="wid">
                <h3 class="heading">Latest Blog</h3>
                <ul class="bloglist">

                    @foreach($latest_blog as $blog)
                        <li>
                            <div class="blogcvr"
                                 style="background-image: url({{ asset('uploads/blog/'.$blog->image)}}) !important;">
                                <div class="blogleft">
                                    <div class="vmidle">
                                        <h3 class="bloghed">{!! $blog->title or '' !!}</h3>
                                        <p>{!! $blog->short_description !!}</p>
                                        <a href="{{ URL::to('/').'/blog/'.$blog->slug}}" class="readmore"
                                           title="Know More">Know More</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    @endif

    @if(isset($testimonials))
        <section class="sec5">
            <div class="wid">
                <h3 class="heading">Testimonials</h3>
                <div class="testimonialcvr">
                    <ul id="testimonialSlider" class="content-slider">

                        @foreach($testimonials as $testimonial)
                            <li>
                                <p>{!! $testimonial->description or '' !!}</p>
                                <span>- {{$testimonial->name or ''}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
    @endif


@endsection

@section('page-script')
    <!--Slider-->
    <script type="text/javascript" src="{{ asset('ui/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('ui/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!--Slider-->
    <script src="{{ asset('ui/js/jquery.lightSlider.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#bannerSlider').lightSlider({
                item: 1,
                auto: true,
                loop: true,
                slideMove: 1,
                mode: 'slide',
                addClass: 'bannerSlider',
                easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed: 3000,
                pause: 5000,
                adaptiveHeight: true,
            });
            $('#testimonialSlider').lightSlider({
                item: 1,
                auto: true,
                loop: true,
                pager: false,
                slideMove: 1,
                mode: 'slide',
                addClass: 'tetimoSlider',
                easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed: 3000,
                pause: 5000,
                adaptiveHeight: true,
            });
            $('#factorySlider').lightSlider({
                item: 2,
                auto: true,
                loop: true,
                pager: false,
                controls: true,
                slideMove: 1,
                slideMargin: 8,
                mode: 'slide',
                addClass: 'factorySlider',
                easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
                speed: 3500,
                pause: 5000,
                adaptiveHeight: false,
            });
        });
    </script>
    <!--Slider Ends-->

    <script type="text/javascript">
        jQuery(document).ready(function () {

            scrWidth = $(window).width();
            if (scrWidth <= 1200) {
                fullScreenState = 'off';
            }
            else {
                fullScreenState = 'on';
            }


            jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay: "none",
                    delay: 16000,
                    startwidth: 1170,
                    hideThumbs: 200,

                    thumbWidth: 100,
                    thumbHeight: 50,
                    thumbAmount: 5,
                    fullWidth: "off",

                    navigationType: "bullet",
                    navigationArrows: "solo",
                    navigationStyle: "preview1",
                    hideTimerBar: "on",
                    touchenabled: "on",
                    onHoverStop: "off",

                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,

                    parallax: "mouse",
                    parallaxBgFreeze: "on",
                    parallaxLevels: [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

                    keyboardNavigation: "off",

                    navigationHAlign: "center",
                    navigationVAlign: "bottom",
                    navigationHOffset: 0,
                    navigationVOffset: 60,

                    soloArrowLeftHalign: "left",
                    soloArrowLeftValign: "center",
                    soloArrowLeftHOffset: 20,
                    soloArrowLeftVOffset: 0,

                    soloArrowRightHalign: "right",
                    soloArrowRightValign: "center",
                    soloArrowRightHOffset: 20,
                    soloArrowRightVOffset: 0,

                    shadow: 0,
                    fullWidth: "on",
                    fullScreen: fullScreenState,

                    spinner: "spinner4",

                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,

                    shuffle: "off",

                    autoHeight: "on",
                    forceFullWidth: "off",


                    hideThumbsOnMobile: "on",
                    hideNavDelayOnMobile: 1500,
                    hideBulletsOnMobile: "on",
                    hideArrowsOnMobile: "on",
                    hideThumbsUnderResolution: 0,

                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    startWithSlide: 0,
                    videoJsPath: "rs-plugin/videojs/",
                    fullScreenOffsetContainer: ""
                });


        });	//ready


    </script>
    <!--Slider Ends-->
@endsection
