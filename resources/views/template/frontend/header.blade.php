<header>
    <div class="wid">
        <div class="logo"><a href="{{route('/')}}" title="Oushadhi"><img src="{{ asset('ui/images/logo.png')}}"
                                                                         title="Oushadhi" alt="Oushadhi logo"></a></div>
        <div class="logorgt">
            <div class="navtop">
                <ul class="phnemail">
                    <li class="pHne"><a href="tel:{{$site_settings['config_contact_number1'] or ''}}"
                                        title="{{$site_settings['config_contact_number1'] or '#'}}">{{$site_settings['config_contact_number1'] or '#'}}</a>
                    </li>
                    <li><a href="tel:{{$site_settings['config_contact_number2'] or ''}}"
                           title="{{$site_settings['config_contact_number2'] or ''}}">{{$site_settings['config_contact_number2'] or ''}}</a>
                    </li>
                    <li class="maIl"><a href="mailto:marketing@oushadhi.org" title="marketing@oushadhi.org">marketing@oushadhi.org</a>
                    </li>
                </ul>
                <ul class="logpanel">
                    @guest
                        <li><a href="{{ route('register') }}">Doctors Sign Up</a>/</li>
                        <li><a href="{{ route('login') }}">Login</a></li>
                    @else
                        <li><a href="{{ route('user.home') }}">{{ Auth::user()->name }}</a></li>
                        <li><a href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endguest
                </ul>

            </div>
            <div class="menuD">
                <a href="#menu" id="nav-toggle" class="menu-link">
                    <span></span>
                </a>
                <nav id="menu" class="menu">
                    <ul class="level-01">
                        <li><a href="{{route('/')}}" title="Home">Home</a></li>
                        <li><a href="{{route('about-us')}}" title="About Us">About Us</a>
                            <span class="has-subnav"></span>
                            <ul class="level-1">
                                <li><a href="{{route('about-us')}}">About Oushadhi</a></li>
                                <li><a href="{{route('directors')}}">Board Directors</a></li>
                                <li><a href="{{route('officers')}}">Officers</a></li>
                                <li><a href="{{route('departments')}}">Departments</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('products')}}" title="Products">Products</a>
                            <span class="has-subnav"></span>
                            <ul class="level-1">
                                <li><a href="{{route('products')}}">Ayurveda</a></li>
                                <li><a href="{{route('s-products')}}">Siddha</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('activities')}}" title="Activities">Activities
                                <span class="has-subnav"></span>
                                <ul class="level-1">
                                    <li><a href="{{route('r-and-d-activities')}}">R&D Activities</a></li>
                                    <li><a href="{{ URL::to('/').'/csr-activities'}}">CSR Activities</a></li>
                                    <li><a href="{{ URL::to('/').'/medicinal-plant-cultivation'}}">Medicinal Plant Cultivation</a></li>
                                </ul>
                            </a>{{--
                            <span class="has-subnav"></span>
                            <ul class="level-1">
                                <li><a href="#">Activities 1</a></li>
                                <li><a href="#">Activities 2</a></li>
                            </ul>--}}
                        </li>
                        <li><a href="{{ URL::to('/').'/hospital'}}" title="Hospital">Hospital</a></li>
                        <li><a href="{{route('media')}}" title="Media">Media</a></li>
                        <li><a href="{{route('tenders')}}" title="Tenders">Tenders</a></li>
                        <li><a href="{{ URL::to('/').'/careers'}}" title="Careers">Careers</a></li>
                        <li><a href="{{route('contact-us')}}" title="Contact">Contact Us</a></li>
                        <li><a href="{{route('raw-materials-required')}}" title="Raw Materials Required">Raw Materials</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
