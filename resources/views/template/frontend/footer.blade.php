<footer class="{{$footer_class or ''}}">
    <div class="wid">
        <div class="fotdrs">
            <a href="{{route('/')}}" class="footerlogo" title="Oushadhi"><img
                    src="{{ asset('ui/images/footerlogo.png')}}" title="Oushadhi"></a>
            <p>{!! $site_settings['config_address'] or '' !!} </p>
            <ul class="phnemail">
                <li class="pHne"><a href="tel:{{$site_settings['config_contact_number1'] or ''}}"
                                    title="{{$site_settings['config_contact_number1'] or ''}}">{{$site_settings['config_contact_number1'] or ''}}</a>
                </li>
                <li><a href="tel:{{$site_settings['config_contact_number2'] or ''}}"
                       title="{{$site_settings['config_contact_number2'] or ''}}">{{$site_settings['config_contact_number2'] or ''}}</a>
                </li>
                <li class="maIl"><a href="mailto:{{$site_settings['config_contact_email'] or ''}}"
                                    title="{{$site_settings['config_contact_email'] or ''}}">{{$site_settings['config_contact_email'] or ''}}</a>
                </li>
            </ul>
        </div>
        <div class="fotmap">
            <iframe
                src="{{ $site_settings['config_map_link'] or '#' }}"
                width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="qklinks">
            <span class="ftrhed">Quick Links</span>
            <ul>
                <li><a href="{{route('/')}}">Home</a></li>
                <li><a href="{{route('products')}}">Products</a></li>
                <li><a href="{{route('about-us')}}">About us</a></li>
                <li><a href="{{ URL::to('/').'/hospital'}}">Hospital</a></li>
                <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                <li><a href="{{route('downloads')}}">Downloads</a></li>
                <li><a href="{{ URL::to('/').'/rti-act'}}">RTI Act</a></li>


            </ul>
        </div>
        <div class="copyrgt">
            <p>© <?php echo date("Y")?> Oushadhi.org All Rights Reserved</p>
            <div class="social">
                <span>Follow us :</span>
                <ul>
                    <li><a class="fb" target="_blank" href="{{ $site_settings['config_facebook_page'] or '#' }}"
                           title="facebook"></a></li>
                    <li><a class="tw" target="_blank" href="{{ $site_settings['config_twitter_page'] or '#' }}"
                           title="twitter"></a></li>
                    <li><a class="ig" target="_blank" href="{{ $site_settings['config_instagram_page'] or '#' }}"
                           title="instagram"></a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>
