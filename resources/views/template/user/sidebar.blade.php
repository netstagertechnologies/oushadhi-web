<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="@if($tab['active_li_item']&&$tab['active_li_item']=='dashboard-li')active @endif"><a
                    href="{{route('user.home')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

            <li class=" @if($tab['active_li_item']&&$tab['active_li_item']=='profile-li-item')active @endif"><a
                    href="{{route('user.profile')}}"><i class="fa fa-user-circle-o"></i> <span>Profile</span></a></li>
            <li class=" @if($tab['active_li_item']&&$tab['active_li_item']=='password-li-item')active @endif"><a
                    href="{{route('user.password')}}"><i class="fa fa-lock"></i> Change Password</a></li>


            <li class="@if($tab['active_li_item']&&$tab['active_li_item']=='quotation-li-item')active @endif"><a
                    href="{{url(\App\Utils::getUserUrlRoute().'/quotations')}}"><i class="fa fa-file"></i>
                    Perfoma Invoice</a></li>

            <li class="@if($tab['active_li_item']&&$tab['active_li_item']=='cancelled-quot-li-item')active @endif"><a
                    href="{{url(\App\Utils::getUserUrlRoute().'/cancelled-quotations')}}"><i class="fa fa-file-excel-o"></i>
                   Cancelled Perfoma Invoice</a></li>

            <?php $not_count = \App\models\Notifications::getUnreadNotificationsCount(Auth::user()->id, 'user');?>

            <li class=" @if($tab['active_li_item']&&$tab['active_li_item']=='notification-li-item')active @endif"><a
                    href="{{url(\App\Utils::getUserUrlRoute().'/notifications')}}"><i class="fa fa-info-circle"></i>
                    Notifications
                    @if($not_count)
                        <span class="pull-right-container">
                      <span class="label label-primary pull-right">{{$not_count}}</span>
                    </span>
                    @endif
                </a></li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
