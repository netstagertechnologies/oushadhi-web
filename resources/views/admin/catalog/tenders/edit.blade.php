@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/tenders/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    @php $tender_type=(isset($page_data->tender_type)&&$page_data->tender_type==2)?2:1;@endphp
                                    <div class="form-group {{ $errors->has('tender_type') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Tender Type</label>

                                        <div class="col-sm-7">
                                            <select class="form-control" name="tender_type" id="tender_type">
                                                <option value="1" {{($tender_type==1)?'selected':''}}>Normal Tender
                                                </option>
                                                <option value="2" {{($tender_type==2)?'selected':''}}>E-Tender</option>
                                            </select>
                                            {!! $errors->first('tender_type', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="normal-tender-flds">
                                         <div
                                            class="form-group  {{ ($errors->has('last_date')||$errors->has('last_time')) ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label required" for="input-category"><span
                                                    data-toggle="tooltip">Last Date and Time</span></label>

                                            <div class="col-sm-7">
                                                <div class="col-sm-6">
                                                    <div class="input-group date">

                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" name="last_date"
                                                               class="form-control pull-right"
                                                               id="lastdatepicker"
                                                               value="{{$page_data->last_date or ''}}">

                                                        {!! $errors->first('last_date', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="input-group">
                                                        <input type="text" name="last_time"
                                                               class="form-control timepicker"
                                                               value="{{$page_data->last_time or ''}}">

                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                        {!! $errors->first('last_time', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('dept') ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label ">Department</label>

                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="dept"
                                                       placeholder="Department"
                                                       value="{{$page_data->dept or ''}}">
                                                {!! $errors->first('dept', '<p class="help-block">:message</p>') !!}

                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label ">Contact Email</label>

                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="contact_email"
                                                       placeholder="Contact Email">
                                                {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}

                                            </div>
                                        </div>
                                        <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label">Contact No.</label>

                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="contact"
                                                       placeholder="Contact"
                                                       value="{{$page_data->contact or ''}}">
                                                {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}

                                            </div>
                                        </div>
                                        <div id="tender-files">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Files</label>
                                                <div class="col-sm-7">
                                                    <table id="tenders"
                                                           class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <td class="text-left">{{ 'Title' }}</td>
                                                            <td class="text-left">{{ 'File' }}</td>
                                                            <td></td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @php $tender_row = 0; @endphp
                                                        @if(isset($tender_files))
                                                            @foreach($tender_files as $tender_file)
                                                                <tr id="tender-row{{ $tender_row }}">
                                                                    <td class="text-left" style="width: 40%;">
                                                                        <input type="hidden"
                                                                               name="tender_file[{{ $tender_row }}][file_id]"
                                                                               value="{{ $tender_file->file_id or ''}}"
                                                                               placeholder="Title"
                                                                               class="form-control"/>
                                                                        <input type="text"
                                                                               name="tender_file[{{ $tender_row }}][title]"
                                                                               value="{{ $tender_file->title or ''}}"
                                                                               placeholder="Unit" class="form-control"/>
                                                                    </td>
                                                                    <td class="text-left">
                                                                        <a href="{{asset('uploads/tenders/'.$tender_file->file_link)}}">Selected
                                                                            file: {{ $tender_file->file_link or ''}}</a>
                                                                        <input type="file"
                                                                               name="tender_file[{{ $tender_row }}][name]"
                                                                               value="{{ $tender_file->price or ''}}"
                                                                               placeholder="Choose File"
                                                                               class="form-control"/>
                                                                        <input type="hidden"
                                                                               name="tender_file[{{ $tender_row }}][file_link]"
                                                                               value="{{ $tender_file->file_link or ''}}"
                                                                               class="form-control"/>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <button type="button"
                                                                                onclick="$('#tender-row{{ $tender_row }}').remove();"
                                                                                data-toggle="tooltip" title="Remove"
                                                                                class="btn btn-danger"><i
                                                                                class="fa fa-minus-circle"></i></button>
                                                                    </td>
                                                                </tr> @php $tender_row = $tender_row + 1; @endphp
                                                            @endforeach
                                                        @endif
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td class="text-right">
                                                                <button type="button" onclick="addTenderFileRow();"
                                                                        data-toggle="tooltip" title="Add File"
                                                                        class="btn btn-primary"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="e-tender-flds">
                                        <div class="form-group {{ $errors->has('tender_link') ? 'has-error' : ''}}">
                                            <label class="col-sm-3 control-label ">E-Tender Link</label>

                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" name="tender_link"
                                                       placeholder="E-Tender Link" value="{{$page_data->tender_link or ''}}">
                                                {!! $errors->first('tender_link', '<p class="help-block">:message</p>') !!}

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label required">Status</label>

                                    <div class="col-sm-7">
                                        <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                        <select type="text" class="form-control"
                                                name="status">
                                            <option value="1" {{$status?'selected':''}}>Enable</option>
                                            <option value="0">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/tenders') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script><!-- date-range-picker -->
    <script src="{{ asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script
        src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>

        //Date picker
        $('#fromdatepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $('#todatepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $('#lastdatepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $('.timepicker').timepicker({
            showInputs: false
        });

        var tender_row = '{{ $tender_row }}';

        function addTenderFileRow() {
            html = '<tr id="tender-row' + tender_row + '">';
            html += '  <td class="text-left" style="width: 50%;"><input type="text" name="tender_file[' + tender_row + '][title]" value="" placeholder="{{ 'Title' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="file" name="tender_file[' + tender_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control" accept="application/pdf,application/vnd.ms-excel"  /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#tender-row' + tender_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#tenders tbody').append(html);

            tender_row++;
        }
       /* $('.e-tender-flds').hide();
        $('#tender_type').change(function () {
            tender_type = $(this).val();
            if (tender_type == 2) {
                $('.normal-tender-flds').hide();
                $('.e-tender-flds').show();
            } else {
                $('.normal-tender-flds').show();
                $('.e-tender-flds').hide();
            }
        });

        $('#tender_type').trigger('change');*/

    </script>
@endsection
