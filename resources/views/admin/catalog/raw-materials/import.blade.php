@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">


                    @include('template.admin.alert')
                    <div class="panel panel-default">
                        <div class="panel-body">


                            <div class="box box-solid">

                                <div class="box-body">

                                    <form class="form-horizontal" method="POST"
                                          action="{{ url(\App\Utils::getUrlRoute().'/raw-materials/importData')}}"
                                          accept-charset="UTF-8" enctype="multipart/form-data">
                                        <div class="box-body">

                                            @csrf
                                            <div class="form-group {{ $errors->has('file') ? 'has-error' : ''}}">

                                                <label for="file" class="col-sm-3 control-label required">CSV file to import</label>

                                                <div class="col-sm-7">
                                                    <input id="file" type="file" class="form-control" name="file" required>

                                                    {!! $errors->first('file', '<p class="help-block">:message</p>') !!}

                                                </div>

                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-1 control-label"></label>
                                            <div class="col-sm-10">

                                                <button type="submit" class="btn btn-success pull-right">Submit</button>
                                                <a href="{{ url(\App\Utils::getUrlRoute().'/raw-materials/') }}"
                                                   class="btn btn-danger pull-left"><i class="fa fa-chevron-left"
                                                                                       aria-hidden="true"></i> Back</a>

                                            </div>
                                        </div>

                                    </form>



                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
