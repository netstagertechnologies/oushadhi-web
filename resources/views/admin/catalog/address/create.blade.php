@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/addresses/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Address</label>
                                        <div class="col-sm-7">
                                            <textarea rows="10" cols="80" class="form-control" placeholder="Enter ..."
                                                      name="address">{{ $page[0]->address or ''}}</textarea>
                                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Mobile</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="mobile"  value="" placeholder="Mobile">
                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Phone</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone">
                                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="email" placeholder="Email">
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div id="banner-image">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Image</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="upload-file" data-height="312" data-width="555"
                                                       name="image"/>
                                                <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                        .jpeg,.png</i></p>
                                                <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                        [385*192]
                                                        pixel</i></p>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Map Link</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="map_link" placeholder="Map Link">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Sort Order</label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="sort_order" placeholder="Sort Order">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1">Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/addresses') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
