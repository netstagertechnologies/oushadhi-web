-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2019 at 11:08 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oushadhi`
--

-- --------------------------------------------------------

--
-- Table structure for table `raw_materials`
--

CREATE TABLE `raw_materials` (
  `raw_material_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(500) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `common_name` varchar(100) DEFAULT NULL,
  `scientific_name` varchar(100) DEFAULT NULL,
  `botanical_name` varchar(100) DEFAULT NULL,
  `quantity` varchar(15) DEFAULT NULL,
  `contact_person` varchar(50) DEFAULT NULL,
  `contact_number` varchar(50) DEFAULT NULL,
  `contact_email` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raw_materials`
--

INSERT INTO `raw_materials` (`raw_material_id`, `title`, `slug`, `description`, `image`, `common_name`, `scientific_name`, `botanical_name`, `quantity`, `contact_person`, `contact_number`, `contact_email`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Clitoria (Fresh)  Needed.', 'clitoria-fresh-needed', NULL, '5cbd7834c82b61555920948.jpg', 'Clitoria (Fresh)', NULL, 'Clitoria ternatea', '150kg', 'John', '12345621', 'info@oushadhi.com', 1, '2019-04-22 13:18:57', '2019-04-22 13:45:48'),
(3, 'Sacred basil (fresh) urgent requirement', 'sacred-basil-fresh-urgent-requirement', NULL, '5cbd792cee4f71555921196.jpg', 'Sacred basil (fresh)', NULL, 'Ocimum tenuiflorum', '50kg', 'Emanual', '78546213', 'manual@ousha.com', 1, '2019-04-22 13:49:56', '2019-04-22 13:49:56'),
(4, 'Raw material-Ginger', 'raw-material-ginger', NULL, '5cbd79d0813131555921360.jpg', 'Ginger', NULL, 'Zingiber officinale', '4000kg', 'MeriJane', '445654657', 'merijane@gmail.com', 1, '2019-04-22 13:52:40', '2019-04-22 13:52:40'),
(5, 'Raw material Phlomis [fresh]-5kg', 'raw-material-phlomis-fresh-5kg', NULL, '5cbd7aa67388f1555921574.jpg', 'Phlomis [fresh]', NULL, 'Leucas aspera', '5kg', 'anju', '2489657', 'anju@netstager.in', 1, '2019-04-22 13:56:14', '2019-04-22 13:56:14'),
(6, 'Red Beed Tree -urgent requirement', 'red-beed-tree-urgent-requirement', NULL, '5cbd7b49e7dee1555921737.jpg', 'Red Beed Tree', NULL, 'Abrus precatorious', '300kg', 'anju', '2489657', 'anju@netstager.in', 1, '2019-04-22 13:58:57', '2019-04-22 13:58:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `raw_materials`
--
ALTER TABLE `raw_materials`
  ADD PRIMARY KEY (`raw_material_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `raw_materials`
--
ALTER TABLE `raw_materials`
  MODIFY `raw_material_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
