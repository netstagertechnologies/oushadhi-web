@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/raw-materials/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                                        <label for="content-label required"
                                               class="col-sm-3 control-label">Description</label>
                                        <div class="col-sm-7">
                                            <textarea rows="5" cols="80" class="form-control"
                                                      placeholder="Enter ..."
                                                      name="description">{{$page_data->description or ''}}</textarea>
                                            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <h4>Material Details</h4>
                                    <hr/>
                                    <div class="form-group {{ $errors->has('common_name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Common Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="common_name"
                                                   placeholder="Common Name" value="{{$page_data->common_name or ''}}">
                                            {!! $errors->first('common_name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('scientific_name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Scientific Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="scientific_name"
                                                   placeholder="Scientific Name" value="{{$page_data->scientific_name or ''}}">
                                            {!! $errors->first('scientific_name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('botanical_name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label ">Botanical Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="botanical_name"
                                                   placeholder="Botanical Name" value="{{$page_data->botanical_name or ''}}">
                                            {!! $errors->first('botanical_name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Quantity</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="quantity"
                                                   placeholder="Quantity" value="{{$page_data->quantity or ''}}">
                                            {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div id="banner-image">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"> Image</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="upload-file" data-height="312"
                                                       data-width="555"
                                                       name="image"/>
                                                <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image
                                                        format
                                                        .jpeg,.png</i></p>
                                                <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                        [555*312]
                                                        pixel</i></p>

                                                @if(isset($page_data->image)&&!empty($page_data->image))
                                                    <input type="hidden" name="bnr_src" value="{{$page_data->image}}"/>
                                                    <img src="{{ URL::to('/')}}/uploads/raw-materials/{{$page_data->image}}"
                                                         data-placeholder="Image" width="186"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <h4>Contact Details</h4>
                                    <hr/>
                                    <div class="form-group {{ $errors->has('contact_person') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Contact Person</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact_person"
                                                   placeholder="Contact Person" value="{{$page_data->contact_person or ''}}">
                                            {!! $errors->first('contact_person', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact_number') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Contact Number</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact_number"
                                                   placeholder="Contact Number" value="{{$page_data->contact_number or ''}}">
                                            {!! $errors->first('contact_number', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('contact_email') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Contact Email</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="contact_email"
                                                   placeholder="Contact Email" value="{{$page_data->contact_email or ''}}">
                                            {!! $errors->first('contact_email', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Save Changes</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/activities') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
