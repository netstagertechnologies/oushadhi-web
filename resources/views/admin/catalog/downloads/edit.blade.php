@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/downloads/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="normal-tender-flds">
                                        <div id="tender-files">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Files</label>
                                                <div class="col-sm-7">
                                                    <table id="downloads"
                                                           class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                        <tr>
                                                            <td class="text-left">{{ 'Title' }}</td>
                                                            <td class="text-left">{{ 'File' }}</td>
                                                            <td></td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @php $tender_row = 0; @endphp
                                                        @if(isset($download_files))
                                                            @foreach($download_files as $download_file)
                                                                <tr id="download-row{{ $tender_row }}">
                                                                    <td class="text-left" style="width: 40%;">
                                                                        <input type="hidden"
                                                                               name="download_file[{{ $tender_row }}][file_id]"
                                                                               value="{{ $download_file->file_id or ''}}"
                                                                               placeholder="Title"
                                                                               class="form-control"/>
                                                                        <input type="text"
                                                                               name="download_file[{{ $tender_row }}][title]"
                                                                               value="{{ $download_file->title or ''}}"
                                                                               placeholder="Unit" class="form-control"/>
                                                                    </td>
                                                                    <td class="text-left">
                                                                        <a href="{{asset('uploads/downloads/'.$download_file->file_link)}}">Selected
                                                                            file: {{ $download_file->file_link or ''}}</a>
                                                                        <input type="file"
                                                                               name="download_file[{{ $tender_row }}][name]"
                                                                               value="{{ $download_file->title or ''}}"
                                                                               placeholder="Choose File"
                                                                               class="form-control"/>
                                                                        <input type="hidden"
                                                                               name="download_file[{{ $tender_row }}][file_link]"
                                                                               value="{{ $download_file->file_link or ''}}"
                                                                               class="form-control"/>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <button type="button"
                                                                                onclick="$('#download-row{{ $tender_row }}').remove();"
                                                                                data-toggle="tooltip" title="Remove"
                                                                                class="btn btn-danger"><i
                                                                                class="fa fa-minus-circle"></i></button>
                                                                    </td>
                                                                </tr> @php $tender_row = $tender_row + 1; @endphp
                                                            @endforeach
                                                        @endif
                                                        </tbody>

                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td class="text-right">
                                                                <button type="button" onclick="addDownloadFileRow();"
                                                                        data-toggle="tooltip" title="Add File"
                                                                        class="btn btn-primary"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/downloads') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script><!-- date-range-picker -->
    <script src="{{ asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script
        src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>
        var tender_row = '{{ $tender_row }}';

        function addDownloadFileRow() {
            html = '<tr id="download-row' + tender_row + '">';
            html += '  <td class="text-left" style="width: 50%;"><input type="text" name="download_file[' + tender_row + '][title]" value="" placeholder="{{ 'Title' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="file" name="download_file[' + tender_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control" accept="application/pdf,application/vnd.ms-excel"  /></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#download-row' + tender_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#downloads tbody').append(html);

            tender_row++;
        }


    </script>
@endsection
