@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <br/>


                            <div class="col-md-12">
                                <div class="row">
                                <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/quotations') }}"
                                      accept-charset="UTF-8" class="navbar-form filter-form" role="search">

                                        <div class="form-group ">
                                            <input type="text" class="form-control"
                                                   value="{{Request::get('quotation_id')}}" name="quotation_id"
                                                   placeholder="Performa Invoice Id">
                                        </div>
                                        <div class="form-group ">
                                                <input type="text" class="form-control"
                                                       value="{{Request::get('dealer')}}" name="dealer"
                                                       placeholder="Search name">
                                        </div>
                                    <div class="form-group ">
                                        <select name="user_type" id="user_type" class="form-control">
                                            <option value="">Select User Type</option>
                                            <?php $utype = (Request::get('user_type') != '') ? Request::get('user_type') : 0;?>
                                            <option value="1" @if($utype ==1) selected="" @endif>Doctor</option>
                                            <option value="2" @if($utype ==2) selected="" @endif>Dealer</option>
                                        </select>
                                    </div>
                                        <div class="form-group ">
                                            <select name="status" id="status" class="form-control">
                                                <option value="">Select Status</option>
                                                <?php $categorys = (Request::get('status') != '') ? Request::get('status') : 0;?>
                                                @foreach($quotation_status as $qt)
                                                    <option value="{{ $qt->status_id }}"
                                                            @if($categorys ==$qt->status_id) selected="" @endif>{{ $qt->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group ">
                                            <div class="input-group date">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="from_date"
                                                       class="form-control pull-right"
                                                       placeholder="From" value="{{Request::get('from_date')}}"
                                                       id="datepicker">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="input-group date">

                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" name="to_date"
                                                       class="form-control pull-right"
                                                       placeholder="To" value="{{Request::get('to_date')}}"
                                                       id="datepicker1">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <div class="form-group ">
                                            <a href="{{ url(\App\Utils::getUrlRoute().'/quotations') }}"
                                               class="btn  btn-warning"><i class="fa fa-close"></i>Clear</a>

                                        </div>
                                </form>
                                </div>
                            </div>

                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">

                                @include('template.admin.alert')
                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th>Performa Invoice</th>
                                        <th>Dealer Name</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>Performa Invoice #{{$d->quotation_id}}</td>
                                                <td>@php $user_stat=\App\User::getUserDetails($d->user_id);@endphp
                                                    @if($user_stat) {{$user_stat->name}}
                                                    @else {{'NA'}}
                                                    @endif
                                                </td>
                                                <td>{{\App\Utils::formatPrice($d->total)}}</td>
                                                <td>@php $quot_stat=\App\models\Common::getQuotationStatusName($d->quotation_status);@endphp
                                                    {{$quot_stat}}</td>
                                                <td>
                                                    <?php if (in_array(2, $crud_permissions)){?> <a
                                                        href="{{url(\App\Utils::getUrlRoute().'/quotations/' . $d->quotation_id ) }}"
                                                        class="btn btn-success"><i
                                                            class="fa fa-eye"></i></a>
                                                    <?php }?>

                                                    <?php if (in_array(3, $crud_permissions)){?> <a
                                                        href="{{url(\App\Utils::getUrlRoute().'/quotations/' . $d->quotation_id . '/edit') }}"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-pencil-square-o"></i></a>
                                                    <?php }?>
                                                    <?php if (in_array(4, $crud_permissions)){?>
                                                    {{--<form method="POST"
                                                          action="{{ url(\App\Utils::getUrlRoute().'/quotations/' . $d->quotation_id) }}"
                                                          accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger" title="Delete"
                                                                onclick="return confirm('Are you sure?')"><i
                                                                class="fa  fa-trash-o" aria-hidden="true"></i></button>
                                                    </form>--}}
                                                    <?php }?>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- bootstrap datepicker -->
    <script
        src="{{ asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset('admin/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>
        //Date picker
        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
        $('#datepicker1').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>
@endsection
