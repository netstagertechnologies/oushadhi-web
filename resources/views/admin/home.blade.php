@extends('layouts.backend_template')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <!-- Main content -->
        <div class="content">
        @include('template.admin.alert')
        <!-- Small boxes (Stat box) -->
            <!-- Info boxes -->
            <div class="row">
                @if(isset($pending_count))
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{$pending_count}}</h3>
                                <p>Pending @if($pending_count>1)  Invoices @else Perfoma Invoice @endif</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios-clock-outline"></i>
                            </div>
                            <a href="{{url(\App\Utils::getUrlRoute().'/quotations?status=1')}}" class="small-box-footer">View all <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
            <!-- /.col -->
                @if(isset($review_count))
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <h3>{{$review_count}}</h3>
                                <p>Reviewed @if($review_count>1) Perfoma Invoices @else Perfoma Invoice @endif</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-registered"></i>
                            </div>
                            <a href="{{url(\App\Utils::getUrlRoute().'/quotations?status=2')}}" class="small-box-footer">View all <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
            <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>
                @if(isset($processing_count))
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{$processing_count}}</h3>
                                <p>Proccessing @if($processing_count>1) Perfoma Invoices @else Perfoma Invoice @endif</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-spinner"></i>
                            </div>
                            <a href="{{url(\App\Utils::getUrlRoute().'/quotations?status=3')}}" class="small-box-footer">View all <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                @endif
            <!-- /.col -->
                @if(isset($cancelled_count))
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3>{{$cancelled_count}}</h3>
                                <p>Cancelled @if($cancelled_count>1) Perfoma Invoices @else Perfoma Invoice @endif</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-ios-close-outline"></i>
                            </div>
                            <a href="{{url(\App\Utils::getUrlRoute().'/quotations?status=5')}}" class="small-box-footer">View all <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
            @endif
            <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-8">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Perfoma Invoice</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Perfoma Invoice</th>
                                        <th>Dealer Name</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!$latest_quotations->isEmpty())
                                        @foreach($latest_quotations as $d)
                                            <tr>
                                                <td>
                                                    <a href="{{ url(\App\Utils::getUrlRoute().'/quotations/'.$d->quotation_id) }}">Perfoma Invoice
                                                        #{{$d->quotation_id}}</a></td>
                                                <td>{{$d->name}}</td>
                                                <td>{{\App\Utils::formatPrice($d->total)}}
                                                </td>
                                                <td>
                                                    @php $quot_stat=\App\models\Common::getQuotationStatusName($d->quotation_status);@endphp
                                                    <span
                                                        class="label
 @switch($d->quotation_status)
                                                        @case(1) label-info
                                            @break
                                                        @case(2)
                                                            label-primary
@break
                                                        @case(3)
                                                            label-warning
@break
                                                        @case(4)
                                                            label-success
@break
                                                        @case(5)
                                                            label-danger
@break
                                                        @default
                                                            label-info
@break
                                                        @endswitch
                                                            ">
                                                        {{$quot_stat}}</span>
                                                </td>
                                            </tr>
                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="5">No Perfoma Invoice found</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <a href="{{url(\App\Utils::getUrlRoute().'/quotations/')}}" class="btn btn-sm btn-default btn-flat pull-right">View All
                                Perfoma Invoice</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->

                    <div class="col-md-6">
                        <!-- Info Boxes Style 2 -->
                        <div class="info-box bg-aqua">
                            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Perfoma Invoices</span>
                                <span class="info-box-number">{{$past_quot_count or 0}}</span>

                                <span class="progress-description">
                                Received within a month
                              </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-number">{{$past_pdt_count or 0}}</span>
                                <span class="info-box-text">Different Products</span>

                                <span class="progress-description">
                                    Quoted within a month
                                 </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                        <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Quantity</span>
                                <span class="info-box-number">{{$past_qty_count or 0}}</span>

                                <span class="progress-description">
                                    Ordered within a month
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->


                    </div>
                    <!-- /.col -->

                    <div class="col-md-6">
                        <!-- USERS LIST -->
                        <div class="box box-danger">
                            <div class="box-header with-border">
                                <h3 class="box-title">Latest Dealers</h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <ul class="users-list clearfix">

                                    @if(!$latest_dealers->isEmpty())
                                        @foreach($latest_dealers as $d)
                                            <li>
                                                <img src="{{asset('admin/dist/img/avatar5.png')}}" alt="User Image">
                                                <a class="users-list-name" href="#">{{$d->name}}</a>
                                            </li>
                                        @endforeach
                                    @else
                                        <li>
                                            <span class="product-info">No products found</span>
                                        </li>
                                    @endif
                                </ul>
                                <!-- /.users-list -->
                            </div>
                            <!-- /.box-body -->
                           {{-- <div class="box-footer text-center">
                                <a href="javascript:void(0)" class="uppercase">View All Users</a>
                            </div>--}}
                            <!-- /.box-footer -->
                        </div>
                        <!--/.box -->
                    </div>
                    <!-- /.col -->


                </div>
                <!-- PRODUCT LIST -->
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Recently Added Products</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                @if(!$latest_products->isEmpty())
                                    @foreach($latest_products as $d)
                                        <li class="item">
                                            <?php $image = ($d->image) ? $d->image : 'placeholder.jpg';?>
                                            <div class="product-img">
                                                <img src="{{asset('uploads/product/'.$image)}}" alt="Product Image">
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript:void(0)" class="product-title">{{$d->title}}</a>
                                                <span class="product-description">
                                          {{$d->category}}
                                        </span>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li class="item">
                                        <div class="product-info">No products found</div>
                                    </li>
                            @endif
                            <!-- /.item -->
                            </ul>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">
                            <a href="{{url(\App\Utils::getUrlRoute().'/product')}}" class="uppercase">View All Products</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->

                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Perfoma Invoice Status</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="chart" id="quot-chart" style="height: 300px; position: relative;"></div>
                                    <!-- ./chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <ul class="chart-legend clearfix">
                                        <li><i class="fa fa-circle-o text-aqua"></i> Pending</li>
                                        <li><i class="fa fa-circle-o text-blue"></i> Reviewed</li>
                                        <li><i class="fa fa-circle-o text-yellow"></i> Processing</li>
                                        <li><i class="fa fa-circle-o text-green"></i> Delivered</li>
                                        <li><i class="fa fa-circle-o text-red"></i> Cancelled</li>
                                    </ul>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-center">Perfoma Invoice status in a month
                        </div>
                        <!-- /.footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->
    </div>
<script>
        $(function () {
            "use strict";

//DONUT CHART
            var donut = new Morris.Donut({
                element: 'quot-chart',
                resize: true,
                colors: [ "#00c0ef", "#337ab7","#f39c12","#00a65a", "#dd4b39"],
                data: [
                    {label: "Pending Perfoma Invoices", value: '{{$pending_count_month or 0}}'},
                    {label: "Reviewed Perfoma Invoices", value: '{{$review_count_month or 0}}'},
                    {label: "Processing Perfoma Invoices", value: '{{$processing_count_month or 0}}'},
                    {label: "Delivered Perfoma Invoices", value: '{{$delivered_count_month or 0}}'},
                    {label: "Cancelled Perfoma Invoices", value: '{{$cancelled_count_month or 0}}'}
                ],
                hideHover: 'auto'
            });
        });
        </script>
@endsection
