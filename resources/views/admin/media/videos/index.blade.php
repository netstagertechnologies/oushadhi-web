@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <?php if (in_array(1, $crud_permissions)){?><a
                                href="{{ url(\App\Utils::getUrlRoute().'/media-videos/create') }}"
                                class="btn btn-success btn-sm" title="Add New Activity">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                            <?php }?>
                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">


                                @include('template.admin.alert')
                                <table class="table table-borderless">

                                    <tr>
                                        <th>Id</th>
                                        <th style="width:50%">Link</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $d)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $d->link }}</td>
                                                <td>{{ ($d->status)?'Enabled':'Disabled' }}</td>
                                                <td>
                                                    <?php if (in_array(3, $crud_permissions)){?> <a
                                                        href="{{url(\App\Utils::getUrlRoute().'/media-videos/' . $d->media_video_id . '/edit') }}"
                                                        class="btn btn-primary"><i
                                                            class="fa fa-pencil-square-o"></i></a>
                                                    <?php }?>
                                                    <?php if (in_array(4, $crud_permissions)){?>
                                                    <form method="POST"
                                                          action="{{ url(\App\Utils::getUrlRoute().'/media-videos/' . $d->media_video_id) }}"
                                                          accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger" title="Delete"
                                                                onclick="return confirm('Are you sure?')"><i
                                                                class="fa  fa-trash-o" aria-hidden="true"></i></button>
                                                    </form>
                                                    <?php }?>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">No records found</td>
                                        </tr>
                                    @endif


                                </table>


                                {{ $page_data->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
