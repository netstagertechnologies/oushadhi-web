@extends('layouts.backend_template')

@section('content')
    <style type="text/css">

        input[type=file] {

            display: inline;

        }

        #image_grid {

             border: 1px solid black;
            display: inline-block;
             padding: 10px;

         }
        #image_preview {
            display: inline-block;
        }
        #image_preview img,#image_grid img {
            height: 150px;
            width: auto;
            padding: 5px;

        }

        .img-wrap {
            position: relative;
            display: inline-block;
        }

        .img-wrap .closebtn {
            position: absolute;
            padding: 1px;
            right: 2px;
            height: 25px;
            text-align: center;
            width: 25px;
            border-radius: 25px;
            z-index: 100;
            color: #fff;
            font-size: 21px;
            font-weight: 700;
            line-height: 1;
            background-color: #ff5353;
        }

    </style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/media-images/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Cover Image</label>
                                        <div class="col-sm-7">
                                            <input type="file" class="upload-file" data-height="97" data-width="91"
                                                   name="image"/>

                                            @if(isset($page_data->cover)&&!empty($page_data->cover))
                                                <input type="hidden" name="cover_src" value="{{$page_data->cover}}"/>
                                                <img src="{{ asset('uploads/media/'.$page_data->cover)}}"
                                                     data-placeholder="Image" width="186"/>
                                            @endif

                                            <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                                    .jpeg,.png</i></p>
                                            <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                    [600*400]
                                                    pixel</i></p>

                                        </div>
                                    </div>
                                    <h4>Media Images</h4>
                                    <hr/>
                                    <p> * <b>Image format</b> - <i class="text-light-blue">allowed image format
                                            .jpeg,.png</i></p>
                                    <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                            [600*400] pixel</i></p>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Cover Image</label>
                                        <div class="col-sm-7">
                                            <input type="file" id="uploadFile" name="uploadFile[]" multiple/>
                                        </div>
                                    </div>


                                    <br/>

                                    <div id="image_grid">
                                        @php $image_row = 0; @endphp
                                        @if(isset($media_images))
                                            @foreach($media_images as $media_image)
                                                <div class="img-wrap" id="image-row{{$image_row}}">
                                                    <input type="hidden" name="media_images[{{$image_row}}][image]" value="{{$media_image->image or ''}}">
                                                    <span class="closebtn" onclick="$('#image-row{{$image_row}}').remove();">&times;</span>
                                                    <img src="{{asset('uploads/media/'.$media_image->image)}}">
                                                </div>
                                                @php $image_row = $image_row + 1; @endphp
                                            @endforeach
                                        @endif
                                        <div id="image_preview"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/media-images') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <script>

        $("#uploadFile").change(function () {

            $('#image_preview').html("");


            var total_file = document.getElementById("uploadFile").files.length;

            for (var i = 0; i < total_file; i++) {

                $('#image_preview').append("<div class='img-wrap'><img height='150' src='" + URL.createObjectURL(event.target.files[i]) + "'></div>");

            }

        });


    </script>
@endsection
