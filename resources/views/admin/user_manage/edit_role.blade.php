@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$page_description}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST"
                              action="{{ url(\App\Utils::getUrlRoute().'/user-role/'. $user_role[0] -> id) }}" accept-charset="UTF-8"  enctype="multipart/form-data">
                            <div class="box-body">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}


                                <div class="form-group {{ $errors->has('role_name') ? 'has-error' : ''}}">
                                    <label for="role_name" class="col-md-4 control-label">{{ 'Role Name *' }}</label>
                                    <div class="col-md-6">
                                        <input class="form-control" name="role_name" type="text" id="role_name" value="{{ $user_role[0]->role_name or ''}}" >
                                        {!! $errors->first('role_name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>

                                <?php echo $get_menu_list; ?>

                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        <a class="btn btn-default" href="{{ url(\App\Utils::getUrlRoute().'/user-role') }}">Back</a>
                                        <input class="btn btn-primary" type="submit" value="{{ $submitButtonText or 'Update' }}">
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
