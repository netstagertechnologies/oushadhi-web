@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle}}</div>
                        <div class="panel-body">

                            <div class="col-md-12">
                                <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/doc-dels') }}"
                                      accept-charset="UTF-8" class="navbar-form navbar-right" role="search">
                                    <div class="row">


                                        <div class="form-group ">
                                            <div class="col-md-3">
                                                <input type="text" class="form-control"
                                                       value="{{Request::get('search')}}" name="search"
                                                       placeholder="Search Name">
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href="{{ url(\App\Utils::getUrlRoute().'/doc-dels') }}"
                                               class="btn  btn-warning">Clear</a>

                                        </div>
                                    </div>
                                </form>
                            </div>

                            <br/>
                            <br/>

                            <div class="table-responsive col-md-12">
                                @include('template.admin.alert')
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Sl No.</th>
                                        <th >Name</th>
                                        <td style="width: 25%">Details</td>
                                        <th>Type</th>
                                        <th>User Verification</th>
                                        <th>Admin Verification</th>
                                    </tr>
                                    @if(!$page_data->isEmpty())
                                        <?php  $i = ($page_data->currentPage() - 1) * $page_data->perPage();?>
                                        @foreach($page_data as $user)
                                            <?php $i++; ?>
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>Email:{{$user->email}}<br>
                                                    Phone: {{$user->phone}} , Mobile: {{$user->mobile}}<br>
                                                    District:{{$user->district}}<br>
                                                    State:{{$user->state}}<br>
                                                    Address:{{$user->address}}<br>
                                                </td>
                                                <td> @if($user->user_type==1)
                                                        Doctor
                                                    @elseif($user->user_type==2)
                                                        Dealer
                                                    @else
                                                        Invalid
                                                    @endif
                                                </td>

                                                <td>{{($user->is_verified)?'Verified':'Not Verified'}}</td>
                                                <td>
                                                    @if($user->is_admin_verified==1)
                                                        <p class="btn btn-success">Verified</p>
                                                    @elseif($user->is_admin_verified==2)
                                                        <p class="btn btn-danger">Rejected</p>
                                                    @else <a
                                                        href="{{url(\App\Utils::getUrlRoute().'/doc-dels/make-verified/'.$user->id ) }}"
                                                        onclick="return confirm('Are you sure? Do you want to make user verified? ')" class="btn btn-success"><i
                                                            class="fa fa-check-circle-o"></i> Make Verified</a>
                                                    <a
                                                        href="{{url(\App\Utils::getUrlRoute().'/doc-dels/make-rejected/'.$user->id ) }}"
                                                        class="btn btn-danger" onclick="return confirm('Are you sure? Do you want to make user rejected? ')" ><i
                                                            class="fa fa-check-circle-o"></i> Make Rejected</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found</td>
                                        </tr>
                                    @endif
                                </table>

                                {{ $page_data->appends($_GET)->links() }}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }

                function changeStatus(id) {
                    $.ajax({
                        url: "{{ route('user.change_status') }}",
                        type: "GET",
                        data: {user: id},
                        dataType: "json",
                        success: function (data) {
                            if (data == 1) {
                                alert('Status Changed...');
                            }
                        }
                    });
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
