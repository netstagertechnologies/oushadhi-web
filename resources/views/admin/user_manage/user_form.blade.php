
<div class="col-md-7">
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $users[0]->name or ''}}">

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Email *</label>
            <input type="email" class="form-control" name="email" placeholder="Email"
                   value="{{ $users[0]->email or ''}}">

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>



    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('role') ? ' has-error' : '' }}">
            <label for="role">User Role *</label>
            <select name="role" id="role" class="form-control">
                <option onclick="" value="" selected="selected">Select Role</option>
                @foreach($roles as $role)
                    <option
                        value="{{$role -> id}}" <?php if (!empty($users)) echo ($users[0]->user_role == $role->id) ? 'selected' : ''; ?> >{{$role -> role_name}}</option>
                @endforeach
            </select>
            @if ($errors->has('role'))
                <span class="help-block">
                    <strong>{{ $errors->first('role') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
    <div class="form-group">
        <label class="col-sm-3 control-label"></label>
        <div class="col-sm-7">

            <button type="submit" class="btn btn-success pull-left">{{ $submitButtonText or 'Save' }}</button>
            <a href="{{ url(\App\Utils::getUrlRoute().'/users') }}"
               class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                    aria-hidden="true"></i> Back</a>

        </div>
    </div>

</div>
<!-- /.col-md-7 -->
