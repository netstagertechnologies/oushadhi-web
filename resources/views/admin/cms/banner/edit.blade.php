@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/banner/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Title</label>

                                        {{ method_field('PATCH') }}
                                        {{ csrf_field() }}
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="title" placeholder="Title"
                                                   value="{{$page_data->title or ''}}">
                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <h4>Banner Images</h4><hr/>
                                    <p> * <b>Image fromat</b> - <i class="text-light-blue">allowed image format
                                            .jpeg,.png</i></p>
                                    <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                            [1920*683] pixel</i></p>

                                    <table id="banners"
                                           class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <td class="text-left" style="width:25%">{{ 'Title' }}</td>
                                            <td class="text-left" style="width:20%">{{ 'Image' }}</td>
                                            <td class="text-left" style="width:30%">{{ 'Link' }}</td>
                                            <td class="text-left"style="width:25%">{{ 'Order' }}</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @php $banner_row = 0; @endphp
                                        @if(isset($banner_images))
                                            @foreach($banner_images as $banner_image)
                                                <tr id="banner-row{{ $banner_row }}">
                                                    <td class="text-left">
                                                        <input type="hidden"
                                                               name="banner_image[{{ $banner_row }}][banner_image_id]"
                                                               value="{{ $banner_image->banner_image_id or ''}}"
                                                               class="form-control"/>
                                                        <input type="text"
                                                               name="banner_image[{{ $banner_row }}][title]"
                                                               value="{{ $banner_image->title or ''}}"
                                                               placeholder="Title" class="form-control"/>
                                                    </td>
                                                    <td class="text-left">
                                                        <input type="file"
                                                               name="banner_image[{{ $banner_row }}][name]"
                                                               placeholder="Choose File" class="form-control"/>
                                                        <input type="hidden"
                                                               name="banner_image[{{ $banner_row }}][image]"
                                                               value="{{ $banner_image->image or ''}}" class="form-control"/>
                                                        <img src="{{asset('uploads/banner/'.$banner_image->image)}}" height="100"/>

                                                    </td>
                                                    <td class="text-left">
                                                        <input type="text"
                                                               name="banner_image[{{ $banner_row }}][link]"
                                                               value="{{ $banner_image->link or '#'}}"
                                                               placeholder="Link" class="form-control"/>
                                                    </td>
                                                    <td class="text-left">
                                                        <input type="text"
                                                               name="banner_image[{{ $banner_row }}][sort_order]"
                                                               value="{{ $banner_image->sort_order or '#'}}"
                                                               placeholder="Order" class="form-control"/>
                                                    </td>
                                                    <td class="text-right">
                                                        <button type="button"
                                                                onclick="$('#banner-row{{ $banner_row }}').remove();"
                                                                data-toggle="tooltip" title="Remove"
                                                                class="btn btn-danger"><i
                                                                class="fa fa-minus-circle"></i></button>
                                                    </td>
                                                </tr> @php $banner_row = $banner_row + 1; @endphp
                                            @endforeach
                                        @endif
                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="4"></td>
                                            <td class="text-right">
                                                <button type="button" onclick="addBannerImage();"
                                                        data-toggle="tooltip" title="Add Image"
                                                        class="btn btn-primary"><i
                                                        class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Submit</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/banner') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <script>

        var banner_row = '{{ $banner_row }}';

        function addBannerImage() {
            html = '<tr id="banner-row' + banner_row + '">';
            html += '  <td class="text-left" style="width: 50%;"><input type="text" name="banner_image[' + banner_row + '][title]" value="" placeholder="{{ 'Title' }}" class="form-control" required /></td>';
            html += '  <td class="text-left"><input type="file" name="banner_image[' + banner_row + '][name]" value="" placeholder="{{ 'Choose file' }}" class="form-control"/></td>';
            html += '  <td class="text-left"><input type="text" name="banner_image[' + banner_row + '][link]" value="" placeholder="{{ 'Link' }}" class="form-control"/></td>';
            html += '  <td class="text-left"><input type="text" name="banner_image[' + banner_row + '][sort_order]" value="" placeholder="{{ 'Order' }}" class="form-control"/></td>';
            html += '  <td class="text-right"><button type="button" onclick="$(\'#banner-row' + banner_row + '\').remove();" data-toggle="tooltip" title="{{ 'Remove' }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#banners tbody').append(html);

            banner_row++;
        }


    </script>
@endsection
