@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small> {{$page_subtitle}}</small>
            </h1>


            <!-- Main content -->
            <section class="content">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Horizontal Form -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <div class="col-sm-12">
                                    <div class="box-header  pull-left">
                                        <h2 class="box-title">{{$page_subtitle_desc}}</h2>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" form="form-banner" data-toggle="tooltip" title="Save"
                                                class="btn btn-primary" data-original-title="Save"><i
                                                class="fa fa-save"></i></button></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form class="form-horizontal" method="POST" id="form-banner"
                                  action="{{url(\App\Utils::getUrlRoute().'/settings/')}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">
                                    <?php if(isset($error['warning'])){?>
                                    <div class="alert alert-danger alert-dismissible"><i
                                            class="fa fa-exclamation-circle"></i> {{$error['warning']}}
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                    </div>
                                    <?php }?>
                                    {{ csrf_field() }}
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs" id="settings_tb">
                                            <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
                                            <li><a href="#tab_2" data-toggle="tab">Other</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1">

                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Name' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_meta_title" type="text"
                                                               value="{{ $settings_data['config_meta_title'] or '' }}"/>
                                                    </div>
                                                </div>

                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Description' }}</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control" rows="4"
                                                                  name="config_meta_description" type="text"
                                                        >{{ $settings_data['config_meta_description'] or '' }}</textarea>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Address' }}</label>
                                                    <div class="col-md-10">
                                                        <textarea class="form-control" rows="5" name="config_address">{{ $settings_data['config_address'] or '' }}</textarea>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label required">Email</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_email" type="text"
                                                               value="{{ $settings_data['config_email'] or '' }}"
                                                               required />
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Contact Number1' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_contact_number1"
                                                               type="text"
                                                               value="{{ $settings_data['config_contact_number1'] or '' }}"/>
                                                    </div>
                                                </div>

                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Contact Number2' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_contact_number2"
                                                               type="text"
                                                               value="{{ $settings_data['config_contact_number2'] or '' }}"/>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Helpline Number' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_helpline_number"
                                                               type="text"
                                                               value="{{ $settings_data['config_helpline_number'] or '' }}"/>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Contact Email' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_contact_email"
                                                               type="text"
                                                               value="{{ $settings_data['config_contact_email'] or '' }}"/>
                                                    </div>
                                                </div>

                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Quotation Email' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_quotation_email"
                                                               type="text"
                                                               value="{{ $settings_data['config_quotation_email'] or '' }}"/>
                                                    </div>
                                                </div>

                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Map Link (embedded)' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_map_link"
                                                               type="text"
                                                               value="{{ $settings_data['config_map_link'] or '' }}"/>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Facebook Page' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_facebook_page"
                                                               type="text"
                                                               value="{{ $settings_data['config_facebook_page'] or '' }}"/>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Twitter Page' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_twitter_page"
                                                               type="text"
                                                               value="{{ $settings_data['config_twitter_page'] or '' }}"/>
                                                    </div>
                                                </div>
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Instagram' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_instagram_page"
                                                               type="text"
                                                               value="{{ $settings_data['config_instagram_page'] or '' }}"/>
                                                    </div>
                                                </div>


                                            </div>
                                            <!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <div
                                                    class="form-group">
                                                    <label for="title"
                                                           class="col-md-2 control-label">{{ 'Default items per page' }}</label>
                                                    <div class="col-md-10">
                                                        <input class="form-control" name="config_default_per_page"
                                                               type="text"
                                                               value="{{ $settings_data['config_default_per_page'] or '' }}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->
        </section>
    </div>

@endsection
