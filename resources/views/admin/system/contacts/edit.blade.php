@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">{{$page_subtitle_desc}}</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST"
                                  action="{{url(\App\Utils::getUrlRoute().'/contacts/'.$edit_id)}}"
                                  accept-charset="UTF-8" enctype="multipart/form-data">
                                <div class="box-body">

                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group {{ $errors->has('contact_type') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Type</label>
                                        @php $c_typ=(isset($page_data->contact_type)&&$page_data->contact_type=='directors')?'directors':'officers';@endphp
                                        <div class="col-sm-7">
                                            <select class="form-control" name="contact_type" placeholder="Type">
                                                <option value="directors" {{($c_typ=='directors')?'selected':''}}>Directors</option>
                                                <option value="officers" {{($c_typ=='officers')?'selected':''}}>Officers</option>
                                            </select>
                                            {!! $errors->first('contact_type', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Name</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name"
                                                   value="{{$page_data->name or ''}}" placeholder="Name">
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('designation') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label required">Designation</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="designation"
                                                   placeholder="Designation" value="{{$page_data->designation or ''}}">
                                            {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group {{ $errors->has('mob_no') ? 'has-error' : ''}}">
                                        <label class="col-sm-3 control-label">Mobile Number</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="mob_no"
                                                   placeholder="Mobile Number" value="{{$page_data->mob_no or ''}}">
                                            {!! $errors->first('mob_no', '<p class="help-block">:message</p>') !!}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Residence No</label>

                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="res_no"
                                                   placeholder="Residence No" value="{{$page_data->res_no or ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Email</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="email" placeholder="Email"
                                                   value="{{$page_data->email or ''}}">
                                        </div>
                                    </div>
                                    <div id="banner-image">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"> Image</label>
                                            <div class="col-sm-7">
                                                <input type="file" class="upload-file" data-height="91" data-width="97"
                                                       name="image"/>
                                                <p> * <b>Image format</b> - <i class="text-light-blue">allowed image
                                                        format
                                                        .jpeg,.png</i></p>
                                                <p> * <b>Image Size</b> - <i class="text-light-blue">allowed image size
                                                        [283*284]
                                                        pixel</i></p>

                                                @if(isset($page_data->image)&&!empty($page_data->image))
                                                    <input type="hidden" name="img_src" value="{{$page_data->image}}"/>
                                                    <img src="{{ asset('uploads/contacts/'.$page_data->image)}}"
                                                         data-placeholder="Image" width="186"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Sort Order</label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" name="sort_order" value="{{$page_data->sort_order or ''}}" placeholder="Sort Order">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label required">Status</label>

                                        <div class="col-sm-7">
                                            <?php $status = (isset($page_data->status)) ? $page_data->status : 1;?>
                                            <select type="text" class="form-control"
                                                    name="status">
                                                <option value="1" {{$status?'selected':''}}>Enable</option>
                                                <option value="0">Disable</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">

                                        <button type="submit" class="btn btn-success pull-left">Save Changes</button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/contacts') }}"
                                           class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                aria-hidden="true"></i> Back</a>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{ asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>
@endsection
