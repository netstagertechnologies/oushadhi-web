@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Horizontal Form -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$page_description}}</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <form method="POST" action="{{ url(\App\Utils::getUrlRoute().'/my-profile') }}"
                                  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ csrf_field() }}


                                <div class="col-md-7">
                                    <div class="col-md-12">
                                        <div
                                            class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name">Name *</label>
                                            <input type="text" class="form-control" name="name" placeholder="Name"
                                                   value="{{ $users[0]->name or ''}}">

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-12 -->


                                    <div class="col-md-12">
                                        <div
                                            class="form-group margin-b-5 margin-t-5{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="Password">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-12 -->

                                    <div class="col-md-12">
                                        <div
                                            class="form-group margin-b-5 margin-t-5{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="password" class="form-control" name="password_confirmation"
                                                   placeholder="Confirm Password">

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                                            @endif
                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col-md-12 -->
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7">

                                            <button type="submit" class="btn btn-success pull-left">Save Changes
                                            </button>
                                            <a href="{{ url(\App\Utils::getUrlRoute().'/users') }}"
                                               class="btn btn-danger pull-right"><i class="fa fa-chevron-left"
                                                                                    aria-hidden="true"></i> Back</a>

                                        </div>
                                    </div>

                                </div>
                                <!-- /.col-md-7 -->


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
