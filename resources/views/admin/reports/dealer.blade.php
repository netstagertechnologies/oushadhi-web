@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">Order Records</div> -->
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-12">


                                    <form method="GET" action="{{ url(\App\Utils::getUrlRoute().'/dealer-report') }}"
                                          accept-charset="UTF-8" class="navbar-form filter-form" role="search">

                                            <div class="form-group ">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" placeholder="Date from"
                                                           @if(Request::get('date_from') != '') value="{{Request::get('date_from')}}"
                                                           @endif  class="form-control " id="datepicker1"
                                                           name="date_from">
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" placeholder="Date to"
                                                           @if(Request::get('date_to') != '')  value="{{Request::get('date_to')}}"
                                                           @endif class="form-control " id="datepicker2" name="date_to">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <div class="col-md-3">
                                                    <input type="text" class="form-control"
                                                           value="{{Request::get('search')}}" name="search"
                                                           placeholder="Search key" value="">
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                                <a href="{{ url(\App\Utils::getUrlRoute().'/dealer-report') }}"
                                                   class="btn  btn-warning">Clear</a>

                                            </div>
                                    </form>
                                </div>
                            </div>

                            <hr>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="ul-example">
                                    <thead>
                                    <tr class="bg-info">
                                        <th>Sl.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Total Quotation</th>
                                        <th>Total Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($quot_products as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->total_quotation}}</td>
                                            <td>{{ \App\Utils::formatPrice($item->total_price)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                {{ $quot_products->appends($_GET)->links() }}
                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <script type="text/javascript">
                $(function () {
                    $('#datepicker1').datepicker({
                        format: 'dd-mm-yyyy',
                        autoclose: true
                    });
                    $('#datepicker2').datepicker({
                        format: 'dd-mm-yyyy',
                        autoclose: true
                    });
                });
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

            </script>
            <script type="text/javascript">
                $(function () {
                    $('#datepicker').datepicker({
                        format: 'dd/mm/yyyy',
                        autoclose: true
                    });
                });
                $(document).ready(function() {

                    $('#ul-example').DataTable({
                        'paging'      : true,
                        'lengthChange': false,
                        'searching'   : true,
                        'ordering'    : true,
                        'info'        : true,
                        'autoWidth'   : false});
                } );
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
