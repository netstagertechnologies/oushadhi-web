@extends('layouts.backend_template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                {{$page_title}}
                <small>{{$page_subtitle}}</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">Order Records</div> -->
                        <div class="panel-body">

                            <div class="row">
                                <form method="GET"
                                      action="{{ url(\App\Utils::getUrlRoute().'/performance-report') }}"
                                      accept-charset="UTF-8" class="navbar-form filter-form" role="search">

                                    <div class="form-group ">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" placeholder="Date from"
                                                   @if(Request::get('date_from') != '') value="{{Request::get('date_from')}}"
                                                   @endif  class="form-control " id="datepicker1"
                                                   name="date_from">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" placeholder="Date to"
                                                   @if(Request::get('date_to') != '') value="{{Request::get('date_to')}}"
                                                   @endif class="form-control " id="datepicker2" name="date_to">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <button class="btn btn-primary" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <a href="{{ url(\App\Utils::getUrlRoute().'/performance-report') }}"
                                           class="btn  btn-warning">Clear</a>

                                    </div>
                                </form>
                                <div class="col-md-7">

                                    <div class="box  box-primary box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Quotation counts based on status</h3>
                                        </div>
                                        <div class="box-body">
                                            <br/><br/>
                                            @foreach($status_data as $data)
                                                <div class="col-xs-3 col-md-4 text-center">
                                                    @php $perc=0;
                                            if($total_quot>0)
                                            $perc=(int)((($data->total_quotations)/$total_quot)*100);
                                                    @endphp
                                                    <input type="text" class="knob" value="{{$perc}}" data-width="90"
                                                           data-height="90" readonly
                                                           data-fgColor="{{$data->color or '#3c8dbc'}}">

                                                    <div class="knob-label">
                                                        <a onclick="gotoQuotationStatus({{$data->status_id}})">{{$data->total_quotations}}
                                                            Quotation in </a>
                                                    </div>
                                                    <div class="knob-label">{{$data->status_title}}</div>

                                                </div>
                                            @endforeach


                                        </div>

                                        <br/><br/>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="box  box-primary box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">Quotation counts based on status</h3>
                                        </div>
                                        <div class="box-body">
                                            <br/><br/>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>Total Quotation</td>
                                                    <td><span class="badge bg-green">{{$total_quot or 0}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td>Total Amount</td>
                                                    <td><span
                                                            class="badge bg-orange">{{$quotation_data->total_amount or 0}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Total Quantity</td>
                                                    <td><span
                                                            class="badge bg-primary">{{$quotation_data->total_qty or 0}}</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Total Different Product</td>
                                                    <td><span
                                                            class="badge bg-aqua">{{$quotation_data->total_products or 0}}</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


    </div>


    <script src="{{asset('admin/bower_components/jquery-knob/js/jquery.knob.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            $('#datepicker1').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            });
            $('#datepicker2').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>
    <script type="text/javascript">
        $(function () {
            $('#datepicker').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true
            });
        });
        $(document).ready(function () {

            $('#ul-example').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false
            });
        });

        function gotoQuotationStatus(ststaus) {
            window.location.href = "{{url(\App\Utils::getUrlRoute().'/quotations/')}}" + '?status=' + ststaus + "&date_from=" + $('#datepicker1').val() + "&date_to=" + $('#datepicker2').val();
        }

        $(function () {
            /* jQueryKnob */

            $(".knob").knob({
                'format': function (value) {
                    return value + '%';
                },
                /*change : function (value) {
                 //console.log("change : " + value);
                 },
                 release : function (value) {
                 console.log("release : " + value);
                 },
                 cancel : function () {
                 console.log("cancel : " + this.value);
                 },*/
                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.value);
                            this.o.cursor
                            && (sa = ea - 0.3)
                            && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.previousColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                    }
                }
            });
            /* END JQUERY KNOB */

        });
    </script>
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
