-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 21, 2019 at 10:14 PM
-- Server version: 5.7.27
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oushadhi_oushadhi`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `activity_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`activity_id`, `title`, `slug`, `description`, `image`, `status`, `date_added`, `date_modified`) VALUES
(1, 'OUSHADHI ACTIVITIES', 'oushadhi-activities-4', 'Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept. Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.', '5c7a7f53154531551531859.jpg', 1, '2019-03-02 13:04:19', '2019-03-02 13:06:01'),
(2, 'OUSHADHI ACTIVITIES', 'oushadhi-activities-3', 'Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept. Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.', '5c7a7f6d0ac521551531885.jpg', 1, '2019-03-02 13:04:45', '2019-03-02 13:05:41'),
(3, 'OUSHADHI ACTIVITIES', 'oushadhi-activities-3', 'Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept. Largest producer of Ayurveda medicines in public sector in India One among the few Public Sector companies, consistently making profit and paying dividend to Government of Kerala since 1999. Supplies medicine to Government of Kerala for the distribution to common man through ISM Dept.', '5c7a7f9391da61551531923.jpg', 1, '2019-03-02 13:05:23', '2019-03-02 13:05:23');

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(11) NOT NULL,
  `title` varchar(555) NOT NULL,
  `address` text,
  `email` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `image` varchar(555) DEFAULT NULL,
  `map_link` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `title`, `address`, `email`, `mobile`, `phone`, `image`, `map_link`, `status`, `sort_order`, `date_added`, `date_modified`) VALUES
(1, 'Oushadhi Sales Depot', 'Arogyabhavan, Opposite Ayurveda College, M.G Road,\r\nThiruvananthapuram', 'tvmbranch@oushadhi.org', NULL, '0471 2332981', '5d0c5d37ee41b1561091383.JPG', NULL, 1, 7, '0000-00-00 00:00:00', '2019-08-21 16:51:28'),
(2, 'Oushadhi Sales counter', 'Panchakarma Institute Campus, Poojappura, Thiruvananthapuram-12', NULL, NULL, NULL, 'contact2.jpg', NULL, 1, 7, '0000-00-00 00:00:00', '2019-08-03 13:58:09'),
(3, 'Oushadhi Panchakarma Hospital and research institute', 'Shornur road, Thrissur-22', 'panchakarma@oushadhi.org', NULL, 'Land Phone : 0487 2334396', '5cf4ea721c29a1559554674.jpeg', NULL, 1, 4, '0000-00-00 00:00:00', '2019-08-03 13:57:33'),
(4, 'Oushadhi Subcenter North Region', 'Medical College P.O, Pariyaram, Kannur', 'rdc@oushadhi.org', NULL, '0497 2808303, 2808275', '5cf4eb4fda6e41559554895.JPG', NULL, 1, 2, '0000-00-00 00:00:00', '2019-08-21 16:48:15'),
(5, 'Oushadhi Subcenter Southern Region', 'Forest range office compound, Pathanapuram, Kollam Dist. Pin : - 689695', 'rdcsouth@oushadhi.org', NULL, '0475 2352899, 2015525', 'contact5.jpg', NULL, 1, 3, '0000-00-00 00:00:00', '2019-08-21 16:47:51'),
(6, 'Proprietary Ayurvedic Medicine Manufacturing Unit', 'Muttathara, Thiruvananthapuram', 'oushadhimuttathara@gmail.com', NULL, '0471 2502648', '5d0324d2c14d71560487122.JPG', NULL, 1, 5, '0000-00-00 00:00:00', '2019-08-21 16:47:01'),
(7, 'Oushadhi', 'The Pharmaceutical Corporation (IM) Kerala Limited \r\nKuttanellur, Thrissur', 'mail@oushadhi.org', NULL, '0487 2459800', '', 'https://www.google.com/maps/place/Oushadhi/@10.4923007,76.2537914,17z/data=!4m12!1m6!3m5!1s0x3ba7f01c786d9dd3:0x6550f210ac42c95a!2sOushadhi!8m2!3d10.4922954!4d76.2559801!3m4!1s0x3ba7f01c786d9dd3:0x6550f210ac42c95a!8m2!3d10.4922954!4d76.2559801?hl=en-US', 1, 1, '2019-08-01 14:58:32', '2019-08-21 16:44:40');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super` tinyint(1) NOT NULL DEFAULT '0',
  `user_role` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `is_super`, `user_role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'WebsiteAdmin', 'admin@oushadhi.org', '$2y$10$LNtLmxSYPDQVV/b2wZ7xfu0WE8grzws8XXT2KgTvEWBvN0Np/N4bK', 1, 1, 'sPwBJWEDqxXXqIMsfMR5NSXhXuUoC5GiRz2uaq1H6hrHCswwOgTA0dftWQZ0', '2019-02-28 07:09:09', '2019-08-28 09:19:59'),
(2, 'Tempadmin', 'tempadmin@gmail.com', '$2y$10$bxPS8WTWe.QtqhNipJQ2V.m9YOej.1E7uILHCyCcMIMgsofBB6YIq', 1, 1, 'fXwJRF27zO3ZOgiiiVDmMzMT1C5RzKpcEAk9qm3P3cK7nbKVPihBZgBmcd16', '2019-03-07 04:54:02', '2019-08-28 09:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `title`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Sample', 1, '2019-03-02 13:24:30', '2019-08-21 15:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `banner_images`
--

CREATE TABLE `banner_images` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(500) NOT NULL,
  `link` varchar(500) DEFAULT '#',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner_images`
--

INSERT INTO `banner_images` (`banner_image_id`, `banner_id`, `title`, `image`, `link`, `sort_order`) VALUES
(10, 1, NULL, '5d5a920b719d71566216715-Oush1.jpg', '#', 2),
(11, 1, NULL, '5d5a920b7306a1566216715-Oush2.jpg', '#', 1),
(12, 1, NULL, '5d5a920b74bcd1566216715-Oush3.jpg', '#', 3);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `title`, `slug`, `short_description`, `description`, `image`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Healthy living in Karkidakam', 'healthy-living-in-karkidakam', NULL, '<p>Karkidakam is the last month of the Malayalam calendar. The month is from July 16 to August 16. Ayurveda is gaining profound importance in the name of &lsquo;Karkidaka Chikitsa&rsquo;, especially in the recent years. The reason being pure marketing of Ayurveda, the science is not to be limited just as a Karkidaka chikitsa krama, as the regimens to be practised in every rthu (season) and rthu sandhis (transformation period of two seasons) are clearly mentioned in Ayurveda.</p>\r\n\r\n<p>Why this month is so peculiar to Keralites, lies in the fact that Karkidakam was once said as the &lsquo; Panja Masam&rsquo; with severe downpour, spread of epidemics, famines resulting from scarcity of stored cereals etc. The season being the transformation period marking the end of summer and beginning of rain, sudden climatic change from hot to cold, also tremble the normalcy of living body. The unbalanced tridoshas and the degraded digestive system affect natural immunity of the body and it becomes an easy target for pathogens. So our ancestors had followed several customs, to minimize the inflictions during rthu sandhi, for the spiritual and physical wellbeing.</p>\r\n\r\n<p><strong>Why Rhtucharya?</strong></p>\r\n\r\n<ul>\r\n	<li>Ayurveda stresses the importance of &lsquo;Rithu Charya&rsquo; or the diet and lifestyles to be followed in a particular season to be free from the harmful effects of climatic changes and also to balance the deranged &lsquo;Doshas&rsquo; ( Thridoshas &ndash; Vata , Pitta, Kapha).</li>\r\n	<li>Karkidaka is the end of greeshma kala (Summer) and beginning of Varsha kala (rainy season), which as per the Ayurvedic classics is the prakopa kala (aggravation) of Vata dosha and also the Chaya kala (Accumulation) of Pitta dosha in our body.The metabolism is sluggish during this period and it is likely to experience loss of appetite. Therefore, the food and regimens in this period should be as such that it corrects these deranged doshas.</li>\r\n</ul>\r\n\r\n<p><strong>Diet in Varsha Ritu (rainy season)</strong></p>\r\n\r\n<ul>\r\n	<li>Consume light and fresh foods prepared from barley, rice and wheat</li>\r\n	<li>Include cow&rsquo;s ghee, lean meat, lentils, green gram, rice and wheat in daily diet.</li>\r\n	<li>Take Sour and salted soups of vegetables.</li>\r\n	<li>Drink boiled and cooled water mixed with little honey</li>\r\n	<li>Add ginger and green gram in your daily diet</li>\r\n	<li>Eat warm food and avoid eating uncooked foods and salads</li>\r\n	<li>Avoid drinking excess of fluids at this further slows down the metabolism.</li>\r\n	<li>Avoid consuming stale food.</li>\r\n	<li>Avoid curds, red meat and any foodstuff, which takes longer time to digest. One may have buttermilk instead of curds.</li>\r\n	<li>Vata samana ahara like &lsquo;Njavara ari&rsquo; with choornas which support pitta (like ariyaru) processed in it, prepared as a porridge, which is commonly called as &lsquo;Karkidaka Kanji&rsquo;, is taken during this season.</li>\r\n</ul>\r\n\r\n<p><strong>Regimen in Varsha Ritu (rainy season)</strong></p>\r\n\r\n<ul>\r\n	<li>Avoid Sleeping in daytime as it hampers digestion and slows down the metabolism</li>\r\n	<li>Avoid over exertion&nbsp;</li>\r\n	<li>Always keep the surrounding dry and clean. Do not allow water to get accumulated around.</li>\r\n	<li>Keep body warm&nbsp;</li>\r\n	<li>Avoid getting wet in the rains.&nbsp;</li>\r\n	<li>Panchakarma can be done.</li>\r\n	<li>Vata samana therapies like Abhyanga, seka etc will help to bring back the deranged vata dosha to the normal state and prevent it from causing diseases in the body.</li>\r\n</ul>', '5c790a7777ecd1551436407.jpg', 1, '2019-03-01 10:33:27', '2019-07-02 14:19:56'),
(2, 'Oushadhi - Largest producer of Ayurvedic medicines', 'oushadhi-largest-producer-of-ayurvedic-medicines', 'Oushadhi -The Pharmaceutical Corporation (Indian Medicines) Kerala Ltd is an Ayurvedic medicine manufacturing company situated at  Kuttanellur, in Thrissur City of Kerala state. It is a Government of Kerala owned company', '<p>Acceptance of Ayurveda and its Treatment Modalities has now reached a new height and the whole world is looking at this branch for remedy for several human health problems. The branch has already made giant strides in development, as more and more opportunities are starting to build up. Simultaneously Ayurveda medicine market also has shown tremendous growth over a period of last 10 years and is poised for a major leap in coming years. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Oushadhi</strong>&nbsp;-<strong>The&nbsp;Pharmaceutical Corporation (Indian Medicines) Kerala Ltd</strong>&nbsp;is an Ayurvedic medicine manufacturing company situated at &nbsp;<a href=\"https://en.wikipedia.org/wiki/Kuttanellur\">Kuttanellur</a>, in&nbsp;<a href=\"https://en.wikipedia.org/wiki/Thrissur\">Thrissur</a>&nbsp;City of&nbsp;<a href=\"https://en.wikipedia.org/wiki/Kerala\">Kerala</a>&nbsp;state. It is a <a href=\"https://en.wikipedia.org/wiki/Public_sector_undertakings_in_Kerala\">Government of Kerala owned</a>&nbsp;company and produces around 450 Ayurvedic formulations and 25 Patent and proprietary medicines.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The company was started in the year 1941, by the then Maharaja of Cochin, for the purpose of meeting the medicine requirements of a Hospital in Thrissur as well as the princely families of Cochin. It was transformed into a Government Pharmacy after independence and registered as a society in 1956. In 1975, it was registered as a &lsquo;Limited Company&rsquo;.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Oushadhi has its GMP and ISO 9001:2015 certified factory at&nbsp;<a href=\"https://en.wikipedia.org/wiki/Kuttanellur\">Kuttanellur</a>,&nbsp;<a href=\"https://en.wikipedia.org/wiki/Thrissur\">Thrissur</a>&nbsp;City and Muttathara, Trivandrum. Also regional distribution centres at&nbsp;<a href=\"https://en.wikipedia.org/wiki/Pariyaram\">Pariyaram</a>&nbsp;in&nbsp;<a href=\"https://en.wikipedia.org/wiki/Kannur_District\">Kannur </a>&nbsp;and Pathanapuram, Kollam. The company has established 30-bed Panchakarma Hospital and Research Institute at&nbsp;<a href=\"https://en.wikipedia.org/wiki/Thrissur\">Thrissur</a>&nbsp;City in 2004, which is being scaled up to 50 beds. Oushadhi has a full-fledged R&amp;D wing, established in the year 2008, with an Ayush approved Lab having technical experts in Ayurveda, Pharmacy, Chemistry, Botany &amp; Microbiology. Oushadhi is the sole supplier of Ayurvedic medicines to Government hospitals and dispensaries in Kerala. Oushadhi also supplies medicines to many other states at concessional prices. The company also owns two medicinal plant nurseries at&nbsp;<a href=\"https://en.wikipedia.org/wiki/Kuttanellur\">Kuttanellur</a>&nbsp;and&nbsp;<a href=\"https://en.wikipedia.org/wiki/Pariyaram\">Pariyaram</a>. Oushadhi has around 650 dealers across Kerala. Presently, <strong>Oushadhi is the largest producer of Ayurvedic medicines in Public sector in India</strong> and it is one among the few public sector companies consistently making profit and paying dividend to Government of Kerala.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Oushadhi is serving the public since more than 75 years and is in the path of consistent progress aiming to be a company with 500 Cr turnover by the year 2025. We extend our wholehearted thanks to all of our supporters and wish the same further.</p>', '5c790a8e1dc0a1551436430.jpg', 1, '2019-03-01 10:33:50', '2019-07-02 14:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `cancelled_quotations`
--

CREATE TABLE `cancelled_quotations` (
  `cancel_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `reason` text,
  `user_id` int(11) NOT NULL,
  `type` varchar(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `last_date` date DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id`, `title`, `last_date`, `dept`, `contact`, `contact_email`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Notification for various post at Oushadhi , Kuttanellur', '2019-09-20', 'Administration', NULL, 'administration@oushadhi.org', 1, '2019-08-28 14:58:22', '2019-09-06 12:05:09'),
(6, 'Notification for the post of Civil Engineer and Trainee Botanist at Oushadhi Kuttanellur', '2019-09-17', 'Administration', NULL, 'administration@oushadhi.org', 0, '2019-08-29 09:23:07', '2019-09-18 09:19:18'),
(7, 'Notification for the post of Management Trainee at Oushadhi, Kuttanellur.', '2019-09-18', 'Administration', '0487-2459800', 'administration@oushadhi.org', 0, '2019-09-14 11:36:04', '2019-09-19 09:52:59'),
(8, 'Notification for the post of Shift Operator and Apprentice At Oushadhi Pathanapuram Subcentre, Kollam', '2019-10-05', 'Administration', '0487-2459800', 'administration@oushadhi.org', 1, '2019-09-18 09:16:26', '2019-09-18 09:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `career_files`
--

CREATE TABLE `career_files` (
  `file_id` int(11) NOT NULL,
  `career_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `file_link` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `career_files`
--

INSERT INTO `career_files` (`file_id`, `career_id`, `title`, `file_link`) VALUES
(16, 6, 'Notification', '5d71fd67e100a1567751527-notification.pdf'),
(17, 1, 'Notification', '5d71fe1d8e14b1567751709-Notification.pdf'),
(18, 7, 'Notification', '5d7c834c21d431568441164-notification for Management Trainee.pdf'),
(19, 8, 'Notification', '5d81a892a24231568778386-Notification.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `career_form`
--

CREATE TABLE `career_form` (
  `career_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mob_no` varchar(15) DEFAULT NULL,
  `message` longtext,
  `doc_path` varchar(500) DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL,
  `is_replied` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `product_type` int(11) NOT NULL DEFAULT '0' COMMENT '0=ayurveda,1=siddha',
  `sort_order` int(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `title`, `image`, `slug`, `parent_id`, `product_type`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Asavams & Arishtams', NULL, 'asavams-&-arishtams', 0, 0, 1, 1, '2019-03-01 11:29:15', '2019-03-01 11:29:15'),
(2, 'Bhasma Sindooram', NULL, 'bhasma-sindooram', 0, 0, 2, 1, '2019-03-01 11:29:31', '2019-03-01 11:29:31'),
(3, 'Gulika', NULL, 'gulika', 0, 0, 6, 1, '2019-03-01 11:29:41', '2019-08-13 12:25:20'),
(4, 'Kashaya  Sookshma Choornam', NULL, 'kashaya-sookshma-choornam', 0, 0, 7, 1, '2019-03-01 11:30:11', '2019-08-13 10:48:09'),
(5, 'Lehyam', NULL, 'lehyam', 0, 0, 11, 1, '2019-03-01 11:30:28', '2019-08-09 15:29:45'),
(6, 'Kashayam', NULL, 'kashayam', 0, 0, 9, 1, '2019-03-01 11:30:37', '2019-08-09 16:44:16'),
(7, 'Thailam', NULL, 'thailam', 0, 0, 15, 1, '2019-03-01 11:30:58', '2019-08-13 14:56:32'),
(8, 'Patent & Proprietary', NULL, 'patent-&-proprietary', 0, 0, 12, 1, '2019-03-01 11:31:12', '2019-03-01 11:31:12'),
(10, 'Rasakriya', NULL, 'rasakriya', 0, 0, 13, 1, '2019-06-11 14:53:20', '2019-07-23 15:38:11'),
(11, 'SIDDHA KUDINEER CHOORNAM', NULL, 'siddha-kudineer-choornam', 0, 1, 2, 1, '2019-07-06 17:56:09', '2019-07-23 12:34:39'),
(12, 'SIDDHA  CHOORNAM', NULL, 'siddha-choornam', 0, 1, 1, 1, '2019-07-06 17:56:23', '2019-07-23 12:34:34'),
(13, 'SIDDHA PURAMARUNTHU', NULL, 'siddha-puramarunthu', 0, 1, 5, 1, '2019-07-06 17:56:45', '2019-07-23 15:35:04'),
(14, 'SIDDHA  LEHYAM', NULL, 'siddha-lehyam', 0, 1, 3, 1, '2019-07-23 15:34:22', '2019-07-23 15:34:22'),
(15, 'SIDDHA  MANAPPAGU', NULL, 'siddha-manappagu', 0, 1, 4, 1, '2019-07-23 15:34:41', '2019-07-23 15:34:41'),
(16, 'SIDDHA  THAILAM', NULL, 'siddha-thailam', 0, 1, 6, 1, '2019-07-23 15:35:15', '2019-07-23 15:35:15'),
(17, 'SIDDHA  THENNER', NULL, 'siddha-thenner', 0, 1, 7, 1, '2019-07-23 15:35:31', '2019-07-23 15:35:31'),
(18, 'SIDDHA VADAKAM', NULL, 'siddha-vadakam', 0, 1, 8, 1, '2019-07-23 15:35:45', '2019-07-23 15:35:45'),
(19, 'SIDDHA VENNAI', NULL, 'siddha-vennai', 0, 1, 9, 1, '2019-07-23 15:36:00', '2019-07-23 15:36:00'),
(21, 'Sandhana Kriya', NULL, 'sandhana-kriya', 0, 0, 14, 1, '2019-08-09 12:50:51', '2019-08-09 12:50:51'),
(22, 'Ghrutham', NULL, 'ghrutham', 0, 0, 5, 1, '2019-08-09 15:03:47', '2019-08-09 15:03:47'),
(23, 'Kashayam Tablet', NULL, 'kashayam-tablet', 0, 0, 10, 1, '2019-08-13 10:46:13', '2019-08-13 10:46:13'),
(24, 'Kashayachoornam', NULL, 'kashayachoornam', 0, 0, 8, 1, '2019-08-13 10:49:29', '2019-08-13 10:49:29'),
(25, 'Choornam (Compound Drugs)', NULL, 'choornam-compound-drugs', 0, 0, 3, 1, '2019-08-13 10:55:09', '2019-08-13 10:55:09'),
(26, 'Choornam (Single Drugs)', NULL, 'choornam-single-drugs', 0, 0, 4, 1, '2019-08-13 10:55:16', '2019-08-13 10:55:16');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `mob_no` varchar(20) NOT NULL,
  `res_no` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `contact_type` varchar(25) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`contact_id`, `name`, `designation`, `mob_no`, `res_no`, `email`, `image`, `contact_type`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, 'E Shibu', 'Marketing Manager', '9447041521', NULL, 'marketing@oushadhi.org', '', 'officers', 5, 1, '2019-03-05 06:08:30', '2019-08-21 16:40:33'),
(2, 'LATHAKUMARI.P.M', 'Financial Controller', '9847433522', '04872201477', 'finance@oushadhi.org', '', 'officers', 2, 1, '2019-03-05 06:09:01', '2019-08-21 16:37:56'),
(3, 'REGHUNANDANAN . V . MENON', 'General Manager', '9447726304', NULL, 'administration@oushadhi.org', '', 'officers', 3, 1, '2019-03-05 06:10:00', '2019-08-21 16:40:42'),
(4, 'VINITHA.S', 'Company Secretary', '9447599377', NULL, 'cs@oushadhi.org', '', 'officers', 4, 1, '2019-03-05 06:10:42', '2019-05-06 11:33:56'),
(6, 'Dr. Rajesh G', 'Manager (HR)', '9447991992', NULL, NULL, '', 'officers', 6, 1, '2019-03-05 06:12:29', '2019-05-06 11:34:17'),
(7, 'R Suresh Kumar', 'Manager (Purchase)', '9447991000', NULL, 'purchase@oushadhi.org', '', 'officers', 7, 1, '2019-03-05 06:17:33', '2019-08-21 16:36:52'),
(8, 'Dr. Figi. C. David', 'Manager (Sales)', '9447991991', NULL, 'marketing@oushadhi.org', '', 'officers', 8, 1, '2019-03-05 06:17:35', '2019-05-06 11:34:42'),
(9, 'Dr. T. Jayasree Gopinath', 'Manager (Production)', '9447467111', NULL, 'production@oushadhi.org', '', 'officers', 9, 1, '2019-03-05 06:17:37', '2019-05-06 11:34:57'),
(10, 'Dr. A. Malini Menon', 'Manager (Quality Control)', '9447982859', NULL, 'quality@oushadhi.org', '', 'officers', 10, 1, '2019-03-05 06:17:39', '2019-05-06 11:35:10'),
(11, 'K.A Chandrasekharan', 'Manager (Works)', '9446572159', NULL, 'maintenance@oushadhi.org', '', 'officers', 11, 1, '2019-03-05 06:17:41', '2019-08-21 16:36:42'),
(12, 'Rajesh. M.P', 'Manager (Accounts)', '8547421579', NULL, 'accounts@oushadhi.org', '', 'officers', 12, 1, '2019-03-05 06:17:43', '2019-08-21 16:36:34'),
(14, 'Dr.  K.R Viswambharan IAS (Rtd)', 'Chairman', '9447134804', NULL, 'chairman@oushadhi.org', '', 'directors', 1, 1, '2019-04-26 17:20:38', '2019-08-21 16:39:33'),
(15, 'K.V Uthaman IFS', 'Managing Director', '9447015566', NULL, 'md@oushadhi.org', '5d4ab8526f4531565177938.jpg', 'directors', 2, 1, '2019-04-26 17:21:29', '2019-08-21 16:36:16'),
(16, 'Dr  Priya . S', 'Director, Indian Systems of Medicine, Govt. of Kerala', '8547102577', NULL, NULL, '5d4ab4b480b941565177012.jpg', 'directors', 3, 1, '2019-04-26 17:22:17', '2019-08-07 16:53:32'),
(17, 'Preetha B. S.', 'Joint Secretary, Finance Department Govt. of Kerala', '9495043472', NULL, NULL, '5d4ab20eb9f251565176334.jpg', 'directors', 4, 1, '2019-04-26 17:22:59', '2019-08-21 16:38:49'),
(18, 'V. Bhooshan', 'Additional Secretary, AYUSH Department Govt of Kerala', '8075961509', NULL, NULL, '', 'directors', 5, 1, '2019-04-26 17:23:29', '2019-08-21 16:36:04'),
(19, 'E. N Mohandas', 'Namasya’ Near Parishath Bhavan Downhill, Malapuram -676519', '9744096336', NULL, NULL, '5d4baab1b04e51565239985.jpg', 'directors', 6, 1, '2019-04-26 17:24:23', '2019-08-21 16:35:55'),
(20, 'K Kunjiraman', 'Ex- MLA', '9447489560', NULL, NULL, '', 'directors', 7, 1, '2019-04-26 17:25:24', '2019-08-21 16:35:45'),
(21, 'A. S Kutty', 'Not Specified', '9447618426', NULL, NULL, '', 'directors', 8, 1, '2019-04-26 17:26:05', '2019-08-21 16:34:34'),
(22, 'Adv. G. R Anil', 'Advocate', '9447800304', NULL, NULL, '5d4ab16f8e61d1565176175.jpg', 'directors', 9, 1, '2019-04-26 17:26:55', '2019-08-07 16:39:35'),
(23, 'K V Uthaman IFS', 'Managing Director', '9447015566', '04872973221', 'md@oushadhi.org', '5d4ab7e24a6051565177826.jpg', 'officers', 1, 1, '2019-05-06 11:31:44', '2019-08-21 16:32:41');

-- --------------------------------------------------------

--
-- Table structure for table `contact_form`
--

CREATE TABLE `contact_form` (
  `contact_form_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mob_no` varchar(15) DEFAULT NULL,
  `message` longtext,
  `is_read` tinyint(1) NOT NULL,
  `is_replied` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `page_content` text,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `title`, `page_content`, `status`, `date_added`, `date_modified`) VALUES
(1, 'MARKETING AND SALES', '<ul>\r\n	<li>Monopoly supplier of Ayurveda medicines to all Government Hospitals, Colleges, Dispensaries under ISM department of Government of Kerala.</li>\r\n	<li>Supplier of medicine to Government Hospitals and Dispensaries of other states viz. Madhya Pradesh, Andhra Pradesh, Chattissgarh, Pondicherry, Rajasthan, Orissa, New Delhi, Karnataka, Punjab, Himachal Pradesh, Hariyana, Sikkim, Tamil Naduand and Andaman Nicobar Islands.</li>\r\n	<li>Supplier of medicine to E.S.I Dispensaries under State E.S.I Department.</li>\r\n	<li>Supplies medicines to general public through over 650 dealers across Kerala and special outlets in metros all over India.</li>\r\n	<li>Annual turnover during 2018-19&nbsp;is 151&nbsp;Crore</li>\r\n</ul>', 1, '2019-05-31 18:22:12', '2019-08-03 09:16:47'),
(2, 'RESEARCH AND DEVELOPMENT', '<p>A new R&amp;D wing was established in Oushadhi in the year 2008. We have well established Ayush accredited laboratory with experts in Pharmacy, Botany, Biochemistry and Microbiology headed by an Ayurvedic Doctor, Now R&amp;D lab is in the path of NABL Accreditation. &nbsp;</p>\r\n\r\n<p><strong>Objectives &amp; Functions.&nbsp; </strong></p>\r\n\r\n<ol>\r\n	<li>To develop products in needful segments as per the demand from Marketing department.</li>\r\n	<li>R&amp;D lab is in the path of NABL Accreditation.</li>\r\n	<li>To develop quality assurance parameters and standardization of raw material and finished products.</li>\r\n	<li>To modify &amp; validate the process in classical products to reduce process time, cost of production with new scientific methods.</li>\r\n	<li>Collaborative projects with TBGRI, CFTRI, NMPB, SMPB &amp;KAU</li>\r\n	<li>Inter disciplinary projects and training for PG scholars.</li>\r\n	<li>COE Activities for &nbsp;&nbsp;standardization of Asava-Arishsta</li>\r\n	<li>Product certification is issued to obtain drug licence of our patent &amp; classical products.</li>\r\n	<li>Outside samples are verified &amp; product certification reports are given to obtain drug Licence.</li>\r\n</ol>', 1, '2019-05-31 18:24:22', '2019-06-14 12:27:41'),
(3, 'QUALITY CONTROL', '<p><strong>Quality Control Department</strong></p>\r\n\r\n<p>Quality control is a routine process involving tests for monitoring the quality of each and every batch of raw drug / process / product and its compliance with predetermined standards.</p>\r\n\r\n<p><strong>Function of QC</strong></p>\r\n\r\n<ul>\r\n	<li>Raw Material Quality Inspection and approval</li>\r\n	<li>In process quality Verification</li>\r\n	<li>Customer-Complaint analysis, take corrective and preventive action</li>\r\n	<li>Tender sample verification process.</li>\r\n	<li>Finished Medicine Quality Inspection and approval for product release</li>\r\n	<li>Technical data verification of lables&nbsp;and cartons</li>\r\n	<li>Quality assurance of packing materials</li>\r\n	<li>Authority to keep&nbsp;control sample of batch wise medicines up to the date of&nbsp; expiry</li>\r\n</ul>', 1, '2019-05-31 18:24:41', '2019-08-05 15:22:36'),
(4, 'HUMAN RESOURCE', '<ul>\r\n	<li>Establishment matters like recruitment, promotion, wage revision etc.</li>\r\n	<li>Legal issues on administrative matter.</li>\r\n	<li>Trade union issues.</li>\r\n	<li>Training and development.</li>\r\n	<li>Law and order issues.</li>\r\n	<li>Enforcement of work culture.&nbsp;</li>\r\n</ul>', 1, '2019-06-03 12:51:42', '2019-06-14 11:06:42'),
(5, 'PURCHASE', '<ul>\r\n	<li>Purchases more than 500 varieties of raw materials through open tender</li>\r\n	<li>Standardized procedure to verify the variety of raw materials.</li>\r\n	<li>More than 45% raw materials come from the forest.</li>\r\n	<li>Annual purchase of Rs. 82 crores.</li>\r\n	<li>Buy back agreement with farmers.</li>\r\n	<li>Maintaining data base of raw materials.</li>\r\n</ul>', 1, '2019-06-03 12:52:23', '2019-06-14 11:10:00'),
(6, 'PRODUCTION', '<ul>\r\n	<li>Produces 498&nbsp;different varieties of medicines.</li>\r\n	<li>Multi-disciplinary team of doctors for supervision.</li>\r\n	<li>Production of medicines as per GMP norms.</li>\r\n	<li>Quality assurance in various stages of production process.</li>\r\n	<li>Modern dosage forms like Tablet, Capsule, Ointment, Granules and Syrup.</li>\r\n	<li>Tradition amalgamates with Modern Technology.</li>\r\n</ul>', 1, '2019-06-03 12:52:43', '2019-08-21 13:38:55'),
(7, 'FINANCE AND ACCOUNTS', '<ul>\r\n	<li>Providing quantitative data for economic decision.</li>\r\n	<li>Fund Management including working capital management.</li>\r\n	<li>Maintaining systematic records.</li>\r\n	<li>Ensuring compliance with accounting standards so as to depict true and fair view of financial position and profitability of the company.</li>\r\n	<li>Preparation of Cost data.</li>\r\n	<li>Ensuring timely remittance of all payments including statutory payments.</li>\r\n	<li>Interpretation of financial statement and their practical application.</li>\r\n	<li>Strengthening internal control through internal audit wing.</li>\r\n</ul>', 1, '2019-06-03 12:53:01', '2019-08-07 15:34:08'),
(8, 'MAINTENANCE', NULL, 1, '2019-06-03 12:53:29', '2019-08-03 12:54:30');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `downloads`
--

INSERT INTO `downloads` (`id`, `title`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Product Brochure', 1, '2018-12-31 14:58:56', '2019-06-03 13:04:30'),
(2, 'Therapeutic Index', 1, '2018-11-20 08:36:14', '2019-06-03 13:05:07'),
(3, 'Annual Report 2017-18', 1, '2019-08-07 17:33:02', '2019-08-07 17:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `download_files`
--

CREATE TABLE `download_files` (
  `file_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `file_link` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `download_files`
--

INSERT INTO `download_files` (`file_id`, `download_id`, `title`, `file_link`) VALUES
(2, 1, 'Product Brochure', '5cf4cc9818d971559547032-Product brochure B_Option2.pdf'),
(4, 2, 'Therapeutic Index', '5cf4cc5d7e37f1559546973-Therapeutic Index Final CTP file 2019 May.pdf'),
(5, 3, 'Annual Report', '5d4abdf62b3df1565179382-Annual Report 2017 -18.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `factory_images`
--

CREATE TABLE `factory_images` (
  `factory_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `factory_images`
--

INSERT INTO `factory_images` (`factory_id`, `title`, `image`, `date_added`, `date_modified`) VALUES
(4, 'QC LAB', '5d03229ae438b1560486554.png', '2019-06-14 09:59:14', '2019-06-14 09:59:14'),
(5, 'PACKING', '5d0322cf1ef081560486607.png', '2019-06-14 10:00:07', '2019-06-14 10:00:07'),
(6, 'RAW MATERIAL CLEANING', '5d03230e0dc611560486670.png', '2019-06-14 10:01:10', '2019-06-14 10:01:10'),
(7, 'FINISHED GOODS STORE', '5d03237cb739e1560486780.png', '2019-06-14 10:03:00', '2019-06-14 10:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_cms`
--

CREATE TABLE `home_page_cms` (
  `h_id` int(11) NOT NULL,
  `h_section` int(11) DEFAULT NULL,
  `h_image` varchar(255) DEFAULT NULL,
  `h_caption` varchar(255) DEFAULT NULL,
  `h_title` varchar(500) DEFAULT NULL,
  `h_link` varchar(255) DEFAULT NULL,
  `h_description` text,
  `h_sort_order` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page_cms`
--

INSERT INTO `home_page_cms` (`h_id`, `h_section`, `h_image`, `h_caption`, `h_title`, `h_link`, `h_description`, `h_sort_order`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'About Us', 'The Pharmaceutical Corporation (IM) Kerala limited', 'https://projectpreview.org/Oushadhi/public/about-us', 'The Pharmaceutical Corporation (IM) Kerala limited, popularly known as Oushadhi, is a fully Kerala Government owned Ayurvedic Medicine Manufacturing Unit. It is the largest producer of Ayurvedic Medicines belonging to Public Sector in the country. Oushadhi manufactures and market high quality ayurvedic medicines at a reasonable price, adhering to classical Ayurvedic text under the direct supervision of Ayurvedic physicians. With a range of 498 plus popular products, Oushadhi meets the entire requirement of Govt. Hospitals and Dispensaries in Kerala and also of other states. Apart from this, it also supplies medicines to ESI Dispensaries, Tribal Development Department etc. of the state. Trusted by millions, Oushadhi is a leading player in the market. Oushadhi has a 650 strong dealer network that spreads across the length and breadth of Kerala. It has made its presence felt in other states also through its various sales outlets in major cities of the country.\r\nOushadhi has a modern full-fledged factory with State of Art Technology at Kuttanellur in Thrissur district of Kerala. While following traditional Ayurvedic medicines manufacturing methods, this factory combines the elements of advanced technology by using modern machinery. A new factory unit was started in Muttathara of Thiruvananthapuram District thereby making a significant leap in improvisation of production infrastructure. To ensure quality to the highest order, Oushadhi has a well-equipped Quality Control Lab that monitors every step of production, right from the procurement of raw materials to packing and storing.  Oushadhi also has GMP (Good Manufacturing Practices) and ISO 9001:2015 certifications for its Production Unit and Ayush accreditation for its Lab.To ensure quality raw materials Oushadhi has set up Medicinal Plant Garden where rare medicinal plants are nurtured.\r\nIn view of strengthening Ayurveda to keep pace with the fast changing world, Oushadhi has set up an R&D wing. This department has already developed many innovative methods for increasing the efficacy of medicines and manufacturing market friendly products. \r\nOushadhi has also ventured into treatment field by starting a modern Panchakarma Hospital in 2005. Another hospital with state of art facility will be completed soon in Thrissur.', 1, '2018-11-26 15:34:57', '2018-11-26 15:34:57'),
(2, 2, '5c7e30fdbdf861551773949.jpg', 'HEALTHY TIPS', 'HEALTHY TIPS', 'https://projectpreview.org/Oushadhi/public/blog/healthy-living-in-karkidakam', 'Healthy living in Karkidakam - ‘Karkidaka Chikitsa’ \r\nBe with Ayuveda. Be with nature.', 1, '2018-11-26 15:34:57', '2018-11-26 15:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `measurement_units`
--

CREATE TABLE `measurement_units` (
  `mu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`media_id`, `title`, `cover`, `slug`, `status`, `date_added`, `date_modified`) VALUES
(1, 'OUSHADHI GALLERY', '5c7dfb9ae53411551760282.jpg', 'oushadhi-gallery', 1, '2019-03-05 04:31:22', '2019-03-05 04:31:22'),
(2, 'OUSHADHI GALLERY2', '5c7dfcb5404bb1551760565.jpg', 'oushadhi-gallery2', 1, '2019-03-05 04:36:05', '2019-03-05 04:36:05'),
(3, 'OUSHADHI GALLERY3', '5c7dfce93b8901551760617.jpg', 'oushadhi-gallery3', 1, '2019-03-05 04:36:57', '2019-03-05 04:43:51'),
(4, 'OUSHADHI GALLERY4', '5c7dfe315ff131551760945.jpg', 'oushadhi-gallery4', 1, '2019-03-05 04:42:25', '2019-03-05 04:42:25'),
(5, 'OUSHADHI GALLERY5', '5c7dfe4b1753e1551760971.jpg', 'oushadhi-gallery5', 1, '2019-03-05 04:42:51', '2019-03-05 04:42:51'),
(6, 'OUSHADHI GALLERY6', '5c7dfe75b88a41551761013.jpg', 'oushadhi-gallery6', 1, '2019-03-05 04:43:33', '2019-03-05 04:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `media_images`
--

CREATE TABLE `media_images` (
  `media_image_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media_images`
--

INSERT INTO `media_images` (`media_image_id`, `media_id`, `image`, `sort_order`) VALUES
(1, 1, '15517602830.jpg', 0),
(2, 1, '15517602831.jpg', 0),
(3, 1, '15517602832.jpg', 0),
(4, 1, '15517602833.jpg', 0),
(5, 1, '15517602834.jpg', 0),
(6, 1, '15517602835.jpg', 0),
(7, 2, '15517605650.jpg', 0),
(8, 2, '15517605651.jpg', 0),
(9, 2, '15517605652.jpg', 0),
(13, 4, '15517609460.jpg', 0),
(14, 4, '15517609461.jpg', 0),
(15, 4, '15517609462.jpg', 0),
(16, 5, '15517609710.jpg', 0),
(17, 5, '15517609711.jpg', 0),
(18, 5, '15517609712.jpg', 0),
(19, 5, '15517609713.jpg', 0),
(20, 6, '15517610130.jpg', 0),
(21, 6, '15517610131.jpg', 0),
(22, 6, '15517610132.jpg', 0),
(23, 6, '15517610133.jpg', 0),
(24, 3, '15517606180.jpg', 0),
(25, 3, '15517606181.jpg', 0),
(26, 3, '15517606182.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `media_videos`
--

CREATE TABLE `media_videos` (
  `media_video_id` int(11) NOT NULL,
  `link` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `media_videos`
--

INSERT INTO `media_videos` (`media_video_id`, `link`, `status`, `date_added`, `date_modified`) VALUES
(1, 'https://www.youtube.com/embed/bNjNdvLhv2A', 1, '2019-03-05 06:18:56', '2019-03-05 06:18:56'),
(2, 'https://www.youtube.com/embed/xT-1Wup7Xao', 1, '2019-03-05 06:19:07', '2019-03-05 06:19:07'),
(3, 'https://www.youtube.com/embed/J-04ErOyv4s', 1, '2019-03-05 06:19:21', '2019-03-05 06:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `menu_master`
--

CREATE TABLE `menu_master` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_parent` int(11) NOT NULL,
  `menu_icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `menu_order` int(10) NOT NULL,
  `admin_access` int(11) NOT NULL,
  `show_menu` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_master`
--

INSERT INTO `menu_master` (`id`, `menu_name`, `menu_path`, `menu_parent`, `menu_icon`, `role_name`, `status`, `menu_order`, `admin_access`, `show_menu`, `created_at`, `updated_at`) VALUES
(1, 'User Accounts', '0', 0, 'fa fa-users', '', 1, 1, 0, 1, '2018-04-24 20:58:18', '2018-04-25 01:48:20'),
(2, 'User Records', 'users', 1, '', 'users', 1, 0, 0, 1, '2018-04-25 03:41:22', '2018-04-25 04:59:44'),
(3, 'User Roles', 'user-role', 1, '', 'user-role', 1, 0, 0, 1, '2018-09-24 18:30:00', '2018-09-24 18:30:00'),
(4, 'CMS', '', 0, 'fa fa-sitemap', '', 1, 5, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(5, 'Pages', 'pages', 4, '', 'pages', 1, 2, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(6, 'Catalog', '', 0, 'fa fa-database', '', 1, 3, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(7, 'Testimonials', 'testimonials', 6, '', 'testimonials', 1, 3, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(8, 'Blog', 'blog', 6, '', 'blog', 1, 2, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(9, 'News', 'news', 6, '', 'news', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(10, 'Activities', 'activities', 6, '', 'activities', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(11, 'Manage Product', '', 0, 'fa fa-cube', '', 1, 2, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(12, 'Category', 'category', 11, '', 'category', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(13, 'System', '', 0, 'fa fa-gear', '', 1, 10, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(14, 'Settings', 'settings', 13, '', 'settings', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(15, 'Product', 'product', 11, '', 'product', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(16, 'Tenders', 'tenders', 6, '', 'tenders', 1, 5, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(17, 'Banner', 'banner', 4, '', 'banner', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(18, 'Media', 'media', 0, 'fa fa-video-camera', 'media', 1, 5, 0, 1, '2018-09-24 18:30:00', '2018-09-24 18:30:00'),
(19, 'Images', 'media-images', 18, '', 'media-images', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(20, 'Videos', 'media-videos', 18, '', 'media-videos', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(21, 'Directors & Officers', 'contacts', 4, '', 'contacts', 1, 2, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(22, 'Home Page', 'home-cms', 4, '', 'home-cms', 1, 3, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(23, 'Performa Invoice', 'quotations', 0, 'fa fa-file', '', 1, 5, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(24, 'Performa Invoice', 'quotations', 23, '', 'quotations', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(25, 'Cancelled Performa Invoice', 'cancelled-quotations', 23, '', 'cancelled-quotations', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(26, 'Reports', 'reports', 0, 'fa fa-bar-chart', '', 1, 5, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(27, 'Productwise report', 'product-report', 26, '', 'product-report', 1, 4, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(28, 'Performa Invoice-Product report', 'quotation-product-report', 26, '', 'quotation-product-report', 1, 3, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(29, 'Performa Invoice-Category report', 'quotation-category-report', 26, '', 'quotation-category-report', 1, 2, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(30, 'Performa Invoice report', 'quotation-report', 26, '', 'quotation-report', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(31, 'Performance report', 'performance-report', 26, '', 'performance-report', 1, 1, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(32, 'Raw Materials Required', 'raw-materials', 6, '', 'raw-materials', 1, 6, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(33, 'Factory Picture', 'factory-images', 6, '', 'factory-images', 1, 6, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(34, 'Address', 'addresses', 6, '', 'addresses', 1, 7, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(35, 'Downloads', 'downloads', 6, '', 'downloads', 1, 8, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(36, 'Careers', 'careers', 6, '', 'careers', 1, 9, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(37, 'Departments', 'departments', 6, '', 'departments', 1, 9, 0, 1, '2019-02-28 18:30:00', '2019-02-28 18:30:00'),
(38, 'Doctors & Dealers', 'doc-dels', 1, '', 'doc-dels', 1, 0, 0, 1, '2018-09-24 18:30:00', '2018-09-24 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_12_084320_create_admins_table', 1),
(4, '2018_07_12_084720_create_writers_table', 1),
(5, '2019_02_28_112542_create_admins_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `title`, `slug`, `image`, `description`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Two medicinal plant nurseries at Kuttanellur and Pariyaram, supported by SMPB.', 'two-medicinal-plant-nurseries-at-kuttanellur-and-pariyaram-supported-by-smpb', '5c79117c3d8391551438204.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text...</p>', 1, '2019-02-20 11:03:24', '2019-03-07 12:15:24'),
(2, 'Largest producer of Ayurveda', 'largest-producer-of-ayurveda', '5c7911a0e804f15514382401.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. industry&#39;s standard dummy text...</p>', 0, '2019-03-01 11:04:00', '2019-06-03 14:34:05');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(11) DEFAULT NULL,
  `not_type` varchar(255) DEFAULT NULL,
  `not_ref_id` int(11) DEFAULT NULL,
  `not_msg` varchar(255) DEFAULT NULL,
  `not_description` text,
  `read_status` tinyint(1) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `type`, `not_type`, `not_ref_id`, `not_msg`, `not_description`, `read_status`, `date_added`, `date_modified`) VALUES
(1, 1, 'admin', 'quotation', 1, 'Customer `anju` added a new Performa Invoice `Performa Invoice#1`', '', 0, '2019-09-06 12:04:55', '2019-09-06 12:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(10) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `page_content` longtext,
  `slug` varchar(500) DEFAULT NULL,
  `banner` varchar(500) DEFAULT NULL,
  `metaTitle` varchar(500) DEFAULT NULL,
  `metaDescription` varchar(500) DEFAULT NULL,
  `metaKeyword` varchar(500) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `title`, `page_content`, `slug`, `banner`, `metaTitle`, `metaDescription`, `metaKeyword`, `createdDate`, `updated_at`, `status`) VALUES
(3, 'Medicinal Plant Cultivation', '<ul class=\"com-list\">\r\n	<li>The medicinal plants nurseries established in Kuttanellur &amp; Pariyaram for propagating medicinal plants cultivation among public.\r\n	\r\n	<li>Around 3 Lakh seedling are being distributed per year from both nurseries.</li>\r\n	</li>\r\n	<li>Also opened a new venture like &ldquo;Oushadha Sasya Vijnana Vyapana Kendram&rdquo; at the above both center in order to get awareness about medicinal plants among students.</li>\r\n	<li>Large scale cultivation of medicinal plants being happened at our Pariyaram Plot in Kannur District.</li>\r\n	<li>Oushadhi also took part the prestigious &ldquo;Grihachaithanyam&rdquo; project by state medicinal Plant board with providing 60,000 Nos of &ldquo;Karivepu, Aryavepu&rdquo; seedlings.</li>\r\n</ul>', 'medicinal-plant-cultivation', 'Medicinal Plant Cultivation', 'Medicinal Plant Cultivation', 'Medicinal Plant Cultivation', 'Medicinal Plant Cultivation', '2018-05-17 00:00:00', '2019-06-03 07:11:20', 1),
(5, 'Special', '<p><strong>Oushadhi Panchakarma Hospital and Research Centre</strong></p>\r\n\r\n<p>&nbsp;</p>', 'special', 'special', 'special', 'special', 'special', '2018-05-17 00:00:00', '2019-06-18 05:42:19', 1),
(6, 'CSR Activities', '<p><strong>CORPORATE SOCIAL RESPONSIBILITY (CSR) POLICY</strong></p>\r\n\r\n<p><strong>FOR THE PHARMACEUTICAL CORPORATION (I.M) KERALA LTD.</strong></p>\r\n\r\n<ol>\r\n	<li>\r\n	<p><strong>INTRODUCTION:</strong></p>\r\n\r\n	<p>Corporate Social Responsibility (CSR) is the Company&rsquo;s commitment to its stakeholders to conduct business in an economically, socially and environmentally sustainable manner that is transparent and ethical. THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. is committed to undertake CSR activities in accordance with the provisions of Section 135 of the Indian Companies Act, 2013 and related Rules.</p>\r\n	</li>\r\n	<li>\r\n	<p><strong>AIMS &amp; OBJECTIVES</strong></p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (i) THE PHARMACEUTICAL CORPORATION (LM.) KERALA LTD shall promote projects Those are:&nbsp; &nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(a) Sustainable and create a long term change;<br />\r\n&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;(b) Have specific and measurable goals in alignment with the company&#39;s philosophy;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(c) Address the most deserving cause or beneficiaries.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;(ii) To establish process and mechanism for the implementation and monitoring of the CSR Activities for the Company.</p>\r\n\r\n<p><strong>1.&nbsp; COMMITTEE COMPOSITION </strong></p>\r\n\r\n<p>The CSR Committee of the Board shall be composed of at least three (3) Directors, Members of the CSR Committee may be replaced by any other member of the Board.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n\r\n<p>The present members of CSR committee are</p>\r\n\r\n<ol>\r\n	<li>Shri. K.V Uthaman IFS (Managing Director)</li>\r\n	<li>Dr. Priya S (Director - Indian System Of Medicine,Govt Of Kerala)</li>\r\n	<li>Preetha B.S (Joint Secretary - Financial Department,Govt Of Kerala)&nbsp; &nbsp; &nbsp;&nbsp;</li>\r\n	<li>Shri. V Bhooshan (Additional Secretary - AYUSH Department,Govt Of Kerala)&nbsp;</li>\r\n	<li>Shri. A.S Kutty&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>\r\n</ol>\r\n\r\n<p><strong>2.&nbsp; COMMITTEE MEETINGS</strong></p>\r\n\r\n<p>The CSR Committee shall meet as often as its members deem necessary to perform the duties and responsibilities but not less than quarterly.</p>\r\n\r\n<ol>\r\n</ol>\r\n\r\n<p><strong>3.&nbsp; DUTIES &amp; RESPONSIBILITIES OF CSR COMMITTEE</strong></p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Review of the CSR activities to be undertaken by THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. The CSR Committee is&nbsp;guided by the list of Activities specified in Schedule VII to the Companies Act, 2013 and appended to this Policy as Appendix - 1. Appendix 1 may be revised in line with any amendments/inclusions made To Schedule VII of the Companies Act, 2014.</p>\r\n	</li>\r\n	<li>\r\n	<p>Formulate and recommend to the Board the CSR activities/programs to be undertaken by the Company.</p>\r\n	</li>\r\n	<li>\r\n	<p>Recommend the CSR Expenditure to be incurred on the CSR activities/programs.</p>\r\n	</li>\r\n	<li>\r\n	<p>Institute a transparent mechanism for implementation of the CSR projects and activities Effectively monitor the Execution of the CSR activities.</p>\r\n	</li>\r\n	<li>\r\n	<p>Prepare an annual report of the CSR activities undertaken for the company and submit such Report to the Board</p>\r\n	</li>\r\n</ol>\r\n\r\n<p><strong>4.&nbsp; RESPONSIBILITY OF THE BOARD</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>(i) Approve the CSR Policy and the CSR Expenditure after taking into consideration the recommendations made by the CSR committee</p>\r\n	</li>\r\n	<li>\r\n	<p>(ii) Ensure the CSR spending every financial year of at least 2%&nbsp;of average net profits made during immediately preceding 3 financial years/ in pursuance with the Policy.</p>\r\n	</li>\r\n	<li>\r\n	<p>(iii)Ensure that CSR activities included in the CSR Policy are undertaken by THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. and those activities are related to the activities specified in Schedule VII of the Companies Act.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>5.&nbsp; CSR EXPENDITURE</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>(i) In every financial year, THE PHARMACEUTICAL CORPORATION (i.M.) KERALA LTD. Shall spend a minimum of 2%&nbsp;of its average Net Profits in the immediately preceding 3 Financial years. Average Net profits shall mean the net profits of the Company as per The Profit &amp; Loss Statement prepared in accordance with the Companies Act, 2013CSR Expenditure shall mean all expenditure incurred in respect of specific projects/programs Relating to the approved CSR activities.</p>\r\n	</li>\r\n	<li>\r\n	<p>(ii) CSR Expenditure shall not include expenditure on an item not in conformity or not in line With activities which fall within the purview of the CSR activities listed in Schedule VII.</p>\r\n	</li>\r\n	<li>\r\n	<p>(iii) CSR Expenditure shall not include Projects or programs or activities undertaken outside India.</p>\r\n	</li>\r\n	<li>\r\n	<p>(iv)The surplus arising out of the CSR activities or projects shall not form part of the business Profit of THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>6. CSR ACTIVITIES &ndash; PROJECTS</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>(i) THE PHARMACEUTICAL CORPORATION (I.M.) KERALA LTD. shall promote CSR Activities/Projects in the field of :</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>A. Promoting preventive health care - conducting free medical camps in all parts of Kerala&nbsp;And organize continuous medical education programme for doctors and sponsorship in Sports activates<br />\r\nB. Ensuring environmental sustainability, ecological balance, protection of flora and fauna -to take up cultivation of medicinal Plants and distribute the same at free of cost.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>(ii)Company may also undertake other CSR activities in line with Schedule VII.</p>\r\n	</li>\r\n	<li>\r\n	<p>(iii)The CSR activities shall be undertaken in locations within India. Company shall give preference to the local areas and the areas around which Company operates while considering the activities to be undertaken and spending the amount earmarked for CSR activities. However, Company has a multi-state presence and hence may be guided by the requirements of the specific CSR activity/program in determining the locations within India.</p>\r\n	</li>\r\n</ul>\r\n\r\n<h1>APPENDIX &ndash; 1</h1>\r\n\r\n<p>CSR ACTIVITIES LISTED INSCHEDULE VII OFTHE COMPANIES ACT, 2013</p>\r\n\r\n<p>CSR shall focus on social, economic and environmental impact rather than mere output and outcome. Activities which are ad hoc and philanthropic in nature shall be avoided. Various activities that can be undertaken in general under CSR are outlined below:</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Eradicating extreme hunger and poverty and malnutrition, promoting preventive healthcare and sanitation and making available safe drinking water;</p>\r\n	</li>\r\n	<li>\r\n	<p>Promotion of education; including special education and employment enhancing vocation skills especially among children, woman, elderly and the differently abled and livelihood enhancement projects;</p>\r\n	</li>\r\n	<li>\r\n	<p>Promoting gender equality and empowering women; setting up homes and hostels for women and orphans, setting up old age homes, day care centers, and such other facilities for senior citizens and measures for reducing inequalities faced by socially and economically backward groups;</p>\r\n	</li>\r\n	<li>\r\n	<p>Ensuring environmental sustainability/ ecological balance, protection of flora and fauna. Animal welfare, agroforestry, conservation of natural resources and maintaining of quality of soil, air and&nbsp;water;</p>\r\n	</li>\r\n	<li>\r\n	<p>protection of national heritage, art and culture including restoration of buildings and sites of historical importance and works of art; setting up of public libraries; promotion and development of traditional arts and handicrafts;</p>\r\n	</li>\r\n	<li>\r\n	<p>Measures for the benefit of armed forces veterans, war widows and their dependents;</p>\r\n	</li>\r\n	<li>\r\n	<p>Training to promote rural sports, nationally recognized sports, and Paralympic sports and Olympic sports;</p>\r\n	</li>\r\n	<li>\r\n	<p>contribution to the Prime Minister&#39;s National Relief Fund or any other fund set up by the Central Government or the State Governments for socio-economic development and relief and welfare of the Scheduled Castes, the Scheduled Tribes, other backward classes, minorities and women;</p>\r\n	</li>\r\n	<li>\r\n	<p>Contributions or funds provided to technology incubators located within academic institutions which are approved by the Central Government; and Rural development projects.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>', 'csr-activities', 'CSR Activities', 'CSR Activities', 'CSR Activities', 'CSR Activities', '2018-05-17 00:00:00', '2019-08-28 09:53:20', 1),
(7, 'RTI Act', '<h2>&nbsp;</h2>\r\n\r\n<h2><strong>Details as required under the Right to Information Act</strong></h2>\r\n\r\n<p>1. The particulars of its organisation, functions and duties</p>\r\n\r\n<p>Please refer the Introductory Chapter of the Web Site</p>\r\n\r\n<p>2. The Powers and duties of its officers and employees</p>\r\n\r\n<p>2.1. <strong>Managing Director</strong><br />\r\nThe powers and duties of the Managing Director includes overall charge of all activities of the organisation, formulation of strategies, provide direction, identify and provide adequate resources, assignment of trained personnel etc.</p>\r\n\r\n<p>2.2. <strong>Financial Controller</strong><br />\r\nThe powers and duties of Financial Controller includes:<br />\r\nAdvice Managing Director in all financial and Accounts matters, Scrutinise and clear the financial proposals, ensure that the expenditures are having proper sanction and within the Budgetory limit, responsible for upto date maintenance of all records, returns and accounts, preparation of Annual Budgets, timely preparation of annual accounts and all other related works connected with Finance and Accounts.</p>\r\n\r\n<p>2.3. <strong>General Manager (P &amp; A)</strong></p>\r\n\r\n<p>The powers and duties of the post includes:<br />\r\nAttend to all personal matters including recruitments, administration of leave, administration of salary and wages, promotions, increments, transfers, disciplinary proceedings, statutory benefits etc. Matters relating to industrial relations and labour welfare are also the responsibility of General Manager.</p>\r\n\r\n<p>2.4. <strong>Secretary</strong><br />\r\nIn addition to the powers, duties and responsibilities assigned to the post of Secretary under the Indian Companies Act, 1956, the Secretary will also have the following duties and responsibilities:<br />\r\nKeep company seal &amp; un issued share certificate in safe custody, Follow up of receipt of money provided in the Budget towards share capital contribution, Issue of Share Certificate after allotment, Payment of Dividend to Govt. of Kerala, Arrange Board Meetings, Annual General Meeting, General Meetings etc. in consultation with Chairman/ MD, Act as one of the trustees of Gratuity Trust and maintain the Trust Account, Arrange the periodical payment of Gratuity Premium to trust (LIC), Arranging monthly meetings of officers and writting minutes of the meeting, Welfare activities of women employees of the corporation, Any other duty assigned by Managing Director from time to time.</p>\r\n\r\n<p>2.5. <strong>Marketing Manager</strong></p>\r\n\r\n<p>The powers and duties of Marketing Manager are:<br />\r\nEvolve strategies for effective marketing of products, prepare Sales targets, monitor despatch of medicines, gather market intelligent report, new Dealership allotments, hold Dealer&rsquo;s and Doctors meetings, appraisal of Dealer performance, liaison between ISM Department, attend to matters relating to advertisement, sales promotions, Trade Fairs etc.</p>\r\n\r\n<p>2.6. <strong>Manager (HR) </strong></p>\r\n\r\n<p>The powers and duties of Manager (HR) are<br />\r\nPromoting application of modern technology in Oushadhi. Conducting training programmes. Organising welfare and recreation programmes. Take necessary actions to control accidents. Supervision of canteen, rest room, wash room etc and other extension activities.</p>\r\n\r\n<p>2.7. <strong>Manager (Purchase)</strong></p>\r\n\r\n<p>The powers and duties of Manager (Purchase) are<br />\r\nManager (Purchase) shall arrange purchase of all items of raw materials, packing materials, stores and spares, machinery, furniture, equipments and all other items required for the Corporation, after taking approval from competent authorities. His duties also includes location of vendors, preparation of tender documents for purchase, ensure observations of procedures for purchase, etc.</p>\r\n\r\n<p>2.8. <strong>Manager (Sales)</strong></p>\r\n\r\n<p>The powers and duties of Manager (Sales) includes:<br />\r\nTo attend matters relating to Sales to Government Departments, Grama Panchayats, Guruvayur Devaswom and similar institutions, act as Nodal Officer, related with Government supply, overall management of distribution to Government institutions, overall control of sales staff etc.<br />\r\nAttending to matters relating to Sales to Dealers within the State of Kerala. This includes, planning distribution of medicines to Agencies, periodical inspection of Dealers, monitoring the sales through outlets directly run by Oushadhi, attending the files regarding advertisement, sales promotion and other publicity activities, etc.</p>\r\n\r\n<p>2.9.<strong> Manager (Production)</strong></p>\r\n\r\n<p>The powers and duties of Manager(Production) includes:<br />\r\nPlanning and implementation of production in the Factory on the basis of prefixed targets, ensure that necessary documents, registers and records in relation to production are properly maintained, ensure co-ordination between sections of production, maintenance of general discipline in the factory, deployment of workers suitably, observation of product/process related statutory requirements etc.</p>\r\n\r\n<p>2.10. <strong>Manager (Q.C)</strong></p>\r\n\r\n<p>The powers and duties of Manager (Q.C) includes<br />\r\nQuality verification of raw materials and finished products. Ensure all the labels are printed as per the regulations of Drugs and Cosmetics act and quality approval of packing materials.</p>\r\n\r\n<p>2.11 .<strong>Manager (Works)</strong></p>\r\n\r\n<p>The powers and duties of Manager (Works) includes<br />\r\nThe Manager (Works) is responsible to ensure the upkeep, repairs and maintenance of all plants, machineries, tools, building and other civil and electrical installation of the organisation, initiate proposals for now construction of buildings, procurement of Plant, Machinery etc.</p>\r\n\r\n<p>2.12 <strong>Manager (Accounts)</strong></p>\r\n\r\n<p>The powers and duties of Manager (Accounts) includes<br />\r\nAttending to works related with receipts and payment of amount, supervising the cash transactions etc.</p>\r\n\r\n<p>2.13 <strong>Manager (P&amp;P)</strong></p>\r\n\r\n<p>The powers and duties of Manager (P&amp;P) includes<br />\r\n1. Visualisation of new plans &amp; Projects.<br />\r\n2. Developing Technical &amp; Financial details for the board approved plans &amp; Projects.<br />\r\n3. Coordinating with consultant by providing technical requirements &amp; planning implementation strategies.<br />\r\n4. Ensuring technical &amp; financial factors during implementation .<br />\r\n5. Supporting other department in trouble shooting.</p>\r\n\r\n<p>3. The procedure followed in the decision making process, including Channels of supervision and accountability</p>\r\n\r\n<p>The organogram of the organisation showing the hierarchical position and flow of information is shown separately. Decisions are taken at each level based on the powers delegated to them.</p>\r\n\r\n<p>4. The norms set by it for the discharge of its functions</p>\r\n\r\n<p>Norms are set up drawing up the financial powers delegated to each post and category of officers.</p>\r\n\r\n<p>5. The Rules, regulations, instructions, manuals and records, held by it or under its control or used by its employees for discharging its functions</p>\r\n\r\n<p>The following documents are mainly followed by the organisation to discharge its duties.</p>\r\n\r\n<p>a) Standing Orders<br />\r\nb) Recruitment Rules<br />\r\nc) Delegation of financial powers to officers<br />\r\nd) Quality System Manual</p>\r\n\r\n<p>1. A statement of the categories of documents that are held by it or Under its control</p>\r\n\r\n<p>a) Sulpher Licence<br />\r\nb) Excise Licence<br />\r\nc) Olive leaf classification for Panchakarma Hospital<br />\r\nd) Drug Licence<br />\r\ne) Factory Licence<br />\r\nf) GMP Certificate<br />\r\ng) ISO 9002 Certificate<br />\r\n&nbsp;</p>\r\n\r\n<p>2. The particulars of any arrangement that exists for consultation with, or representation by the members of the public in relation to the formulation of its policy or implementation thereof.</p>\r\n\r\n<p>NIL</p>\r\n\r\n<p>3. A statement of the boards, councils, committees and other bodies Consisting of two or more persons constituted as its part or for the Purpose of its advice, and as to whether meetings of those boards, Councils, committees and other bodies are open to the public, or The minutes of each meeting are accessible for public.</p>\r\n\r\n<p>Board of Directors<br />\r\n&nbsp;</p>\r\n\r\n<p>9. A Directory of its officers and employees</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<th>Sl.No.</th>\r\n			<th>Name</th>\r\n			<th>Office</th>\r\n			<th colspan=\"2\">Residence</th>\r\n			<th>Mobile</th>\r\n		</tr>\r\n		<tr>\r\n			<td>1</td>\r\n			<td>Dr.K.R Viswambharan IAS (Retd.)</td>\r\n			<td>Chairman</td>\r\n			<td colspan=\"2\">0487-2348043</td>\r\n			<td>9447134804</td>\r\n		</tr>\r\n		<tr>\r\n			<td>2</td>\r\n			<td>Mr.K.V Uthaman IFS</td>\r\n			<td>Managing Director</td>\r\n			<td colspan=\"2\">0487 2973221</td>\r\n			<td>9447015566</td>\r\n		</tr>\r\n		<tr>\r\n			<td>3</td>\r\n			<td>Mrs. P.M Latha Kumari</td>\r\n			<td>Financial Controller</td>\r\n			<td colspan=\"2\">0487 2201477</td>\r\n			<td>9847433522</td>\r\n		</tr>\r\n		<tr>\r\n			<td>4</td>\r\n			<td>Mr. Reghunandanan V. Menon</td>\r\n			<td>General Manager</td>\r\n			<td colspan=\"2\">0487 2363045</td>\r\n			<td>9447726304</td>\r\n		</tr>\r\n		<tr>\r\n			<td>5</td>\r\n			<td>Mrs. Vinitha S.</td>\r\n			<td>Company Secretary</td>\r\n			<td colspan=\"2\">&nbsp;</td>\r\n			<td>9447599377</td>\r\n		</tr>\r\n		<tr>\r\n			<td>6</td>\r\n			<td>Mr. E Shibu</td>\r\n			<td>Marketing Manager</td>\r\n			<td colspan=\"2\">0487 2351851</td>\r\n			<td>9447041521</td>\r\n		</tr>\r\n		<tr>\r\n			<td>7</td>\r\n			<td>Dr. Rajesh G</td>\r\n			<td>Manager (HR)</td>\r\n			<td colspan=\"2\">0487 2334550</td>\r\n			<td>9447991992</td>\r\n		</tr>\r\n		<tr>\r\n			<td>8</td>\r\n			<td>Mr. R Suresh Kumar</td>\r\n			<td>Manager (Purchase)</td>\r\n			<td colspan=\"2\">0487 2330463</td>\r\n			<td>9447991000</td>\r\n		</tr>\r\n		<tr>\r\n			<td>9</td>\r\n			<td>Dr. Figi. C. David</td>\r\n			<td>Manager (Sales)</td>\r\n			<td colspan=\"2\">0487 2443991</td>\r\n			<td>9447991991</td>\r\n		</tr>\r\n		<tr>\r\n			<td>10</td>\r\n			<td>Dr. T. Jayasree Gopinath</td>\r\n			<td>Manager (Production)</td>\r\n			<td colspan=\"2\">0480 2721774</td>\r\n			<td>9447467111</td>\r\n		</tr>\r\n		<tr>\r\n			<td>11</td>\r\n			<td>Dr. A. Malini Menon</td>\r\n			<td>Manager (Quality Control)</td>\r\n			<td colspan=\"2\">0487 2445090</td>\r\n			<td>9447982859</td>\r\n		</tr>\r\n		<tr>\r\n			<td>13</td>\r\n			<td>Mr. K.A Chandrasekharan</td>\r\n			<td>Manager (Works)</td>\r\n			<td colspan=\"2\">0487 2372159</td>\r\n			<td>9446572159</td>\r\n		</tr>\r\n		<tr>\r\n			<td>14</td>\r\n			<td>Mr. Rajesh. M.P -</td>\r\n			<td>Manager (Accounts)</td>\r\n			<td>&nbsp;</td>\r\n			<td colspan=\"2\">8547421579</td>\r\n		</tr>\r\n		<tr>\r\n			<td>15</td>\r\n			<td>Mr.T.Unnikrishnan</td>\r\n			<td>Manager (Projects &amp; Planning)</td>\r\n			<td>0487 2635273</td>\r\n			<td colspan=\"2\">9495433555</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>10. The monthly remuneration received by each of its officers and Employees, including the system of compensation as provided in its regulations.</p>\r\n\r\n<table style=\"width:100.0%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>Sl. No.</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Designation</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>No. of Employees</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Time scale</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Financial Controller</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 68700-110400</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>2</p>\r\n			</td>\r\n			<td>\r\n			<p>General Manager (P &amp; A)</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 68700-110400</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>3</p>\r\n			</td>\r\n			<td>\r\n			<p>Company Secretary</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 55350-101400</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>4</p>\r\n			</td>\r\n			<td>\r\n			<p>Marketing Manager</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 45800-89000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>5</p>\r\n			</td>\r\n			<td>\r\n			<p>Production Manager</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 45800-89000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>6</p>\r\n			</td>\r\n			<td>\r\n			<p>Manager (HR)No (1)</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>7</p>\r\n			</td>\r\n			<td>\r\n			<p>Manager (Purchase)</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>8</p>\r\n			</td>\r\n			<td>\r\n			<p>Manager (Sales)</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>9</p>\r\n			</td>\r\n			<td>\r\n			<p>Manager (Works )</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>10</p>\r\n			</td>\r\n			<td>\r\n			<p>Manager (Accounts)</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>11</p>\r\n			</td>\r\n			<td>\r\n			<p>Deputy Production Manager</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 40500-85000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>12</p>\r\n			</td>\r\n			<td>\r\n			<p>Assistant Production Manager</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 36600-79200</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>13</p>\r\n			</td>\r\n			<td>\r\n			<p>Assistant Manager</p>\r\n			</td>\r\n			<td>\r\n			<p>10</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 35700-75600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>14</p>\r\n			</td>\r\n			<td>\r\n			<p>Senior Assistant</p>\r\n			</td>\r\n			<td>\r\n			<p>10</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 26500-54000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>15</p>\r\n			</td>\r\n			<td>\r\n			<p>Confidential Assistant</p>\r\n			</td>\r\n			<td>\r\n			<p>-</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 22200-48000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>16</p>\r\n			</td>\r\n			<td>\r\n			<p>Micro Biologist</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 22200-48000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>17</p>\r\n			</td>\r\n			<td>\r\n			<p>Bio Chemist</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 22200-48000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>18</p>\r\n			</td>\r\n			<td>\r\n			<p>Technical Supervisor</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 22200-48000</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>19</p>\r\n			</td>\r\n			<td>\r\n			<p>Junior Assistant</p>\r\n			</td>\r\n			<td>\r\n			<p>9</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>20</p>\r\n			</td>\r\n			<td>\r\n			<p>Laboratory Assistant</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>21</p>\r\n			</td>\r\n			<td>\r\n			<p>Boiler Operator</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>22</p>\r\n			</td>\r\n			<td>\r\n			<p>Chief Security Guard</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>23</p>\r\n			</td>\r\n			<td>\r\n			<p>Foreman</p>\r\n			</td>\r\n			<td>\r\n			<p>15</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>24</p>\r\n			</td>\r\n			<td>\r\n			<p>Security Guard</p>\r\n			</td>\r\n			<td>\r\n			<p>6</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 19000-43600</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>25</p>\r\n			</td>\r\n			<td>\r\n			<p>LD Typist</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 18000-41500</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>26</p>\r\n			</td>\r\n			<td>\r\n			<p>Drivers</p>\r\n			</td>\r\n			<td>\r\n			<p>8</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 18000-41500</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>27</p>\r\n			</td>\r\n			<td>\r\n			<p>Electrician</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 17000-37500</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>28</p>\r\n			</td>\r\n			<td>\r\n			<p>Electrical Operator/Mechanical Operatort</p>\r\n			</td>\r\n			<td>\r\n			<p>1</p>\r\n			</td>\r\n			<td>\r\n			<p>Rs. 17000-37500</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:72.15pt\">\r\n			<p>21</p>\r\n			</td>\r\n			<td style=\"height:72.15pt\">\r\n			<p>Last Grade Employees</p>\r\n			</td>\r\n			<td style=\"height:72.15pt\">\r\n			<p>Grade I<br />\r\n			Grade II<br />\r\n			Grade III</p>\r\n			</td>\r\n			<td style=\"height:72.15pt\">\r\n			<p>Rs. 18000-41500<br />\r\n			Rs. 17000-37500</p>\r\n\r\n			<p>Rs. 16500-35700</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>22</p>\r\n			</td>\r\n			<td>\r\n			<p>General Workers</p>\r\n			</td>\r\n			<td>\r\n			<p>Category I<br />\r\n			Category II<br />\r\n			Category III</p>\r\n			</td>\r\n			<td>\r\n			<p>&nbsp;Rs. 8040 -12400<br />\r\n			&nbsp;Rs. 7960 -11650<br />\r\n			&nbsp;Rs. 7880 - 10950</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>11. The manner of execution of subsidy programmes, including the amounts allocated and the details of beneficiaries of such programmes.</p>\r\n\r\n<p>NIL</p>\r\n\r\n<p>12. Particulars of recipients of concessions, permits or authorisations granted by it.</p>\r\n\r\n<p>NIL</p>\r\n\r\n<p>13. Details in respect of the information, available to or held by it, reduced in an electronic form</p>\r\n\r\n<p>Presently not available</p>\r\n\r\n<p>14. The particulars of facilities available to citizens for obtaining information including the working hours of a library or reading room, if maintained for public use.</p>\r\n\r\n<p>They have to approach the organisation. No public reading room available</p>\r\n\r\n<p>15. The names, designations and other particulars of the Public Information officers</p>\r\n\r\n<ul>\r\n	<li>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1. Sri.K.V.Uthaman, Appellate Authority</li>\r\n	<li>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2. Smt. Vinitha.S, State Public Information Officer</li>\r\n	<li>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;3. Sri. M.P.Rajesh, Assistant Public Information Officer</li>\r\n</ul>', 'rti-act', 'RTI Act', 'RTI Act', 'RTI Act', 'RTI Act', '2018-05-17 00:00:00', '2019-08-21 06:09:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('prabeesh@netstager.com', '$2y$10$dbCM597oME.SEVYAK5fsaOcQLDYiUnq5yQbuFZblzKFHC9k1Le5ai', '2019-03-08 03:07:49'),
('anju384@gmail.com', '$2y$10$48Honx2LGqlIpQVdZjh/mu5bx/V4SpvmdymbVuciK0TiNwIM4FHqS', '2019-03-08 10:13:16'),
('ashnaanju@gmail.com', '$2y$10$yvAhaCwN/G/9BOweeSr4mOv4Z/8fGPN377sq2LSViLm09Pj/GU96S', '2019-08-05 08:34:39'),
('shashe721@gmail.com', '$2y$10$vg81JpAnu/9w8CNc.aKbN.Y/9IXIHT/9oaxQV4dyu2mXwTuAPrK5S', '2019-09-09 12:54:34'),
('vijayachandranpillai1954@gmail.com', '$2y$10$iTgzwtl1imwmkG19UZME8eIqXFEf9ppk.VgeQ8iVMXwvwouIZb9xK', '2019-09-14 15:05:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_map`
--

CREATE TABLE `permission_map` (
  `id` int(11) NOT NULL,
  `role_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_map`
--

INSERT INTO `permission_map` (`id`, `role_id`, `menu_id`, `permissions`) VALUES
(1, 2, 24, '1,2,3,4'),
(2, 3, 24, '2,3'),
(3, 3, 25, '2,3'),
(4, 3, 30, '2');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `model` varchar(255) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `show_home` tinyint(1) NOT NULL,
  `total_ingradients` varchar(500) DEFAULT NULL,
  `main_ingradients` varchar(500) DEFAULT NULL,
  `indication` varchar(500) DEFAULT NULL,
  `special_indication` varchar(255) DEFAULT NULL,
  `dosage` varchar(500) DEFAULT NULL,
  `presentation` varchar(255) DEFAULT NULL,
  `p_usage` varchar(500) DEFAULT NULL,
  `rating` int(11) DEFAULT '5',
  `tax` int(11) DEFAULT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text,
  `keywords` text,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(11) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(5, 'ABHAYARISHTAM', 'abhayarishtam', '', NULL, 1, 'a1', 0, 0, '11', 'Terminalia chebula, Emblica officinalis, Citrullus colocynthis, Symplocos cochinchinensis, Piper nigrum, Piper longum, Embelia ribes, Jaggery.', 'Piles, Constipation. Recommended in Anaemia, Fever, Cardiac disorders, Skin diseases, Worm infestation, Sprue, Pthiasis, Oedema.', NULL, '15-25 ml.b.d.,after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'ABHAYARISHTAM', NULL, 'ABHAYARISHTAM', 1, 47, '2019-03-02 07:27:32', '2019-08-09 14:58:09'),
(6, 'AHIPHENASAVAM', 'ahiphenasavam', '5d43d240dea601564725824.jpg', NULL, 1, 'a4a', 0, 0, '9', 'Madhuca indica, Honey, Elettaria cardamomum, Nelumbium speciosum, Santalum album, Opium, Cyperus rotundus, Myristica fragrans, Holarrhena antidysenterica.', 'Diarrhoea and Dysentery.Useful even Cholera.', NULL, '10 drops or as directed by the physician', '10 ml.', NULL, 5, 1, 'AHIPHENASAVAM', NULL, 'AHIPHENASAVAM', 1, 47, '2019-03-02 07:34:44', '2019-08-09 14:59:56'),
(7, 'Amrutharishtam', 'amrutharishtam', '', NULL, 1, 'a2', 0, 0, '23', 'Tinospora cordifolia, Dasamoolam, Jaggery, Apium graveolens, Oldenlandia corymbosa, Cyperus rotundus, Picrorrhiza curroa, Aconitum heterophyllum, Holarrhena antidysenterica.', 'Fever,Flu, Indigestion', NULL, '15 ml-25 ml b.d., after food as directed by the physician', '450 ml.', NULL, 5, 1, 'Amrutharishtam', NULL, 'Amrutharishtam', 1, 41, '2019-03-02 07:36:31', '2019-08-09 15:01:04'),
(8, 'ARAGWADHARISHTAM', 'aragwadharishtam', '5d43d3405f55c1564726080.jpg', NULL, 1, 'a6', 0, 0, '21', 'Cassia fistula, Azadirachta indica, Tinospora cordifolia, Moringa oleifera, Cyclea peltata, Ficus glomerata, Trichosanthes anguina, Jaggery.', 'Skin diseases. Recommended in Chronic wounds, Vitiligo, Scabies etc.', NULL, '15-25ml.b.d.,, afteer food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'ARAGWADHARISHTAM', NULL, 'ARAGWADHARISHTAM', 1, 38, '2019-03-02 07:40:24', '2019-08-09 15:03:11'),
(9, 'ARAVINDASAVAM', 'aravindasavam', '5d43d361a2a741564726113.jpg', NULL, 1, 'a31', 0, 0, '27', 'Nelumbium speciosum (flower), Vetiveria zizanioides, Rubia cordifolia, Elettaria cardamomum, Sida cordifolia, Cyperus rotundus, Hemidesmus indicus, Acorus calamus, Merrimia turpethum, Oldenlandia corymbosa, Glycyrrhiza glabra, Raisins, Sugar, Honey.', 'Skin diseases, Loss of appetite and Indigestion in children. Recommended as a general tonic to children for achieving health and strength to the body.', NULL, '5-20 ml.b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'ARAVINDASAVAM', NULL, 'ARAVINDASAVAM', 1, 40, '2019-03-02 07:41:43', '2019-08-09 15:04:21'),
(10, 'ASOKARISHTAM', 'asokarishtam', '5d43d4cb99d031564726475.jpg', NULL, 1, 'a3', 0, 0, '15', 'Saraca asoka, Jaggery, Apium graveolens, Coscinium fenestratum, Cuminum cyminum, Emblica officinalis, Adhatoda beddomei, Santalum album.', 'Menstrual disorders like Dysmenorrhoea, Menorrhagia etc., Leucorrhoea, Abdominal colic and Indigestion. Recommended as Uterine tonic.', 'In female sterility, it can be used with Aswagandharishtam.', '15-25 ml.b.d., after food or as directed by the Physician.', '450 ml.', NULL, 5, 1, 'ASOKARISHTAM', NULL, 'ASOKARISHTAM', 1, 31, '2019-03-02 07:43:02', '2019-08-09 15:05:41'),
(12, 'AMRUTHADI THAILAM (Valuthu)', 'amruthadi-thailam-valuthu', '5d43f5d1be8a81564734929.jpg', NULL, 7, 't96', 0, 0, '36', 'Tinospora cordifolia, Glycyrrhiza glabra, Boerrhavia diffusa, Alpinia calcarata, Ricinus communis, Holostemma ada-Kodien, Withania somnifera, Sida cordifolia, Ipomoea mauritiana, Aegle marmelos, Hordeum vulgare, Horse gram, Gingelly oil, Santalum album,Cow milk.', 'Rrheumatic disorders, Emaciation, Fatigue, Fracture, Mental disorders. More potent than Amruthadi thailam (cheruthu)', NULL, NULL, '200 ml., 450 ml.', 'For external application only.', 5, 1, 'AMRUTHADI THAILAM (Valuthu)', NULL, 'AMRUTHADI THAILAM (Valuthu)', 1, 27, '2019-03-02 07:47:21', '2019-08-13 16:07:30'),
(13, 'AMRUTHADI THAILAM (Cheruthu)', 'amruthadi-thailam-cheruthu', '', NULL, 7, 't86', 0, 0, '6', 'Tinospora cordifolia, Santalum album, Hemidesmus, indus, Cyperus rotundus, Emblica officinalis, Gingery oil.', 'Rheumatic disorders, Mental diseases, Epistax', NULL, NULL, '200 ml., 450 ml.', 'For external application only.', 5, 1, 'AMRUTHADI THAILAM (Cheruthu)', NULL, 'AMRUTHADI THAILAM (Cheruthu)', 1, 28, '2019-03-02 07:48:46', '2019-08-13 16:03:42'),
(18, 'AYASKRITHI', 'ayaskrithi', '5d43d51a2d2791564726554.jpg', NULL, 1, 'A33', 0, 0, '49', 'Pterocarpus marsupium, Acacia catechu, Santalum album, Pongamia pinnata, Panchakolam, Iron sheets, Honey.', 'Diabetes mellitus, Anaemia, Vitiligo, Haemorrhoids, Skin diseases.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'AYASKRITHI', NULL, 'AYASKRITHI', 1, 21, '2019-03-08 15:20:11', '2019-08-09 15:07:22'),
(19, 'ASWAGANDHARISHTAM', 'aswagandharishtam', '5d43d4ed40bd01564726509.jpg', NULL, 1, 'A5', 0, 0, '27', 'Withania somnifera, Curculigo orchioides, Rubia cordifolia, Glycyrrhiza glabra, Ipomoea mauritiana, Santalum album, Acorus calamus, Hemidesmus indicus, Curcuma longa, Alpinia calcarata, Elettaria cardamomum, Honey.', 'Mental disorders like Insanity, Epilepsy etc., Recommended in Rheumatic complaints, Cardiac complaints. Renders physical strength and vigour.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'ASWAGANDHARISHTAM', NULL, 'ASWAGANDHARISHTAM', 1, 26, '2019-03-08 15:26:55', '2019-08-09 15:06:37'),
(20, 'BALARISHTAM', 'balarishtam', '5d43d73f4deaf1564727103.jpg', NULL, 1, 'A21', 0, 0, '12', 'Sida cordifolia, Withania somnifera, Jaggery, Holostemma ada-kodien, Ricinus communis. Alpinia calcarata, Elettaria cardamomum, Merrimia tridentata, Eugenia caryophyllate, Vetiveria zizanioides, Tribulus terrestris, Woodfordia fruiticosa.', 'Rheumatic and Nervous disorders. Recommended as a general tonic to all age grooups. Renders appetite, physical strength and health. Effective in convalescence period.', NULL, '15-25 ml, b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'BALARISHTAM', NULL, 'BALARISHTAM', 1, 25, '2019-03-08 15:37:38', '2019-08-09 15:08:07'),
(21, 'CHANDANASAVAM', 'chandanasavam', '5d43d7710d0891564727153.jpg', NULL, 1, 'A12', 0, 0, '26', 'Santalum album, Cyperus rotundus, Gmelina arborea, Caesalpinia sappan, Symplocos cochinchinensis, Pterocarpus santalinus, Kaempferia galanga, Trichosanthes anguina, Raisins, Sugar, jaggery.', 'Gonorrhoea, Urinary diseases, Urinary calculi, Haematuria, Leucorrhoea, Anorexia and burning sensation of body. Renders appetite.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'CHANDANASAVAM', NULL, 'CHANDANASAVAM', 1, 14, '2019-03-08 15:39:50', '2019-08-09 15:08:54'),
(22, 'CHAVIKASAVAM', 'chavikasavam', '5d43d8366aacd1564727350.jpg', NULL, 1, 'A37', 0, 0, '34', 'Piper brachystachym, Plumbago rosea, Acorus calamus, Kaempferia galanga, Thriphala, Coriandrum sativum, Baliospermum montanum, Embelia ribes, Thrikatu, Jaggery.', 'Pthiasis, Ascites, Nasal catarrh, Anorexia, Rhinitis, Chronic sinusitis, Cough and other disorders related upper and lower respiratory tract, Dyspepsia.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'CHAVIKASAVAM', NULL, 'CHAVIKASAVAM', 1, 16, '2019-03-08 15:42:10', '2019-08-09 15:10:14'),
(23, 'DANTHYARISHTAM', 'danthyarishtam', '5d43d881a0cb01564727425.jpg', NULL, 1, 'A15', 0, 0, '17', 'Baliospermum montanum, Dasamoolam, Thriphala, Plumbago rosea, Jaggery, Woodfordia fruiticosa.', 'Bleeding piles, Constipation, Loss of appetite. Recommended in Sprue, Worm \r\ninfestation, Oedema.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'DANTHYARISHTAM', NULL, 'DANTHYARISHTAM', 1, 8, '2019-03-08 15:49:20', '2019-08-09 15:11:21'),
(24, 'DASAMOOLARISHTAM', 'dasamoolarishtam', '5d2ec1682fcb21563345256.jpg', NULL, 1, 'A14', 0, 0, '70', 'Dasamoolam, Tinospora cordifolia, Emblica officinalis, Clerodendrum serratum, Alpinia calcarata, Curcuma longa, Piper longum, Cyperus rotundus, Ipomoea mauritiana, Withania somnifera, Raisins, Jaggery, Honey, Santalum album.', 'Rheumatic disorders. Recommended in Cough, Bronchitis, Dyspnoea, Gastric disorders, Vomiting, Dysuria and Anaemia. It is a general tonic and can be used in Post-natal care. Renders physical strength, vigour and vitality.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'DASAMOOLARISHTAM', NULL, 'DASAMOOLARISHTAM', 1, 21, '2019-03-08 15:51:26', '2019-08-09 15:12:32'),
(25, 'DHANWANTHARARISHTAM', 'dhanwanthararishtam', '5d43d914e58571564727572.jpg', NULL, 1, 'A35', 0, 0, '70', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, Asparagus racemosus, Withania somnifera, Hemidesmus indicus, Ipomoea  mauritiana, Acorus calamus, Boerrhavia diffusa, Peucedanum graveolens, Jaggery, Piper longum, Piper nigrum, Santalum album, Nardostachys jatamansi', 'Specially indicated for Post-natal care and Post-natal disorders. Recommended in rheumatic complaints like Hemiplegia, Spondylitis, Sprain and General fatigue.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'DHANWANTHARARISHTAM', NULL, 'DHANWANTHARARISHTAM', 1, 15, '2019-03-08 15:53:52', '2019-08-09 15:13:50'),
(26, 'DRAKSHARISHTAM', 'draksharishtam', '5d43db050bd881564728069.jpg', NULL, 1, 'A17', 0, 0, '10', 'Raisins, Jaggery, Chathurjathakam, Aglaia roxburghiana, Piper longum, Piper nigrum, Embelia ribes.', 'General weakness, Loss of appetite, Dyspnoea, Cough, Bronchitis, Anaemia, Chronic throat diseases. Recommended in Habitual constipation.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'DRAKSHARISHTAM', NULL, 'DRAKSHARISHTAM', 1, 19, '2019-03-08 15:57:04', '2019-08-09 15:15:01'),
(27, 'DURALABHARISHTAM', 'duralabharishtam', '5c94c178a4cbd1553252728.jpg', NULL, 1, 'A16', 0, 0, '12', 'Tragia involucrata, Baliospermum montanum, Cyclea peltata, Plumbago rosea, Terminalia chebula, Emblica officinalis, Dried ginger, Adhatoda beddomei, Sugar, Aglaia roxburghiana, Piper spp., Woodfordia fruiticosa.', 'Haemorrhoids, Anorexia and Constipation.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'DURALABHARISHTAM', NULL, 'DURALABHARISHTAM', 1, 13, '2019-03-08 15:59:01', '2019-08-09 15:16:43'),
(28, 'JEERAKARISHTAM', 'jeerakarishtam', '5c94ac0e1b90f1553247246.jpg', NULL, 1, 'A13', 0, 0, '13', 'Cuminum cyminum, Jaggery, Woodfordia fruiticosa, Dried ginger, Myristica fragrans, Cyperus rotundus, Chathurjathakam, lllicium verum, Eugenia caryophyllata.', 'Post-natal disorders. Indicated in Loss of appetite, Flatulence, Indigestion, Hiccup, Bronchitis Dyspnoea, Sprue.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'JEERAKARISHTAM', NULL, 'JEERAKARISHTAM', 1, 15, '2019-03-08 16:01:33', '2019-08-09 15:17:33'),
(29, 'KANAKASAVAM', 'kanakasavam', '5d43dc4c41c201564728396.jpg', NULL, 1, 'A7', 0, 0, '13', 'Datura stramonium, Adhatoda beddomei, Madhuca indica, Piper longum, Solanum xanthocarpum, Mesua ferrea, Dried ginger, Clerodendrum serratum, Abies spectabilis, Woodfordia fruiticosa, Raisins, Sugar, Honey.', 'A reputed bronchodilator. Recommended in Bronchial asthma, Cough, Hiccup, Dyspnoea etc.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'KANAKASAVAM', NULL, 'KANAKASAVAM', 1, 11, '2019-03-08 16:06:17', '2019-08-09 15:19:43'),
(30, 'KARPOORASAVAM', 'karpoorasavam', '5d43dd63026301564728675.jpg', NULL, 1, 'A8A', 0, 0, '7', 'Rectified spirit, Elettaria cardamomum, Cyperus rotundus, Dried ginger, Cuminum cyminum, Piper nigrum, Camphor.', 'Abdominal colic, Dysentery, Diarrhoea, Indigestion.', NULL, '5-10 drops with luke warm Cumin seed water.', '10 ml.', NULL, 5, 1, 'KARPOORASAVAM', NULL, 'KARPOORASAVAM', 1, 22, '2019-03-08 16:08:26', '2019-08-09 15:20:24'),
(31, 'KHADIRARISHTAM', 'khadirarishtam', '5d43ddc88e7771564728776.jpg', NULL, 1, 'A11', 0, 0, '18', 'Acacia catechu, Psoralia corylifolia, Coscinium fenestratum, Thriphala, Honey, Sugar, Woodfordia fruiticosa, lllicium verum, Myristica fragrans, Piper longum, Thrijathakam, Eugenia caryophyllata, Mesua ferrea.', 'Skin diseases. Recommended as an excellent blood purifier. Also indicated in Anaemia, Obesity, Worm infestation; cough, spleenic disorders.', NULL, '15 - 25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'KHADIRARISHTAM', NULL, 'KHADIRARISHTAM', 1, 14, '2019-03-08 16:37:42', '2019-08-09 15:21:14'),
(33, 'KUMARYASAVAM', 'kumaryasavam', '5d43dde360bcd1564728803.jpg', NULL, 1, 'A10', 0, 0, '45', 'Aloe vera, Jaggery, Honey, Iron, Embelia ribes, Thrikatu, Picrorrhiza curroa, Coriandrum sativum, Curcuma longa, Coscinium fenestratum, Sida cordifolia, Glycyrrhiza glabra, Mucuna pruriens, Boerrhavia diffusa, Woodfordia fruiticosa, Ferri sulphuretum (processed)', 'Dysmenorrhoea, Oligomenorrhoea, Anorexia, Urinary tract infection and Abdominal colic. Used as an Uterine tonic, increases strength & complexion.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'KUMARYASAVAM', NULL, 'KUMARYASAVAM', 1, 13, '2019-03-08 16:41:14', '2019-08-09 15:22:21'),
(34, 'LODHRASAVAM', 'lodhrasavam', '5d43deff2496d1564729087.jpg', NULL, 1, 'A26', 0, 0, '32', 'Symplocos  cochinchinensis,  Chonemorpha  macrophylla,  Kaempferia \r\ngalanga,  Embelia  ribes,  Clerodendrum  serratum,  Aconitum  heterophyllum, \r\nTrichosanthes  anguina,  Solanum  indicum,  Cuminum  cyminum,  Thriphala, \r\nHoney.', 'Diabetes, Anaemia, Worm infestation. Recommended to reduce excess fat in \r\nthe body. Vitiligo, skin diseases.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'LODHRASAVAM', NULL, 'LODHRASAVAM', 1, 12, '2019-03-08 16:43:24', '2019-08-09 12:46:58'),
(35, 'LOHASAVAM', 'lohasavam', '', NULL, 1, 'A25', 0, 0, '14', 'Iron (processed), Thrikatu, Thriphala, Cuminum cyminum, Embelia ribes, Cyperus rotundus, Plumbago rosea, Woodfordia fruiticosa, Honey, Jaggery.', 'Anaemia. Recommended in Skin disorders, Pthiasis, Bleeding piles, Sprue, Cardiac disorders and Obesity.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'LOHASAVAM', NULL, 'LOHASAVAM', 1, 12, '2019-03-08 16:44:57', '2019-08-09 15:24:07'),
(36, 'MADHOOKASAVAM', 'madhookasavam', '5d43df77aa9761564729207.jpg', NULL, 1, 'A22', 0, 0, '9', 'Madhuca indica, Embelia ribes, Plumbago rosea, Semicarpus anacardium, Rubia cordifolia, Honey, Elettaria cardamomum, Vetiveria zizanioides, Santalum album.', 'Sprue. Recommended in Fatigue, Emaciation, Skin disorders and Dropsy, Diabetes.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml', NULL, 5, 1, 'MADHOOKASAVAM', NULL, 'MADHOOKASAVAM', 1, 6, '2019-03-08 16:46:49', '2019-08-09 15:25:07'),
(37, 'MRUDWEEKARISHTAM', 'mrudweekarishtam', '5d43e15b5c8ae1564729691.jpg', NULL, 1, 'A24', 0, 0, '17', 'Raisins, Sugar candy, Honey, Woodfordia fruiticosa, lllicium verum, Piper nigrum, Elettaria cardamomum, Piper longum, Plumbago rosea, Piper aurantiacum, Santalum album, Camphor.', 'Piles, Pthiasis, Diseases related to ear, nose and throat. Recommended in General fatigue, Skin problems.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'MRUDWEEKARISHTAM', NULL, 'MRUDWEEKARISHTAM', 1, 11, '2019-03-08 16:48:50', '2019-08-09 15:25:51'),
(38, 'MUSTHARISHTAM', 'mustharishtam', '5d43e1a61bce51564729766.jpg', NULL, 1, 'A23', 0, 0, '10', 'Cyperus rotundus, Jaggery, Woodfordia fruiticosa, Carum carvi, Dried ginger, Piper nigrum, Eugenia caryophyllata, Trigonella foenum-graecum, Plumbago rosea, Cuminum cyminum.', 'Intestinal colic, Indigestion, Loss of appetite, Sprue, Diarrhoea and Worm infestation.\r\nEffectively used with Aravindasavam in children', NULL, '15 - 25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'MUSTHARISHTAM', NULL, 'MUSTHARISHTAM', 1, 10, '2019-03-08 16:51:16', '2019-08-09 15:28:39'),
(39, 'PARTHARISHTAM', 'partharishtam', '', NULL, 1, 'A18', 0, 0, '5', 'Terminalia arjuna, Raisins, Madhuca indica (flower), Jaggery, Woodfordia fruiticosa.', 'Cardiac complaints. Recommended to retain physical strength. Can be given in Hypertension and associated symptoms.', NULL, '15 - 25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'PARTHARISHTAM', NULL, 'PARTHARISHTAM', 1, 14, '2019-03-08 16:52:57', '2019-08-09 15:29:39'),
(40, 'PIPPALYASAVAM', 'pippalyasavam', '5d43e3d9224d31564730329.jpg', NULL, 1, 'A19', 0, 0, '26', 'Piper longum, Piper nigrum, Curcuma longa, Plumbago rosea, Embelia ribes, Cyclea peltata.Emblica officinalis, Santalum album, Nardostachys jatamansi, Elettaria cardamomum, Raisins, Jaggery, Woodfordia fruiticosa.', 'Anorexia, General fatigue, Pthiasis, Ascites, Sprue, Piles, Loss of appetite, Flatulence and Splenic disorders.', NULL, '15 - 25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'PIPPALYASAVAM', NULL, 'PIPPALYASAVAM', 1, 10, '2019-03-08 16:55:00', '2019-08-09 15:30:30'),
(41, 'POOTHIKASAVAM', 'poothikasavam', '5d43e3fac90d51564730362.jpg', NULL, 1, 'A39', 0, 0, '5', 'Holoptelia integrifolia, Thrikatu, Jaggery.', 'Haemorrhoids, Splenic diseases, Ascites, Constipation. Improves digestion.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'POOTHIKASAVAM', NULL, 'POOTHIKASAVAM', 1, 11, '2019-03-08 16:56:25', '2019-08-09 15:31:07'),
(42, 'PUNARNAVASAVAM', 'punarnavasavam', '5d43e44d3a0221564730445.jpg', NULL, 1, 'A20', 0, 0, '24', 'Boerrhavia diffusa, Azadirachta indica, Tinospora cordifolia, Raisins, Adhatoda beddomei, Ricinus communis, Solanum melongena, var. insanum, Tribulus  terrestris, Thrikatu, Sugar, Honey.', 'Dropsy, Anaemia, Liver disorders, Fever, Splenic disorders. Ascities, Swelling.', NULL, '15 - 25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'PUNARNAVASAVAM', NULL, 'PUNARNAVASAVAM', 1, 8, '2019-03-08 16:58:11', '2019-08-09 15:31:53'),
(43, 'ROHITHAKARISHTAM', 'rohithakarishtam', '5d43e4d64b7df1564730582.jpg', NULL, 1, 'A41', 0, 0, '14', 'Aphanamixis polystachya, Piper longum, Piper retrofractum, Plumbago Zeylanica, Zingiber officinalis, Cinnamomum zeylanicum, Eletteria Cardamomum, Cinnamomum tamala, Terminalia chebula, Terminalia Bellarica, Emblica officinalis, Woodfordia fruticosa', 'Indigestion, Ascitis, Problems related spleen', NULL, '12-24 ml or as directed by a physician', '450 ml', NULL, 5, 1, 'ROHITHAKARISHTAM', NULL, 'ROHITHAKARISHTAM', 1, 13, '2019-03-08 17:00:18', '2019-08-09 15:32:41'),
(44, 'SARASWATHARISHTAM', 'saraswatharishtam', '5d43e5c26f76c1564730818.jpg', NULL, 1, 'A28A', 0, 0, '19', 'Bacopa monnieri, Ipomoea mauritiana, Asparagus racemosus, Honey, Sugar,  Merrimia turpethum, Acorus calamus, Withania somnifera, Tinospora cordifolia, Embelia ribes, Gold (24 carat-processed).', 'Increases memory power, restores mental harmony. Recommended in Mental disorders, Stammering and Fear, good for children above three years.', NULL, '2 - 4 drops preferably with milk at bed time', '10 ml', NULL, 5, 1, 'SARASWATHARISHTAM', NULL, 'SARASWATHARISHTAM', 1, 41, '2019-03-08 17:02:17', '2019-08-09 15:34:24'),
(45, 'SARIBADYASAVAM', 'saribadyasavam', '5d43e5e39f9301564730851.jpg', NULL, 1, 'A32', 0, 0, '26', 'Hemidesmus indicus, Symplocos cochinchinensis, Kaempferia galanga, Caesalpinia sappan, Tinospora cordifolia, Vetiveria zizanioides, Santalum album, Pterocarpus santalinus, Picrorrhiza curroa, Amomum subulatum, Elettaria cardamomum, Holostemma ada-kodien, Jaggery, Raisins.', 'Arthritic complaints, Uriwo-genital disorders, Leucorrhoea, Painful and burning micturition, Diabetes.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'SARIBADYASAVAM', NULL, 'SARIBADYASAVAM', 1, 14, '2019-03-08 17:11:53', '2019-08-09 15:35:15'),
(46, 'USEERASAVAM', 'useerasavam', '5d43f37ae7f901564734330.jpg', NULL, 1, 'A36', 0, 0, '25', 'Vetiveria zizanioides, Coleus zeylanicus, Aglaia roxburghiana, Caesalpinia sappan, Rubia cordifoiia, Andrographis paniculata, Oldenlandia corymbosa, Raisins, Sugar, Honey.', 'Epistaxis, Recommended in Anaemia, Skin disorders, Worm infestation, Swelling etc. Diabetes.', NULL, '15-25 ml. b.d. after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'USEERASAVAM', NULL, 'USEERASAVAM', 1, 8, '2019-03-08 17:13:21', '2019-08-09 15:36:05'),
(47, 'VASARISHTAM', 'vasarishtam', '5d43f3ce8d5821564734414.jpg', NULL, 1, 'A29', 0, 0, '12', 'Adhatoda beddomei, Thrikatu, Chathurjathakam, Piper cubeba, Coleus zeylanicus, Jaggery, Woodfordia fruiticosa.', 'Productive cough, Bronchitis, Laryngitis. Recommended in Epistaxis.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'VASARISHTAM', NULL, 'VASARISHTAM', 1, 17, '2019-03-08 17:14:48', '2019-08-09 15:36:57'),
(48, 'VEPPUKADI', 'veppukadi', '5d43f444c92e61564734532.jpg', NULL, 21, 'A27', 0, 0, '9', 'Oryza sativa, Flattened rice, Horse gram, Fried paddy, Panicum italicum, Common millet, Dried ginger, Lemon, Apium graveolens.', 'Rheumatic Complaints.', NULL, 'For sitz bath, “Dhara” in rheumatic pains. Recommended as an additive with “Kizhi” in antirheumatic fomentation.', '450 ml., 5 ltr.', NULL, 5, 1, 'VEPPUKADI', NULL, 'VEPPUKADI', 1, 34, '2019-03-08 17:17:05', '2019-08-09 15:41:33'),
(49, 'VIDARYADYASAVAM', 'vidaryadyasavam', '5d43f46b15be31564734571.jpg', NULL, 1, 'A38', 0, 0, '20', 'Ipomoea mauritiana, Ricinus communis, Boerrhavia diffusa, Cedrus deodara, Teramnus labialis, Phaseolus trilobus, Mucuna pruriens, Hemidesmus indicus, Holostemma ada-kodien, Desmodium gangeticum, Pseudarthria viscida, Jaggery.', 'Fatigue, Emaciation, General weakness, Cough, Respiratory disorders, Fever and Cardiac complaints.', NULL, '15-25 ml. b.d., after food or as directed by the physician.', '450 ml.', NULL, 5, 1, 'VIDARYADYASAVAM', NULL, 'VIDARYADYASAVAM', 1, 13, '2019-03-08 17:19:23', '2019-08-09 15:38:00'),
(50, 'ANUTHAILAM', 'anuthailam', '5c94b91bd3aab1553250587.jpg', NULL, 7, 'T1A', 0, 0, '28', 'Holostemma ada-Kodien, Cedrus deodara, Cyperus rotundus, Santalum album, \r\nCoscinium  fenestratum, Asparagus  racemosus,  Glycyrrhiza  glabra,  Embelia \r\nribes, Desmodium gangeticum, Sida cordifolia, Gingelly oil, Goat milk.', 'Diseases related with nose, throat, head. Recommended in Headache,\r\nAnosmia, Chronic sinusitis, Nasal polyps etc.', NULL, NULL, '10 ml., 50 ml.', 'For nasal application only. 4-10 drops in nostrils or as directed by the\r\nphysician.', 5, 1, 'ANUTHAILAM', NULL, 'ANUTHAILAM', 1, 37, '2019-03-08 17:24:09', '2019-08-13 16:08:39'),
(51, 'ARIMEDADI THAILAM', 'arimedadi-thailam', '', NULL, 7, 'T4', 0, 0, '40', 'Acacia ferruginea, Acacia catechu, Red ochre, Santalum album, Glycyrrhiza \r\nglabra, Coscinium fenestratum, Curcuma longa, Cyperus rotundus, Thriphala, \r\nRubia cordifolia, Myristica fragrans, Eugenia caryophyllata, Camphor, Gingelly \r\noil.', 'Pyorrhoea, Stomatitis, Gingivitis and Dental diseases.', NULL, NULL, '200 ml., 450 ml.', 'Apply inside the mouth or use for gargling. Apply over head before bath in\r\ndental diseases or use as directed by the physician.', 5, 1, 'ARIMEDADI THAILAM', NULL, 'ARIMEDADI THAILAM', 1, 20, '2019-03-08 17:26:16', '2019-08-13 16:09:19'),
(52, 'ARUKALADI THAILAM', 'arukaladi-thailam', '5d43d3a73b0461564726183.jpg', NULL, 7, 'T6', 0, 0, '7', 'Eclipta alba, Tinospora cordifolia, Elephantopus scaber, Cynodon dactylon, \r\nCardiospermum halicacabum, Musa sapientum (rhizome), Gingelly oil.', 'Anaemia. Effective in prophylactic period of Jaundice', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'ARUKALADI THAILAM', NULL, 'ARUKALADI THAILAM', 1, 26, '2019-03-08 17:28:13', '2019-08-13 16:11:07'),
(55, 'ASANAMANJISHTADI THAILAM', 'asanamanjishtadi-thailam', '5d43d4a2250da1564726434.jpg', NULL, 7, 'T3', 0, 0, '35', 'Pterocarpus marsupium, Aegle marmelos, Sida cordifolia, Tinospora cordifolia, \r\nRubia cordifolia, Hemidesmus indicus, Thriphala, Chathurjathakam, Vetiveria \r\nzizanioides, Gingelly oil, Aloe vera, Cow milk, Camphor.', 'Eye diseases, Headache.\r\nRecommended in diseases related to head, Otorrhoea, Otitis media.', NULL, NULL, '200 ml., 450 ml', 'For external application over head.\r\nCan be dropped in ear or as directed by the physician.', 5, 1, 'ASANAMANJISHTADI THAILAM', NULL, 'ASANAMANJISHTADI THAILAM', 1, 22, '2019-03-08 17:45:04', '2019-08-13 16:12:39'),
(56, 'ASANAVILWADI VELICHENNA', 'asanavilwadi-velichenna', '', NULL, 7, 'T92', 0, 0, '11', 'Pterocarpus marsupium, Aegle marmelos, Sida cordifolia, Tinospora cordifolia, \r\nGlycyrrhiza glabra, Thriphala, Dried ginger, Coconut oil, Cow milk.', 'Headache, Otorrhoea, Burning sensation in eye, Rhinitis.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'ASANAVILWADI VELICHENNA', NULL, 'ASANAVILWADI VELICHENNA', 1, 19, '2019-03-08 17:49:17', '2019-08-13 16:14:49'),
(57, 'ASANAVILWADI THAILAM', 'asanavilwadi-thailam', '', NULL, 7, 'T2', 0, 0, '11', 'Pterocarpus marsupium, Aegle marmelos, Sida cordifolia, Tinospora cordifolia, \r\nGlycyrrhiza glabra, Thriphala. Dried ginger, Gingelly oil, Cow milk.', 'Headache, Otorrhoea, Eye diseases, Rhinitis.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'ASANAVILWADI THAILAM', NULL, 'ASANAVILWADI THAILAM', 1, 21, '2019-03-08 17:51:31', '2019-08-13 16:14:08'),
(58, 'BALADHATHRYADI THAILAM', 'baladhathryadi-thailam', '', NULL, 7, 'T58', 0, 0, '58', 'Sida cordifolia, Emblica officinalis, Tinospora cordifolia, Vetiveria zizanioides, \r\nSantalum album, Thrijathakam, Asparagus racemosus, Ipomoea mauritiana, \r\nRaisins,  Saffron,  Kaempferia  galanga,  Hemidesmus  indicus,  Eugenia \r\ncaryophyllata, Cedrus deodara, Thriphala, Gingelly oil, Cow milk.', 'Burning sensation over head, Insanity, Headache, Diseases of nervous\r\nsystem.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head', 5, 1, 'BALADHATHRYADI THAILAM', NULL, 'BALADHATHRYADI THAILAM', 1, 15, '2019-03-08 17:55:38', '2019-08-13 16:15:14'),
(59, 'BALAGULUCHYADI THAILAM', 'balaguluchyadi-thailam', '5d43d534844691564726580.jpg', NULL, 7, 'T37', 0, 0, '12', 'Sida cordifolia, Tinospora cordifolia, Cedrus deodara, Santalum album,\r\nWithania somnifera, Alpinia calcarata, Gingelly oil.', 'Burning sensation, Pain, Swelling etc. related to Arthritis. Recommended in \r\nRheumatoid arthritis.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head and affected areas', 5, 1, 'BALAGULUCHYADI THAILAM', NULL, 'BALAGULUCHYADI THAILAM', 1, 12, '2019-03-11 09:45:04', '2019-08-13 16:33:16'),
(60, 'BALAGULUCHYADI VELICHENNA', 'balaguluchyadi-velichenna', '5d43d54de281b1564726605.jpg', NULL, 7, 'T97', 0, 0, '12', 'Sida cordifolia, Tinospora cordifolia, Cedrus deodara, Santalum album,\r\nWithania somnifera, Alpinia calcarata, Coconut oil.', 'Burning sensation, Pain, Swelling etc. related to Arthritis. Recommended in \r\nRheumatoid arthritis.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'BALAGULUCHYADI VELICHENNA', NULL, 'BALAGULUCHYADI VELICHENNA', 1, 11, '2019-03-11 09:49:24', '2019-08-13 16:33:54'),
(61, 'BALAHATADI THAILAM', 'balahatadi-thailam', '5d43d7177da541564727063.jpg', NULL, 7, 'TC4', 0, 0, '9', 'Sida cordifolia, Emblica officinalis, Tinospora cordifolia, Phaseolus radiatus, \r\nPhaseolus mungo, Gingelly oil, Santalum album, Saussurea lappa, Glycyrrhiza \r\nglabra.', 'Headache', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'BALAHATADI THAILAM', NULL, 'BALAHATADI THAILAM', 1, 10, '2019-03-11 09:53:54', '2019-08-13 16:34:22'),
(62, 'BALASWAGANDHADI THAILAM', 'balaswagandhadi-thailam', '', NULL, 7, 'T36', 0, 0, '22', 'Sida cordifolia, Withania somnifera, Coccus lacca, Alpinia calcarata,\r\nGlycyrrhiza glabra, Curcuma longa, Peucedanum graveolens, Gingelly oil,\r\nCow milk, Butter milk.', 'Rheumatic complaints, Fatigue, Physical weakness, Mental diseases,\r\nEmaciation. Recommended in Post-natal care.', NULL, NULL, '200 ml., 450 ml.', 'For external application all over body.', 5, 1, 'BALASWAGANDHADI THAILAM', NULL, 'BALASWAGANDHADI THAILAM', 1, 13, '2019-03-11 10:02:32', '2019-08-13 16:40:48'),
(64, 'BHRINGAMALAKADI THAILAM', 'bhringamalakadi-thailam', '', NULL, 7, 'T39', 0, 0, '5', 'Eclipta alba, Emblica officinalis, Glycyrrhiza glabra, Cow milk, Gingelly oil.', 'Enhances growth of hair, gives cooling effect to head and eyes. Recommended \r\nin Eye diseases, Headache, Otitis media.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'BHRINGAMALAKADI THAILAM', NULL, 'BHRINGAMALAKADI THAILAM', 1, 9, '2019-03-11 10:09:32', '2019-08-13 16:42:14'),
(65, 'CHANDANADI THAILAM', 'chandanadi-thailam', '5d43d80f079f01564727311.jpg', NULL, 7, 'T28', 0, 0, '29', 'Santalum album, Glycyrrhiza glabra, Withania somnifera, Vetiveria zizanioides, \r\nCurcuma  longa,  Sida  cordifolia,  Elettaria  cardamomum,  Clerodendrum \r\nserratum, Cow milk, Gingelly oil.', 'Mental disorders. Renders cooling and conditioning to head.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'CHANDANADI THAILAM', NULL, 'CHANDANADI THAILAM', 1, 12, '2019-03-11 10:15:20', '2019-08-13 16:45:16'),
(66, 'CHEMPARATHYADI VELICHENNA', 'chemparathyadi-velichenna', '5d43d8543769d1564727380.jpg', NULL, 7, 'T19', 0, 0, '11', 'Leaves of Hibiscus rosa-sinensis, Aegle marmelos, Piper betel,\r\nOcimum sanctum, Indigofera tinctoria, Ixora coccinea, Carum carvi,\r\nCoconut oil.', 'Skin disorders like Scabies, Erysepelas, Itching, etc. Emaciation seen in\r\ninfants and children.', NULL, NULL, '200 ml., 450 ml.', 'For external application over body and head.', 5, 1, 'CHEMPARATHYADI VELICHENNA', NULL, 'CHEMPARATHYADI VELICHENNA', 1, 12, '2019-03-11 10:17:41', '2019-08-13 16:49:49'),
(67, 'CHINCHADI THAILAM', 'chinchadi-thailam', '', NULL, 7, 'T18', 0, 0, '10', 'Tamarindus indica (leaves), Gingelly oil, Moringa oleifera, Panchalavanam, \r\nYellow resin, Ocimum malabaricum.', 'Rheumatic diseases.', NULL, NULL, '200 ml., 450 ml.', 'For external application only. Wet fomentation is suggested for better results.', 5, 1, 'CHINCHADI THAILAM', NULL, 'CHINCHADI THAILAM', 1, 15, '2019-03-11 10:19:48', '2019-08-13 16:51:17'),
(68, 'DHANWANTHARAM THAILAM 101 AVARTHI', 'dhanwantharam-thailam-101-avarthi', '', NULL, 7, 'T72A', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', 'Can be used internally as additive in Kashayam or milk or suitable vehicles\r\nand externally for ‘pichu’, ‘dhara’, ‘nasya’, ‘vasthi’ or as directed by the\r\nphysician.', '5-12 drops.', '10 g.', 'For external application all over body', 5, 1, 'DHANWANTHARAM THAILAM 101 AVARTHI', NULL, 'DHANWANTHARAM THAILAM 101 AVARTHI', 1, 7, '2019-03-11 10:22:37', '2019-08-13 17:09:08'),
(69, 'DHANWANTHARAM THAILAM 14 AVARTHI', 'dhanwantharam-thailam-14-avarthi', '', NULL, 7, 'T69A', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', NULL, NULL, NULL, 'For external application all over body', 5, 1, 'DHANWANTHARAM THAILAM 14 AVARTHI', NULL, 'DHANWANTHARAM THAILAM 14 AVARTHI', 1, 5, '2019-03-11 10:44:32', '2019-08-13 15:18:57'),
(70, 'DHANWANTHARAM THAILAM 21 AVARTHI', 'dhanwantharam-thailam-21-avarthi', '', NULL, 7, 'T70A', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', NULL, NULL, NULL, 'For external application all over body', 5, 1, 'DHANWANTHARAM THAILAM 21 AVARTHI', NULL, 'DHANWANTHARAM THAILAM 21 AVARTHI', 1, 6, '2019-03-11 10:51:56', '2019-08-13 15:19:12'),
(72, 'DHANWANTHARAM THAILAM 41 AVARTHI', 'dhanwantharam-thailam-41-avarthi', '', NULL, 7, 'T71A', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', NULL, NULL, NULL, 'For external application all over body', 5, 1, 'DHANWANTHARAM THAILAM 41 AVARTHI', NULL, 'DHANWANTHARAM THAILAM 41 AVARTHI', 1, 7, '2019-03-11 10:58:49', '2019-08-13 15:19:29'),
(73, 'DHANWANTHARAM THAILAM 7 AVARTHI', 'dhanwantharam-thailam-7-avarthi', '', NULL, 7, 'T68A', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', NULL, NULL, NULL, 'For external application all over body', 5, 1, 'DHANWANTHARAM THAILAM 7 AVARTHI', NULL, 'DHANWANTHARAM THAILAM 7 AVARTHI', 1, 8, '2019-03-11 11:01:12', '2019-08-13 15:18:41'),
(74, 'DHANWANTHARAM KUZHAMBU', 'dhanwantharam-kuzhambu', '', NULL, 7, 'T24', 0, 0, '50', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, \r\nAsparagus  racemosus,  Rubia  cordifolia,  Withania  somnifera,  Hemidesmus \r\nindicus,  Santalum  album,  Ipomoea  mauritiana, Acorus  calamus,  Rock  salt, \r\nAsphaltum,  Boerrhavia  diffusa,  Thriphala,  Chathurjathakam,  Gingelly  oil, \r\nCastor oil, Cow milk, Cow ghee.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain,\r\nContusions, Emaciation, Mental disorders, Hydrocele, Hernia, Vaginal\r\ndisorders, Menstrual irregularities, Uriogenital disorders. Recommended in\r\nAntenatal as well as Post-natal care. and better for ‘Pizhichil’.', NULL, NULL, '200 ml., 450 ml.', 'Only for external use below the neck.', 5, 1, 'DHANWANTHARAM KUZHAMBU', NULL, 'DHANWANTHARAM KUZHAMBU', 1, 12, '2019-03-11 11:02:44', '2019-08-13 17:02:39'),
(75, 'DHANWANTHARAM THAILAM', 'dhanwantharam-thailam-6', '5d43d8e8c96cd1564727528.jpg', NULL, 7, 'T23', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam,\r\nAsparagus racemosus, Rubia cordifolia, Withania somnifera, Hemidesmus\r\nindicus, Santalum album, Ipomoea mauritiana, Acorus calamus, Rock salt,\r\nAsphaltum, Boerrhavia diffusa, Thriphala, Chathurjathakam, Gingelly oil,\r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain, \r\nContusions,  Emaciation,  Mental  disorders,  Hydrocele,  Hernia,  Vaginal \r\ndisorders, Menstrual irregularities, Urinogenital disorders. Recommended in \r\nAntenatal as well as Post-natal care.', NULL, NULL, '200 ml., 450 ml', 'For external application all over the body.', 5, 1, 'DHANWANTHARAM THAILAM', NULL, 'DHANWANTHARAM THAILAM', 1, 12, '2019-03-11 11:05:32', '2019-08-13 16:55:35'),
(76, 'DINESELADI VELICHENNA', 'dineseladi-velichenna', '5d43d95a27e041564727642.jpg', NULL, 7, 'T22', 0, 0, '37', 'Ventilago madraspatana, Curcuma longa, Calotropis gigantea, Cassia fistula, \r\nKsheerivriksha (stem bark), Elettaria cardamomum, Nardostachys jatamansi, \r\nSaussurea  lappa,  Myristica  fragrans,  Pearl  oyster,  Saffron,  Commiphora \r\nwightii, Coconut oil.', 'Scabies, Skin disorders, Itching, Urticaria, Allergic disorders and Skin\r\nblemishes.', NULL, NULL, '200 ml., 450 ml.', 'For external application only.', 5, 1, 'DINESELADI VELICHENNA', NULL, 'DINESELADI VELICHENNA', 1, 10, '2019-03-11 11:10:42', '2019-08-13 16:52:26'),
(78, 'ELADI THAILAM', 'eladi-thailam', '5d43db4d01eeb1564728141.jpg', NULL, 7, 'T7', 0, 0, '30', 'Elettaria cardamomum, Amomum subulatum, Kaempferia galanga, Pearl\r\noyster, Myristica fragrans, Saffron, Commiphora wightii, Yellow resin, Gingelly\r\noil.', 'Scabies and other skin disorders like Itching, Skin blemishes. Improves\r\ncomplexion.', NULL, NULL, '200 ml., 450 ml.', 'For external application only', 5, 1, 'ELADI THAILAM', NULL, 'ELADI THAILAM', 1, 20, '2019-03-11 11:19:32', '2019-08-13 17:11:22'),
(79, 'ELADI VELICHENNA', 'eladi-velichenna', '5d43db2ea51ab1564728110.jpg', NULL, 7, 'T8', 0, 0, '30', 'Elettaria cardamomum, Amomum subulatum, Kaempferia galanga, Pearl oyster, \r\nMyristica fragrans, Saffron, Commiphora wightii, Yellow resin, Coconut oil.', 'Same as Eladi Thailam. Especially in Dandruff.', NULL, NULL, '200 ml., 450 ml.', 'External application over head and body.', 5, 1, 'ELADI VELICHENNA', NULL, 'ELADI VELICHENNA', 1, 18, '2019-03-11 11:23:01', '2019-08-13 15:23:26'),
(80, 'GANDHA THAILAM', 'gandha-thailam', '5c94b6f7626501553250039.jpg', NULL, 7, 'T15A', 0, 0, '45', 'Sesamum indicum, Cow milk, Glycyrrhiza glabra, Eugenia caryophyllata,\r\nPeucedanum graveolens, Sida cordifolia, Jeevaneeyaganam, Elettaria\r\ncardamomum, Amomum subulatum, Kaempferia galanga, Pearl oyster,\r\nMyristica fragrans, Saffron, Commiphora wightii, Yellow resin, Alpinia calcarata.', 'Fracture, Sprain, Blows, Contusions, Crushing etc. over vital points, Rheumatic \r\ndiseases. Recommended for remodelling and strengthening the bones.', NULL, 'Internally 5-12 drops with milk/lukewarm water or suitable kashayas at bed \r\ntime or as directed by the physician.', '10 ml.', 'Internal as well as external application. Externally apply over the affected\r\narea.', 5, 1, 'GANDHA THAILAM', NULL, 'GANDHA THAILAM', 1, 15, '2019-03-11 11:26:08', '2019-08-13 17:12:34'),
(81, 'GANDHARVAHASTHA THAILAM', 'gandharvahastha-thailam', '', NULL, 7, 'T16', 0, 0, '5', 'Ricinus communis, Hordeum vulgare, Dried ginger, Cow milk, Castor oil.', 'An effective and harmless purgative in Low back-ache, Body aches and\r\nRheumatic complaints.', NULL, NULL, '50 ml.', 'Internally 5 - 15 ml. with hot milk or juice of Vitex negundo leaves or suitable\r\nkashaya at bed time or as directed by the physician.', 5, 1, 'GANDHARVAHASTHA THAILAM', NULL, 'GANDHARVAHASTHA THAILAM', 1, 9, '2019-03-11 11:30:04', '2019-08-13 17:13:22'),
(82, 'GOPADMAJADI VELICHENNA', 'gopadmajadi-velichenna', '5d43dba8c8e5e1564728232.jpg', NULL, 7, 'T17', 0, 0, '14', 'Cynodon dactylon, Hemidesmus indicus, Ksheerivriksha (buds), Coconut oil, \r\nCow milk.', 'Scabies, Erysepelas, Abscess and Burning sensation in skin disorders.', NULL, NULL, '200 ml., 450 ml.', 'For external application only', 5, 1, 'GOPADMAJADI VELICHENNA', NULL, 'GOPADMAJADI VELICHENNA', 1, 11, '2019-03-11 11:32:50', '2019-08-13 17:13:59'),
(83, 'GULGULUMARICHADI THAILAM', 'gulgulumarichadi-thailam', '5d43dbd8bb3571564728280.jpg', NULL, 7, 'T63', 0, 0, '17', 'Commiphora wightii, Piper nigrum, Embelia ribes, Yellow resin, Yellow orpiment, Curcuma longa, Gingelly oil, Coconut oil, Hydnocarpus oil.', 'Chronic skin diseases, Keratitis, Crack foot.', NULL, NULL, '500 g.', 'For external application over affected area.', 5, 1, 'GULGULUMARICHADI THAILAM', NULL, 'GULGULUMARICHADI THAILAM', 1, 12, '2019-03-11 11:40:31', '2019-08-13 17:14:53'),
(84, 'HINGUTHRIGUNAM THAILAM', 'hinguthrigunam-thailam', '5d43dc06287991564728326.jpg', NULL, 7, 'T67', 0, 0, '4', 'Ferula asafoetida, Rock salt, Castor oil, Allium sativum.', 'Hydrocele, Pthiasis, Ascites.', NULL, '5-25 ml. with Cow urine or suitable kashayas in empty stomach or as directed \r\nby the physician.', '50 g., 100 g.', NULL, 5, 1, 'HINGUTHRIGUNAM THAILAM', NULL, 'HINGUTHRIGUNAM THAILAM', 1, 11, '2019-03-11 11:42:15', '2019-08-13 15:25:59'),
(85, 'JATHYADI VELICHENNA', 'jathyadi-velichenna', '5d43dc2d863d91564728365.jpg', NULL, 7, 'T20', 0, 0, '19', 'Jasminum grandiflorum, Oldenlandia corymbosa, Cyathula prostrata, Aerva \r\nlanata,  Azadirachta  indica,  Vitex  negundo,  Coconut  oil,  Rubia  cordifolia, \r\nCurcuma  longa,  Pongamia  pinnata,  Blue  vitriol,  Bee  wax.  For  external \r\napplication only.', 'Chronic wounds, Wounds in vital points, Burns, Abscess.', NULL, NULL, '200 ml., 450 ml.', 'A tampon soaked in this, inserted into the deep wounds, cleans and cures the\r\nwounds speedily. For external application.', 5, 1, 'JATHYADI VELICHENNA', NULL, 'JATHYADI VELICHENNA', 1, 10, '2019-03-11 11:45:29', '2019-08-13 17:22:42'),
(86, 'JEEVANTHYADI YAMAKAM', 'jeevanthyadi-yamakam', '', NULL, 7, 'T64B', 0, 0, '10', 'Holostemma ada-Kodien, Rubia cordifolia, Blue vitriol, Gingelly oil, Buffallo \r\nghee, Yellow resin, Bee wax.', 'Chilblain, Cracks in foot, palms and lips, Skin diseases.', NULL, NULL, '50 g., 100 g.', 'For external application', 5, 1, 'JEEVANTHYADI YAMAKAM', NULL, 'JEEVANTHYADI YAMAKAM', 1, 11, '2019-03-11 11:48:44', '2019-08-13 17:23:14'),
(87, 'KANJUNNYADI VELICHENNA', 'kanjunnyadi-velichenna', '5d43dcaa734a01564728490.jpg', NULL, 7, 'T81', 0, 0, '7', 'Eclipta alba, Tinospora cordifolia, Emblica officinalis, Galena, Glycyrrhiza\r\nglabra, Coconut oil, Cow milk.', 'Headache, Ophthalmic disturbances. Prevents premature greying and loss of hair. Enhances growth of hair.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'KANJUNNYADI VELICHENNA', NULL, 'KANJUNNYADI VELICHENNA', 1, 14, '2019-03-11 11:50:30', '2019-08-13 17:24:29'),
(88, 'KANJUNNYADI THAILAM', 'kanjunnyadi-thailam', '', NULL, 7, 'T10', 0, 0, '7', 'Eclipta alba, Tinosporacordifolia, Emblica officinalis, Galena, Glycyrrhiza\r\nglabra, Gingelly oil, Tinospora cordifolio, Cow milk.', 'Headache, Ophthalmic disturbances. Prevents premature greying and loss of \r\nhair. Enhances growth of hair.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'KANJUNNYADI THAILAM', NULL, 'KANJUNNYADI THAILAM', 1, 10, '2019-03-11 11:53:00', '2019-08-13 17:23:50'),
(89, 'KARAPPAN THAILAM', 'karappan-thailam', '5d2eeaa1d87f41563355809.jpg', NULL, 7, 'T9', 0, 0, '22', 'Eclipta alba, Asparagus racemosus, Tectona grandis (Young shoot), Cynodon \r\ndactylon,  Coconut  oil,  Vetiveria  zizanioides,  Thriphala,  Embelia  ribes, \r\nSantalum album, Rubia cordifolia.', 'Paediatric skin disorders. Can be also used for Poisonous skin disorders in \r\nadults.', NULL, NULL, '200 ml., 450 ml.', 'For external application only', 5, 1, 'KARAPPAN THAILAM', NULL, 'KARAPPAN THAILAM', 1, 11, '2019-03-11 11:54:35', '2019-08-13 17:24:59'),
(92, 'KARNAPOORANA THAILAM', 'karnapoorana-thailam', '5d43dd3a6b68c1564728634.jpg', NULL, 7, 'T11', 0, 0, '8', 'Aconitum heterophyllum, Ferula asafoetida, Peucedanum graveolens.\r\nCinnamomum zeylanicum, Unaqua NaCI, Piper nigrum, Gingelly oil, Mustard\r\noil, Dhanyamlam.', 'Otalgia, Deafness, Otorrhoea and other ear diseases.', NULL, '2-3 drops in ears b.d. with bearable warmth or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'KARNAPOORANA THAILAM', NULL, 'KARNAPOORANA THAILAM', 1, 12, '2019-03-11 12:03:34', '2019-08-13 17:25:41'),
(93, 'KARPASASTHYADI KUZHAMBU', 'karpasasthyadi-kuzhambu', '', NULL, 7, 'T98', 0, 0, '19', 'Gossypium herbaceum, Sida cordifolia, Phaseolus mungo, Alpinia calcarata, \r\nPeucedanum graveolens. Dried Ginger, Moringa oleifera, Boerrhavia diffusa, \r\nGingelly oil, Castor oil, Cow ghee, Goat milk.', 'Hemiplegia, Facial palsy, Brachial palsy.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'KARPASASTHYADI KUZHAMBU', NULL, 'KARPASASTHYADI KUZHAMBU', 1, 6, '2019-03-11 12:05:18', '2019-08-13 17:26:33'),
(94, 'KARPOORADI THAILAM', 'karpooradi-thailam', '5d43dd85ab6dc1564728709.jpg', NULL, 7, 'TC3', 0, 0, '3', 'Camphor, Apium graveolens, Coconut oil.', 'Pain and Swelling over joints.', NULL, NULL, '450 ml., 200 ml., 100 ml.', 'For external application.', 5, 1, 'KARPOORADI THAILAM', NULL, 'KARPOORADI THAILAM', 1, 14, '2019-03-11 12:06:58', '2019-08-13 17:26:56'),
(95, 'KARPASASTHYADI THAILAM', 'karpasasthyadi-thailam', '', NULL, 7, 'T13', 0, 0, '17', 'Gossypium herbaceum, Sida cordifolia, Phaseolus mungo, Alpinia calcarata, \r\nPeucedanum graveolens, Dried Ginger, Moringa oleifera, Boerrhavia diffusa, \r\nGingelly oil, Goat milk.', 'Hemiplegia, Facial palsy, Brachial palsy.', NULL, NULL, '200 ml., 450 ml.', 'Application over head and body. Used for ‘Nasya’ in facial palsy and brachial\r\npalsy. Fomentation with Calotropis leaves is preferable after applying over the\r\naffected area.', 5, 1, 'KARPASASTHYADI THAILAM', NULL, 'KARPASASTHYADI THAILAM', 1, 7, '2019-03-11 12:08:33', '2019-08-13 17:26:07'),
(96, 'KOTTAMCHUKKADI KUZHAMBU', 'kottamchukkadi-kuzhambu', '', NULL, 7, 'TX1', 0, 0, '14', 'Tamarindus indica (Leaves), Saussurea lappa, Dried ginger, Acorus calamus, \r\nAllium sativum, Alpinia calcarata, Gingelly oil, Curd, Cow Ghee, Castor Oil.', 'Rheumatic complaints. Recommended for relieving Muscle cramp, Numbness, \r\nLassitude and Swelling seen in rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'For external application', 5, 1, 'KOTTAMCHUKKADI KUZHAMBU', NULL, 'KOTTAMCHUKKADI KUZHAMBU', 1, 6, '2019-03-11 12:11:01', '2019-08-13 15:30:49'),
(97, 'KOTTAMCHUKKADI THAILAM', 'kottamchukkadi-thailam', '', NULL, 7, 'T14', 0, 0, '12', 'Tamarindus indica (Leaves), Saussurea lappa, Dried ginger, Acorus calamus, \r\nAllium sativum, Alpinia calcarata, Gingelly oil, Curd.', 'Rheumatic complaints. Recommended for relieving Muscle cramp, Numbness, \r\nLassitude and Swelling seen in rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'KOTTAMCHUKKADI THAILAM', NULL, 'KOTTAMCHUKKADI THAILAM', 1, 8, '2019-03-11 12:12:59', '2019-08-13 15:30:37'),
(98, 'KSHEERABALA 101 AVARTHI', 'ksheerabala-101-avarthi', '', NULL, 7, 'T77A', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil.', 'Rheumatic complaints.', NULL, '5-10 drops', '10 g.', 'Preferably for internal use as additive in kashaya or in lukewarm Cumin seed\r\nwater or milk and for external application over head and affected areas.', 5, 1, 'KSHEERABALA 101 AVARTHI', NULL, 'KSHEERABALA 101 AVARTHI', 1, 16, '2019-03-11 12:18:27', '2019-08-13 15:42:41'),
(99, 'KSHEERABALA 14 AVARTHI', 'ksheerabala-14-avarthi', '', NULL, 7, 'T74A', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'Internal as well as external application on head and body. Can be used for\r\n‘Nasya’, ‘Pichu’, ‘Dhara’, ‘Vasthi’', 5, 1, 'KSHEERABALA 14 AVARTHI', NULL, 'KSHEERABALA 14 AVARTHI', 1, 8, '2019-03-11 12:23:59', '2019-08-13 17:31:30'),
(100, 'KSHEERABALA 21 AVARTHI', 'ksheerabala-21-avarthi', '', NULL, 7, 'T75A', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'Internal as well as external application on head and body. Can be used for\r\n‘Nasya’, ‘Pichu’, ‘Dhara’, ‘Vasthi’', 5, 1, 'KSHEERABALA 21 AVARTHI', NULL, 'KSHEERABALA 21 AVARTHI', 1, 4, '2019-03-11 12:29:50', '2019-08-13 15:42:03'),
(101, 'KSHEERABALA 41 AVARTHI', 'ksheerabala-41-avarthi', '', NULL, 7, 'T76A', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'Internal as well as external application on head and body. Can be used for\r\n‘Nasya’, ‘Pichu’, ‘Dhara’, ‘Vasthi’', 5, 1, 'KSHEERABALA 41 AVARTHI', NULL, 'KSHEERABALA 41 AVARTHI', 1, 9, '2019-03-11 12:32:32', '2019-08-13 15:42:27');
INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(102, 'KSHEERABALA 7 AVARTHI', 'ksheerabala-7-avarthi', '', NULL, 7, 'T73A', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'Internal as well as external application on head and body. Can be used for\r\n‘Nasya’, ‘Pichu’, ‘Dhara’, ‘Vasthi’', 5, 1, 'KSHEERABALA 7 AVARTHI', NULL, 'KSHEERABALA 7 AVARTHI', 1, 12, '2019-03-11 12:34:26', '2019-08-13 15:41:33'),
(103, 'KSHEERABALA THAILAM', 'ksheerabala-thailam', '', NULL, 7, 'T54', 0, 0, '3', 'Sida cordifolia, Cow milk, Gingelly oil.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'Internal as well as external application on head and body. Can be used for\r\n‘Nasya’, ‘Pichu’, ‘Dhara’, ‘Vasthi’', 5, 1, 'KSHEERABALA THAILAM', NULL, 'KSHEERABALA THAILAM', 1, 6, '2019-03-11 12:36:11', '2019-08-13 17:30:40'),
(104, 'KURUNTHOTTI ENNA', 'kurunthotti-enna', '5d43de20677201564728864.jpg', NULL, 7, 'T12', 0, 0, '18', 'Sida cordifolia, Boerrhavia diffusa, Hemidesmus indicus, Cuminum cyminum, \r\nOak gall, Eugenia caryophyllata, Gingelly oil, Aegle marmelos.', 'Rheumatic diseases.', NULL, NULL, '200 ml., 450 ml.', 'For external application', 5, 1, 'KURUNTHOTTI ENNA', NULL, 'KURUNTHOTTI ENNA', 1, 10, '2019-03-11 12:38:48', '2019-08-13 17:34:12'),
(106, 'LAKSHADI KUZHAMBU', 'lakshadi-kuzhambu', '5d43de920c8181564728978.jpg', NULL, 7, 'T85', 0, 0, '17', 'Coccus lacca, Withania somnifera, Curcuma longa, Santalum album, Alpinia \r\ncalcarata, Gingelly oil, Castor oil, Cow ghee, Curd.', 'Consumption, Physical weakness, emaciation due to Polio myelitis, Mental\r\ndisorders, etc. Recommended for the development of body and to correct\r\nblood circulation by massaging all over body in children and adults..  Recommended  in  Rheumatic  diseases,  Bone \r\nimpairments.', NULL, NULL, '200 ml., 450 ml.', 'For external application. Should not be applied on head.', 5, 1, 'LAKSHADI KUZHAMBU', NULL, 'LAKSHADI KUZHAMBU', 1, 13, '2019-03-11 13:01:40', '2019-08-13 17:37:19'),
(107, 'LAKSHADI THAILAM', 'lakshadi-thailam', '5d43dec004fd71564729024.jpg', NULL, 7, 'T44', 0, 0, '15', 'Coccus lacca, Withania somnifera, Curcuma longa, Santalum album, Alpinia \r\ncalcarata, Gingelly oil, Curd', 'Consumption, Physical weakness, emaciation due to Polio myelitis, Mental \r\ndisorders,  etc.  Recommended  for  the  development  of  body  and  to  correct \r\nblood circulation by massaging all over body in children and adults.', NULL, NULL, '200 ml., 450 ml.', 'For external application over body and head.', 5, 1, 'LAKSHADI THAILAM', NULL, 'LAKSHADI THAILAM', 1, 9, '2019-03-11 13:05:37', '2019-08-13 17:34:48'),
(108, 'LAKSHADI VELICHENNA', 'lakshadi-velichenna', '5d43decc43a9b1564729036.jpg', NULL, 7, 'T94', 0, 0, '15', 'Coccus lacca, Withania somnifera, Curcuma longa, Santalum album, Alpinia \r\ncalcarata, Coconut oil, Curd.', 'Consumption, Physical weakness, emaciation due to Polio myelitis, Mental\r\ndisorders, etc. Recommended for the development of body and to correct\r\nblood circulation by massaging all over the body in children and adults.\r\n. Especially recommended in Rhinitis, Cough,\r\nBronchial disorders.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head and body.', 5, 1, 'LAKSHADI VELICHENNA', NULL, 'LAKSHADI VELICHENNA', 1, 8, '2019-03-11 13:09:29', '2019-08-13 17:36:45'),
(109, 'MADHUYASHTYADI THAILAM', 'madhuyashtyadi-thailam', '', NULL, 7, 'T41', 0, 0, '32', 'Glycyrrhiza glabra, Desmodium gangeticum, Phyllanthus niruri, Santalum\r\nalbum, Tinospora cordifolia, Peucedanum graveolens, Rubia cordifolia,\r\nCoriandrum sativum, Gingelly oil, Cow ghee.', 'Arthritis and related symptoms like Burning sensation, Pain, Swelling,\r\nLassitude etc.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head and body. Also recommended to use in ‘vasthi’ for Arthritis.', 5, 1, 'MADHUYASHTYADI THAILAM', NULL, 'MADHUYASHTYADI THAILAM', 1, 8, '2019-03-11 13:12:16', '2019-08-13 17:38:14'),
(110, 'MAHACHANDANADI THAILAM', 'mahachandanadi-thailam', '5d43df928ac471564729234.jpg', NULL, 7, 'T57', 0, 0, '44', 'Santalum album, Coleus zeylanicus, Sida cordifolia, Emblica officinalis,\r\nGlycyrrhiza glabra, Vetiveria zizanioides, Bacopa monnieri, Aloe vera, Nelumbium\r\nspeciosum, Ksheerivriksha (Stem bark and bud), Jeevaneeyaganam.', 'Burning sensation all over the body, Neck stiffness, Fainting, Insomnia, Mental \r\ndisorders.', NULL, NULL, '200 ml., 450 ml.', 'For external application on head and body.', 5, 1, 'MAHACHANDANADI THAILAM', NULL, 'MAHACHANDANADI THAILAM', 1, 9, '2019-03-11 13:15:39', '2019-08-13 17:39:57'),
(111, 'AGASTHYARASAYANAM', 'agasthyarasayanam', '5d2ece99446471563348633.jpg', NULL, 5, 'H1', 0, 0, '27', 'Dasamoolarn, Kaempferia galanga, Sida cordifolia, Clerodendrum serratum, Hordeum vulgare, Terminalia chebula, Jaggery, Cow ghee, Gingelly oil, Honey, Piper longum.', 'Cough, Asthma, Bronchitis, Sinusitis, Nasal catarrh, Hiccup and other Respiratory disorders.', NULL, '5 - 15 g. b.d., or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'AGASTHYARASAYANAM', NULL, 'AGASTHYARASAYANAM', 1, 48, '2019-03-11 13:26:57', '2019-08-09 17:03:59'),
(113, 'AJAMAMSARASAYANAM', 'ajamamsarasayanam', '5d2ececb4c9ad1563348683.jpg', NULL, 5, 'H3C', 0, 0, '30', 'Dasamoolarn, Alpinia calcarata, Asparagus racemosus, Mutton soup, Jeevaneeyaganam, Cow milk, Honey, Cow ghee, Sugar.', 'Rheumatic complaints, Tremors and Emaciation. Renders physical strength, vigour and vitality. Recommended in Post-natal care.', NULL, '5 -15 g. b.d., or as directed by the physician.', '400 g.', NULL, 5, 1, 'AJAMAMSARASAYANAM', NULL, 'AJAMAMSARASAYANAM', 1, 47, '2019-03-11 13:32:24', '2019-08-09 17:05:48'),
(115, 'ASWAGANDHADI LEHYAM', 'aswagandhadi-lehyam', '5c94c1f2313511553252850.jpg', NULL, 5, 'H28', 0, 0, '9', 'Withania somnifera, Hemidesmus indicus, Cuminum cyminum, Curculigo orchioides, Raisins, Cow ghee, Honey, Elettaria Cardamomum, Jaggery.', 'Renders physical strength and vigour. Recommended to regain health after long term treatment. Indicated in Emaciation, General fatigue and Sexual disorders.', NULL, '5 - 15 g. b.d., preferably with milk or as directed by the physician.', '400 g.', NULL, 5, 1, 'ASWAGANDHADI LEHYAM', NULL, 'ASWAGANDHADI LEHYAM', 1, 67, '2019-03-11 13:39:54', '2019-08-09 17:07:20'),
(117, 'CHYAVANAPRASAM', 'chyavanaprasam', '5d2eff15b986a1563361045.jpg', NULL, 5, 'H10', 0, 0, '48', 'Dasamoolam, Sida cordifolia, Piper longum, Phyllanthus niruri, Elettaria cardamomum, Raisins, Santalum album, Tinospora cordifolia, Adhatoda beddomei, Withania somnifera, Emblica officinalis, Cow ghee, Gingeliy oil, Sugarcandy, Chathurjathakam, Honey, Curcuma angustifolia.', 'Recommended as a prophylactic medicine in the recovery of Bronchitis, Asthma, Arthritis, Metabolic disorders, etc. Renders immunity power, health and vitality. Improves the centres of intelligence, memory, complexion etc.', NULL, '5 -15 g. b.d., or as directed by the physician.', '200 g.t 500 g.', NULL, 5, 1, 'CHYAVANAPRASAM', NULL, 'CHYAVANAPRASAM', 1, 48, '2019-03-11 13:46:17', '2019-08-09 17:10:10'),
(118, 'CHINCHADI LEHYAM', 'chinchadi-lehyam', '5d2eddadb28db1563352493.jpg', NULL, 5, 'H9', 0, 0, '23', 'Tamarindus indica, Jaggery, Iron /processed). Aegle marmelos, Thrikatu, Thriphala, Thrijathakam, Alpinia calcarata, Glycyrrhiza glabra, Gingelly oil.', 'Anaemia, Anorexia, Heart burn, Dyspepsia.', NULL, '5 - 15 g. b.d., after food or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'CHINCHADI LEHYAM', NULL, 'CHINCHADI LEHYAM', 1, 22, '2019-03-11 13:48:26', '2019-08-09 17:08:35'),
(119, 'DASAMOOLARASAYANAM', 'dasamoolarasayanam', '5d2ee51a7f10c1563354394.jpg', NULL, 5, 'H14', 0, 0, '48', 'Dasamoolam, Sida cordifolia, Ricinus communis, Adhatoda beddomei, Clerodendrum serratum, Kaempferia galanga, Thriphala, Jaggery, Sugar, Chathurjathakam, Thrikatu, Camphor, Saffron, Honey.', 'Asthma, Cough, Dyspnoea, Respiratory disorders, Rheumatic complaints etc.', NULL, '2 - 5 g. intermittently or as directed by the physician.', '100 g., 25 g.', NULL, 5, 1, 'DASAMOOLARASAYANAM', NULL, 'DASAMOOLARASAYANAM', 1, 18, '2019-03-11 13:52:10', '2019-08-09 17:12:01'),
(120, 'MAHAMASHA THAILAM', 'mahamasha-thailam', '5d43dfb0252611564729264.jpg', NULL, 7, 'T40', 0, 0, '39', 'Phaseolus mungo, Sida cordifolia, Dasamoolam, Alpinia calcarata,\r\nJeevaneeyaganam, Thrikatu, Rock salt, Ricinus communis, Gingelly oil,\r\nMutton soup, Cow milk.', 'Emaciation of extremities, Numbness, Contraction, Hamiplegia, Facial\r\nparalysis, Deafness, Sciatica, Brachial palsy, ear ache, Tinnitus', NULL, NULL, '200 ml., 450 ml', 'Can be used for external application as well as for vasthi.', 5, 1, 'MAHAMASHA THAILAM', NULL, 'MAHAMASHA THAILAM', 1, 9, '2019-03-11 13:55:15', '2019-08-13 17:41:27'),
(122, 'MAHANARAYANA THAILAM', 'mahanarayana-thailam', '', NULL, 7, 'TC12', 0, 0, '34', 'Asparagus racemosus, Sida cordifolia, Emblica officinalis, Gingelly oil,\r\nCow milk', 'All types of Vatik disorders , Rheumatic  Arthritis , headache etc…', NULL, NULL, '200ml, 450ml', 'External application & for Vasti, Dhara and Pichu', 5, 1, 'MAHANARAYANA THAILAM', NULL, 'MAHANARAYANA THAILAM', 1, 8, '2019-03-11 13:57:32', '2019-08-13 17:42:02'),
(123, 'DRAKSHADI LEHYAM', 'drakshadi-lehyam', '5d2ee5b7240541563354551.jpg', NULL, 5, 'H22', 0, 0, '8', 'Raisins, Piper longum, Sugar, Glycyrrhiza glabra, Dried ginger, Curcuma  angustifolia, Emblica officinalis, Honey.', 'Anaemia, Jaundice, Fatigue and Anorexia.', NULL, '5 - 15 g. b.d., after food or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'DRAKSHADI LEHYAM', NULL, 'DRAKSHADI LEHYAM', 1, 24, '2019-03-11 14:00:41', '2019-08-09 17:12:42'),
(124, 'ELADI RASAYANAM', 'eladi-rasayanam', '5d2ee619a21041563354649.jpg', NULL, 5, 'H4', 0, 0, '23', 'Elettaria cardamomum, Thriphala, Thrikatu, Plumbago rosea, Pterocarpus marsupium, Embelia ribes, Cow ghee, Bamboo manna, Thrijathakam, Sugar, Honey.', 'Cough and other respiratory complaints, Pthiasis, Anaemia, Emaciation. Also recommended to improve memory power and appetite.', NULL, '5 - 15 g. b.d., or as directed by the physician.', '50 g.', NULL, 5, 1, 'ELADI RASAYANAM', NULL, 'ELADI RASAYANAM', 1, 11, '2019-03-11 14:02:58', '2019-08-09 17:13:25'),
(125, 'GOMOOTHRAHAREETHAKI', 'gomoothrahareethaki', '', NULL, 5, 'H8', 0, 0, '3', 'Terminalia chebula, Cow urine, Honey.', 'Piles, Skin diseases, Dropsy, Pthiasis, Ascites, Carbuncles, Scrofula, Obesity, Anaemia, Rheumatic diseases.', NULL, '5 - 15 g. b.d., or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'GOMOOTHRAHAREETHAKI', NULL, 'GOMOOTHRAHAREETHAKI', 1, 13, '2019-03-11 14:05:17', '2019-08-09 17:14:08'),
(126, 'MALATHYADI VELICHENNA', 'malathyadi-velichenna', '5d43e01af36661564729370.jpg', NULL, 7, 'TC5', 0, 0, '5', 'Jasminum grandiflorum, Plumbago rosea, Nerium odorum, Pongamia pinnata, \r\nCoconut oil.', 'Alopacia, Dandruff, Scalp itching, Sores over scalp.', NULL, NULL, '200 ml., 450 ml.', 'External application over head.', 5, 1, 'MALATHYADI VELICHENNA', NULL, 'MALATHYADI VELICHENNA', 1, 4, '2019-03-11 14:05:54', '2019-08-13 17:42:31'),
(127, 'HARIDRAKHANDAM', 'haridrakhandam', '5d2ee83c4ea6c1563355196.jpg', NULL, 5, 'H31', 0, 0, '18', 'Curcuma longa, Cow milk, Cow ghee, Sugarcandy, Thrikatu, Chathurjathakam, Embelia ribes, Thriphala, Merrimia turpethum, Cyperus rotundus, Loha sindooram.', 'Urticaria, Skin disorders, Worm infestation, Anaemia, Eosinophilia and other allergic conditions like Sneezing, Itching etc.', NULL, '5 -10 g. b.d., with luke warm water/ milk or as directed by the physician.: 50 g.', '50 g.', NULL, 5, 1, 'HARIDRAKHANDAM', NULL, 'HARIDRAKHANDAM', 1, 11, '2019-03-11 14:09:15', '2019-08-09 17:14:47'),
(128, 'HRIDYAVIRECHANAM', 'hridyavirechanam', '5d2ee855761031563355221.jpg', NULL, 5, 'H21', 0, 0, '7', 'Merrimia turpethum, Sugar, Thrijathakam, Honey.', 'Safe laxative in bilious disorders.', NULL, 'As directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'HRIDYAVIRECHANAM', NULL, 'HRIDYAVIRECHANAM', 1, 14, '2019-03-11 14:11:28', '2019-08-09 17:15:16'),
(129, 'MARICHADI THAILAM', 'marichadi-thailam', '5d43e05d741931564729437.jpg', NULL, 7, 'T84', 0, 0, '4', 'Piper nigrum, Gingelly oil, Coconut oil, Goat milk.', 'Nasal catarrh, Cold, Bronchitis, Sneezing, Cough, Sinusitis.', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'MARICHADI THAILAM', NULL, 'MARICHADI THAILAM', 1, 8, '2019-03-11 14:11:55', '2019-08-13 15:49:24'),
(130, 'KALYANAGULAM', 'kalyanagulam', '5d2eea1cce4b51563355676.jpg', NULL, 5, 'H5', 0, 0, '22', 'Embelia ribes, Thriphala, Coriandrum sativum, Panchalavanam, Emblica officinalis, Jaggery, Gingelly oil.', 'A powerful laxative, indicated in skin diseases, Piles, Jaundice, Splenic disorders, Sprue, Anaemia, Fistula, Ascites etc.', NULL, 'As directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'KALYANAGULAM', NULL, 'KALYANAGULAM', 1, 22, '2019-03-11 14:14:18', '2019-08-09 17:15:53'),
(131, 'ANNABHEDI SINDOORAM', 'annabhedi-sindooram', '5d43d32308fc41564726051.jpg', NULL, 2, 'B1', 0, 0, NULL, 'Ferri sulphus (processed). Ferrous sulphate', 'Anaemia, Obesity, Rheumatic complaints, Skin disorders. Improves absorption of nutrients.', NULL, '125 - 250 mg. with 1 tsp. Lemon juice and q.s. honey o.d. or as directed by the physician.', '5g.,10g.', NULL, 5, 1, 'ANNABHEDI SINDOORAM', NULL, 'ANNABHEDI SINDOORAM', 1, 43, '2019-03-11 14:15:20', '2019-08-13 10:54:47'),
(132, 'KOOSHMANDARASAYANAM', 'kooshmandarasayanam', '5d2eea75ecc751563355765.jpg', NULL, 5, 'H7', 0, 0, '12', 'Benincasa hispida, Cow ghee, Sugarcandy, Thrikatu, Cuminum Cyminum, Thrijathakam, Coriandrum sativum, Honey.', 'Bronchitits, Hiccup, Emaciation, Epistaxis, Malabsorption. Recommended to restore the physical strength during convalescence period. Renders health, vigour and improves memory power.', NULL, '5 -15 g. b.d., or as directed by the physician.', '200g., 500 g.', NULL, 5, 1, 'KOOSHMANDARASAYANAM', NULL, 'KOOSHMANDARASAYANAM', 1, 14, '2019-03-11 14:17:24', '2019-08-09 17:16:31'),
(133, 'MURIVENNA', 'murivenna', '5d43e180b9b311564729728.jpg', NULL, 7, 'Murivenna', 0, 0, '9', 'Pongamia pinnata, Piper betel, Moringa oleifera, Allium cepa, Coconut oil,\r\nAsparagus racemosus.', 'Wounds, Injuries, Fractures, Sprains, Contusions, Muscular pain.', NULL, NULL, '100 ml., 200 ml., 450 ml.', 'For external application. A’pichu’ prepared with “Murivenna” relieves pain and inflammation.', 5, 1, 'MURIVENNA', NULL, 'MURIVENNA', 1, 16, '2019-03-11 14:19:34', '2019-08-13 17:43:30'),
(135, 'MANIBHADRA LEHYAM', 'manibhadra-lehyam', '', NULL, 5, 'H15', 0, 0, '5', 'Embelia ribes, Emblica officinalis, Terminalia chebula, Merrimia turpethum, Jaggery.', 'A safe purgative in Skin diseases, Cough, Respiratory diseases, Ascites, Vitiligo, Carbuncles, Worm infestation, Pthisis.', NULL, '5 - 15 g. with luke warm water in empty stomach or as directed by the physician.', '200 g,, 500 g.', NULL, 5, 1, 'MANIBHADRA LEHYAM', NULL, 'MANIBHADRA LEHYAM', 1, 19, '2019-03-11 14:22:39', '2019-08-09 17:19:22'),
(136, 'NAGARADI THAILAM', 'nagaradi-thailam', '', NULL, 7, 'T88', 0, 0, '43', 'Dried ginger, Plumbago rosea, Cedrus deodara, Thriphala, Commiphora\r\nwightii, Alpinia calcarata, Curcuma longa, Coscinium fenestratum,\r\nChathurjathakam, Piper Spp., Symplocos cochinchinensis, Gingelly oil, Cow\r\nmilk. Bee wax.', 'Headache, Sinusitis, Nasal catarrh, Oral ailments etc.', NULL, NULL, '200 ml., 450 ml', 'For external application over head and for gargling.', 5, 1, 'NAGARADI THAILAM', NULL, 'NAGARADI THAILAM', 1, 5, '2019-03-11 14:23:23', '2019-08-13 17:44:01'),
(137, 'NARASIMHA RASAYANAM', 'narasimha-rasayanam', '5d2eed8925cbb1563356553.jpg', NULL, 5, 'H30', 0, 0, '18', 'Acacia catechu, Plumbago rosea, Pterocarpus marsupium, Thriphala, Iron (processed), Eclipta alba, Cow milk, Cow ghee, Sugarcandy, Sugar.', 'Loss of hair, Premature greying, Alopacia. Renders body fitness, complexion, health and vigour. Effective in Anaemia. Improves memory power.', NULL, '5 -15 g. b.d., followed by rice gruel prepared in milk.', '200 g.', NULL, 5, 1, 'NARASIMHA RASAYANAM', NULL, 'NARASIMHA RASAYANAM', 1, 33, '2019-03-11 14:24:55', '2019-08-09 17:20:02'),
(138, 'NALPAMARADI KERAM', 'nalpamaradi-keram', '5d43e1d167c5d1564729809.jpg', NULL, 7, 'T25', 0, 0, '16', 'Curcuma longa (fresh), Oldenlandia corymbosa, Ksheerivriksha (stem),\r\nThriphala, Santalum album, Vetiveria zizanioides, Saussurea lappa, Coconut\r\noil.', 'Itches, Scabies, Erysepelas and other Skin diseases especially in children.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'NALPAMARADI KERAM', NULL, 'NALPAMARADI KERAM', 1, 15, '2019-03-11 14:25:59', '2019-08-13 17:44:35'),
(140, 'NARAYANA THAILAM', 'narayana-thailam', '5d43e1f7d36601564729847.jpg', NULL, 7, 'T26', 0, 0, '33', 'Withania somnifera, Sida cordifolia, Dasamoolam, Azadirachta indica, Boerrhavia \r\ndiffusa, Acorus calamus, Cow milk, Gingelly oil, Asparagus racemosus.', 'Hemiplegia, Lumbago, Sciatica, Arthritis, Low back-ache, Hernia, Hydrocele, \r\nCervical spondylosis.', NULL, NULL, '200 ml., 450 ml.', 'For external application as well as ‘Vasthi’, ‘Pichu’, ‘Dhara’ & Nasyam.', 5, 1, 'NARAYANA THAILAM', NULL, 'NARAYANA THAILAM', 1, 7, '2019-03-11 14:27:22', '2019-08-13 15:50:41'),
(143, 'PULIMKUZHAMBU', 'pulimkuzhambu', '', NULL, 5, 'H33', 0, 0, '16', 'Tamarindus indica, Coleus aromaticus, Butter milk, Allium sativum, Thrikatu, Ferula asafoetida, Carum carvi, Brassica nigra, Plumbago rosea, Apium graveolens, Cuminum cyminum, Rock salt.', 'Pthisis, Hernia. Recommended in the Post-natal care, Gastro intestinal disorders like colic, loss of appetite, indigestion, flatulence etc.', NULL, '5 -15 g. with buttermilk or luke warm water before food or as directed by the physician.', '100 g.', NULL, 5, 1, 'PULIMKUZHAMBU', NULL, 'PULIMKUZHAMBU', 1, 13, '2019-03-11 14:36:22', '2019-08-09 17:21:41'),
(144, 'SATHAVARIGULAM', 'sathavarigulam', '5d2ef50b6bcae1563358475.jpg', NULL, 5, 'H19', 0, 0, '17', 'Asparagus racemosus, Sugar, Asphaltum, Cow ghee, Ipomoea mauritiana, Phyllanthus niruri, Glycyrrhiza glabra, Curculigo orchioides, Tribulus terrestris, Piper longum, Curcuma angustifolia.', 'Leucorrhoea, Menorrhagia, Menopausal syndrome, Seminal disorders, Urinary diseases, Fainting, Anaemia, General fatigue.', NULL, '5 - 15g. b.d., after food or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'SATHAVARIGULAM', NULL, 'SATHAVARIGULAM', 1, 25, '2019-03-11 14:38:58', '2019-08-09 17:22:19'),
(145, 'NEELI THAILAM', 'neeli-thailam', '5d43e267c2c9b1564729959.jpg', NULL, 7, 'T27', 0, 0, '15', 'Indigofera tinctoria, Ocimum sanctum, Vitex negundo, Aristolochia indica,\r\nThrikatu, Withania somnifera, Glycyrrhiza glabra, Santalum album, Saussurea\r\nlappa, Coconut oil.', 'Poisonous insect bites, Chronic skin diseases.', NULL, NULL, '200 ml., 450 ml.', 'For external application only.', 5, 1, 'NEELI THAILAM', NULL, 'NEELI THAILAM', 1, 10, '2019-03-11 14:42:26', '2019-08-13 17:46:46'),
(146, 'SOUBHAGYA SUNTI MODAKAM', 'soubhagya-sunti-modakam', '5d2ef52199f651563358497.jpg', NULL, 5, 'H36', 0, 0, '28', 'Cyperus rotundus, Trapa natans, Nilumbo nucifera, Cyperus rotundus, Cuminum cyminum, Carum carvi, Myristica fragrans, Iron(Incinerated), Mica (Incinerated), Zingiber officinale, Sugar, Ghee', 'Loss of appetite,Diarrhoea,Indigestion,Postnatal problems', NULL, '5-15g with water/milk or as directed by physician', '200g., 500g.', NULL, 5, 1, 'SOUBHAGYA SUNTI MODAKAM', NULL, 'SOUBHAGYA SUNTI MODAKAM', 1, 18, '2019-03-11 14:42:34', '2019-08-09 17:25:06'),
(147, 'GANDHAKA RASAYANAM', 'gandhaka-rasayanam', '5d43db807aceb1564728192.jpg', NULL, 2, 'B5', 0, 0, NULL, 'Sulphur(processed).', 'Skin disorders, Blood impurities.', NULL, '125 - 250 mg. b.d. with suitable Kashayam/honey or as directed by the physician.', '5 g., 10 g.', NULL, 5, 1, 'GANDHAKA RASAYANAM', NULL, 'GANDHAKA RASAYANAM', 1, 14, '2019-03-11 14:43:13', '2019-08-13 10:56:33'),
(148, 'SOORANAVALEHAM', 'sooranavaleham', '', NULL, 5, 'H26', 0, 0, '12', 'Wild Corm (Purified), Ghee, Sugar candy, Piper longurn, Dried ginger, Cuminum cyminum, Coriandrum sativum, Thrijathakam, Honey.', 'Piles and Haemorrhoids.', NULL, '10-15g. twice daily after food.', '200 g.', NULL, 5, 1, 'SOORANAVALEHAM', NULL, 'SOORANAVALEHAM', 1, 18, '2019-03-11 14:44:49', '2019-08-09 17:24:37'),
(149, 'NEELIBHRINGADI VELICHENNA', 'neelibhringadi-velichenna', '5d43e234599b91564729908.jpg', NULL, 7, 'T30', 0, 0, '12', 'Indigofera tinctoria, Eclipta alba, Cardiospermum halicacabum, Emblica\r\nofficinalis, Glycyrrhiza glabra, Abrus precatorius, Galena, Coconut oil, Cow\r\nmilk, Buffalo milk, Goat milk, Coconut milk.', 'Promotes growth of hair by strengthening the hair roots. Prevents premature greying and Alopacia. Recommended in headache', NULL, NULL, '100 ml., 200 ml., 450 ml.', 'Massage over the scalp.', 5, 1, 'NEELIBHRINGADI VELICHENNA', NULL, 'NEELIBHRINGADI VELICHENNA', 1, 12, '2019-03-11 14:45:26', '2019-08-13 17:46:19'),
(150, 'NEELIBHRINGADI THAILAM', 'neelibhringadi-thailam', '5d43e2447189e1564729924.jpg', NULL, 7, 'T29', 0, 0, '12', 'Indigofera tinctoria, Eclipta alba, Cardiospermum halicacabum, Emblica\r\nofficinalis, Glycyrrhiza glabra, Abrus precatorius, Galena, Gingelly oil, Cow\r\nmilk, Buffalo milk, Goat milk, Coconut milk.', 'Promotes growth of hair by strengthening the hair roots. Prevents premature \r\ngreying and Alopacia. Recommended in headache', NULL, NULL, '200 ml., 450 ml.', 'Massage over the scalp.', 5, 1, 'NEELIBHRINGADI THAILAM', NULL, 'NEELIBHRINGADI THAILAM', 1, 14, '2019-03-11 14:48:55', '2019-08-13 17:45:40'),
(151, 'KANTHA SINDOORAM', 'kantha-sindooram', '5d43dd1607cbd1564728598.jpg', NULL, 2, 'B4', 0, 0, NULL, 'Wrought iron (processed).', 'Iron deficiency anaemia, Ascites, Pthisis, Diabetes, Sprue and Jaundice.', NULL, '125 - 200 mg. o.d. with Butter milk/Lemon juice and honey or as directed by the physician.', '5g., 10 g.', NULL, 5, 1, 'KANTHA SINDOORAM', NULL, 'KANTHA SINDOORAM', 1, 11, '2019-03-11 14:49:47', '2019-08-09 17:46:39'),
(152, 'SUKUMARA RASAYANAM', 'sukumara-rasayanam', '5d2ef55238f161563358546.jpg', NULL, 5, 'H20', 0, 0, '31', 'Boerrhavia diffusa, Dasamoolam, Holostemma ada-kodien, Withania somnifera, Ricinus communis, Asparagus racemosus, Thrinapanchamoolam, Sphaeranthus indicus, Raisins, Jaggery, Cow ghee, Castor oil, Glycyrrhiza glabra, Cuminum cyminum, Rock salt, Piper spp., Dried ginger.', 'Menstrual irregularities and other Uterine disorders. Recommended in Hydrocele, Urinary disorders, Hyperacidity. Renders physical strength, and good complexion. Proven remedy for Sterility of both sex.', NULL, '5 - 15 g. b.d., or as directed by the physician.', '200 g, 500 g.', NULL, 5, 1, 'SUKUMARA RASAYANAM', NULL, 'SUKUMARA RASAYANAM', 1, 23, '2019-03-11 14:50:40', '2019-08-09 17:25:53'),
(153, 'PANCHAMLA THAILAM', 'panchamla-thailam', '5d43e2ee1c3c01564730094.jpg', NULL, 7, 'TC8', 0, 0, '15', 'Leaves of Ziziphus jujuba, Punica granatum, Garcinia morella, Tamarindus \r\nindica, Rheum emodi, Gingelly oil, Curd.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'For external application over the affected area.', 5, 1, 'PANCHAMLA THAILAM', NULL, 'PANCHAMLA THAILAM', 1, 9, '2019-03-11 14:53:38', '2019-08-13 17:47:14'),
(155, 'PARANTHYADI THAILAM', 'paranthyadi-thailam', '5d43e32ef25e11564730158.jpg', NULL, 7, 'T32', 0, 0, '46', 'Ixora coccinea, Salacia fruiticosa, Ksheerivriksha (stem), Coconut oil, Lemon,\r\nBacopa monnieri, Withania somnifera, Curcuma longa, Aristolochia indica,\r\nThriphala, Thrikatu, Opium, Camphor, Blue vitriol.', 'Snake  bite  and  Poisonous  insect  bites,  Chronic  skin  diseases,  Scabies, \r\nItching, Erysepelas.', NULL, NULL, '200 ml., 450 ml.', 'For external application only.', 5, 1, 'PARANTHYADI THAILAM', NULL, 'PARANTHYADI THAILAM', 1, 7, '2019-03-11 14:56:56', '2019-08-13 17:47:40'),
(156, 'LOHA SINDOORAM', 'loha-sindooram', '5d43df1f47c8b1564729119.jpg', NULL, 2, 'B10', 0, 0, NULL, 'Ferrum (processed).', 'Anaemia, Jaundice, Worm infestation, Obesity, Diabetes, Splenic disorders, Dropsy, Skin disorders, Ascites, Hyperacidity, Peptic ulcer.', NULL, '125 - 200 mg. b.d. with honey/ ghee/ Lemon juice/Thriphala kashayam/ Thriphala choornam/ Thrikatu choornam or as directed by the physician.', '5g., 10 g.', NULL, 5, 1, 'LOHA SINDOORAM', NULL, 'LOHA SINDOORAM', 1, 15, '2019-03-11 15:00:01', '2019-08-09 17:46:25'),
(157, 'PARINATHAKERIKSHEERA THAILAM', 'parinathakeriksheera-thailam', '5d43e3896e5bd1564730249.jpg', NULL, 7, 'T31', 0, 0, '5', 'Lemon, Coconut milk, Curcuma longa, Yellow resin, Gingelly oil.', 'Brachial palsy.', NULL, NULL, '200ml., 450ml.', 'For external application only, followed with fomentation, gives more relief.', 5, 1, 'PARINATHAKERIKSHEERA THAILAM', NULL, 'PARINATHAKERIKSHEERA THAILAM', 1, 5, '2019-03-11 15:00:22', '2019-08-09 12:50:50'),
(158, 'PINDATHAILAM', 'pindathailam', '5d43e3a24a3cf1564730274.jpg', NULL, 7, 'T33', 0, 0, '5', 'Hemidesmus indicus, Rubia cordifolia, Yellow resin, Bee wax, Gingelly oil.', 'Rheumatoid arthritis and related symptoms like burning sensation, swelling \r\nand redness.', NULL, NULL, '200 ml., 450 ml.', 'For external application over body.', 5, 1, 'PINDATHAILAM', NULL, 'PINDATHAILAM', 1, 11, '2019-03-11 15:03:47', '2019-08-09 12:51:26'),
(159, 'MANDOORA BHASMAM', 'mandoora-bhasmam', '', NULL, 2, 'B16', 0, 0, NULL, 'Iron rust (processed).', 'Splenic disorders, Hepatomegaly, Cirrhosis, Jaundice, Dropsy, Anaemia.', NULL, '125 - 200 mg. b.d. with honey/Thriphala kashayam/juice of Boerrhavia diffusa, Lemon juice or as directed by the physician.', '10g.', NULL, 5, 1, 'MANDOORA BHASMAM', NULL, 'MANDOORA BHASMAM', 1, 7, '2019-03-11 15:06:16', '2019-08-09 17:47:28'),
(160, 'PIPPALYADYANUVASANA THAILAM', 'pippalyadyanuvasana-thailam', '', NULL, 7, 'T34', 0, 0, '13', 'Piper longum, Randia dumetorum, Aegle marmelos, Acorus calamus,\r\nPlumbago rosea, Gingelly oil, Cow milk.', 'Piles  and  related  symptoms  like  straining  while  defaecation,  nervous \r\nweakness,  lassitude,  swelling  over  inguinal  regions,  swelling  at  rectum, \r\ncatarrh, constipation, flatulence.', NULL, NULL, '200 ml., 450 ml.', 'For unctuous enema. (Anuvasana Vasthi)', 5, 1, 'PIPPALYADYANUVASANA THAILAM', NULL, 'PIPPALYADYANUVASANA THAILAM', 1, 9, '2019-03-11 15:09:00', '2019-08-13 17:50:11'),
(161, 'PANAVIRALADI BHASMAM', 'panaviraladi-bhasmam', '5d43e29bc61441564730011.jpg', NULL, 2, 'B12', 0, 0, '4', 'Borassus flabellifer, Achyranthus aspera, Hygrophila auriculata, Musa paradisiaca', 'Inflammatoryconditions', NULL, 'Along with buttermilk / rice gruel or as directed by physician', '50g', NULL, 5, 1, 'PANAVIRALADI BHASMAM', NULL, 'PANAVIRALADI BHASMAM', 1, 11, '2019-03-11 15:14:18', '2019-08-09 17:47:54'),
(162, 'PRABHANJANAVIMARDANAM KUZHAMBU', 'prabhanjanavimardanam-kuzhambu', '', NULL, 7, 'T35', 0, 0, '37', 'Sida cordifolia, Moringa oleifera, Calotropis gigantea, Strobilanthes ciliatus, \r\nRicinus  communis,  Merrimia  tridentata,  Gingelly  oil,  Castor  oil,  Cow  ghee, \r\nCow milk, Curd, Peucedanum graveolens, Rock salt, Alpinia calcarata.', 'All kinds of rheumatic diseases and Arthritis. Especially for Lumbago, Sciatica, \r\nLumbar spondylosis, Low back-ache.', NULL, NULL, '200 ml., 450 ml.', 'For external application. Can be used for ‘Dhara’, ‘Pizhichil’, ‘Vasthi’ etc.', 5, 1, 'PRABHANJANAVIMARDANAM KUZHAMBU', NULL, 'PRABHANJANAVIMARDANAM KUZHAMBU', 1, 7, '2019-03-11 15:15:43', '2019-08-13 15:53:24'),
(163, 'PRASARANYADI THAILAM (CHERUTHU)', 'prasaranyadi-thailam-cheruthu', '', NULL, 7, 'T78', 0, 0, '17', 'Merrimia tridentata, Curd, Gingelly oil, Cow milk, Rock salt, Acorus calamus, \r\nAlpinia calcarata.', 'Brachial palsy, Lassitude over extremities etc.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'PRASARANYADI THAILAM (CHERUTHU)', NULL, 'PRASARANYADI THAILAM (CHERUTHU)', 1, 6, '2019-03-11 15:20:49', '2019-08-13 15:53:43'),
(164, 'PRAVALA BHASMAM', 'pravala-bhasmam', '5d43e41358f0d1564730387.jpg', NULL, 2, 'B9', 0, 0, NULL, 'Red coral (processed)', 'Dropsy, Respiratory diseases, Cardiac disorders like palpitation and tachycardia, Dysuria, Osteomyelitis.', NULL, '125-250 mg. b.d. with rice wash/honey/milk/ghee etc. as directed by the physician.', '5 g., 10 g.', NULL, 5, 1, 'PRAVALA BHASMAM', NULL, 'PRAVALA BHASMAM', 1, 13, '2019-03-11 15:21:28', '2019-08-09 17:48:23'),
(165, 'ASHTAKSHARI GULIKA (TABLET)', 'ashtakshari-gulika-tablet', '', NULL, 3, 'P2', 0, 0, '15', 'Opium (purified), Santalum album, Cinnabar (processed), Cyperus rotundus,\r\nPiper longum, Dried ginger, Ferula asafoetida', 'Sprue, Dysentery and Digestive disorders.', NULL, NULL, '100 Nos', '½-1 tablet b.d., before food with Honey or as directed by the physician.', 5, 1, 'ASHTAKSHARI GULIKA (TABLET)', NULL, 'ASHTAKSHARI GULIKA (TABLET)', 1, 32, '2019-03-11 15:22:05', '2019-08-13 14:10:48'),
(166, 'PRASARANYADI THAILAM (VALUTHU)', 'prasaranyadi-thailam-valuthu', '', NULL, 7, 'T82', 0, 0, '36', 'Merrimia  tridentata,  Strobilanthes  ciliatus,  Sida  cordifolia,  Rubia  cordifolia, \r\nCedrus deodara, Withania somnifera, Gingelly oil, Curd, Mutton soup, Cow \r\nmilk.', 'Brachial  palsy,  Lassitude,  Pain  and  Nervous  weakness  of  extremities. \r\nEnhances movements of extremities after the attack of Hemiplegia & diseases, \r\nrelated to bone.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'PRASARANYADI THAILAM (VALUTHU)', NULL, 'PRASARANYADI THAILAM (VALUTHU)', 1, 6, '2019-03-11 15:22:39', '2019-08-13 15:54:00'),
(167, 'RASNADI THAILAM', 'rasnadi-thailam', '5d43e4a36199e1564730531.jpg', NULL, 7, 'T43', 0, 0, '17', 'Alpinia calcarata, Dasamoolam, Glycyrrhiza glabra, Sida cordifolia, Gingelly \r\noil, Cow milk.', 'Arthritis, Facial palsy, Lumbago, Low back-ache.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'RASNADI THAILAM', NULL, 'RASNADI THAILAM', 1, 12, '2019-03-11 15:25:00', '2019-08-13 15:54:19'),
(168, 'RASOTHAMADI LEPAM', 'rasothamadi-lepam', '', NULL, 7, 'T66', 0, 0, '11', 'Mercury,  Sulphur,  Cuminum  cyminum,  Carum  carvi,  Curcuma  longa, \r\nCoscinium  fenestratum,  Piper  nigrum, Arsenic,  Cinnabar,  Coconut  oil,  Bee \r\nwax.', 'Crack foot, Keratitis, Chilblain. Also recommended in Skin diseases.', NULL, NULL, '50 g., 500 g.', 'For external application.', 5, 1, 'RASOTHAMADI LEPAM', NULL, 'RASOTHAMADI LEPAM', 1, 9, '2019-03-11 15:30:30', '2019-08-13 15:54:35'),
(169, 'SAHACHARADI KUZHAMBU', 'sahacharadi-kuzhambu', '', NULL, 7, 'TX2', 0, 0, '34', 'Strobilanthes ciliatus, Dasamoolam, Asparagus racemosus, Asphaltum,\r\nGingelly Oil, Castor Oil, Cow Ghee.', 'Contractions, Varicose vein, Tremors, Hemiplegia, Muscular pain, Weakness, \r\nConvulsions.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'SAHACHARADI KUZHAMBU', NULL, 'SAHACHARADI KUZHAMBU', 1, 10, '2019-03-11 15:34:07', '2019-08-13 17:53:02'),
(170, 'CHANDANADIVARTHI', 'chandanadivarthi', '5c94b7fcaf2491553250300.jpg', NULL, 3, 'P15', 0, 0, '8', 'Santalum album, Pterocarpus marsupium, Coccus lacca, Strychnos potatorum,\r\nPicrorrhiza curroa, Jasminum bud, Camphor, Red ochre.', 'Redness, Itching and Inflammation in eyes. Also recommended in Watering of\r\neyes, Conjunctivitis.', NULL, NULL, '50 Nos', 'For ophthalmic application only. May be applied with rose water or breast\r\nmilk', 5, 1, 'CHANDANADIVARTHI', NULL, 'CHANDANADIVARTHI', 1, 27, '2019-03-11 15:34:22', '2019-08-13 12:28:53'),
(171, 'ABHAYA CHOORNAM', 'abhaya-choornam', '', NULL, 26, 'C20', 0, 0, NULL, 'Terminalia chebula.', 'Dropsy, Eye diseases, Constipation, Flatulence, Bronchitis, Rejuvinates the body.', NULL, '5 -15 g. with luke warm water/Honey or as directed by the physician.', '50 g, 500 g.', NULL, 5, 1, 'ABHAYA CHOORNAM', NULL, 'ABHAYA CHOORNAM', 1, 40, '2019-03-11 15:35:49', '2019-08-13 15:29:03'),
(172, 'ABHAYADI CHOORNAM', 'abhayadi-choornam', '', NULL, 25, 'C21', 0, 0, '13', 'Terminalia chebula, Piper longum, Raisins, Acorus calamus.', 'Constipation, Piles, Indigestion, Flatulence.', NULL, '5 -15 g. before food with luke warm water or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'ABHAYADI CHOORNAM', NULL, 'ABHAYADI CHOORNAM', 1, 32, '2019-03-11 15:42:53', '2019-08-13 13:42:47'),
(173, 'SAHACHARADI THAILAM', 'sahacharadi-thailam', '5d43e503eba1c1564730627.jpg', NULL, 7, 'T53', 0, 0, '32', 'Strobilanthes ciliatus,. Dasamoolam, Asparagus racemosus, Asphaltum,\r\nGingelly oil, Cow milk', 'Contractions, Varicose vein, Tremors, Hemiplegia, Muscular pain, Weakness, \r\nConvulsions.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'SAHACHARADI THAILAM', NULL, 'SAHACHARADI THAILAM', 1, 8, '2019-03-11 15:47:37', '2019-08-13 17:53:27'),
(175, 'SATHAHWADI KUZHAMBU', 'sathahwadi-kuzhambu', '5d43e64a1f4dc1564730954.jpg', NULL, 7, 'T49', 0, 0, '7', 'Peucedanum graveolens, Trigonella foenum-graecum, Rubia cordifolia, Sida \r\ncordifolia, Gingelly oil.', 'Rheumatic complaints.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'SATHAHWADI KUZHAMBU', NULL, 'SATHAHWADI KUZHAMBU', 1, 11, '2019-03-11 15:52:43', '2019-08-13 17:53:53'),
(176, 'AGNIMUKHA CHOORNAM', 'agnimukha-choornam', '', NULL, 25, 'C3', 0, 0, '10', 'Myristica fragrans, Panchakolam, Elettaria cardamomum, Zingiber officinales, Cow ghee, Sugarcandy.', 'Anorexia, Nausea, Loss of appetite, Vomitting, Indigestion, Cough, Flatulence.', NULL, '5 - 15g. with luke warm water or as directed by the physician.', '50 g, 500 g.', NULL, 5, 1, 'AGNIMUKHA CHOORNAM', NULL, 'AGNIMUKHA CHOORNAM', 1, 20, '2019-03-11 15:53:00', '2019-08-13 13:43:09'),
(177, 'AMAYA CHOORNAM', 'amaya-choornam', '', NULL, 26, 'C24', 0, 0, '1', 'Saussurea lappa', 'Cough, Bronchial diseases, Skin diseases, Redness and swelling due to rheumatic complaints', NULL, '0.2-1.0g', '50g,500g', NULL, 5, 1, 'AMAYA CHOORNAM', NULL, 'AMAYA CHOORNAM', 1, 21, '2019-03-11 16:01:47', '2019-08-13 14:21:04'),
(178, 'SUDDHADOORVADI VELICHENNA', 'suddhadoorvadi-velichenna', '5d43e70b574db1564731147.jpg', NULL, 7, 'T52', 0, 0, '19', 'Cynodon dactylon, Glycyrrhiza glabra, Coconut oil.', 'Chronic wounds, Skin diseases.', NULL, NULL, '200 ml., 450 ml.', 'For external application.', 5, 1, 'SUDDHADOORVADI VELICHENNA', NULL, 'SUDDHADOORVADI VELICHENNA', 1, 12, '2019-03-11 16:26:15', '2019-08-13 17:54:44'),
(179, 'SUDDHABALA THAILAM', 'suddhabala-thailam', '', NULL, 7, 'T51', 0, 0, '3', 'Sida cordifolia, Gingelly oil. Cow milk.', 'Rheumatic complaints, Eye diseases.', NULL, NULL, '200 ml., 450 ml.', 'For external application over foot, head and body, good for pregnant ladies &\r\nPost natal case.', 5, 1, 'SUDDHABALA THAILAM', NULL, 'SUDDHABALA THAILAM', 1, 6, '2019-03-11 16:29:09', '2019-08-13 17:54:21'),
(180, 'SURASADI THAILAM', 'surasadi-thailam', '5d2ef5ce4c4d41563358670.jpg', NULL, 7, 'T59', 0, 0, '19', 'Ocimum sanctum, Ocimum caryophyllatum, Ocimum basilicum, Embelia\r\nribes, Achyranthes aspera, Gmelina arborea, Pseudarthria viscida, Solanum\r\nnigrum, Coconut oil.', 'Sinusitis, Bronchial diseases, Cough, Chronic wounds.', NULL, NULL, '200 ml., 450 ml.', 'For external application on head.', 5, 1, 'SURASADI THAILAM', NULL, 'SURASADI THAILAM', 1, 10, '2019-03-11 16:31:24', '2019-08-13 17:55:10'),
(181, 'THEKARAJA THAILAM', 'thekaraja-thailam', '', NULL, 7, 'T83', 0, 0, '3', 'Eclipta alba, Terminalia chebula, Gingelly oil.', 'Asthma, Cough, Bronchitis, Headache.', NULL, NULL, '200 ml., 450 ml.', 'For external application overhead.', 5, 1, 'THEKARAJA THAILAM', NULL, 'THEKARAJA THAILAM', 1, 9, '2019-03-11 16:33:21', '2019-08-13 17:55:30'),
(183, 'AMRUTHADI CHOORNAM', 'amruthadi-choornam', '', NULL, 25, 'C72', 0, 0, '5', 'Tinospora cordifolia, Dried ginger, Tribulus terrestris, Sphaeranthus indicus,\r\nCrataeva religiosa', 'Rheumatic fever, Hiccup, Breathing apnoea.', NULL, '5 -15 g. b.d. before food along with whey or rice wash or as directed by the\r\nphysician.', '50 g., 500 g.', NULL, 5, 1, 'AMRUTHADI CHOORNAM', NULL, 'AMRUTHADI CHOORNAM', 1, 18, '2019-03-11 16:44:13', '2019-08-13 13:44:07'),
(184, 'THRIPHALADI VELICHENNA', 'thriphaladi-velichenna', '5d43f35bcae341564734299.jpg', NULL, 7, 'T91', 0, 0, '37', 'Thriphala, Tinospora cordifolia, Pterocarpus marsupium, Emblica officinalis, \r\nEclipta alba, Withania somnifera, Kaempferia galanga, Galena, Coconut oil, \r\nCow milk.', 'Diseases above neck. Specially indicated in Eye diseases. Relieves Headache,Sinusitis, Rhinitis, Otitis media, premature greying etc.', NULL, NULL, '200 ml., 450 ml', 'For external application overhead.', 5, 1, 'THRIPHALADI VELICHENNA', NULL, 'THRIPHALADI VELICHENNA', 1, 6, '2019-03-11 16:44:34', '2019-08-13 17:56:06'),
(185, 'THRIPHALADI THAILAM', 'thriphaladi-thailam', '', NULL, 7, 'T21', 0, 0, '37', 'Thriphala, Tinospora cordifolia, Pterocarpus marsupium, Emblica officinalis, \r\nEclipta alba, Withania somnifera, Kaempferia galanga, Galena, Gingelly oil, \r\nCow milk.', 'Diseases above the neck. Specially indicated in Eye diseases. Relieves Headache, \r\nSinusitis, Rhinitis, Otitis media, premature greying etc.', NULL, NULL, '200 ml., 450 ml.', 'For external application overhead.', 5, 1, 'THRIPHALADI THAILAM', NULL, 'THRIPHALADI THAILAM', 1, 5, '2019-03-11 16:46:36', '2019-08-13 17:55:49'),
(186, 'VACHALASUNADI THAILAM', 'vachalasunadi-thailam', '5d43f3992a9991564734361.jpg', NULL, 7, 'T46', 0, 0, '5', 'Acorus calamus, Allium sativum, Curcuma longa, Gingelly oil, Aegle\r\nmarmelos.', 'Deafness, Otorrhoea, Otitis media and other ear diseases.', NULL, NULL, '200 ml., 450 ml.', 'Two drops in ear and to apply over head. Lukewarm oil may be used.', 5, 1, 'VACHALASUNADI THAILAM', NULL, 'VACHALASUNADI THAILAM', 1, 7, '2019-03-11 16:48:21', '2019-08-13 17:56:34'),
(187, 'VAJRAKA THAILAM', 'vajraka-thailam', '5d43f3b2da6971564734386.jpg', NULL, 7, 'T45', 0, 0, '22', 'Alstonia scholaris, Albizzia lebbeck, Nerium odorum, Azadirachta indica,\r\nThriphala, Thrikatu, Gingelly oil, Cow urine.', 'Skin disorders like Psoriasis, Dry excema, Dermatitis etc. Also recommended in Fistula.', NULL, NULL, '200 ml., 450 ml.', 'For external application over the affected area.', 5, 1, 'VAJRAKA THAILAM', NULL, 'VAJRAKA THAILAM', 1, 8, '2019-03-11 16:50:34', '2019-08-13 17:57:02'),
(189, 'ASANA CHOORNAM', 'asana-choornam', '', NULL, 26, 'C130', 0, 0, '1', 'Pterocarpus marsupium', 'worm infestation, diabetes, skin diseases, excess fat, anaemia.', NULL, '50-100 g of the drug for decoctionor as directed by the physician', NULL, NULL, 5, 1, 'ASANA CHOORNAM', NULL, 'ASANA CHOORNAM', 1, 17, '2019-03-11 16:56:09', '2019-08-13 15:55:59'),
(190, 'VATHAMARDANAM KUZHAMBU', 'vathamardanam-kuzhambu', '5d43f3e96a34c1564734441.jpg', NULL, 7, 'T90', 0, 0, '11', 'Gingelly oil, Castor oil, Coconut oil, Peucedanum graveolens, Rubia cordifolia, \r\nTrigonella foenum-graecum, Brassica nigra, Bee wax, Yellow resin, Camphor, \r\nDhanyamlam.', 'Contractions, Cramp, Numbness, Lassitude, Sprain and other Rheumatic\r\ncomplaints.', NULL, NULL, '200 ml., 450 ml.', 'For external application over the affected area with wet/dry fomentation.', 5, 1, 'VATHAMARDANAM KUZHAMBU', NULL, 'VATHAMARDANAM KUZHAMBU', 1, 7, '2019-03-11 16:56:27', '2019-08-13 17:57:33'),
(191, 'VATHAVIDHWAMSINI THAILAM', 'vathavidhwamsini-thailam', '5d43f4094d3161564734473.jpg', NULL, 7, 'T47', 0, 0, '16', 'Strobilanthes ciliatus, Sida cordifolia, Tinospora cordifolia, Alpinia calcarata,\r\nWithania somnifera, Gingelly oil.', 'Rheumatic diseases', NULL, NULL, '200 ml., 450 ml.', 'For external application over the affected area with wet/dry fomentation.', 5, 1, 'VATHAVIDHWAMSINI THAILAM', NULL, 'VATHAVIDHWAMSINI THAILAM', 1, 8, '2019-03-11 16:59:29', '2019-08-13 17:58:04'),
(192, 'BRAHMI GHRUTHAM', 'brahmi-ghrutham', '5d2edc4abbded1563352138.jpg', NULL, 22, 'G17', 0, 0, '12', 'Bacopa monnieri, Thrikatu, Merrimia turpethum, Baliospermum montanum, Clitoria ternatea, Cassia fistula, Embelia ribes, Cow ghee.', 'Mental disorders like Phobia, Mania, Depression, Insanity. Improves memory power, I.Q. Also recommended in Stammering, Lack of concentration, Bedwetting, Epilepsy.', NULL, '10-20 ml. b.d., before food or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'BRAHMI GHRUTHAM', NULL, 'BRAHMI GHRUTHAM', 1, 32, '2019-03-11 17:05:30', '2019-08-09 16:08:52'),
(193, 'ASHTACHOORNAM', 'ashtachoornam-2', '5d5bbf9964c0f1566293913.png', NULL, 25, 'C24', 0, 0, '8', 'Thrikatu, Apium graveolens, Rock salt, Cuminum cyminum, Carum carvi, Ferula asafoetida.', 'Colic, Loss of appetite, Dyspepsia, Anorexia, Sprue, Worm infestation.', NULL, '5 -15 g. before food with honey/buttermilk or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'ASHTACHOORNAM', NULL, 'ASHTACHOORNAM', 1, 18, '2019-03-11 17:06:29', '2019-08-20 15:08:33'),
(194, 'BRAHMIKALLYANAKA GHRUTHAM', 'brahmikallyanaka-ghrutham', '5d2edd242f8411563352356.jpg', NULL, 22, 'G30', 0, 0, '38', 'Bacopa  monnieri, Thrikatu,  Merrimia  turpethum,  Baliospermum  montanum,  Clitoria ternatea, Cassia fistula, Embelia ribes, Thriphala, Citrullus colocynthis,  Elettaria  cardamomum,  Hemidesmus  indicus,  Desmodium  gangeticum, Punica granatum, Caesalpinia sappan, Santalum album, Cow ghee.', 'Phobia,  Anxiety  neurosis,  Epilepsy,  Insanity  and  other  mental  disorders.  \r\nImproves the centres of memory, intelligence etc.', NULL, '10-20 ml. b.d., or as directed by the physician', '200 ml., 450 ml.', NULL, 5, 1, 'BRAHMIKALLYANAKA GHRUTHAM', NULL, 'BRAHMIKALLYANAKA GHRUTHAM', 1, 19, '2019-03-11 17:12:03', '2019-08-09 16:10:02'),
(195, 'DADIMADI GHRUTHAM', 'dadimadi-ghrutham', '5d2ee40a6163e1563354122.jpg', NULL, 22, 'G7', 0, 0, '6', 'Punica granatum, Coriandrum sativum, Plumbago rosea, Dried ginger, Piper  longum, Cow ghee.', 'Anaemia, Malabsorption, Anorexia, Hyperacidity, Indigestion. Renders health and strength. Also recommended in Infertility and Antenatal care.', NULL, '10 - 20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'DADIMADI GHRUTHAM', NULL, 'DADIMADI GHRUTHAM', 1, 15, '2019-03-11 17:14:55', '2019-08-09 16:11:14'),
(196, 'DHANWANTHARA GHRUTHAM', 'dhanwanthara-ghrutham', '5d2ee54736a5e1563354439.jpg', NULL, 22, 'G10', 0, 0, '39', 'Dasamoolam,  Kaempferia  galanga,  Boerrhavia  diffusa,  Hordeum  vulgare,  Pongamia pinnata, Acorus calamus, Merrimia turpethum, Embelia ribes, Cow  ghee.', 'A  preventive  of  neuropathy  and  carbuncles  in  Diabetic  patients.  Also recommended in Anaemia, Ascites, Dropsy, Piles, Wasting etc.', NULL, '10 - 20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'DHANWANTHARA GHRUTHAM', NULL, 'DHANWANTHARA GHRUTHAM', 1, 17, '2019-03-11 17:19:39', '2019-08-09 16:12:16'),
(197, 'ASWAGANDHA CHOORNAM', 'aswagandha-choornam', '', NULL, 26, 'C22', 0, 0, NULL, 'Withania somnifera', 'Consumption, General debility, Loss of libido. Also recommended to regain body strength and power.', NULL, '5 - 15 g. preferably with milk at bed time or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'ASWAGANDHA CHOORNAM', NULL, 'ASWAGANDHA CHOORNAM', 1, 17, '2019-03-11 17:20:04', '2019-08-13 15:30:40'),
(198, 'DHATHRYADI GHRUTHAM', 'dhathryadi-ghrutham', '5d2ee56d9469f1563354477.jpg', NULL, 22, 'G35', 0, 0, '11', 'Emblica officinalis, Ipomoea mauritiana, Saccharum officinaram, Asparagus \r\nracemosus, Benincasa hispida, Raisins, Glycyrrhiza glabra, Santalum album, \r\nCow ghee, Cow milk, Sugar.', 'Menorrhagia, Leucorrhoea, Anaemia, Epistaxis, Fainting, Insanity, Alcoholism. \r\nSpecial indication to female sterility.', NULL, '10 - 20 ml. b.d., followed by milk or as directed by the physician.', '200 ml.', NULL, 5, 1, 'DHATHRYADI GHRUTHAM', NULL, 'DHATHRYADI GHRUTHAM', 1, 10, '2019-03-11 17:22:22', '2019-08-09 15:09:06'),
(199, 'DURVA GHRUTHAM', 'durva-ghrutham', '5d2ee5f6b3d2e1563354614.jpg', NULL, 22, 'G11', 0, 0, '4', 'Cynodon dactylon, Glycyrrhiza glabra, Cow ghee, Cow milk.', 'Eye diseases, Wounds related to eyes.', NULL, 'An ophthalmic application only.', '200 ml., 450 ml.', NULL, 5, 1, 'DURVA GHRUTHAM', NULL, 'DURVA GHRUTHAM', 1, 17, '2019-03-11 17:29:17', '2019-08-09 15:09:46'),
(200, 'GULGULUTHIKTHAKAM GHRUTHAM', 'gulguluthikthakam-ghrutham', '', NULL, 22, 'G4', 0, 0, '32', 'Commiphora  wightii,  Azadirachta  indica,  Tinospora  cordifolia,  Adhatoda \r\nbeddomei, Trichosanthes anguina, Solanum melongena, var.insanum, Cyclea \r\npeltata,  Embelia  ribes,  Curcuma  longa,  Alpinia  calcarata,  Aconitum \r\nheterophyllum, Cow ghee.', 'Rheumatic complaints, Rheumatoid arthritis, Chronic skin diseases, Wounds, \r\nVitiligo,  Anal  fistula,  Nasal  catarrh,  Chronic  sinusitis,  Sinuses,  Scabies, \r\nRheumatic boils, Carbuncles etc.', NULL, '10 - 20 ml. b.d., in empty stomach or as directed by the physician.', '200 ml., 450 ml', NULL, 5, 1, 'GULGULUTHIKTHAKAM GHRUTHAM', NULL, 'GULGULUTHIKTHAKAM GHRUTHAM', 1, 18, '2019-03-11 17:32:16', '2019-08-09 15:11:15'),
(201, 'AVIPATHI CHOORNAM', 'avipathi-choornam', '', NULL, 25, 'C1', 0, 0, '11', 'Thrikatu, Thrijathakam, Cyperus rotundus, Embelia ribes, Emblica officinalis, Merrimia turpethum, Sugar.', 'Safe purgative in bilious disorders like scabies, urticaria, allergic conditions and effective in spider and rat poison.', NULL, '5 - 15 g. with luke warm water in empty stomach or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'AVIPATHI CHOORNAM', NULL, 'AVIPATHI CHOORNAM', 1, 13, '2019-03-11 17:33:33', '2019-08-13 13:52:21'),
(202, 'INDUKANTHAM GHRUTHAM', 'indukantham-ghrutham', '5d2ee89476fb11563355284.jpg', NULL, 22, 'G1', 0, 0, '20', 'Holoptelia integrifolia, Cedrus deodara, Dasamoolam, Shadpalam, Cow ghee, \r\nCow milk', 'Anorexia, Colic, Intermittent fever. Promotes health and strength by improving \r\nmetabolism, Ascitis.', NULL, '10 - 20 ml. b.d., in empty stomach or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'INDUKANTHAM GHRUTHAM', NULL, 'INDUKANTHAM GHRUTHAM', 1, 10, '2019-03-11 17:35:35', '2019-08-09 15:12:27'),
(203, 'JATHYADI GHRUTHAM', 'jathyadi-ghrutham', '5d2ee9afb8b111563355567.jpg', NULL, 22, 'G5', 0, 0, '13', 'Jasminum  grandiflorum,  Oldenlandia  corymbosa,  Cyathula  prostrata,  Vitex negundo,  Curcuma  longa,  Coscinium  fenestratum,  Azadirachta  indica, Glycyrrhiza glabra, Cow ghee.', 'Chronic wounds and Ulcers, Wounds on vital points. Cleanses and heals the wounds. Also recommended in burns.', NULL, 'For external application only as Jathyadi thailam.', '200 ml., 450 ml.', NULL, 5, 1, 'JATHYADI GHRUTHAM', NULL, 'JATHYADI GHRUTHAM', 1, 11, '2019-03-11 17:43:11', '2019-08-09 16:13:15'),
(204, 'JEEVANTHYADI GHRUTHAM', 'jeevanthyadi-ghrutham', '5c94b0c2f2ac01553248450.jpg', NULL, 22, 'G6', 0, 0, '16', 'Holostemma  ada-Kodien,  Withania  somnifera,  Piper  nigrum,  Raisins,  Thriphala, Cow ghee, Cow milk.', 'Cataract. Recommended in biliary ailments. Renders vitality.', NULL, 'Internally 10-20 ml. at bed time with Varachoornam as additive. Externally can be used for ‘Tharpanam’ in eyes.', '200 ml., 450 ml.', NULL, 5, 1, 'JEEVANTHYADI GHRUTHAM', NULL, 'JEEVANTHYADI GHRUTHAM', 1, 12, '2019-03-11 17:48:21', '2019-08-09 16:13:43'),
(205, 'KALLYANAKA GHRUTHAM', 'kallyanaka-ghrutham', '', NULL, 22, 'G2', 0, 0, '29', 'Thriphala, Citrullus colocynthis, Curcuma longa, Desmodium gangeticum, Embelia ribes, Punica granatum, Baliospermum montanum, Santalum album, Cow ghee.', 'Epilepsy,  Insanity,  Delusions,  Secondary  amenorrhoea,  Dysmenorrhoea,  Oligospermia, Emaciation, Loss of memory power, Loss of retension, Arthritis, Infertility  of  both  sex,  Intermittent  fever,  Dyspepsia.  Renders  health, complexion, vigour and vitality.', NULL, '10 - 20 ml. b.d., in empty stomach or as directed by the physician.', '200 ml., 450 ml', NULL, 5, 1, 'KALLYANAKA GHRUTHAM', NULL, 'KALLYANAKA GHRUTHAM', 1, 9, '2019-03-11 17:53:11', '2019-08-09 16:14:13'),
(206, 'KARASKARA GHRUTHAM', 'karaskara-ghrutham', '', NULL, 22, 'G3', 0, 0, '8', 'Strychnos Nuxvomica (Purified), Asparagus racemosus, Glycyrrhiza glabra, Sida cordifolia, Saccharum officinarum, Musa sapientum (Rhizome), Raisins, Cow ghee, Cow milk.', 'Rheumatic fever and associated symptoms.', NULL, '10-20 ml. b.d., in empty stomach or as directed by the physician.', '200 mi., 450 ml.', NULL, 5, 1, 'KARASKARA GHRUTHAM', NULL, 'KARASKARA GHRUTHAM', 1, 8, '2019-03-11 17:55:51', '2019-08-09 16:14:47'),
(207, 'AVIPATHIKARA CHOORNAM', 'avipathikara-choornam', '', NULL, 25, 'C121', 0, 0, '14', 'Zingiber officinale, Piper nigrum, Piper longum, Terminalia chebula, Emblica officinalis, Terminalia bellirica, Cyperus rotundus', 'Safe purgative in bilious disorders like scabies, urticaria, allergic Conditions and effective in spider and rat poison, Piles', NULL, '5-15g.with lukewarm water or as directed by the physician.', '10 g, 50g, 500g.', NULL, 5, 1, 'AVIPATHIKARA CHOORNAM', NULL, 'AVIPATHIKARA CHOORNAM', 1, 9, '2019-03-12 09:45:51', '2019-08-13 13:53:16'),
(208, 'THALEESAVATAKAM GRANULES', 'thaleesavatakam-granules', '5d2ef642e26961563358786.jpg', NULL, 5, 'H11E', 0, 0, '12', 'Abies spectabilis, Piper nigrum, Piper spp., Piper longurn, Piper officinarum, Dried ginger, Chathurjathakam, Vetiveria zizanioides, Sugar.', 'Sprue, Nausea, Vomitting, Fever, Anaemia, symptoms related with Alcoholism, Indigestion, Diarrhoea.', NULL, '5 -15 g. intermittently or b.d., with buttermilk/Arishta/honey or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'THALEESAVATAKAM GRANULES', NULL, 'THALEESAVATAKAM GRANULES', 1, 12, '2019-03-12 09:51:46', '2019-08-09 17:26:37');
INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(209, 'CHANDANA CHOORNAM', 'chandana-choornam', '', NULL, 26, 'C35', 0, 0, '1', 'Santalum album', 'Burning sensation, Polydipsia, leprosy, Skin disease, cardiac debility,\r\nDiarrhoea', NULL, '3-6g', '50g,500g', NULL, 5, 1, 'CHANDANA CHOORNAM', NULL, 'CHANDANA CHOORNAM', 1, 12, '2019-03-12 09:54:09', '2019-08-13 15:31:14'),
(210, 'VALIYAMADHUSNUHI RASAYANAM', 'valiyamadhusnuhi-rasayanam', '5d2ef7451f4dc1563359045.jpg', NULL, 5, 'H16', 0, 0, '32', 'Thrikatu, Thriphala, Chathurjathakam, Jeerakathrayam, Embelia ribes, Merrimia turpethum, Smilax chinensis (purified), Sulphur (Processed), Commiphora wightii (purified), Sugar, Cow ghee, Honey.', 'Skin diseases, Carbuncles, Fistula, Sinus, Piles, Scrofula, Sexually Transmitted Diseases.', NULL, '5 - 15 g. b.d., or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'VALIYAMADHUSNUHI RASAYANAM', NULL, 'VALIYAMADHUSNUHI RASAYANAM', 1, 17, '2019-03-12 09:54:57', '2019-08-09 17:27:14'),
(211, 'VILWADI LEHYAM', 'vilwadi-lehyam', '5c94beeb3fb011553252075.jpg', NULL, 5, 'H17', 0, 0, '11', 'Aegle marmelos, Jaggery, Cyperus rotundus, Coriandrum sativum, Cuminum cyminum, Thrijathakam, Thrikatu.', 'Vomitting, Anorexia, Cough, Asthma, Nausea, Digestive disorders.', NULL, '5 -15 g. b.d., or intermittently or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'VILWADI LEHYAM', NULL, 'VILWADI LEHYAM', 1, 25, '2019-03-12 09:58:19', '2019-08-09 17:28:07'),
(212, 'CHITRAKA CHOORNAM', 'chitraka-choornam-2', '', NULL, 26, 'C36', 0, 0, '1', 'Plumbago zeylanica', 'Dyspepsia, Inflammations, cough, bronchitis, leprosy, colic', NULL, '1-2g', '50g,500g', NULL, 5, 1, 'CHITRAKA CHOORNAM', NULL, 'CHITRAKA CHOORNAM', 1, 5, '2019-03-12 09:59:19', '2019-08-13 15:33:26'),
(213, 'VYOSHADI VATAKAM', 'vyoshadi-vatakam', '5d43f4ab472a81564734635.jpg', NULL, 5, 'H32', 0, 0, '13', 'Thrikatu, Plumbago rosea, Abies spectabilis, Piper officinarum, Tamarindus indica (rachis), Rheum emodi, Cumicnum cyminum, Thrijathakam, Jaggery.', 'Chronic sinusitis, Rhinitis, Anorexia, Loss of appetite, Cough and Cold.', NULL, '2 - 10 g. intermittently or as directed by the physician.', '50 g.', NULL, 5, 1, 'VYOSHADI VATAKAM', NULL, 'VYOSHADI VATAKAM', 1, 19, '2019-03-12 10:00:18', '2019-08-09 17:28:29'),
(214, 'VIDARYADI LEHYAM', 'vidaryadi-lehyam', '', NULL, 5, 'H37', 0, 0, '23', 'Pueraria tuberosa, Ricinus communis, Boerhavia diffusa, Cedrus deodara, Mucuna purieta, jaggery, cow’s ghee, honey etc.', 'for general immunity, in debility, body pain, breathing problems, cough etc.', NULL, NULL, NULL, '5-15 gm bd or as directed by physician.', 5, 1, 'VIDARYADI LEHYAM', NULL, 'VIDARYADI LEHYAM', 1, 22, '2019-03-12 10:02:30', '2019-08-09 17:27:40'),
(215, 'DADIMASHTAKACHOORNAM', 'dadimashtakachoornam', '', NULL, 25, 'C9', 0, 0, '14', 'Punica granatum, Curcuma angustifolia, Chathurjathakam, Cuminum cyminum, Coriandrum sativum, Apium graveolens, Piper officinarum, Thrikatu, Sugar.', 'Diarrhoea, Dysentery, Sprue, Colic, Nausea, Anorexia, Pthiasis.', NULL, '5 -15 g. b.d. before food with luke warm water/butter milk or as directed by the physician.', '50g, 500g.', NULL, 5, 1, 'DADIMASHTAKACHOORNAM', NULL, 'DADIMASHTAKACHOORNAM', 1, 5, '2019-03-12 10:05:48', '2019-08-13 13:53:48'),
(216, 'KADALI RASAYANAM', 'kadali-rasayanam', '5c94bc86bd9961553251462.jpg', NULL, 5, 'H40', 0, 0, '8', 'Curculigo orchioides, Musa paradisiaca, Withania somnifera, sugar, ghee etc.', 'Menstrual problems, emaciation, white discharge per vaginaetc.', NULL, '5-10 gm with milk or as directed by physician', '200 gm.', NULL, 5, 1, 'KADALI RASAYANAM', NULL, 'KADALI RASAYANAM', 1, 32, '2019-03-12 10:06:10', '2019-08-09 17:30:57'),
(217, 'DANTHI CHOORNAM', 'danthi-choornam', '', NULL, 26, 'C40', 0, 0, '1', 'Baliospermum montanum', 'Haemorrhoids', NULL, NULL, '50gm-500gm', '5 – 10gm along with honey/ buttermilk/ lukewarm water or as directed by the\r\nphysician', 5, 1, 'DANTHI CHOORNAM', NULL, 'DANTHI CHOORNAM', 1, 6, '2019-03-12 10:11:18', '2019-08-13 15:56:46'),
(218, 'ELADI CHOORNAM', 'eladi-choornam', '', NULL, 25, 'C126', 0, 0, '9', 'Elettaria cardamomum, Cinnamomum zeylanicum, Messua Nagassarium, Piper Iongum, Cyperus rotundus, Santalum album Ziziphus jujuba, Oriza sativa, Callicarpa macrophylla', 'Cough,Asthma,Vomitting', NULL, '2 to 4 g with honey, sugar or as directed by physician', '50g.', NULL, 5, 1, 'ELADI CHOORNAM', NULL, 'ELADI CHOORNAM', 1, 16, '2019-03-12 10:15:23', '2019-08-13 13:54:44'),
(219, 'AMRUTHOTHARAM KASHAYA SOOKSHMA CHOORNAM', 'amruthotharam-kashaya-sookshma-choornam', '', NULL, 4, 'K73A', 0, 0, '3', 'Tinospora Cordifolia, Terminalia chebula, Dried ginger', 'Fever, Flu, Cold & Headache. Especially in Rheumatic fever', NULL, NULL, '100g.', NULL, 5, 1, 'AMRUTHOTHARAM KASHAYA SOOKSHMA CHOORNAM', NULL, 'AMRUTHOTHARAM KASHAYA SOOKSHMA CHOORNAM', 1, 23, '2019-03-12 10:15:25', '2019-08-13 11:30:18'),
(220, 'AMRUTHADI KASHAYA SOOKSHMA CHOORNAM', 'amruthadi-kashaya-sookshma-choornam', '5c94ae09e28d71553247753.jpg', NULL, 4, 'K74A', 0, 0, '6', 'Tinospora cordifolia, Cyperus rotundus, Dried ginger, Andrographis paniculata, Ocimum sanctum, Vitex negundu', 'Fever, Flu, Bronchitis, Cough, Anorexia', NULL, NULL, '70g.', NULL, 5, 1, 'AMRUTHADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'AMRUTHADI KASHAYA SOOKSHMA CHOORNAM', 1, 42, '2019-03-12 10:17:33', '2019-08-13 11:30:07'),
(221, 'ELADIGANA CHOORNAM', 'eladigana-choornam', '', NULL, 25, 'C5', 0, 0, '29', 'Elettaria cardamomum, Amomum subulatum, Nardostachys jatamansi, Kaempferia galanga, Pearl oyster, Saffron, Commiphora wightii, Yellow resin.', 'Scabies, Itches, Dandruff. Improves complexion.', NULL, 'External application only.', '50 g., 500 g.', NULL, 5, 1, 'ELADIGANA CHOORNAM', NULL, 'ELADIGANA CHOORNAM', 1, 13, '2019-03-12 10:17:58', '2019-08-13 13:55:06'),
(222, 'DASAMOOLAKATUTRAYAM KASHAYA SOOKSHMA CHOORNAM', 'dasamoolakatutrayam-kashaya-sookshma-choornam', '', NULL, 4, 'K75A', 0, 0, '14', 'Dasamoolam, Trikatu, Adathoda beddomei', 'Respiratory disorders, Rhinitis, cold, Bodyache, Headache, Cough, Flu', NULL, NULL, '250g, 500g, l kg, 2.5kg', NULL, 5, 1, 'DASAMOOLAKATUTRAYAM KASHAYA SOOKSHMA CHOORNAM', NULL, 'DASAMOOLAKATUTRAYAM KASHAYA SOOKSHMA CHOORNAM', 1, 9, '2019-03-12 10:20:43', '2019-08-13 11:31:33'),
(223, 'GOKSHURA CHOORNAM', 'gokshura-choornam', '', NULL, 26, 'C34', 0, 0, NULL, 'Tribulus terrestris', 'Urinary infection, Haematuria, Incontinence of urine, Urolithiasis, Prostatitis,\r\nCystitis, Dropsy, Dysuria.', NULL, '5 - 15 g. with luke warm water/Tender coconut water/Bruhathyadi Kashayam or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'GOKSHURA CHOORNAM', NULL, 'GOKSHURA CHOORNAM', 1, 11, '2019-03-12 10:22:26', '2019-08-13 15:34:23'),
(224, 'GANDHARVAHASTADI KASHAYA SOOKSHMA CHOORNAM', 'gandharvahastadi-kashaya-sookshma-choornam', '', NULL, 4, 'K76A', 0, 0, '8', 'Ricinus communis, Holoptelia integrifolia, Plumbago rosea, Dried ginger, Terminalia chebula.', 'Rheumatic complaints, Flatulence, Loss of appetite, Constipation, Anorexia, \r\n Low back ache', NULL, NULL, '250g, 500g, l kg, 2.5kg', NULL, 5, 1, 'GANDHARVAHASTADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'GANDHARVAHASTADI KASHAYA SOOKSHMA CHOORNAM', 1, 10, '2019-03-12 10:26:52', '2019-08-13 11:31:47'),
(225, 'GRUHADHOOMADI LEPACHOORNAM', 'gruhadhoomadi-lepachoornam', '', NULL, 25, 'C19', 0, 0, '6', 'Soot, Acorus calamus, Saussurea lappa, Peucedanum graveolens, Curcuma longa, Coscinium fenestratum.', 'Swelling related with Rheumatic complaints and Rheumatoid arthritis.', NULL, 'For external application only. Boil with rice wash or juice of Moringa leaves or Tamarind leaves, into a paste form, apply in bearable warmth over affected area or.as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'GRUHADHOOMADI LEPACHOORNAM', NULL, 'GRUHADHOOMADI LEPACHOORNAM', 1, 11, '2019-03-12 10:27:38', '2019-08-13 13:55:38'),
(226, 'GULUCHYADI KASHAYA SOOKSHMA CHOORNAM', 'guluchyadi-kashaya-sookshma-choornam', '', NULL, 4, 'K77A', 0, 0, '5', 'Tinospora cordifolia, Caesalpinia sappan, Azadirachata indica, Coriandrum sativum, Pterocarpus santalinus Indication.', 'Fever, Vomitting, Burning sensation, Thirst, Skin disorders, Fainting due to excessive heat.', NULL, NULL, '100g., 70g.', NULL, 5, 1, 'GULUCHYADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'GULUCHYADI KASHAYA SOOKSHMA CHOORNAM', 1, 8, '2019-03-12 10:30:40', '2019-08-13 11:32:03'),
(227, 'GULGULUPANCHAPALAM CHOORNAM', 'gulgulupanchapalam-choornam', '', NULL, 25, 'C8', 0, 0, '7', 'Commiphora wightii (purified & processed), Piper longum, Thriphala, Cinnamomum zeylanicum, Elettaria cardamomum.', 'Skin diseases, Pthiasis, Fistula, Sinus, Chronic ulcers.', NULL, '5-15 g. with honey and ghee or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'GULGULUPANCHAPALAM CHOORNAM', NULL, 'GULGULUPANCHAPALAM CHOORNAM', 1, 13, '2019-03-12 10:30:53', '2019-08-13 13:56:11'),
(229, 'MAHARASNADI KASHAYA SOOKSHMA CHOORNAM', 'maharasnadi-kashaya-sookshma-choornam', '', NULL, 4, 'K78A', 0, 0, '26', 'Alpinia calcarata, Sida cordifolia, Ricinus communis, Acorus calamus, Boerrhavia diffusa, Tinospora cordifolia, Cassia fistula', 'Rheumatis diseases like Hemiplegia, Brachial palsy, Rheumatic fever', NULL, NULL, '250g, 500g, l kg, 2.5kg', NULL, 5, 1, 'MAHARASNADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'MAHARASNADI KASHAYA SOOKSHMA CHOORNAM', 1, 12, '2019-03-12 10:34:55', '2019-08-13 11:32:29'),
(230, 'PATHYADI KASHAYA SOOKSHMA CHOORNAM', 'pathyadi-kashaya-sookshma-choornam', '', NULL, 4, 'K79A', 0, 0, '10', 'Terminalia Chebula, Gmelina arborea, Dried ginger, Cyperus rotundus, Acorus calamus, Solanum indicum, Coriandrum sativum, Cedrus deodara, Clerodendrum serratum, Oldenlandia corymbosa', 'Bronchitis, Anorexia, Cough, Flu, Dryness of Mouth, Colic.', NULL, NULL, '70g.', NULL, 5, 1, 'PATHYADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'PATHYADI KASHAYA SOOKSHMA CHOORNAM', 1, 10, '2019-03-12 10:38:11', '2019-08-13 11:33:18'),
(231, 'PUNARNAVADI KASHAYA SOOKSHMA CHOORNAM', 'punarnavadi-kashaya-sookshma-choornam', '', NULL, 4, 'K80A', 0, 0, '8', 'Boerrhavia diffusa, Azadirachta indica, So!anum indicum, Tinospora cordifolia, Terminalia chebula', 'Dropsy, Bodyache, Fever, Bronchial disturbances, Urinary infections, Productive cough, Arthritis, Anaemia', NULL, NULL, '250g, 500g, l kg, 2.5kg', NULL, 5, 1, 'PUNARNAVADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'PUNARNAVADI KASHAYA SOOKSHMA CHOORNAM', 1, 11, '2019-03-12 10:41:04', '2019-08-13 11:33:31'),
(232, 'HINGUVACHADI CHOORNAM', 'hinguvachadi-choornam', '', NULL, 25, 'C17', 0, 0, '24', 'Ferula asafoetida, Acorus calamus, Terminalia chebula, Punica granatum, Coriandrum sativum, Kaempferia galanga, Thrikatu, Thrilavanam, Soda carbonas impura (processed), Potassium carbonas Impura (processed).', 'Dyspepsia, Loss of appetite, Anorexia, Constipation, Urinary disorders, Splenic disorders, Pain in the chest, neck, anus, vagina and Rheumatic ailments.', NULL, '5 -15 g. with Cumin seed water / Butter milk before food or as directed by the physician', '50 g., 500 g.', NULL, 5, 1, 'HINGUVACHADI CHOORNAM', NULL, 'HINGUVACHADI CHOORNAM', 1, 5, '2019-03-12 10:43:12', '2019-08-13 13:57:02'),
(234, 'JATAMAYADI LEPACHOORNAM', 'jatamayadi-lepachoornam', '', NULL, 25, 'C84', 0, 0, '8', 'Nardostachys jatamansi, Saussurea lappa, Santalum album, Boswellia serrata, Indian valerian, Withania somnifera, Pinus longifolia, Alpinia calcarata.', 'Arthritis, Sciatica, Rheumatic pain and swelling.', NULL, NULL, '200g.', 'For external application. Boil with Moringa leaves juice/ricewash into a paste form, apply over affected area or as directed by the physician.', 5, 1, 'JATAMAYADI LEPACHOORNAM', NULL, 'JATAMAYADI LEPACHOORNAM', 1, 6, '2019-03-12 10:51:12', '2019-08-13 13:59:43'),
(236, 'JEERAKA CHOORNAM', 'jeeraka-choornam', '', NULL, 26, 'C37', 0, 0, NULL, 'Cuminum cyminum', 'Cardiac disorders, Cough, Flatulence, Uterine disorders.', NULL, '5 - 15 g. with luke warm water/Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'JEERAKA CHOORNAM', NULL, 'JEERAKA CHOORNAM', 1, 5, '2019-03-12 10:56:14', '2019-08-13 15:36:12'),
(237, 'KACHOORADI CHOORNAM', 'kachooradi-choornam', '', NULL, 25, 'C78D', 0, 0, '8', 'Sida cordifolia, Fried grain.', 'Feverishness, Fits, Pyrexia, Epistaxis, Headache.', NULL, NULL, '50 g., 500 g.', 'For external application over head (Pichudharanam), mixed with coconut milk, breastmilk or other suitable vehicles or as directed by the physician.', 5, 1, 'KACHOORADI CHOORNAM', NULL, 'KACHOORADI CHOORNAM', 1, 4, '2019-03-12 11:31:33', '2019-08-13 14:00:06'),
(238, 'KOOSHMANDASWARASA GHRUTHAM', 'kooshmandaswarasa-ghrutham', '', NULL, 22, 'G32', 0, 0, '3', 'Benincasa hispida, Glycyrrhiza glabra, Cow ghee.', 'Insanity, Delusions, Depression, Epilepsy, Amnesia, Stammering, Secondary amenorrhoea. Recommended in ADHD.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'KOOSHMANDASWARASA GHRUTHAM', NULL, 'KOOSHMANDASWARASA GHRUTHAM', 1, 10, '2019-03-12 11:33:02', '2019-08-09 16:15:28'),
(239, 'MAHAKALYANAKA GHRUTHAM', 'mahakalyanaka-ghrutham', '5d2eecd2e87891563356370.jpg', NULL, 22, 'G25', 0, 0, '31', 'Hemidesmus indicus, Curcuma longa, Desmodium gangeticum, Solanum melongena, Rubia cordifolia, Embelia ribes, Elettaria cardamomum, Santalum album, Nervilia plicata, Withania somnifera, Cow ghee, Cow milk.', 'Mental disorders, Amnesia, Stammering etc. More effective than Kalyanaka ghrutham.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'MAHAKALYANAKA GHRUTHAM', NULL, 'MAHAKALYANAKA GHRUTHAM', 1, 13, '2019-03-12 11:39:40', '2019-08-09 16:15:56'),
(240, 'MAHATPANCHAGAVYA GHRUTHAM', 'mahatpanchagavya-ghrutham', '', NULL, 22, 'G26', 0, 0, '46', 'Dasamoolam, Thriphala, Indigofera tinctoria, Cyathula prostrata, Cassia fistula, Cow ghee, Cow dung, Cow urine, Cow milk, Curd, Thrikatu, Merrimia turpethum, Clerodendrum serratum.', 'Epilepsy, Intermittent fever, Anal fistula, Piles.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'MAHATPANCHAGAVYA GHRUTHAM', NULL, 'MAHATPANCHAGAVYA GHRUTHAM', 1, 11, '2019-03-12 11:44:05', '2019-08-09 16:17:46'),
(241, 'MAHAPAISACHIKA GHRUTHAM', 'mahapaisachika-ghrutham', '5d2eecf71cbdf1563356407.jpg', NULL, 22, 'G28', 0, 0, '25', 'Nardostachys jatamansi, Terminalia chebula, Mucuna pruriens, Nervilia plicata, Acorus calamus, Bacopa monnieri, Kaempferia galanga, Coriandrum sativum, Cow ghee.', 'Mental disorders like Phobia, Depression, Psychosis etc.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'MAHAPAISACHIKA GHRUTHAM', NULL, 'MAHAPAISACHIKA GHRUTHAM', 1, 13, '2019-03-12 11:46:05', '2019-08-09 16:16:22'),
(243, 'MAHATHIKTHAKA GHRUTHAM', 'mahathikthaka-ghrutham', '5d2eed458c9f71563356485.jpg', NULL, 22, 'G18', 0, 0, '34', 'Alstonia scholaris, Thriphala, Cassia fistula, Coscinium fenestraatum, Azadirachta indica, Santalum album, Citrullus colocynthis, Tinospora cordifolia, Trichosanthes anguina, Aconitum heterophyllum, Emblica officinalis, Cow ghee.', 'Acute and Chronic skin diseases. Recommneded in Erysepelas, Carbuncles, Boils, Abscess, Chronic wounds and ulcers, Sinuses, Blood impurity, Anaemia etc.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'MAHATHIKTHAKA GHRUTHAM', NULL, 'MAHATHIKTHAKA GHRUTHAM', 1, 10, '2019-03-12 11:48:15', '2019-08-09 16:17:16'),
(244, 'NEELIDALADI GHRUTHAM', 'neelidaladi-ghrutham', '5d2ef2ad30f701563357869.jpg', NULL, 22, 'G13', 0, 0, '15', 'Indigofera tinctoria, Ocimum sanctum, Vitex negundo, Aristolochia indica, Thrikatu, Withania somnifera, Santalum album, Saussurea lappa, Allium sativum, Cow ghee.', 'Chronic and poisonous skin diseases. Recommended in Poisonous insect bite.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'NEELIDALADI GHRUTHAM', NULL, 'NEELIDALADI GHRUTHAM', 1, 6, '2019-03-12 11:50:30', '2019-08-09 16:18:21'),
(245, 'KANTAKARICHOORNAM', 'kantakarichoornam', '', NULL, 26, 'C28', 0, 0, NULL, 'Solanum xanthocarpum', 'Cough and Flatulence', NULL, '5-10g. with Honey', '50g.,500g.', NULL, 5, 1, 'KANTAKARICHOORNAM', NULL, 'KANTAKARICHOORNAM', 1, 7, '2019-03-12 11:51:41', '2019-08-13 15:38:33'),
(246, 'NEELI GHRUTHAM', 'neeli-ghrutham', '', NULL, 22, 'G12', 0, 0, '15', 'Indigofera tinctoria, Azadirachta indica, Pongamia pinnata, Cow ghee, Acorus calamus, Thrikatu, Curcuma longa, Santalum album, Aristolochia indica.', 'Chronic and poisonous skin diseases.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'NEELI GHRUTHAM', NULL, 'NEELI GHRUTHAM', 1, 14, '2019-03-12 11:53:55', '2019-08-09 16:18:58'),
(247, 'KARPOORADI CHOORNAM', 'karpooradi-choornam-2', '', NULL, 25, 'C6', 0, 0, '11', 'Camphor, Cinnamomum zeylanicum, lllicium verum, Myristica fragrans, Aril, Eugenia caryophyllata, Mesua ferrea, Thrikatu, Sugar.', 'Cough and Consumption, Anorexia. Promotes appetite and digestion.', NULL, '2 - 5 g. intermittently with honey/10-15 g. b.d., with honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'KARPOORADI CHOORNAM', NULL, 'KARPOORADI CHOORNAM', 1, 4, '2019-03-12 12:00:21', '2019-08-13 14:00:32'),
(248, 'KOLAKULATHADI CHOORNAM', 'kolakulathadi-choornam', '5c94b17d033e21553248637.jpg', NULL, 25, 'C93', 0, 0, '11', 'Ziziphus jujuba, Horse gram, Cedrus deodara, Alpinia calcarata, Linseed, Black gram, Acorus calamus, Seeds of Ricinus communis.', 'Rheumatic pain and swelling. Recommended to use in “Dhanyakkizhi”', NULL, NULL, '200g.', 'Mix with Veppukadi/juice of Moringa leaves/Juice of Tamarind leaves/Luke warm water, Boil, make a paste and apply over the affected area.', 5, 1, 'KOLAKULATHADI CHOORNAM', NULL, 'KOLAKULATHADI CHOORNAM', 1, 12, '2019-03-12 12:04:11', '2019-08-13 14:01:36'),
(249, 'KOTTAMCHUKKADI CHOORNAM', 'kottamchukkadi-choornam', '', NULL, 25, 'C83A', 0, 0, '10', 'Saussurea lappa, Dried ginger, Acorus calamus, Cedrus deodara, Alpinia calcarata, Tamarindus indica (leaves).', 'Rheumatic pain and swelling.', NULL, NULL, '50 g., 500 g.', 'For external application with rice wash/Tamarindus leaf juice or as directed by the physician.', 5, 1, 'KOTTAMCHUKKADI CHOORNAM', NULL, 'KOTTAMCHUKKADI CHOORNAM', 1, 7, '2019-03-12 12:09:09', '2019-08-13 14:01:53'),
(250, 'KUTAJA CHOORNAM', 'kutaja-choornam', '5d43de3f5b2061564728895.jpg', NULL, 26, 'C29', 0, 0, '1', 'Holarrhena antidysenterica', 'Amoebic dysentery, Diarrhoea, Asthma, Malaria, Skin disease, Piles', NULL, '20-30g for decoction', '50g,500g', NULL, 5, 1, 'KUTAJA CHOORNAM', NULL, 'KUTAJA CHOORNAM', 1, 8, '2019-03-12 12:16:58', '2019-08-13 14:28:34'),
(251, 'MARICHA CHOORNAM', 'maricha-choornam', '5d43e086291e41564729478.jpg', NULL, 26, 'C50', 0, 0, NULL, 'Piper nigrum.', 'Cough, Bronchitis, Worm infestation, fever.', NULL, '5 - 15 g. with luke warm water/Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'MARICHA CHOORNAM', NULL, 'MARICHA CHOORNAM', 1, 5, '2019-03-12 12:19:50', '2019-08-13 15:40:25'),
(252, 'MISI CHOORNAM', 'misi-choornam', '', NULL, 26, 'C52', 0, 0, NULL, 'Anethum graveolens', 'Loss of appetite, Anorexia, Colic.', NULL, '5 - 15 g. with luke warm water/Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'MISI CHOORNAM', NULL, 'MISI CHOORNAM', 1, 17, '2019-03-12 12:51:08', '2019-08-13 15:40:53'),
(253, 'MUSTHA CHOORNAM', 'mustha-choornam', '', NULL, 26, 'C53', 0, 0, NULL, 'Cyperus rotundus.', 'Anorexia, Indigestion, Diarrhoea and Dysentery.', NULL, '5 -15 g. with honey / ghee / milk or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'MUSTHA CHOORNAM', NULL, 'MUSTHA CHOORNAM', 1, 7, '2019-03-12 12:54:10', '2019-08-13 15:42:27'),
(254, 'NAGARADI LEPA CHOORNAM', 'nagaradi-lepa-choornam', '', NULL, 25, 'C97', 0, 0, '5', 'Zingiber officinale, Acorus calamus', 'Traumatic inflammations, Oedema etc', NULL, 'For external use only. As directed by the physician', '50g, 500g', NULL, 5, 1, 'NAGARADI LEPA CHOORNAM', NULL, 'NAGARADI LEPA CHOORNAM', 1, 6, '2019-03-12 13:42:15', '2019-08-13 15:16:22'),
(255, 'NAGAKESARA CHOORNAM', 'nagakesara-choornam', '', NULL, 26, 'C42', 0, 0, '1', 'Mesua nagassarium', 'Asthma,cough,leprosy,scabies', NULL, '1-3g', '50g,500g', NULL, 5, 1, 'NAGAKESARA CHOORNAM', NULL, 'NAGAKESARA CHOORNAM', 1, 7, '2019-03-12 13:44:24', '2019-08-13 15:43:53'),
(256, 'NAVAYASACHOORNAM', 'navayasachoornam', '', NULL, 25, 'C75D', 0, 0, '10', 'Thrikatu, Thriphala, Cyperus rotundus, Embelia ribes, Plumbago rosea, Loha bhasmam (Calcinated Iron).', 'Anaemia, Skin diseases, Cardiac complaints, Piles, Jaundice.', NULL, '1 - 3 g. with honey and ghee once in a day after food or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'NAVAYASACHOORNAM', NULL, 'NAVAYASACHOORNAM', 1, 7, '2019-03-12 13:46:55', '2019-08-13 15:17:08'),
(257, 'NIMBADI CHOORNAM', 'nimbadi-choornam', '', NULL, 25, 'C71', 0, 0, '15', 'Azadirachta indica, Curcuma longa, Coscinium fenestratum, Ocimum sanctum (leaves and spike), Trichosanthes anguina, Saussurea lappa.', 'Skin diseases, Itches all over body.', NULL, NULL, '50 g., 500 g.', 'For external application only. Used for applying over the body with lukewarm water or butter milk.', 5, 1, 'NIMBADI CHOORNAM', NULL, 'NIMBADI CHOORNAM', 1, 9, '2019-03-12 13:49:47', '2019-08-13 14:04:12'),
(258, 'NISA CHOORNAM', 'nisa-choornam', '', NULL, 26, 'C43', 0, 0, NULL, 'Curcuma longa.', 'Diabetes, Wounds, Skin diseases, Worm infestation. Promotes complexion.', NULL, '5 - 15 g. decoction of Thriphala/Honey/Ghee or as directed by the physician. Can also be used for external application.', '50 g., 500 g.', NULL, 5, 1, 'NISA CHOORNAM', NULL, 'NISA CHOORNAM', 1, 8, '2019-03-12 13:54:31', '2019-08-13 17:00:54'),
(259, 'PANCHAKOLA CHOORNAM', 'panchakola-choornam', '', NULL, 25, 'C44', 0, 0, '5', 'Panchakolam', 'Loss of appetite, colic.', NULL, '5-10 g. with Honey/lukewarm water in empty stomach.', '50 g., 500 g.', NULL, 5, 1, 'PANCHAKOLA CHOORNAM', NULL, 'PANCHAKOLA CHOORNAM', 1, 6, '2019-03-12 13:59:08', '2019-08-13 14:04:36'),
(260, 'PANCHAKOLADI CHOORNAM', 'panchakoladi-choornam', '', NULL, 25, 'C73', 0, 0, '5', 'Panchakolam, Ferula asatoetida, Thrilavanam.', 'Pthisis, Anorexia, Indigestion, Sprue. Recommended in post-natal care.', NULL, '5 -15 g. with luke warm water or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'PANCHAKOLADI CHOORNAM', NULL, 'PANCHAKOLADI CHOORNAM', 1, 6, '2019-03-12 14:00:35', '2019-08-13 15:18:36'),
(261, 'PIPPALEE CHOORNAM', 'pippalee-choornam', '', NULL, 26, 'C47', 0, 0, NULL, 'Piper longum.', 'Cough, Anorexia, Splenic disorders, Anaemia, increases breast milk.', NULL, '5 -15 g. with honey /milk or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'PIPPALEE CHOORNAM', NULL, 'PIPPALEE CHOORNAM', 1, 11, '2019-03-12 14:05:34', '2019-08-13 15:45:07'),
(262, 'PIPPALIMOOLA CHOORNAM', 'pippalimoola-choornam', '', NULL, 26, 'C48', 0, 0, NULL, 'Roots of Piper spp. (wild) Pipper Longum.', 'Cough, Anorexia, Splenic disorders, Anaemia, increase breast milk.', NULL, '5-10 g. with Honey/Milk.', '50g.,500g.', NULL, 5, 1, 'PIPPALIMOOLA CHOORNAM', NULL, 'PIPPALIMOOLA CHOORNAM', 1, 5, '2019-03-12 14:10:00', '2019-08-13 15:45:48'),
(263, 'PUNARNAVA CHOORNAM', 'punarnava-choornam', '', NULL, 26, 'C127', 0, 0, '1', 'Boerhavia diffusa', 'Inflammation, leucorrhoea, lumbago.scabies, anaemia, cough', NULL, 'As directed by physician', '50g.,500g.', NULL, 5, 1, 'PUNARNAVA CHOORNAM', NULL, 'PUNARNAVA CHOORNAM', 1, 10, '2019-03-12 14:14:45', '2019-08-13 16:00:50'),
(264, 'PURANADHATHREEPHALA CHOORNAM', 'puranadhathreephala-choornam', '', NULL, 26, 'C49', 0, 0, NULL, 'Emblicaofficinalis.', 'Eye diseases, Diabetes, Wounds, also for vitamin C deficiency.', NULL, '5-15 g. with lukewarm water/Honey/Ghee or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'PURANADHATHREEPHALA CHOORNAM', NULL, 'PURANADHATHREEPHALA CHOORNAM', 1, 8, '2019-03-12 14:22:07', '2019-08-13 15:47:20'),
(265, 'PUSHYANUGACHOORNAM', 'pushyanugachoornam', '', NULL, 25, 'C70', 0, 0, '26', 'Cyclea peltata, Syzygium cumini, Mangifera indica (cotyledon), Rotula aquatica, Coscinium fenestratum, Symplocos cochinchinensis, Red ochre, Glycyrrhiza glabra, Terminalia arjuna.', 'Urino-genital diseases, Leucorrhoea, Menorrhagia, Menstrual disorders, Bleeding piles, Dysentery, Sprue and Epistaxis.', NULL, '5 -10 g. b,d. with rice wash /milk / honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'PUSHYANUGACHOORNAM', NULL, 'PUSHYANUGACHOORNAM', 1, 8, '2019-03-12 14:27:48', '2019-08-13 14:06:20'),
(266, 'RAJANNYADI CHOORNAM', 'rajannyadi-choornam', '', NULL, 25, 'C126', 0, 0, '8', 'Curcuma longa, Cedrus deodara, Pinus longifolia, Piper officinarum, Solanum melongena. var.insanum, Solanum xanthocarpum, Pseudarthria viscida, Peucedanum graveolens.', 'Ideal remedy for Infantile diarrhoea, Dysentery, Emesis, Fever, Cough and all ailments during the period of teeth eruption.', NULL, '2 - 5 g. with honey/butter or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'RAJANNYADI CHOORNAM', NULL, 'RAJANNYADI CHOORNAM', 1, 4, '2019-03-12 14:37:56', '2019-08-13 14:06:46'),
(267, 'RASNADI CHOORNAM', 'rasnadi-choornam', '', NULL, 25, 'C130', 0, 0, '24', 'Alpinia calcarata, Withania somnifera, Aloe extract, Acorus calamus, Red ochre, Curcuma longa, Glycyrrhiza glabra, Sida cordifolia, Cyperus rotundus, Thrikatu, Santalum album, Tamarindus indica (rachis).', 'Cold, Nasal catarrh, Headache, Sneezing, Fever.', NULL, NULL, '50 g., 500 g.', 'For external application i.e. rubbing over the crown & for pain, apply over affected area by making a paste with lemon juice.', 5, 1, 'RASNADI CHOORNAM', NULL, 'RASNADI CHOORNAM', 1, 8, '2019-03-12 14:40:23', '2019-08-13 14:07:08'),
(268, 'PADADARVADI GHRUTHAM', 'padadarvadi-ghrutham', '', NULL, 22, 'G15', 0, 0, '15', 'Cyclea peltata, Coscinium fenestratum, Bacopa monnieri, Cyperus rotundus, Piper longum, Cow ghee.', 'Snake poison, Skin diseases with burning sensation, thirst and other poisonous skin diseases.', NULL, '10 - 20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'PADADARVADI GHRUTHAM', NULL, 'PADADARVADI GHRUTHAM', 1, 5, '2019-03-12 14:42:25', '2019-08-09 16:42:49'),
(269, 'PANCHAGAVYA GHRUTHAM', 'panchagavya-ghrutham', '5d2ef369a6ca81563358057.jpg', NULL, 22, 'G27', 0, 0, '5', 'Cow dung, Cow urine, Cow milk, Curd, Cow ghee.', 'Mental diseases.  Improves  Memory  power  and  concentration,  Jaundice,  Fever, Epilepsy', NULL, '10-20 ml. b.d., before food or as directed by the physician.', '200 ml., 450 ml', NULL, 5, 1, 'PANCHAGAVYA GHRUTHAM', NULL, 'PANCHAGAVYA GHRUTHAM', 1, 6, '2019-03-12 14:48:04', '2019-08-09 16:40:39'),
(270, 'PATOLADI GHRUTHAM', 'patoladi-ghrutham', '5d2ef338296071563358008.jpg', NULL, 22, 'G14', 0, 0, '20', 'Trichosanthes  anguina,  Azadirachta  indica,  Thriphala,  Bacopa  monnieri,  Glycyrrhiza glabra, Emblica officinalis, Santalum album, Cow ghee.', 'Cataract and other Eye diseases, Skin problems, ear & rasal ailments.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'PATOLADI GHRUTHAM', NULL, 'PATOLADI GHRUTHAM', 1, 10, '2019-03-12 14:49:59', '2019-08-09 16:51:15'),
(271, 'PHALASARPIS', 'phalasarpis', '5d2ef3eca80e21563358188.jpg', NULL, 22, 'G34', 0, 0, '21', 'Rubia cordifolia, Thriphala, Acorus calamus, Withania somnifera, Cow ghee,  Cow milk.', 'Genital  and  sexual  disorders,  Secondary  amenorrhoea,  Oligospermia. \r\nImproves the reproductive capacity of both sex.', NULL, '10-20 ml. b.d., before food or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'PHALASARPIS', NULL, 'PHALASARPIS', 1, 11, '2019-03-12 14:52:54', '2019-08-09 16:51:52'),
(272, 'PURANA GHRUTHAM', 'purana-ghrutham', '5d2ef424da7e51563358244.jpg', NULL, 22, 'G16', 0, 0, '1', 'Cows ghee', 'Mental problems, disorders pertaining to head and brain, Gynaecological disorders', NULL, 'As directed by the physician', '200 ml., 450 ml.', NULL, 5, 1, 'PURANA GHRUTHAM', NULL, 'PURANA GHRUTHAM', 1, 10, '2019-03-12 14:58:56', '2019-08-09 17:02:09'),
(273, 'SARASWATHA CHOORNAM', 'saraswatha-choornam', '', NULL, 25, 'C68', 0, 0, '10', 'Dried ginger, Apium graveolens, Curcuma longa, Coscinium fenestratum, Rocksalt, Acorus calamus, Glycyrrhiza glabra, Saussurea lappa, Piper longum, Cuminum cyminum.', 'Indistinct speech followed by paralysis, Stammering, Weak memory power.', NULL, '5 - 15 g. b.d. after food preferably with milk/Ghee or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SARASWATHA CHOORNAM', NULL, 'SARASWATHA CHOORNAM', 1, 6, '2019-03-12 15:00:13', '2019-08-13 14:09:15'),
(274, 'SARPAGANDHI CHOORNAM', 'sarpagandhi-choornam', '5d43e6143be441564730900.jpg', NULL, 26, 'C62', 0, 0, NULL, 'Rauwolfia serpentina', 'Hypertension, Poisonous bites, Insomnia, mental disorders.', NULL, '½ g. - 2 g. with Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SARPAGANDHI CHOORNAM', NULL, 'SARPAGANDHI CHOORNAM', 1, 6, '2019-03-12 15:03:06', '2019-08-13 15:47:53'),
(275, 'SARPAGANDHYADI CHOORNAM', 'sarpagandhyadi-choornam', '5c94bb321dbc71553251122.jpg', NULL, 25, 'C63', 0, 0, '2', 'Rauwolfia serpentina, Aristolochia indica.', 'Effective for snake poison.', NULL, '1-5 g. with honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SARPAGANDHYADI CHOORNAM', NULL, 'SARPAGANDHYADI CHOORNAM', 1, 7, '2019-03-12 15:05:21', '2019-08-13 14:09:52'),
(276, 'RASNADASAMOOLA GHRUTHAM', 'rasnadasamoola-ghrutham', '5d2ef472e10f41563358322.jpg', NULL, 22, 'G19', 0, 0, '28', 'Alpinia  calcarata,  Dasamoolam,  Asparagus  racemosus,  Dolichos  biflorus,  Ziziphus jujuba, Jeevaneeyaganam, Mutton soup, Cow ghee, Cow milk.', 'As a prophylactic in Cough and Bronchial disorders. Rheumatic complaints, \r\nVertigo, Tremors.', NULL, '10 - 20 ml. b.d., before food or as directed ty the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'RASNADASAMOOLA GHRUTHAM', NULL, 'RASNADASAMOOLA GHRUTHAM', 1, 10, '2019-03-12 15:08:57', '2019-08-09 16:52:36'),
(277, 'SATHAVARI CHOORNAM', 'sathavari-choornam', '', NULL, 26, 'C59', 0, 0, NULL, 'Asparagus racemosus.', 'Diabetes, Leucorrhoea, Chilblain, Burning sensation over extremities.', NULL, '5 -15 g. with milk or as directed by the physician. For Diabetes preferably with antidiabetic decoctions.', '50 g., 500 g.', NULL, 5, 1, 'SATHAVARI CHOORNAM', NULL, 'SATHAVARI CHOORNAM', 1, 11, '2019-03-12 15:13:07', '2019-08-13 15:48:29'),
(278, 'SARASWATHA GHRUTHAM (CHERUTHU)', 'saraswatha-ghrutham-cheruthu', '5d2ef4d0392da1563358416.jpg', NULL, 22, 'G33', 0, 0, '10', 'Terminalia chebula, Thrikatu, Cyclea peltata, Acorus calamus, Moringa oleifera, Cow milk, Cow ghee, Rock salt.', 'Amnesia, Dysphasia, Ataxia after a stroke, Hemiplegia, Stammering, Mental  disorders,  Parkinsonism.  Also  recommended  to  improve  memory  power, retention power, etc.', NULL, '10-20 ml. in empty stomach or as directed by the physician.', '200 ml.', NULL, 5, 1, 'SARASWATHA GHRUTHAM (CHERUTHU)', NULL, 'SARASWATHA GHRUTHAM (CHERUTHU)', 1, 12, '2019-03-12 15:15:04', '2019-08-09 16:53:37'),
(279, 'SHADPALA GHRUTHAM', 'shadpala-ghrutham', '', NULL, 22, 'G23', 0, 0, '8', 'Panchakolam, Yavaksharam, Cow ghee, Cow milk.', 'Consumption, Intermittent fever, Loss of appetite, Indigestion, Anorexia, Splenic disorders, Asthma, General fatigue. Helps to regain health and strength during convalescence period.', NULL, '10-20 ml. b.d., before food or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'SHADPALA GHRUTHAM', NULL, 'SHADPALA GHRUTHAM', 1, 12, '2019-03-12 15:18:37', '2019-08-09 16:54:33'),
(281, 'SHADDHARANA CHOORNAM', 'shaddharana-choornam', '', NULL, 25, 'C90D', 0, 0, '6', 'Plumbago rosea of Holarrhena antidysenterica, Cyclea peltata, Picrorrhiza\r\nkurroa, Aconitum heterophyllum.Terminalia chebula.', 'Rheumatic fever and associated symptoms.', NULL, '5–10 g. with Honey/Butter Milk/Suitable kashayam, twice daily before food or\r\nas directed by the Physician.', '50g.,500g.', NULL, 5, 1, 'SHADDHARANA CHOORNAM', NULL, 'SHADDHARANA CHOORNAM', 1, 11, '2019-03-12 15:21:24', '2019-08-13 15:20:58'),
(282, 'SITHOPALADI CHOORNAM', 'sithopaladi-choornam', '', NULL, 25, 'C120', 0, 0, '5', 'Elettaria cardamomum, Piper iongum, Bambusa arundinacea, Cinnamomum\r\nzeylanicum, Sugar', 'Loss of appetite, Indigestion, Asthma, Fever, Burning sensation of hands and feet, Weakness', NULL, '1 to 3g with Honey or as directed by physician', '50 g, 500g', NULL, 5, 1, 'SITHOPALADI CHOORNAM', NULL, 'SITHOPALADI CHOORNAM', 1, 4, '2019-03-12 15:23:56', '2019-08-13 15:23:47'),
(283, 'SUKUMARA GHRUTHAM', 'sukumara-ghrutham', '5c94c2e7d36561553253095.jpg', NULL, 22, 'G24', 0, 0, '32', 'Boerrhavia diffusa, Dasamoolam, Holostemma ada-kodien, Withania somnifera, Ricinus communis, Asparagus racemosus, Thrinapanchamoolam, Sphaeranthus indicus, Jaggery, Castor oil, Cow ghee, Cow milk, Rock salt.', 'Hyperacidity, Hernia, Pthiasis, Constipation, Urinogenital disorders, Menstrual disorders, Vaginal diseases, Infertility of both sex, Haemorrhoids, Hydrocele.', NULL, '10-20 ml. b.d., before food or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'SUKUMARA GHRUTHAM', NULL, 'SUKUMARA GHRUTHAM', 1, 14, '2019-03-12 15:25:49', '2019-08-09 16:57:01'),
(284, 'SOMALATHA CHOORNAM', 'somalatha-choornam', '', NULL, 26, 'C65', 0, 0, NULL, 'Sarcostemma acidium', 'Mental diseases. Renders good sleep', NULL, '5 -15 g. with Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SOMALATHA CHOORNAM', NULL, 'SOMALATHA CHOORNAM', 1, 10, '2019-03-12 15:26:46', '2019-08-13 15:49:24'),
(285, 'THIKTHAKA GHRUTHAM', 'thikthaka-ghrutham', '5d2ebefea01421563344638.jpg', NULL, 22, 'G8', 0, 0, '15', 'Trichosanthes anguina, Coscinium fenestratum, Azadirachta indica, Bacopa monnieri, Santalum album, Cow ghee.', 'Bilious skin disorders, Erysepelas, Acene vulgaris, Anaemia, Piles, Sprue, Jaundice, Allergic and Chronic skin diseases. Recommended as a blood purifier, Piles, ascities & mental disorders.', NULL, '10-20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'THIKTHAKA GHRUTHAM', NULL, 'THIKTHAKA GHRUTHAM', 1, 8, '2019-03-12 15:28:17', '2019-08-09 16:57:40'),
(286, 'SUDARSANACHOORNAM', 'sudarsanachoornam', '', NULL, 25, 'C16', 0, 0, '54', 'Thriphala, Cyperus rotundus, Solanum xanthocarpum, Kaempferia galanga, Thrikatu, Tinospora cordifolia, Piccrorrhiza curroa, Oldenlandia corymbosa, Azadirachta indica, Clerodendrum serratum, Desmodium gangeticum, Pseudarthria viscida, Abies spectabilis, Andrographis paniculata.', 'Fever, Flu, Indigestion, Jaundice and Skin disorders.', NULL, '5 -15 g. with honey/luke warm water or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SUDARSANACHOORNAM', NULL, 'SUDARSANACHOORNAM', 1, 9, '2019-03-12 15:28:37', '2019-08-13 14:13:09'),
(287, 'THRAIPHALA GHRUTHAM', 'thraiphala-ghrutham', '', NULL, 22, 'G9', 0, 0, '5', 'Thriphala, Cow ghee, Cow milk.', 'Cataract and other Ophthalmic disturbances.', NULL, '10-20 ml. at bed time with honey or Vara choorna as additive or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'THRAIPHALA GHRUTHAM', NULL, 'THRAIPHALA GHRUTHAM', 1, 7, '2019-03-12 15:32:42', '2019-08-09 16:58:05'),
(288, 'SUNDI CHOORNAM', 'sundi-choornam', '', NULL, 26, 'C60', 0, 0, NULL, 'Dried ginger. (Zingiber officinale)', 'Colic, Cough, Dysentery, Loss of appetite, Indigestion.', NULL, '5 -15 g. with honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SUNDI CHOORNAM', NULL, 'SUNDI CHOORNAM', 1, 11, '2019-03-12 15:36:15', '2019-08-13 15:49:55'),
(289, 'VAJRAKA GHRUTHAM', 'vajraka-ghrutham', '', NULL, 22, 'G20', 0, 0, '10', 'Adhatoda beddomei, Tinospora cordifolia, Azadirachta indica, Thriphala, Pongamia pinnata, Trichosanthes anguina, Solanum melongena, Cow ghee.', 'Chronic abscess, Skin diseases, Jaundice.', NULL, '10-20 ml. b.d., after food or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'VAJRAKA GHRUTHAM', NULL, 'VAJRAKA GHRUTHAM', 1, 10, '2019-03-12 15:38:11', '2019-08-09 16:58:35'),
(290, 'SWETHASANKHUPUSHPA CHOORNAM', 'swethasankhupushpa-choornam', '', NULL, 26, 'C61', 0, 0, NULL, 'Clitoria ternatea.', 'Mental diseases, induces sleep', NULL, '5 -15 g. with Milk/Honey/Ghee or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'SWETHASANKHUPUSHPA CHOORNAM', NULL, 'SWETHASANKHUPUSHPA CHOORNAM', 1, 12, '2019-03-12 15:38:42', '2019-08-13 15:50:22'),
(292, 'THALEESAPATHRADI CHOORNAM', 'thaleesapathradi-choornam', '', NULL, 25, 'C10', 0, 0, '7', 'Abies spectabilis, Thrikatu, Cinnamomum zeylanicum, Elettaria cardamomum, Sugar.', 'Cough, Bronchitis, Loss of appetite, Indigestion, Anorexia and Flu.', NULL, '5 - 10 g. intermittently with honey or as directed by the physician.', '10g, 50 g., 500 g.', NULL, 5, 1, 'THALEESAPATHRADI CHOORNAM', NULL, 'THALEESAPATHRADI CHOORNAM', 1, 4, '2019-03-12 15:41:29', '2019-08-13 14:13:58'),
(293, 'VIDARYADI GHRUTHAM (With soup)', 'vidaryadi-ghrutham-with-soup', '', NULL, 22, 'G36', 0, 0, '22', 'Ipomoea mauritiana, Ricinus communis, Boerrhavia diffusa, Cedrus deodara, Desmodium trifoliatum, Jeevanapanchamoolam, Laghu panchamoolam, Cow ghee, Curd, Cow milk, Mutton soup.', 'More effective than Vidaryadi ghrutham. It is mainly recommended in Convulsive diseases. Helps to regain health, strength, vigour and vitality.', NULL, '10 - 20 ml. b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'VIDARYADI GHRUTHAM (With soup)', NULL, 'VIDARYADI GHRUTHAM (With soup)', 1, 14, '2019-03-12 15:43:13', '2019-08-09 17:00:47'),
(294, 'VIDARYADI GHRUTHAM', 'vidaryadi-ghrutham-2', '5d2ef78d30a911563359117.jpg', NULL, 22, 'G21', 0, 0, '20', 'Ipomoea mauritiana, Ricinus communis, Boerrhavia diffusa, Cedrus deodara, Desmodium trifoliatum, Jeevanapanchamoolam, Laghu panchamoolam, Cow ghee, Cow milk.', 'Cough, Bronchitis, Cardiac disorders like palpitation, tachycardia etc. Fatigue, Helpful to regain health and body weight. Can be recommended in Anasarca.', NULL, '10-20 ml.b.d., or as directed by the physician.', '200 ml., 450 ml.', NULL, 5, 1, 'VIDARYADI GHRUTHAM', NULL, 'VIDARYADI GHRUTHAM', 1, 17, '2019-03-12 15:44:32', '2019-08-09 17:01:36'),
(295, 'TRIKATU CHOORNAM', 'trikatu-choornam-2', '', NULL, 25, 'C125', 0, 0, '3', 'Zingiber officinale, Piper nigrum, Piper Iongum', 'Loss of appetite, IndigestionProblems of throat,Cold,Skin disease', NULL, '1 to 3 g with honey, warm water or as directed by physician', '50g.500g', NULL, 5, 1, 'TRIKATU CHOORNAM', NULL, 'TRIKATU CHOORNAM', 1, 2, '2019-03-12 15:44:52', '2019-08-13 14:14:26'),
(297, 'UNMADANASINI CHOORNAM', 'unmadanasini-choornam', '', NULL, 25, 'C26', 0, 0, '11', 'Saussurea lappa, Rock salt, Withania somnifera, Thrikatu, Cyclea peltata. Clitoria ternatea.', 'Mental disorders like Insanity, Psychosis, etc', NULL, '5 - 15 g. with honey/Brahmi juice/Ghee in empty stomach or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'UNMADANASINI CHOORNAM', NULL, 'UNMADANASINI CHOORNAM', 1, 6, '2019-03-12 15:54:01', '2019-08-13 14:14:58'),
(298, 'USEERA CHOORNAM', 'useera-choornam', '', NULL, 26, 'C25', 0, 0, NULL, 'Vetiveria zizanioides.', 'Urinary disorders, Excessive thirst, Burning sensation, removes bad odour from body', NULL, '5 -15 g. with Milk/Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'USEERA CHOORNAM', NULL, 'USEERA CHOORNAM', 1, 10, '2019-03-12 16:06:04', '2019-08-13 15:51:57'),
(299, 'VACHA CHOORNAM', 'vacha-choornam', '', NULL, 26, 'C55', 0, 0, NULL, 'Acorus calamus', 'Mental diseases. Also recommended in Colic, Flatulence, Constipation.', NULL, '5 -10 g. with honey/Ghee or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'VACHA CHOORNAM', NULL, 'VACHA CHOORNAM', 1, 7, '2019-03-12 16:34:31', '2019-08-13 15:52:49'),
(300, 'VAISWANARA CHOORNAM', 'vaiswanara-choornam', '', NULL, 25, 'C15', 0, 0, '6', 'Rock salt, Cuminum cyminum, Apium graveolens, Piper longum, Dried ginger, Terminalia chebula.', 'Colic, Piles, Flatulence, Constipation, Dysuria, Ascites, Anorexia, Indigestion etc.', NULL, '5 - 15 g. before food with luke warm water/butter milk/Cumin seed water or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'VAISWANARA CHOORNAM', NULL, 'VAISWANARA CHOORNAM', 1, 3, '2019-03-12 16:36:48', '2019-08-13 14:15:29'),
(301, 'VARA CHOORNAM / THRIPHALA CHOORNAM', 'vara-choornam-thriphala-choornam', '', NULL, 25, 'C14', 0, 0, '3', 'Thriphala.', 'Powerful remedy in eye diseases. Also recommended in Ulcers of mouth and skin, Obesity, Blood impurities, Constipation. Used both externally and internally. Decoction made by Varachoornam can be used as a powerful antiseptic lotion for cleaning wounds, gargling, eye irrigation.', NULL, '5 -15 g. with honey/butter/ghee (internally) or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'VARA CHOORNAM / THRIPHALA CHOORNAM', NULL, 'VARA CHOORNAM / THRIPHALA CHOORNAM', 1, 12, '2019-03-12 16:38:17', '2019-08-13 14:15:54'),
(302, 'VIDANGA CHOORNAM', 'vidanga-choornam', '', NULL, 26, 'C58', 0, 0, 'Embelia ribes', 'Worm infestation, skin diseaes, temporary contraceptive.', NULL, NULL, '5 - 15 g. with Honey/Anthelmintic decoctions or as directed by the physician', '50 g., 500 g.', NULL, 5, 1, 'VIDANGA CHOORNAM', NULL, 'VIDANGA CHOORNAM', 1, 8, '2019-03-12 16:41:24', '2019-08-13 15:54:05'),
(303, 'YASHTI CHOORNAM', 'yashti-choornam', '', NULL, 26, 'C54', 0, 0, NULL, 'Glycyrrhiza glabra', 'Eye diseases, Poison, Wounds, Hoarseness of voice, Thirst, Oral ailments.', NULL, '5 - 15 g. with honey or as directed by the physician. Can be used for external application in wounds, eye wash, gargling.', '50 g., 500 g.', NULL, 5, 1, 'YASHTI CHOORNAM', NULL, 'YASHTI CHOORNAM', 1, 23, '2019-03-12 16:43:28', '2019-08-13 15:54:34'),
(304, 'YAVANYADI CHOORNAM', 'yavanyadi-choornam', '', NULL, 25, 'C11', 0, 0, '13', 'Cuminum cyminum, Tamarindus indica, Rheum emodi, Dried ginger, Punica granatum, Ziziphus jujuba, Sugar, Coriandrum sativum, Black salt, Apium graveolens, Cyperus rotundus, Piper longum, Piper nigrum.', 'Anorexia, Diarrhoea, Cough and Uneasiness in chest, Flatulence. Also recommended in Piles and Sprue.', NULL, '5 -15 g. intermittently or b.d. before food with honey/ buttermilk or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'YAVANYADI CHOORNAM', NULL, 'YAVANYADI CHOORNAM', 1, 8, '2019-03-12 16:45:34', '2019-08-13 14:16:32'),
(305, 'YOGARAJA CHOORNAM', 'yogaraja-choornam', '', NULL, 25, 'C69', 0, 0, '21', 'Panchakolam, Cyclea peltata, Embelia ribes, Acorus calamus, Picrorrhiza curroa, Commiphora wightii (Purified and processed)', 'Piles, Fistula, Pthisis, Anaemia., Anorexia and Rheumatic complaints.', NULL, '5 - 15 g. with honey/antirheumatic decoctions or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'YOGARAJA CHOORNAM', NULL, 'YOGARAJA CHOORNAM', 1, 7, '2019-03-12 16:47:15', '2019-08-13 15:27:15'),
(306, 'ASHTACHOORNAM SYRUP', 'ashtachoornam-syrup', '5d2ec30b2d1d81563345675.jpg', NULL, 8, 'C96A', 1, 0, '9', 'Ashtachoornam, Honey, Syrup base.', 'Loss of appetite, Indigestion, Colic, Worm infestation.', NULL, '2.5-5 ml. b.d. before food or as directed by the physician. (Paediatric dose)', '200 ml.', NULL, 5, 1, 'ASHTACHOORNAM SYRUP', NULL, 'ASHTACHOORNAM SYRUP', 1, 51, '2019-03-12 17:04:42', '2019-08-09 15:44:38'),
(307, 'BURNCURE OINTMENT', 'burncure-ointment', '5d2edd5c1f0581563352412.jpg', NULL, 8, 'TC1', 1, 0, '4', 'Pothos scandens, Coconut oil, White petroleum jelly, Bee wax.', 'All types of burns. Prevents blister, Inflammation and Discolouration.', NULL, NULL, '25 g.', 'Provides cooling effect while applying over the affected area.', 5, 1, 'BURNCURE OINTMENT', NULL, 'BURNCURE OINTMENT', 1, 44, '2019-03-12 17:06:41', '2019-08-28 16:48:18'),
(308, 'CHANDRAPRABHA GULIKA (TABLET)', 'chandraprabha-gulika-tablet', '', NULL, 3, 'P16B', 0, 0, '36', 'Embelia ribes.Thrikatu, Thriphala, Acorus calamus, Rock salt, Potassium\r\ncarbonate, Aconitum heterophyllum, Asphaltum, Commiphora wightii (purified),\r\nEclipta alba.', 'Urino-genital diseases like Haematuria, Albuminuria, Urinary calculi, Dysuria, \r\nUrinary  infection,  Incontinence  of  urine,  Spermatorrhoea,  Leucorrhoea, \r\nDiabetes  insipides.  Also recommended as prophylactic medicine in  post-\r\nsurgical effects of urino-genital diseases.', NULL, '2 tablets b.d. with milk, Brihathyadi kashayam, honey or as directed by the \r\nphysician.', '100 Nos', NULL, 5, 1, 'CHANDRAPRABHA GULIKA (TABLET)', NULL, 'CHANDRAPRABHA GULIKA (TABLET)', 1, 27, '2019-03-12 17:06:57', '2019-08-13 14:13:35'),
(311, 'CHARNGERYADI GULIKA (TABLET)', 'charngeryadi-gulika-tablet', '', NULL, 3, 'P17', 0, 0, '16', 'Oxalis corniculata, Cyperus rotundus, Apium graveolens, Punica granatum, \r\nMyristica  fragrans,  Santalum  album,  Aconitum  heterophyllum,  Opium \r\n(purified).', 'Diarrhoea, Dysentery, Intestinal colic, Indigestion.', NULL, '1 tablet b.d. with butter milk, luke warm water or suitable Kashayam/Arishtam \r\nor as directed by the physician.', '100 Nos.', NULL, 5, 1, 'CHARNGERYADI GULIKA (TABLET)', NULL, 'CHARNGERYADI GULIKA (TABLET)', 1, 22, '2019-03-12 17:14:26', '2019-08-13 12:32:22'),
(312, 'DIABET DRINKS', 'diabet-drinks', '5d2ee58f0de5a1563354511.jpg', NULL, 8, 'C88D', 1, 0, '4', 'Salacia fruiticosa, Pterocarpus marsupium, Vetiveria zizanioides, Caesalpinia sappan.', 'Excessive thirst, Perspiration etc. related with Diabetes Mellitus.', NULL, '3 - 5 g. powder in 2 litre water, boil for ten minutes and use whole day.', '50 g.', 'For preparing drinking water for diabetic patients.', 5, 1, 'DIABET DRINKS', NULL, 'DIABET DRINKS', 1, 39, '2019-03-12 17:16:11', '2019-08-21 14:38:15'),
(313, 'CHUKKUMTHIPPALLYADI GULIKA (TABLET)', 'chukkumthippallyadi-gulika-tablet', '', NULL, 3, 'P18', 0, 0, '18', 'Dried ginger, Piper longum, Aloe extract, Stag’s horn, Elaeocarpus tuberculatus, \r\nAndrographis  paniculata,  Yellow  resin,  White  arsenic  (processed),  Piper \r\nnigrum, Camphor, Civet, Red orpiment (processed), Blue vitriol (processed), \r\nCardiospermum halicacabum.', 'Flu, Fever, Anorexia, Distaste, Sprue.', NULL, '1 tablet b.d. with honey/ ginger juice/thulasi juice or other suitable vehicles or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'CHUKKUMTHIPPALLYADI GULIKA (TABLET)', NULL, 'CHUKKUMTHIPPALLYADI GULIKA (TABLET)', 1, 14, '2019-03-12 17:17:21', '2019-08-13 12:30:41'),
(314, 'DASANGAM GULIKA (TABLET)', 'dasangam-gulika-tablet', '', NULL, 3, 'P24', 0, 0, '10', 'Acorus calamus, Ferula asafoetida, Embelia ribes, Rock salt, Elephant pepper, \r\nCyclea peltata, Cyperus rotundus, Thrikatu.', 'Poisonous insect bites.', NULL, 'For external and internal application. Internally 1 - 2 tabs. b.d. with honey/\r\nThulasi juice/Kashayam', '100 Nos.', NULL, 5, 1, 'DASANGAM GULIKA (TABLET)', NULL, 'DASANGAM GULIKA (TABLET)', 1, 12, '2019-03-12 17:19:32', '2019-08-13 12:33:19'),
(315, 'DHANWANTHARAM GULIKA (TABLET)', 'dhanwantharam-gulika-tablet', '', NULL, 3, 'P27', 0, 0, '16', 'Elettaria cardamomum, Dried ginger, Terminalia chebula, Cuminum cyminum, \r\nElaeocarpus tuberculatus, Camphor, Rosewater.', 'Dyspnoea, Cough, Hiccup, Vomitting. Also recommended in Rheumatic\r\ndiseases, Tuberculosis.', NULL, '1 tablet b.d. or as directed by the physician.', '100 Nos', NULL, 5, 1, 'DHANWANTHARAM GULIKA (TABLET)', NULL, 'DHANWANTHARAM GULIKA (TABLET)', 1, 28, '2019-03-12 17:21:07', '2019-08-13 14:16:22'),
(316, 'DOOSHEEVISHARI GULIKA (TABLET)', 'doosheevishari-gulika-tablet', '', NULL, 3, 'P26', 0, 0, '14', 'Piper longum, Nardostachys jatamansi, Symplocos cochinchinensis, Santalum \r\nalbum, Red ochre, Glycyrrhiza glabra.', 'Chronic ill effects of Poisons, Insect bite. Snake bite etc.', NULL, NULL, '100 Nos.', 'For  external  and  internal  application.  Internally  1  tab.  b.d.  with  honey/ \r\nKashayam/Thulasi juice.', 5, 1, 'DOOSHEEVISHARI GULIKA (TABLET)', NULL, 'DOOSHEEVISHARI GULIKA (TABLET)', 1, 19, '2019-03-12 17:23:48', '2019-08-13 14:17:23'),
(317, 'ELADI GULIKA', 'eladi-gulika', '', NULL, 3, 'P79', 0, 0, '8', 'Elattaria cardomum, Vitis vinifera, Sugar, Glycerrhiza glabra, \r\nPhoenix dactylifera etc.', 'Vomiting, Nausea, hiccough, cough, dyspnea, fever, excess thirst, \r\nsplenomegaly etc.', NULL, '2-4 tabs intermittently along with honey or as directed by physician.', '10 x10 blisters', NULL, 5, 1, 'ELADI GULIKA', NULL, 'ELADI GULIKA', 1, 13, '2019-03-12 17:31:46', '2019-08-13 12:31:51'),
(318, 'GOPEECHANDANADI GULIKA (TABLET)', 'gopeechandanadi-gulika-tablet', '', NULL, 3, 'P13', 0, 0, '56', 'Hemidesmus indicus, Santalum album, Cyperus rotundus, Ariyaru, Thriphala,\r\nThrikatu, Jeerakathrayam, Allium sativum, Raisins, Oak gall, Camphor,\r\nSaffron, Sugar, Coleus aromaticus, Ocimum sanctum.', 'Scabies, Fever, Chronic skin disorders, Itching, Eczema especially of children.', NULL, NULL, '100 Nos.', 'Paediatric dose: 1/2 to 1 tablet b.d. with breastmilk/honey/Thulasi juice/Coleus \r\njuice or as directed by the physician.', 5, 1, 'GOPEECHANDANADI GULIKA (TABLET)', NULL, 'GOPEECHANDANADI GULIKA (TABLET)', 1, 19, '2019-03-12 17:39:27', '2019-08-13 14:18:00');
INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(319, 'GOROCHANADI GULIKA (TABLET)', 'gorochanadi-gulika-tablet', '', NULL, 3, 'P14', 0, 0, '45', 'Elaeocarpus tuberculatus, Civet, Horns of Goat, Deer etc. Gold (processed),\r\nAndrographis paniculata, Galena (processed) Camphor, Leucas aspera,\r\nThrikatu, Thriphala, Borax (processed), Oak gall, Zingiber officinale (fresh).', 'Chronic fever. Recommended in Intermittent fever, Respiratory disorders and\r\nCardiac disorders.', NULL, NULL, '100 Nos', '1 tablet b.d. with honey/Thulasi juice/Coleus juice/ginger juice/suitable\r\nKashayam.', 5, 1, 'GOROCHANADI GULIKA (TABLET)', NULL, 'GOROCHANADI GULIKA (TABLET)', 1, 24, '2019-03-12 17:41:28', '2019-08-13 14:19:18'),
(320, 'JATHEEMUKULADI VARTHI', 'jatheemukuladi-varthi', '', NULL, 3, 'P19', 0, 0, '5', 'Jasminum grandiflorum (Buds), Red ochre, Coccus lacca, Santalum album, \r\nRose water.', 'Effective for all kinds of Eye diseases, especially in congestion and wounds.', NULL, NULL, '50 Nos.', 'For ophthalmic application only with honey/breast milk/rose water.', 5, 1, 'JATHEEMUKULADI VARTHI', NULL, 'JATHEEMUKULADI VARTHI', 1, 14, '2019-03-12 17:43:31', '2019-08-13 12:35:59'),
(321, 'KACHOORADIVATTU (TABLET)', 'kachooradivattu-tablet', '', NULL, 3, 'P8', 0, 0, '28', 'Kaempferia galanga, Emblica officinalis, Glycyrrhiza glabra, Santalum album, \r\nAlpinia  calcarata,  Tamarindus  indica  (rachis),  Camphor,  Sida  cordifolia, \r\nSaffron, Tender coconut water, Rose water.', 'For external application & Thalam in Headache, Mental disorders, Fever, Eye \r\ndiseases.', NULL, 'For external application over crown, with breastmilk; lemon juice, coleus juice \r\netc. or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'KACHOORADIVATTU (TABLET)', NULL, 'KACHOORADIVATTU (TABLET)', 1, 12, '2019-03-12 17:45:06', '2019-08-13 14:20:09'),
(322, 'KAISORAGULGULU GULIKA (TABLET)', 'kaisoragulgulu-gulika-tablet', '', NULL, 3, 'P9B', 0, 0, '15', 'Commiphora wightii (purified), Thriphala, Tinospora cordifolia, Thrikatu,\r\nEmbelia ribes, Baliospermum montanum, Merrimia turpethum, Cow ghee.', 'Skin diseases. Chronic arthritis, Chronic wounds, Pthiasis, Diabetic carbuncle, \r\nAscites, Dropsy, Anaemia, Acne etc.', NULL, '2 tablets - b.d. with suitable Kashaya or Asavarishta or milk or as directed by \r\nthe physician.', '100 Nos.', NULL, 5, 1, 'KAISORAGULGULU GULIKA (TABLET)', NULL, 'KAISORAGULGULU GULIKA (TABLET)', 1, 15, '2019-03-12 17:47:42', '2019-08-13 14:21:35'),
(323, 'KANCHANARAGUGGULU GULIKA (TABLET)', 'kanchanaraguggulu-gulika-tablet', '', NULL, 3, 'P70', 0, 0, '15', 'Bauhinia variegata, Terminalia chebula, Terminalia bellerica, Emblica\r\nofficinalis, Commiphora wightii etc.', 'Diseases associated with Thyroid gland, tumors & extra growths, Fistula &\r\nskin disceases.', NULL, '2 tablets bd along with suitable kashaya or as directed by physician', NULL, NULL, 5, 1, 'KANCHANARAGUGGULU GULIKA (TABLET)', NULL, 'KANCHANARAGUGGULU GULIKA (TABLET)', 1, 10, '2019-03-13 09:31:53', '2019-08-13 14:23:42'),
(324, 'KARUTHAVATTU TABLET', 'karuthavattu-tablet', '', NULL, 3, 'P5', 0, 0, '6', 'Boswellia serrata, Yellow resin, Aloe vera. Headache, Contusions, Local\r\nswelling, Sprains.', 'For external application on in Headache, Contusions, Local swelling, Sprains.', NULL, 'For applying over affected areas.', '100 Nos.', NULL, 5, 1, 'KARUTHAVATTU TABLET', NULL, 'KARUTHAVATTU TABLET', 1, 12, '2019-03-13 09:33:34', '2019-08-13 14:22:22'),
(325, 'LAGHUSUTASEKHARA RASAM', 'laghusutasekhara-rasam', '5c94bc3f65b761553251391.jpg', NULL, 3, 'P67', 1, 0, '3', 'Red ochre, Zingiber officinale, Piper betle', 'Heart burn, Gastritis, hyper acidity, head ache, migraine, stomatitis, burning sensation', NULL, '2 Tablets twice daily before food', '100 nos', NULL, 5, 1, 'LAGHUSUTASEKHARA RASAM', NULL, 'LAGHUSUTASEKHARA RASAM', 1, 30, '2019-03-13 09:40:25', '2019-08-13 12:41:24'),
(326, 'KHADIRA GULIKA (TABLET)', 'khadira-gulika-tablet', '', NULL, 3, 'P11', 0, 0, '40', 'Acacia catechu, Acacia ferruginea, Red ochre, Santalum album, Glycyrrhiza \r\nglabra,  Coscinium  fenestratum,  Thriphala,  Chathurjathakam,  Myristica \r\nfragrans, Eugenia caryophyllata, Camphor.', 'Toothache, Pyorrhoea, Gingivitis, Stomatitis, Bleeding gum', NULL, NULL, '50 Nos.', 'For oral cavity application with Honey or Thriphala Kashayam. Crush and mix \r\nwith luke warm water for gargling.', 5, 1, 'KHADIRA GULIKA (TABLET)', NULL, 'KHADIRA GULIKA (TABLET)', 1, 15, '2019-03-13 09:41:31', '2019-08-13 12:40:04'),
(327, 'KRIMIGHNAVATI (TABLET)', 'krimighnavati-tablet', '', NULL, 3, 'P10', 0, 0, '3', 'Ferula asafoetida, Allium sativum, Vitex negundo.', 'Worm infestation, Colic, Sprue.', NULL, '½-1 tablet b.d. with honey/Turmeric juice/Ginger juice or as directed by the \r\nphysician.', '100 Nos.', NULL, 5, 1, 'KRIMIGHNAVATI (TABLET)', NULL, 'KRIMIGHNAVATI (TABLET)', 1, 12, '2019-03-13 09:44:29', '2019-08-13 12:39:35'),
(328, 'LAKSHAGUGGULU GULIKA (TABLET)', 'lakshaguggulu-gulika-tablet', '', NULL, 3, 'P69', 0, 0, '6', 'Laccifera lacca (secretion), Cissus quadrangularis, Terminalia arjuna,\r\nWithania somnifera, Sida veronicaefolia, Commiphora wightii.', 'Bone fracture, improper alignment of bones, Ostcoporosis etc.', NULL, '2 tablets bd along with suitable kashaya or as directed by physician', NULL, NULL, 5, 1, 'LAKSHAGUGGULU GULIKA (TABLET)', NULL, 'LAKSHAGUGGULU GULIKA (TABLET)', 1, 17, '2019-03-13 09:48:07', '2019-08-13 14:33:17'),
(329, 'MANASAMITHRAVATAKAM (TABLET)', 'manasamithravatakam-tablet', '', NULL, 3, 'P35', 0, 0, '71', 'Sida cordifolia, Aegle marmelos, Red coral (purified), Clitoria ternatea, Gold \r\n(processed),  Acorus  calamus,  Santalum  album,  Pearl  (processed),  Piper \r\nlongum,  Silver  (processed),  Iron  (processed),  Lotus  stamen,  Withania \r\nsomnifera,  Thriphala,  Sarcostemma  acidum,  Bacopa  monnieri, Asphaltum, \r\nSaffron.', 'Insanity, Epilepsy and Psychosis. Recommended in Neurological disorders, \r\nHypertension, INSOMNIA, Memory booster.', NULL, '1-2 tablet with milk at bedtime or as directed by the physician.', '100 Nos', NULL, 5, 1, 'MANASAMITHRAVATAKAM (TABLET)', NULL, 'MANASAMITHRAVATAKAM (TABLET)', 1, 11, '2019-03-13 09:50:33', '2019-08-13 14:33:42'),
(330, 'MANDOORAVATAKAM', 'mandooravatakam', '', NULL, 3, 'P34', 0, 0, '16', 'Iron pyrites (processed), Coscinium fenestratum, Cedrus deodara, Thrikatu, \r\nThriphala,  Cyperus  rotundus,  Plumbago  rosea,  Embelia  ribes,  Rust  iron \r\n(processed), Cow’s urine.', 'Anaemia, Dropsy, Skin diseases, Diabetes mellitus, Bleeding piles and\r\nJaundice.', NULL, '½ -1 tablet with buttermilk or lime juice after food (o.d) or as directed by the \r\nphysician.', '50 Nos.', NULL, 5, 1, 'MANDOORAVATAKAM', NULL, 'MANDOORAVATAKAM', 1, 12, '2019-03-13 09:53:06', '2019-08-13 14:34:15'),
(332, 'MARMAVATTU (TABLET)', 'marmavattu-tablet', '5d43e0b8c85991564729528.jpg', NULL, 3, 'P33', 0, 0, '5', 'Curcuma  angustifolia,  Boswellia  serrata,  Aloe  extract,  Opium,  Aloe  vera \r\n(Juice).', 'Contusions over vital points, Sprains, Carbuncles etc.', NULL, NULL, '100 Nos.', 'For external application only, with Moringa juice/egg white over the affected area.', 5, 1, 'MARMAVATTU (TABLET)', NULL, 'MARMAVATTU (TABLET)', 1, 20, '2019-03-13 09:56:50', '2019-08-13 12:45:08'),
(334, 'MUKKAMUKKATUKADI GULIKA (TABLET)', 'mukkamukkatukadi-gulika-tablet', '', NULL, 3, 'P36', 0, 0, '22', 'Thriphala, Thrikatu, Cuminum cyminum, Carum carvi, Acorus calamus,\r\nAndrographis paniculata, Rock salt, Camphor, Aloe extract, Ferula asafoetida,\r\nVitex negundo.', 'Chronic fever. Intermittent fever. Indigestion.', NULL, '1 tablet b.d. with Ginger juice/Thulasi juice/Honey or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'MUKKAMUKKATUKADI GULIKA (TABLET)', NULL, 'MUKKAMUKKATUKADI GULIKA (TABLET)', 1, 12, '2019-03-13 09:58:32', '2019-08-13 14:37:00'),
(335, 'NAVAYASAM GULIKA (TABLET)', 'navayasam-gulika-tablet', '5d2eedbf71ded1563356607.jpg', NULL, 3, 'P28', 0, 0, '10', 'Thrikatu, Cyperus rotundus, Plumbago rosea, Embelia ribes, Thriphala, Rust \r\niron (processed).', 'Anaemia, Liver disorders, Skin diseases, General fatigue.', NULL, '1  tablet  with  lemon  juice  and  honey  o.d.  after  food,  or  as  directed  by  the \r\nphysician.', '100 Nos.', NULL, 5, 1, 'NAVAYASAM GULIKA (TABLET)', NULL, 'NAVAYASAM GULIKA (TABLET)', 1, 33, '2019-03-13 09:59:55', '2019-08-13 12:46:02'),
(336, 'OUSHADHI HAIRTONE', 'oushadhi-hairtone', '5d2ee80ee26751563355150.jpg', NULL, 8, 'T79', 1, 0, '19', 'Caryota urens, Terminalia bellirica (whole plant), Emblica officinalis. Eclipta alba, Indigofera tinctoria, Terminalia chebula, Lotus stammen, Glycyrrhiza glabra, Galena.', 'Dandruff, Premature greying, Loss of hair.', NULL, NULL, '100 ml., 200 ml.', 'Massage the scalp with hair-tone for ten minutes before bath. Avoid soap.', 5, 1, 'OUSHADHI HAIRTONE', NULL, 'OUSHADHI HAIRTONE', 1, 47, '2019-03-13 10:01:04', '2019-08-28 16:49:36'),
(337, 'PONKARADI GULIKA (TABLET)', 'ponkaradi-gulika-tablet', '', NULL, 3, 'P32', 0, 0, '10', 'Borax (processed), Caesalpinia bonduc, Rock salt, Allium sativum, Ferula\r\nasafoetida, Iron pyrites (processed), Kantha sindooram, Cuminum cyminum,\r\nOpium (purified), Zingiber officinalis.', 'Intestinal colic, Dysmenorrhoea.', NULL, '1 tab b.d. with Ginger juice.', '100 Nos.', NULL, 5, 1, 'PONKARADI GULIKA (TABLET)', NULL, 'PONKARADI GULIKA (TABLET)', 1, 18, '2019-03-13 10:02:04', '2019-08-13 14:39:43'),
(338, 'PUSHYANUGAM GULIKA (TABLET)', 'pushyanugam-gulika-tablet', '', NULL, 3, 'P31', 0, 0, '28', 'Cyclea peltata, Rotula aquatica, Aegle marmelos, Aconitum heterophyllum, \r\nSymplocos cochinchinensis, Red ochre, Saffron, Raisins.', 'Menstrual disorders, Menorrhagia, Bleeding piles, Dysentery, Leucorrhoea.', NULL, '1 tab b.d. in milk/rice wash/honey.', '100 Nos.', NULL, 5, 1, 'PUSHYANUGAM GULIKA (TABLET)', NULL, 'PUSHYANUGAM GULIKA (TABLET)', 1, 7, '2019-03-13 10:06:04', '2019-08-13 12:47:27'),
(339, 'OUSHADHI TOOTH POWDER', 'oushadhi-tooth-powder', '5d2ef702218e71563358978.jpg', NULL, 8, 'C87C', 1, 0, '16', 'Mangifera indica (leaves), Elettaria cardamomum, Glycyrrhiza glabra, Red ochre, Myristica fragrans, Eugenia caryophyllata, Camphor.', 'Pyorrhoea, Bleeding and Spongy gums, Toothache. Prevents Dental Caries, strengthens teeth and gum. Useful to all age groups and restores oral hygiene.', NULL, 'For dental application.', '25g.', NULL, 5, 1, 'OUSHADHI TOOTH POWDER', NULL, 'OUSHADHI TOOTH POWDER', 1, 25, '2019-03-13 10:06:13', '2019-08-09 15:55:40'),
(340, 'RAJAHPRAVARTHINI VATI', 'rajahpravarthini-vati', '', NULL, 3, 'P81', 0, 0, '5', 'Aloe extract, Ferrous sulphate, Asafoetida, borax, Aloe vera', 'Dysmenorrhoea, irregular menstruation etc.', NULL, '1 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'RAJAHPRAVARTHINI VATI', NULL, 'RAJAHPRAVARTHINI VATI', 1, 9, '2019-03-13 10:09:24', '2019-08-13 12:47:56'),
(341, 'SARVAROGAKULANTHAKAM GULIKA (TABLET)', 'sarvarogakulanthakam-gulika-tablet', '5c94b426640741553249318.jpg', NULL, 3, 'P48', 0, 0, '4', 'Aconitum ferox (purified), Piper longum, Cinnabar (processed), Lemon juice.', 'Flatulence, Dyspnoea, Anorexia, Chronic fever, Arthritis, Tuberculosis.', NULL, '1 tab b.d. with suitable Kashayam or as direted by the physician.', '100 Nos.', NULL, 5, 1, 'SARVAROGAKULANTHAKAM GULIKA (TABLET)', NULL, 'SARVAROGAKULANTHAKAM GULIKA (TABLET)', 1, 7, '2019-03-13 10:11:05', '2019-08-13 12:48:59'),
(342, 'SHANMAKSHIKA VARTHI', 'shanmakshika-varthi', '', NULL, 3, 'P47', 0, 0, '6', 'Piper nigrum, Emblica officinalis, Conch (processed), Zinci carbonas\r\n(processed), Galena (processed), Iron pyrites (processed).', 'Allergic conditions of eyes, Cataract, Watering of eyes, Pterygium.', NULL, 'Ophthalmic application with honey/breast milk or as directed by the\r\nphysician.', '50 Nos.', NULL, 5, 1, 'SHANMAKSHIKA VARTHI', NULL, 'SHANMAKSHIKA VARTHI', 1, 7, '2019-03-13 10:14:03', '2019-08-13 14:41:36'),
(343, 'SIVA GULIKA', 'siva-gulika', '5d43e6e1c10141564731105.jpg', NULL, 3, 'P44', 0, 0, '59', 'Asphaltum, Thriphala, Dasamoolam, Tinospora cordifolia, Sida cordifolia,\r\nTrichosanthes anguina, Hemidesmus indicus, Withania somnifera, Ipomoea\r\nmauritiana, Chathurjathakam, Sugar, Cow ghee, Honey, Gingelly oil, Jasmine\r\nflower (for flavour).', 'Chronic arthritis, Rheumatic complaints, Urinogenital disorders,\r\nSpermatorrhoea, Splenic disorders, Liver disorders, Mental disorders, Skin\r\ndiseases, Vitiligo, Infertility, Gout', NULL, '1 tao b.d. with milk/honey/suitable Kashayam or as directed by the physician', '10 Nos.', NULL, 5, 1, 'SIVA GULIKA', NULL, 'SIVA GULIKA', 1, 23, '2019-03-13 10:15:38', '2019-08-13 14:43:16'),
(344, 'OUSHADHI DAHASAMANI', 'oushadhi-dahasamani', '5d2ee44910d091563354185.jpg', NULL, 8, 'C89D', 1, 0, '7', 'Acacia catechu, Sarasaparilla, Vetiveria zizanioides, Caesalpinia sappan, Elettaria cardamomum, Dried ginger, Petrocarpus marsupicum.', 'Thirst, Dryness of mouth, Distaste, Fatigue.', NULL, '3 - 5 g. powder in one litre water boil and cool, can be used as drinking water for all age groups.', '50g.', NULL, 5, 1, 'OUSHADHI DAHASAMANI', NULL, 'OUSHADHI DAHASAMANI', 1, 27, '2019-03-13 10:16:26', '2019-08-09 15:51:35'),
(346, 'OUSHADHI COUGH SYRUP', 'oushadhi-cough-syrup', '5c94bce40acb51553251556.jpg', NULL, 8, 'TX4A', 1, 0, '17', 'Adhatoda beddomei, Clerodendrum serratum, Thrikatu, Terminalia bellirica, Glycyrrhiza glabra, Ocimum sanctum, Coleus aromaticus, Camphor.', 'Allergic dry cough, Throat irritation, Hoarseness of voice.', NULL, 'Adult: 5ml-10ml Twice or Thrice daily. Children: 2.5ml-5ml Twice or Thrice daily.', '100ml.', NULL, 5, 1, 'OUSHADHI COUGH SYRUP', NULL, 'OUSHADHI COUGH SYRUP', 1, 23, '2019-03-13 10:18:41', '2019-08-09 15:50:42'),
(347, 'SURYAPRABHA GULIKA (TABLET)', 'suryaprabha-gulika-tablet', '', NULL, 3, 'P50', 0, 0, '12', 'Cinnabar (processed), Curcuma longa, Sulphur (processed), Ferula\r\nasafoetida, Thriphala, Thrikatu, Cuminum cyminum, Aconitum ferox (purified).', 'Fever, Cough, Chest infection, Flu.', NULL, '1 tab b.d. with Ginger juice/honey or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'SURYAPRABHA GULIKA (TABLET)', NULL, 'SURYAPRABHA GULIKA (TABLET)', 1, 7, '2019-03-13 10:19:50', '2019-08-13 14:43:56'),
(348, 'SWASANANDAM GULIKA TABLET', 'swasanandam-gulika-tablet', '', NULL, 3, 'P45', 0, 0, '6', 'Cinnabar (processed), Aconitum ferox (purified), Thriphala, Camphor, Lemon \r\njuice.', 'Asthma, Cough, Bronchitis, Dyspnoea, Flatulence.', NULL, '1 lab b.d. with Thulasi juice/Cumin seed water/Honey/Adhatoda leaf juice or \r\nas directed by the physician.', '100 Nos', NULL, 5, 1, 'SWASANANDAM GULIKA TABLET', NULL, 'SWASANANDAM GULIKA TABLET', 1, 16, '2019-03-13 10:22:31', '2019-08-13 14:44:34'),
(349, 'THRIPHALADI GULIKA (TABLET)', 'thriphaladi-gulika-tablet', '', NULL, 3, 'P56', 0, 0, '19', 'Thriphala, Panchalavanam, Saussurea lappa, Picorrhiza curroa, Sida\r\ncordifolia, Pongamia pinnata.', 'Eye diseases.', NULL, '1 tablet with decoction of Thriphala o.d. or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'THRIPHALADI GULIKA (TABLET)', NULL, 'THRIPHALADI GULIKA (TABLET)', 1, 9, '2019-03-13 10:24:51', '2019-08-13 14:45:05'),
(350, 'URAMARUNNU GULIKA', 'uramarunnu-gulika', '', NULL, 3, 'P4', 0, 0, '24', 'Thrikatu, Ariyaru, Eugenia caryophyllata, Oak gall, Allium sativum, Coleus\r\naromaticus.', 'Neonatal digestive disorders. Improves digestive power.', NULL, '¼-½ tab with breast milk and honey b.d. or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'URAMARUNNU GULIKA', NULL, 'URAMARUNNU GULIKA', 1, 4, '2019-03-13 10:32:22', '2019-08-13 14:46:32'),
(351, 'VAYU GULIKA (TABLET)', 'vayu-gulika-tablet', '', NULL, 3, 'P6', 0, 0, '35', 'Andrographis paniculata, Thriphala, Thrijathakam, Santalum album, Acorus\r\ncalamus, Thrikatu, Galena (processed), Borax (processed), Aconitum ferox\r\n(purified), Red orpiment (processed), Cinnabar (processed), Cuminum\r\ncyminum.', 'Flatulence, Dyspnoea, Bronchial complaints, Cardiac disorders, Indigestion, etc.', NULL, '1 tablet b.d. with Cumin seed water/suitable vehicles.', '100 Nos.', NULL, 5, 1, 'VAYU GULIKA (TABLET)', NULL, 'VAYU GULIKA (TABLET)', 1, 8, '2019-03-13 10:35:54', '2019-08-13 13:29:53'),
(352, 'VETTUMARAN GULIKA (TABLET)', 'vettumaran-gulika-tablet', '', NULL, 3, 'P43', 0, 0, '6', 'Borax (processed), Piper nigrum, Aconitum ferox (purified), \r\nCinnabar (processed), Piper longum, Apium graveolens, Zingiber officinale.', 'Fever, Flu, Mumps.', NULL, '1 tab with Ginger juice and honey b.d. or as directed by the physician.', '100 Nos.', NULL, 5, 1, 'VETTUMARAN GULIKA (TABLET)', NULL, 'VETTUMARAN GULIKA (TABLET)', 1, 30, '2019-03-13 10:40:28', '2019-08-13 12:51:26'),
(353, 'OUSHADHI BLISS BALM', 'oushadhi-bliss-balm', '5c94b86e7a3c91553250414.jpg', NULL, 8, 'TX3A', 1, 0, '14', 'Kachooradi Choornam Ointment base', 'Head ache, Cold, Feverishness.', NULL, NULL, '10g.', 'For external application over forehead', 5, 1, 'OUSHADHI BLISS BALM', NULL, 'OUSHADHI BLISS BALM', 1, 39, '2019-03-13 10:42:46', '2019-08-09 15:49:08'),
(354, 'VIDANGADI GULIKA (TABLET)', 'vidangadi-gulika-tablet', '', NULL, 3, 'P55', 0, 0, '4', 'Embelia ribes, Hibiscus rosasenensis, Ferula asafoetida.', 'Dysmenorrhoea', NULL, '1-2 Tab. with Thila kashayam.', '100 Nos.', NULL, 5, 1, 'VIDANGADI GULIKA (TABLET)', NULL, 'VIDANGADI GULIKA (TABLET)', 1, 3, '2019-03-13 10:44:34', '2019-08-13 14:47:14'),
(355, 'VILWADI GULIKA (TABLET', 'vilwadi-gulika-tablet', '', NULL, 3, 'P41', 0, 0, '14', 'Aegle marmelos, Ocimum sanctum (spike), Pongamia pinnata, Indian valerian, \r\nCedrus deodara, Thriphala, Thrikatu, Curcuma longa, Coscinium fenestratum, \r\nGoat urine.', 'Snake poison, Insect poison, Rat bite, Diarrhoea, Dysentery, Indigestion etc.', NULL, 'For external as well as internal application with Thulasi juice/honey.', '100 Nos.', NULL, 5, 1, 'VILWADI GULIKA (TABLET', NULL, 'VILWADI GULIKA (TABLET', 1, 10, '2019-03-13 10:49:15', '2019-08-13 12:52:10'),
(357, 'VIMALAVARTHI', 'vimalavarthi', '5d43f48fda0e81564734607.jpg', NULL, 3, 'P42', 0, 0, '10', 'Glycyrrhiza glabra, Piper longum, Piper nigrum, Symplocos cochinchinensis, \r\nCoscinium fenestratum, Thriphala, Camphor, Rose water.', 'Cataract, Redness and Contusions in eyes, Allergic conditions of eyes.', NULL, NULL, '50 Nos.', 'For ophthalmic use only.', 5, 1, 'VIMALAVARTHI', NULL, 'VIMALAVARTHI', 1, 10, '2019-03-13 10:55:26', '2019-08-13 12:52:27'),
(358, 'PRAMEHOUSHADHI', 'pramehoushadhi-2', '5d4bb19c23d601565241756.jpg', NULL, 8, 'C86', 1, 1, '14', 'Emblica officinalis, Salacia fruiticosa, Curcuma longa, Trigonella foenum- graecum, Vernonia anthelmintica, Phyllanthus amarus, Mimordica charantia.', 'Diabetes mellitus. Reduces Drowsiness, Fatigue and Constipation related with Diabetes.', NULL, '5-10 gm. powder in boiled and cooled water, preferably prepared with DIABET DRINKS in empty stomach in the morning. Dose may be maintained according to the blood sugar level as directed by the physician.', '100 g., 250 g.', NULL, 5, 1, 'PRAMEHOUSHADHI', NULL, 'PRAMEHOUSHADHI', 1, 287, '2019-03-13 10:56:39', '2019-08-21 14:17:33'),
(359, 'YOGARAJAGULGULU GULIKA (TABLET', 'yogarajagulgulu-gulika-tablet', '5c94c28c8e7f31553253004.jpg', NULL, 3, 'P59', 0, 0, '28', 'Plumbago rosea, Cuminum cyminum, Embelia ribes, Cedrus deodara, Rock \r\nsalt, Alpinia calcarata, Tribulus terrestris, Thriphala, Thrikatu, Potassi carbonas, \r\nCommiphora wightii (purified).', 'Rheumatic diseases, Skin diseases, Piles, Arthritis, Fistula, Male genital\r\ndisorders, Epilepsy. Improves the regeneration of cells, thereby leading to\r\nincreased vigour and vitality', NULL, '2  tablets  -  b.d.  with  suitable  Kashayam/Asavarishtam  or  as  directed  by physician.', '100 Nos.', NULL, 5, 1, 'YOGARAJAGULGULU GULIKA (TABLET', NULL, 'YOGARAJAGULGULU GULIKA (TABLET', 1, 14, '2019-03-13 10:57:30', '2019-08-13 14:49:52'),
(360, 'PRAMEHOUSHADHI TABLET', 'pramehoushadhi-tablet', '', NULL, 8, 'P68', 1, 0, '14', 'Emblica officinalis, Salacia oblonga, Curcuma longa, Trigonella foenum graceum, Vernonia anthelmintica, Phyllanthus amarus, Caesalpinia sappan, Pterocarpus marsupium, Strychnos potatorum, Ixora coccinea, Symplocos cochinchinensis, Aerva lanata, Vetiveria Zizanioides, Momordica charantia', 'Diabetes Mellitus. Reduces drowsiness, Fatigue, and constipation related with diabetes', NULL, 'I -2 tab twice daily 10 min before food or as directed by the physician', '10 x 10 blister of 500mg tab', NULL, 5, 1, 'PRAMEHOUSHADHI TABLET', NULL, 'PRAMEHOUSHADHI TABLET', 1, 33, '2019-03-13 10:59:06', '2019-08-09 15:57:29'),
(362, 'PSORSET OINTMENT', 'psorset-ointment', '5c94c050d97411553252432.jpg', NULL, 8, 'TX6A', 1, 0, '5', 'Wrightia tinctoria, sodium borate, Coconut oil, Ointment base.', 'Psoriasis, Dermatitis, Allergic Skin Diseases. ', NULL, NULL, '50g', 'For external application over forehead', 5, 1, 'PSORSET OINTMENT', NULL, 'PSORSET OINTMENT', 1, 41, '2019-03-13 11:07:42', '2019-08-09 15:58:36'),
(363, 'AMRUTHOTHARAM KASHAYAM', 'amruthotharam-kashayam-2', '5c94aa45910441553246789.jpg', NULL, 6, 'D1', 0, 0, '3', 'Tinospora cordifolia, Terminalia chebula, Dried ginger.', 'Fever, Flu, Cold and Headache. Used as a digestant in Rheumatic fever.', NULL, NULL, '200 ml', NULL, 5, 1, 'AMRUTHOTHARAM KASHAYAM', NULL, 'AMRUTHOTHARAM KASHAYAM', 1, 25, '2019-03-13 11:18:10', '2019-08-09 16:49:02'),
(366, 'PSORSET OIL', 'psorset-oil', '5d4bb1e4a9c1c1565241828.jpg', NULL, 8, 'TX5A', 1, 1, '3', 'Wrightia tinctoria, sodium borate, Coconut oil', 'Psoriasis, Dermatitis, Allergic Skin Diseases.', NULL, 'For external application over forehead', '100ml.', NULL, 5, 1, 'PSORSET OIL', NULL, 'PSORSET OIL', 1, 312, '2019-03-13 11:39:20', '2019-08-21 14:50:14'),
(368, 'OUSHADHI THENGINPOOKKULAMRUTHAM', 'oushadhi-thenginpookkulamrutham', '5c94bf228fc781553252130.jpg', NULL, 8, 'H27', 1, 0, '29', 'Cocos nucifera (inflorescence), Ariyaru, Jeerakathrayam, Thrikatu, Jaggery, Coconut juice, Ghee, Honey.', 'Low back-ache in both sex, Leucorrhoea, Menorrhagia, Post -natal disorders, Increases breast milk, improves health and vigour.', NULL, '2 tsp. b.d. after food preferably with milk.', '400g.', NULL, 5, 1, 'OUSHADHI THENGINPOOKKULAMRUTHAM', NULL, 'OUSHADHI THENGINPOOKKULAMRUTHAM', 1, 34, '2019-03-13 11:43:30', '2019-08-09 15:53:59'),
(369, 'BALAGULUCHYADI KASHAYAM', 'balaguluchyadi-kashayam', '5d2ecdc77d8331563348423.jpg', NULL, 6, 'D47', 0, 0, '3', 'Sida cordifolia, Tinospora cordifolia, Cedrus deodara.', 'Arthritis, Rheumatic complaints.', NULL, NULL, '200 ml.', NULL, 5, 1, 'BALAGULUCHYADI KASHAYAM', NULL, 'BALAGULUCHYADI KASHAYAM', 1, 18, '2019-03-13 11:43:49', '2019-08-13 11:08:08'),
(370, 'SHADDHARANAM TABLET', 'shaddharanam-tablet', '', NULL, 8, 'P63', 1, 0, '6', 'Plumbago rosea, Seeds of Holarrhena antidysenterica, Cyclea peltata, Picrorrhiza curroa, Aconitum heterophyllum, Terminalia chebula.', 'Rheumatic fever and associated symptoms.', NULL, '1 -2 tabs with Honey/Buttermilk/Suitable kashayam, twice/thrice daily before food or as directed by the Physician', '10 x 10 Blister pack.', NULL, 5, 1, 'SHADDHARANAM TABLET', NULL, 'SHADDHARANAM TABLET', 1, 33, '2019-03-13 11:45:15', '2019-08-09 16:03:49'),
(371, 'BALAJEERAKADI KASHAYAM', 'balajeerakadi-kashayam', '5d2ecd85acbe41563348357.jpg', NULL, 6, 'D49', 0, 0, '10', 'Sida cordifolia,Cuminum cyminum, Aegle marmelos, Cyperus rotundus, Adhatoda beddomei, Dried ginger, Cedrus deodara, Pseudarthria viscida, Saccharum officinarum, Fried grain.', 'Respiratory disorders, Cough, Chest pain, Flatulence.', NULL, NULL, '200 ml.', NULL, 5, 1, 'BALAJEERAKADI KASHAYAM', NULL, 'BALAJEERAKADI KASHAYAM', 1, 10, '2019-03-13 11:45:35', '2019-08-13 11:09:06'),
(374, 'SUDARSANAM TABLET', 'sudarsanam-tablet', '5d2ec3638c5d41563345763.jpg', NULL, 8, 'P62', 1, 0, '54', 'Sudarsanachoornam', 'Fever and associated symptoms.', NULL, '1-2 tabs, with Honey/Suitable kashayam, twice/thrice daily before food or as directed by the Physician.', '10 x 10 Blister pack', NULL, 5, 1, 'SUDARSANAM TABLET', NULL, 'SUDARSANAM TABLET', 1, 69, '2019-03-13 11:50:37', '2019-08-09 16:06:52'),
(375, 'VIGOR PLUS (Capsules)', 'vigor-plus-capsules', '5d2ec25626e9e1563345494.jpg', NULL, 8, 'P64', 1, 1, '13', 'Withania somnifera, Mucuna Pruriens, Asparagus Racemosus, Tribulus terrestris, Boerrhavia diffusa, Piper longum, Silajathu', 'General fatigue, sexual Dysfunctions, Emaciation, Improves physical strength & Vigor, Recommended for the management of male infertility.', NULL, '2 capsules twice daily after food preferably with milk.', '60 caps in White HDPE Bottles', NULL, 5, 1, 'VIGOR PLUS (Capsules)', NULL, 'VIGOR PLUS (Capsules)', 1, 354, '2019-03-13 11:52:14', '2019-08-28 16:49:18'),
(376, 'CHIRUVILWADI KASHAYAM', 'chiruvilwadi-kashayam', '', NULL, 6, 'D6', 0, 0, '6', 'Holoptelia integrifolia, Boerrhavia diffusa, Plumbago rosea, Terminalia chebula, Piper longum, Dried ginger.', 'Piles, Fistula, Pthiasis, Indigestion, Loss of appetite.', NULL, NULL, '200 ml.', NULL, 5, 1, 'CHIRUVILWADI KASHAYAM', NULL, 'CHIRUVILWADI KASHAYAM', 1, 9, '2019-03-13 11:52:15', '2019-08-13 11:19:22'),
(377, 'LIPOCARE TABLET', 'lipocare-tablet', '5c94c0ba9763c1553252538.jpg', NULL, 8, 'P65', 1, 0, '10', 'Piper longum, Piper longum root, Piper chaba, Plumbago Indica, Zingiber officinalis, Terminalia chebula, Terminalia bellirica, Emblica officinalis, Murraya koenigii, Alllium sativum', 'Hypercholesterolemia and associated symptoms', NULL, '2 tabs twice daily half an hour before food.', '100Nos.', NULL, 5, 1, 'LIPOCARE TABLET', NULL, 'LIPOCARE TABLET', 1, 67, '2019-03-13 11:56:49', '2019-08-09 15:47:23'),
(378, 'OUSHADHI BABY OIL', 'oushadhi-baby-oil', '5c94b8c9098801553250505.jpg', NULL, 8, 'TX7A', 1, 0, '5', 'Coconut Oil, Virgin Coconut Oil, Ocimum sanctum, Ixora coccinea, Carum carvi', 'Application on head and body for Children', NULL, NULL, '100 ml.', NULL, 5, 1, 'OUSHADHI BABY OIL', NULL, 'OUSHADHI BABY OIL', 1, 34, '2019-03-13 11:58:32', '2019-08-09 15:48:28'),
(379, 'CARDOCARE TABLET', 'cardocare-tablet', '5c94c1b429d391553252788.jpg', NULL, 8, 'P66', 1, 0, '12', 'Withania somnifera, Rauvoifia serpentina, Nardostachys jatamansi, Boerhavia diffusa, Coral, Punica granatum, Moringa oleifera, Terminalia arjuna', 'High blood pressure & associated symptoms', NULL, '1-2 tabs/ Once/ Twice daily as directed by Physician.', '100 Nos.', NULL, 5, 1, 'CARDOCARE TABLET', NULL, 'CARDOCARE TABLET', 1, 48, '2019-03-13 12:00:23', '2019-08-09 15:45:49'),
(380, 'DASAMOOLAKADUTRAYAM KASHAYAM', 'dasamoolakadutrayam-kashayam-2', '5d2eccedbcd4a1563348205.jpg', NULL, 6, 'D7', 0, 0, '14', 'Dasamoolam, Thrikatu, Adhatoda beddomei.', 'Respiratory disorders, Rhinitis, Cold, Bodyache, Headache, Cough, Flu, Fever and Low back-ache.', NULL, NULL, '200 ml', NULL, 5, 1, 'DASAMOOLAKADUTRAYAM KASHAYAM', NULL, 'DASAMOOLAKADUTRAYAM KASHAYAM', 1, 12, '2019-03-13 12:01:59', '2019-08-13 11:23:28'),
(382, 'OUSHADHI FACEPACK', 'oushadhi-facepack', '5d2ee699c3cbc1563354777.jpg', NULL, 8, 'CC15', 1, 1, '9', 'curcuma aromatica, Coriandrum sativum, rubia cordifolia, Curcuma longa, Acorus calamus etc.', 'Varnyam, Enhance complextion of the face.', NULL, 'External application along with rose water/honey/buttermilk according to skin type.', '50gm.', NULL, 5, 1, 'OUSHADHI FACEPACK', NULL, 'OUSHADHI FACEPACK', 1, 315, '2019-03-13 12:04:03', '2019-08-21 14:28:40'),
(383, 'DHANADANAYANADI KASHAYAM', 'dhanadanayanadi-kashayam', '5d2eccc8dd43c1563348168.jpg', NULL, 6, 'D42', 0, 0, '14', 'Caesalpinia bonduc, Dried ginger, Moringa oleifera, Alpinia calcarata, Acorus calamus, Crataeva religiosa, Allium sativum, Piper longum, Plumbago rosea, Ricinus communis, Cedrus deodara, Cyperus rotundus, Terminalia chebula, Clerodendrum serratum', 'Rheumatic complaints especially Facial palsy, Hemiplegia.', NULL, NULL, '200 ml', NULL, 5, 1, 'DHANADANAYANADI KASHAYAM', NULL, 'DHANADANAYANADI KASHAYAM', 1, 14, '2019-03-13 12:04:25', '2019-08-13 11:26:23'),
(384, 'DHANWANTHARAM KASHAYAM', 'dhanwantharam-kashayam', '5d2ecc9cbd5051563348124.jpg', NULL, 6, 'D15', 0, 0, '44', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, Asparagus racemosus, Withania somnifera, Ipomoea mauritiana, Santalum album, Cedrus deodara, Rubia cordifolia, Boerrhavia diffusa, Glycyrrhiza glabra, Peucedanum graveolens, Hemidesmus indicus, Elettaria cardamomum, Thriphala.', 'Useful in Rheumatic complaints. Recommended in Postnatal care for getting relief from post-partum strain and for improving physical strength.', NULL, NULL, '200 ml', NULL, 5, 1, 'DHANWANTHARAM KASHAYAM', NULL, 'DHANWANTHARAM KASHAYAM', 1, 10, '2019-03-13 12:06:28', '2019-08-13 11:31:29'),
(385, 'DRAKSHADI KASHAYAM', 'drakshadi-kashayam', '5d2ecc510981a1563348049.jpg', NULL, 6, 'D8', 0, 0, '16', 'Raisins, Madhuca indica, Glycyrrhiza glabra, Symplocos cochinchinensis, Gmelina arborea, Hemidesmus indicus, Cyperus rotundus, Emblica officinalis, Coleus zeylanicus, Lotus stamen, stem and rhizome, Caesalpinia sappan, Santalum album, Vetiveria zizanioides, Phoenix farinifera.', 'General  debility,  Fatigue,  Giddiness,  Fainting,  Epistaxis,  Dizziness,  Fever,  Vomitting. Can be recommended in Antenatal care. Jaundice.', NULL, NULL, '200 ml.', NULL, 5, 1, 'DRAKSHADI KASHAYAM', NULL, 'DRAKSHADI KASHAYAM', 1, 17, '2019-03-13 12:08:48', '2019-08-13 11:33:18'),
(386, 'DUSPARSAKADI KASHAYAM', 'dusparsakadi-kashayam', '', NULL, 6, 'D57', 0, 0, '5', 'Tragia involucrata, Aegle marmelos, Cuminum cyminum, Dried ginger, Cyclea peltata', 'Pain and other symptoms related with Piles and fistula.', NULL, NULL, '200ml', NULL, 5, 1, 'DUSPARSAKADI KASHAYAM', NULL, 'DUSPARSAKADI KASHAYAM', 1, 9, '2019-03-13 12:16:07', '2019-08-13 11:34:25'),
(387, 'ELAKANADI KASHAYAM', 'elakanadi-kashayam', '5d2ecbffbc3c91563347967.jpg', NULL, 6, 'D2', 0, 0, '19', 'Elettaria cardamomum, Piper longum, Glycyrrhiza glabra, Dried ginger, Cyperus rotundus, Adhatoda beddomei, Azadirachta indica, Tinospora cordifolia, Coleus zeylanicus, Dasamoolam.', 'Bronchitis, Cough, Flu, Fever, Dyspnoea, Consumption, Anorexia.', NULL, NULL, '200 ml', NULL, 5, 1, 'ELAKANADI KASHAYAM', NULL, 'ELAKANADI KASHAYAM', 1, 9, '2019-03-13 12:18:00', '2019-08-13 11:39:10'),
(388, 'GANDHARVAHASTHADI KASHAYAM', 'gandharvahasthadi-kashayam', '', NULL, 6, 'D4', 0, 0, '8', 'Ricinus communis, Holoptelia integrifolia, Plumbago rosea, Dried ginger, Terminalia chebula, Boerrhavia diffusa, Tragia involucrata, Curculigo orchioides', 'Rheumatic complaints, Flatulence, Loss of appetite, Constipation, Anorexia, Low back-ache.', NULL, NULL, '200 ml.', NULL, 5, 1, 'GANDHARVAHASTHADI KASHAYAM', NULL, 'GANDHARVAHASTHADI KASHAYAM', 1, 11, '2019-03-13 12:23:19', '2019-08-13 11:40:37'),
(389, 'GULGULUTHIKTHAKAM KASHAYAM', 'gulguluthikthakam-kashayam', '5d2ee77fdbfbb1563355007.jpg', NULL, 6, 'D41', 0, 0, '31', 'Commiphora wightii, Panchathiktham, Cyclea peltata, Embelia ribes, Curcuma longa, Alpinia calcarata, Aconitum heterophyllym.', 'Rheumatic  complaints,  Arthritis,  Chronic  skin  diseases,  Fistula,  Sinuses, Pthiasis.', NULL, NULL, '200 ml', NULL, 5, 1, 'GULGULUTHIKTHAKAM KASHAYAM', NULL, 'GULGULUTHIKTHAKAM KASHAYAM', 1, 13, '2019-03-13 12:34:05', '2019-08-13 11:47:21'),
(390, 'GULUCHYADI KASHAYAM', 'guluchyadi-kashayam', '5c94c145a94e61553252677.jpg', NULL, 6, 'D5', 0, 0, '5', 'Tinospora cordifolia, Caesalpinia sappan, Azadirachta indica, Coriandrum sativum, Pterocarpus santalinus', 'Fever, Vomitting, Burning sensation, Thirst, Skin disorders, Fainting due to excessive heat.', NULL, NULL, '200ml', NULL, 5, 1, 'GULUCHYADI KASHAYAM', NULL, 'GULUCHYADI KASHAYAM', 1, 12, '2019-03-13 12:37:53', '2019-08-13 11:48:10'),
(391, 'INDUKANTHAM KASHAYAM', 'indukantham-kashayam', '', NULL, 6, 'D55', 0, 0, '18', 'Holoptelia integrifolia, Cedrus deodara, Dasamoolam, Shadpalam', 'Anorexia, Colic, Intermittent fever. Promotes health and strength by improving metabolism', NULL, NULL, '200 ml', NULL, 5, 1, 'INDUKANTHAM KASHAYAM', NULL, 'INDUKANTHAM KASHAYAM', 1, 11, '2019-03-13 12:42:04', '2019-08-13 11:49:40'),
(392, 'KATHAKAKHADIRADI KASHAYAM', 'kathakakhadiradi-kashayam', '5d2ecb9919bab1563347865.jpg', NULL, 6, 'D51', 0, 0, '12', 'Strychnos potatorum, Acacia catechu, Emblica officinalis, Salacia fruiticosa, Coscinium fenestratum, Biophytum sensitivum, Ziziphus jujuba, Curcuma longa, Cyclea peltata, Mangifera indica (nut), Terminalia chebula, Cyperus rotundus.', 'Diabetes mellitus (NIDDM). Also recommended in the symptoms related with DM.', NULL, NULL, '200 ml', NULL, 5, 1, 'KATHAKAKHADIRADI KASHAYAM', NULL, 'KATHAKAKHADIRADI KASHAYAM', 1, 9, '2019-03-13 12:45:50', '2019-08-13 11:51:01'),
(393, 'KOKILAKSHAM KASHAYAM', 'kokilaksham-kashayam', '', NULL, 6, 'D60', 0, 0, '1', 'Hygrophila auriculata', 'Rheumatoid Arthrities, Gout.', NULL, NULL, NULL, NULL, 5, 1, 'KOKILAKSHAM KASHAYAM', NULL, 'KOKILAKSHAM KASHAYAM', 1, 10, '2019-03-13 12:47:28', '2019-08-09 17:00:46'),
(395, 'MAHAMANJISHTADI KASHAYAM', 'mahamanjishtadi-kashayam', '', NULL, 6, 'D69', 0, 0, '45', 'Rubia cordifolia, Holorrhena antidysentrica, Cyperus rotundus, Tinospora cordiflolalia, Zingiber officinale etc', 'Vatharaktham, paralysis, filariasis, obesity, eye diseases etc.', NULL, NULL, NULL, NULL, 5, 1, 'MAHAMANJISHTADI KASHAYAM', NULL, 'MAHAMANJISHTADI KASHAYAM', 1, 14, '2019-03-13 12:51:43', '2019-08-13 11:52:37'),
(396, 'MAHARASNADI KASHAYAM', 'maharasnadi-kashayam', '5d2ecb4062f781563347776.jpg', NULL, 6, 'D22', 0, 0, '26', 'Alpinia calcarata, Sida cordifolia, Ricinus communis, Acorus calamus, Boerrhavia diffusa, Tinospora cordifolia, Cassia fistula, Tribulus terrestris, Withania somnifera, Aconitum heterophyllum, Solanum melongena', 'Rheumatic diseases like Hemiplegia, Brachial palsy, Sciatica etc., Rheumatic fever. Also recommended in Hernia, Elephantiasis, Lassitude, Numbness.', NULL, NULL, '200 ml', NULL, 5, 1, 'MAHARASNADI KASHAYAM', NULL, 'MAHARASNADI KASHAYAM', 1, 11, '2019-03-13 12:59:01', '2019-08-13 11:53:27'),
(397, 'MAHATHIKTHAKAM KASHAYAM', 'mahathikthakam-kashayam', '5d2ecb2061dfe1563347744.jpg', NULL, 6, 'D48', 0, 0, '33', 'Symplocos cochinchinensis, Oldenlandia corymbosa, Cassia fistula, Picrorrhiza curroa, Thriphala, Curcuma longa, Coscinium fenestratum, Azadirachta indica, Tinospora cordifolia, Bacopa monnieri.', 'Chronic skin disorders,  Jaundice,  Vitiligo,  Impetigo,  Fistula,  Sinuses,  Scrofula, Cataract, Cellulitis.', NULL, NULL, '200 ml.', NULL, 5, 1, 'MAHATHIKTHAKAM KASHAYAM', NULL, 'MAHATHIKTHAKAM KASHAYAM', 1, 7, '2019-03-13 13:06:46', '2019-08-13 11:54:10'),
(398, 'MANJISHTADI KASHAYAM', 'manjishtadi-kashayam', '5d2ecadd3203f1563347677.jpg', NULL, 6, 'D23', 0, 0, '9', 'Rubia cordifolia, Thriphala, Solanum indicum, Acorus calamus, Cedrus deodara, Curcuma longa, Tinospora cordifolia, Azadirachta indica.', 'Arthritis, Skin diseases, Diabetes, Chilblain', NULL, NULL, '200 ml.', NULL, 5, 1, 'MANJISHTADI KASHAYAM', NULL, 'MANJISHTADI KASHAYAM', 1, 13, '2019-03-13 13:10:25', '2019-08-13 11:54:49'),
(399, 'MUSALIKHADIRADI KASHAYAM', 'musalikhadiradi-kashayam', '5d2ecab5594e21563347637.jpg', NULL, 6, 'D25', 0, 0, '6', 'Curculigo orchioides, Acacia catechu, Emblica officinalis, Tribulus terrestris,  Asparagus racemosus, Syzygium cumini.', 'Leucorrhoea, Dysfunctional uterine bleeding, Albuminuria.', NULL, NULL, '200 ml', NULL, 5, 1, 'MUSALIKHADIRADI KASHAYAM', NULL, 'MUSALIKHADIRADI KASHAYAM', 1, 9, '2019-03-13 13:12:07', '2019-08-13 11:56:17'),
(400, 'MUSTHAKARANJADI KASHAYAM', 'musthakaranjadi-kashayam', '5d2eca940fb6b1563347604.jpg', NULL, 6, 'D24', 0, 0, '8', 'Cyperus rotundus, Pongamia pinnata, Aconitum heterophyllum, Plumbago rosea, Aegle marmelos, Dried ginger, Piper spp., Holarrhena antidysenterica.', 'Diarrhoea, Dysentery, Colic.', NULL, NULL, '200 ml', NULL, 5, 1, 'MUSTHAKARANJADI KASHAYAM', NULL, 'MUSTHAKARANJADI KASHAYAM', 1, 6, '2019-03-13 13:20:18', '2019-08-13 11:58:13'),
(401, 'AMRUTHOTHARAM KASHAYACHOORNAM', 'amruthotharam-kashayachoornam', '', NULL, 24, 'K1', 0, 0, '3', 'Tinospora cordifolia, Terminalia chebula, Dried ginger.', 'Fever, Flu, Cold and Headache. Used as a digestant in Rheumatic fever.', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'AMRUTHOTHARAM KASHAYACHOORNAM', NULL, 'AMRUTHOTHARAM KASHAYACHOORNAM', 1, 24, '2019-03-13 13:32:07', '2019-08-13 11:36:41'),
(402, 'ASHTAVARGAM KASHAYACHOORNAM', 'ashtavargam-kashayachoornam', '', NULL, 24, 'K42A', 0, 0, '8', 'Sida cordifolia, Strobilanthes ciliatus, Ricinus communis, Dried ginger, Alpinia calcarata, Cedrus deodara, Vitex negundo, Allium sativum.', 'Rheumatic complaints, Hemiplegia, Numbness, Vertigo, Hypertension.', NULL, NULL, '250 g., 500 g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'ASHTAVARGAM KASHAYACHOORNAM', NULL, 'ASHTAVARGAM KASHAYACHOORNAM', 1, 17, '2019-03-13 13:40:24', '2019-08-13 12:48:17'),
(403, 'BHADRADARVADI KASHAYACHOORNAM', 'bhadradarvadi-kashayachoornam', '', NULL, 24, 'K21', 0, 0, '15', 'Cedrus deodara, Indian valerian, Saussurea lappa, Dasamoolam, Sida cordifolia', 'Rheumaticdiseases.', NULL, NULL, '250 g., 500 g., 1 kg, 2.5 kg.', NULL, 5, 1, 'BHADRADARVADI KASHAYACHOORNAM', NULL, 'BHADRADARVADI KASHAYACHOORNAM', 1, 10, '2019-03-13 13:45:48', '2019-08-13 12:48:49'),
(405, 'NAYOPAYAM KASHAYAM', 'nayopayam-kashayam', '5d2eca6aa25911563347562.jpg', NULL, 6, 'D9', 0, 0, '3', 'Terminalia chebula, Cuminum cyminum, Dried ginger', 'Dyspnoea, Hiccup, Flatulence, Nausea, Vomitting', NULL, NULL, '200 ml.', NULL, 5, 1, 'NAYOPAYAM KASHAYAM', NULL, 'NAYOPAYAM KASHAYAM', 1, 7, '2019-03-13 13:55:08', '2019-08-13 12:00:17'),
(406, 'NIMBADI KASHAYAM', 'nimbadi-kashayam', '5d2eca3ced26f1563347516.jpg', NULL, 6, 'D58', 0, 0, '10', 'Azadirachta indica, Tinospora cordifolia, Zingiber officinale, Emblica officinalis, Terminalia chebula', 'Skin diseases, abscess, etc.', NULL, NULL, '200 ml', NULL, 5, 1, 'NIMBADI KASHAYAM', NULL, 'NIMBADI KASHAYAM', 1, 11, '2019-03-13 13:56:48', '2019-08-13 12:01:00'),
(407, 'CHIRUVILWADI KASHAYACHOORNAM', 'chiruvilwadi-kashayachoornam', '', NULL, 24, 'K6', 0, 0, '6', 'Holoptelia integrifolia, Boerrhavia diffusa, Plumbago rosea, Terminalia chebula, Piper longum, Dried ginger.', 'Piles, Fistula, Pthiasis, Indigestion, Loss of appetite.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg', NULL, 5, 1, 'CHIRUVILWADI KASHAYACHOORNAM', NULL, 'CHIRUVILWADI KASHAYACHOORNAM', 1, 5, '2019-03-13 13:58:22', '2019-08-13 12:51:30'),
(408, 'NIRGUNNYADI KASHAYAM', 'nirgunnyadi-kashayam', '5d2eca01afc441563347457.jpg', NULL, 6, 'D10', 0, 0, NULL, 'Vitexnigundo, Curcuma longa, Plumbagozeylanica, Embeliaribes, Cedrusdeodara etc', 'Worm infestation, indigestion, colic.', NULL, NULL, '200 ml.', NULL, 5, 1, 'NIRGUNNYADI KASHAYAM', NULL, 'NIRGUNNYADI KASHAYAM', 1, 9, '2019-03-13 13:58:28', '2019-08-13 11:57:04'),
(410, 'DASAMOOLAM KASHAYACHOORNAM', 'dasamoolam-kashayachoornam', '', NULL, 24, 'K7', 0, 0, '10', 'Dasamoolam', 'Rheumatic Complaints, Respiratory Complaints, Flu, Cold, Cough.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'DASAMOOLAM KASHAYACHOORNAM', NULL, 'DASAMOOLAM KASHAYACHOORNAM', 1, 7, '2019-03-13 14:08:15', '2019-08-13 11:41:41'),
(411, 'NISAKATHAKADI KASHAYAM', 'nisakathakadi-kashayam', '', NULL, 6, 'D11', 0, 0, '8', 'Curcuma  longa,  Strychnos  potatorum,  Emblica  officinalis,  Ixora  coccinea, Symplocos  cochinchinensis,  Aerva  lanata,  Salacia  fruiticosa,  Vetiveria zizanioides.', 'Diabetes mellitus (NIDDM)', NULL, NULL, '200 ml', NULL, 5, 1, 'NISAKATHAKADI KASHAYAM', NULL, 'NISAKATHAKADI KASHAYAM', 1, 7, '2019-03-13 14:09:29', '2019-08-13 12:03:05'),
(412, 'DHANWANTHARAM KASHAYACHOORNAM', 'dhanwantharam-kashayachoornam', '', NULL, 24, 'K15', 0, 0, '44', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam, Asparagus racemosus, Withania somnifera, Ipomoea mauritiana, Santalum album, Cedrus deodara, Rubia cordifolia, Boerrhavia diffusa, Glycyrrhiza glabra, Peucedanum graveolens, Hemidesmus indicus, Elettaria cardamomum, Thriphala.', 'Useful in Rheumatic complaints. Recommended in Post natal care for getting relief from post-partum strain and for improving physical strength.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'DHANWANTHARAM KASHAYACHOORNAM', NULL, 'DHANWANTHARAM KASHAYACHOORNAM', 1, 4, '2019-03-13 14:11:25', '2019-08-13 11:45:56'),
(413, 'PADOLADIGANAM KASHAYAM', 'padoladiganam-kashayam', '5d2ec9c42b1ce1563347396.jpg', NULL, 6, 'D30', 0, 0, '6', 'Trichosanthes anguina, Picrorrhiza kurroa, Santalum album, Chonemorpha macrophylla, Tinospora cordifolia, Cyclea peltata.', 'Acute and Chronic skin diseases, Vomitting, Poisonous bites, Jaundice.', NULL, NULL, '200 ml', NULL, 5, 1, 'PADOLADIGANAM KASHAYAM', NULL, 'PADOLADIGANAM KASHAYAM', 1, 11, '2019-03-13 14:14:42', '2019-08-13 12:03:49'),
(414, 'DRAKSHADI KASHAYACHOORNAM', 'drakshadi-kashayachoornam', '', NULL, 24, 'K8', 0, 0, '16', 'Raisins, Madhuca indica, Glycyrrhiza glabra, Symplocos cochinchinensis, Gmelina arborea, Hemidesmus indicus, Cyperus rotundus, Emblica officinalis, Coleus zeylanicus, Lotus stamen, stem and rhizome, Caesalpinia sappan, Santalum album, Vetiveria zizanioides, Phoenix farinifera.', 'General debility, Fatigue, Giddiness, Fainting, Epistaxis, Dizziness, Fever, Vomitting. Can be recommended in Antenatal care. Jaundice.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'DRAKSHADI KASHAYACHOORNAM', NULL, 'DRAKSHADI KASHAYACHOORNAM', 1, 8, '2019-03-13 14:15:39', '2019-08-13 11:47:14'),
(415, 'PANCHATHIKTHAKAM KASHAYAM', 'panchathikthakam-kashayam', '5d2ec98c61af81563347340.jpg', NULL, 6, 'D17', 0, 0, '3', 'Azadirachta indica, Tinospora cordifolia, Adhatoda beddomei, Trichosanthes anguina, Solanum melongena.', 'Blood impurities, Chronic skin diseases and Intermittent fever.', NULL, NULL, '200 ml', NULL, 5, 1, 'PANCHATHIKTHAKAM KASHAYAM', NULL, 'PANCHATHIKTHAKAM KASHAYAM', 1, 11, '2019-03-13 14:18:04', '2019-08-13 12:05:59'),
(416, 'ELAKANADI KASHAYACHOORNAM', 'elakanadi-kashayachoornam', '', NULL, 24, 'K2', 0, 0, '19', 'Elettaria cardamomum, Piper longum, Glycyrrhiza glabra, Dried ginger, Cyperus rotundus, Adhatoda beddomei, Azadirachta indica, Tinospora cordifolia, Coleus zeylanicus, Dasamoolam.', 'Bronchitis, Cough, Flu, Fever, Dyspnoea, Consumption, Anorexia.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'ELAKANADI KASHAYACHOORNAM', NULL, 'ELAKANADI KASHAYACHOORNAM', 1, 9, '2019-03-13 14:18:04', '2019-08-13 12:55:05'),
(417, 'PATHYADI KASHAYAM', 'pathyadi-kashayam', '5d2ec9693d4fc1563347305.jpg', NULL, 6, 'D18', 0, 0, '10', 'Terminalia chebula, Gmelina arborea, Dried ginger, Cyperus rotundus, Acorus calamus, Solanum indicum, Coriandrum sativum, Cedrus deodara, Clerodendrum serratum, Oldenlandia corymbosa', 'Bronchitis, Anorexia, Cough, Flu, Dryness of mouth, Colic.', NULL, NULL, NULL, NULL, 5, 1, 'PATHYADI KASHAYAM', NULL, 'PATHYADI KASHAYAM', 1, 12, '2019-03-13 14:21:24', '2019-08-13 12:06:47'),
(418, 'PATHYAKSHADHATHRYADI KASHAYAM', 'pathyakshadhathryadi-kashayam', '5d2ec94b6f5421563347275.jpg', NULL, 6, 'D50', 0, 0, NULL, 'Thriphala.Solanum indicum, Curcuma longa, Azadirachta indica, Tinospora cordifolia', 'Headache, Diseases related with ear, nose and throat, Eye diseases,  Neck pain.', NULL, NULL, '200 ml', NULL, 5, 1, 'PATHYAKSHADHATHRYADI KASHAYAM', NULL, 'PATHYAKSHADHATHRYADI KASHAYAM', 1, 10, '2019-03-13 14:25:49', '2019-08-13 12:08:17'),
(419, 'GANDHARVAHASTHADI KASHAYACHOORNAM', 'gandharvahasthadi-kashayachoornam', '', NULL, 24, 'K4', 0, 0, '8', 'Ricinus communis, Holoptelia integrifolia, Plumbago rosea, Dried ginger, Terminalia chebula, Boerrhavia diffusa, Tragia involucrata, Curculigo orchioides', 'Rheumatic complaints, Flatulence, Loss of appetite, Constipation, Anorexia, Low back-ache.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg', NULL, 5, 1, 'GANDHARVAHASTHADI KASHAYACHOORNAM', NULL, 'GANDHARVAHASTHADI KASHAYACHOORNAM', 1, 5, '2019-03-13 14:27:13', '2019-08-13 12:55:35'),
(420, 'PRASARANYADI KASHAYAM', 'prasaranyadi-kashayam', '', NULL, 6, 'D43', 0, 0, '6', 'Merrimia tridentata, Phaseolus mungo, Sida cordifolia, Allium sativum, Alpinia calcarata, Dried ginger', 'Brachial palsy and other Rheumatic complaints.', NULL, NULL, '200 ml', NULL, 5, 1, 'PRASARANYADI KASHAYAM', NULL, 'PRASARANYADI KASHAYAM', 1, 8, '2019-03-13 14:27:46', '2019-08-13 12:08:55'),
(421, 'PUNARNAVADI KASHAYAM', 'punarnavadi-kashayam', '5d2ec8dea79171563347166.jpg', NULL, 6, 'D19', 0, 0, '8', 'Boerrhavia diffusa, Azadirachta indica, Trichosanthes anguina, Dried ginger, Solanum indicum, Tinospora cordifoila, Coscinium fenestratum, Terminalia chebula', 'Dropsy, Bodyache, Fever, Bronchial disturbances, Urinary infection, Productive cough, Arthritis, Rheumatic complaints, Anaemia.', NULL, NULL, '200 ml', NULL, 5, 1, 'PUNARNAVADI KASHAYAM', NULL, 'PUNARNAVADI KASHAYAM', 1, 11, '2019-03-13 14:29:57', '2019-08-13 12:10:11'),
(422, 'RASNASAPTHAKAM KASHAYAM', 'rasnasapthakam-kashayam', '', NULL, 6, 'D44', 0, 0, '7', 'Alpinia calcarata, Tinospora cordifolia, Cassia fistula, Cedrus deodara, Tribulus terrestris, Ricinus communis, Boerrhavia diffusa', 'Rheumatic fever and related symptoms like joint pain, swelling, body ache and fever.', NULL, NULL, '200 ml', NULL, 5, 1, 'RASNASAPTHAKAM KASHAYAM', NULL, 'RASNASAPTHAKAM KASHAYAM', 1, 9, '2019-03-13 14:37:44', '2019-08-13 12:11:21'),
(423, 'RASNEIRANDADI KASHAYAM', 'rasneirandadi-kashayam-2', '5d2ec8b2e7b511563347122.jpg', NULL, 6, 'D26', 0, 0, '14', 'Alpinia calcarata, Ricinus communis, Sida cordifolia, Strobilanthes ciliatus, Asparagus racemosus, Tragia involucrata, Adhatoda beddomei, Tinospora cordifolia, Cedrus deodara, Aconitum heterophyllum, Cyperus rotundus, Asteracantha longifolia, Kaempferia galanga, Dried ginger.', 'Rheumatic complaints, Arthritis, Lumbago, Joint pain, Vertigo, Painful Swelling over joints.', NULL, NULL, '200 ml', NULL, 5, 1, 'RASNEIRANDADI KASHAYAM', NULL, 'RASNEIRANDADI KASHAYAM', 1, 7, '2019-03-13 14:40:55', '2019-08-13 12:12:23'),
(424, 'RASONADI KASHAYAM', 'rasonadi-kashayam', '5d2ec859777b61563347033.jpg', NULL, 6, 'D61', 0, 0, '4', 'Allium sativum, Nigella sativa, Piper longum, Desmodium gangeticum', 'Vathanulomanam', NULL, NULL, NULL, NULL, 5, 1, 'RASONADI KASHAYAM', NULL, 'RASONADI KASHAYAM', 1, 10, '2019-03-13 14:42:16', '2019-08-13 10:20:48'),
(425, 'SAHACHARADI KASHAYAM', 'sahacharadi-kashayam', '5d2ece48c40b21563348552.jpg', NULL, 6, 'D46', 0, 0, '3', 'Zingiber officinale, Strobilanthus ciliates,Cedrus deodara', 'Rheumatic diseases, Varicosity, Lassitude', NULL, NULL, '200 ml.', NULL, 5, 1, 'SAHACHARADI KASHAYAM', NULL, 'SAHACHARADI KASHAYAM', 1, 9, '2019-03-13 14:44:29', '2019-08-13 12:13:58'),
(426, 'GOJIHWADI KASHAYA CHOORNAM', 'gojihwadi-kashaya-choornam', '', NULL, 24, 'K70', 0, 0, '15', 'Portulaea oleracea, Glycyrrhiza glabra, Anethum graveolens, Vitis vinifera, Ficus racemosa, Ziziphus jujuba, Adhatoda beddomei', 'Cold & Feverishness, Cough, Asthma', NULL, NULL, '50g.,500g.', NULL, 5, 1, 'GOJIHWADI KASHAYA CHOORNAM', NULL, 'GOJIHWADI KASHAYA CHOORNAM', 1, 5, '2019-03-13 14:45:19', '2019-08-13 12:56:09'),
(427, 'SAPTHASARAM KASHAYAM', 'sapthasaram-kashayam', '5d2ece62bf0511563348578.jpg', NULL, 6, 'D29', 0, 0, '7', 'Boerrhavia  diffusa,  Aegle  marmelos,  Horse  gram,  Ricinus  communis, \r\nStrobilanthes ciliatus, Dried ginger, Premna serratifolia.', 'Rheumatic  complaints,  Dysmenorrhoea,  Low  back-ache,  Menstrual \r\nirregularities, Indigestion, Ascites, Splenic disorders.', NULL, NULL, '200 ml', NULL, 5, 1, 'SAPTHASARAM KASHAYAM', NULL, 'SAPTHASARAM KASHAYAM', 1, 9, '2019-03-13 14:46:47', '2019-08-13 10:23:50'),
(428, 'GULGULUTHIKTHAKAM KASHAYACHOORNAM', 'gulguluthikthakam-kashayachoornam', '', NULL, 24, 'K39A', 0, 0, '31', 'Commiphora wightii, Panchathiktham, Cyclea peltata, Embelia ribes, Curcuma longa, Alpinia calcarata, Aconitum heterophyllym.', 'Rheumatic complaints, Arthritis, Chronic skin diseases, Fistula, Sinuses, Pthisis.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'GULGULUTHIKTHAKAM KASHAYACHOORNAM', NULL, 'GULGULUTHIKTHAKAM KASHAYACHOORNAM', 1, 6, '2019-03-13 14:49:49', '2019-08-13 12:57:34'),
(429, 'SUKUMARAM KASHAYAM', 'sukumaram-kashayam', '5d2ec82b98fd21563346987.jpg', NULL, 6, 'D56', 0, 0, '27', 'Boerrhavia diffusa, Dasamoolam, Holostemma adakodien, Withania somnifera, Ricinus communis, Asparagus racemosus, Thrinapanchamoolam.Sphaeranthus indicus.', 'Hyperacidity, Hernia, Pthiasis, Constipation, Piles,Hydrocele, Menstrual disorders, Urinogenital disorders. Improves bowel evacuation.', NULL, NULL, '200 ml', NULL, 5, 1, 'SUKUMARAM KASHAYAM', NULL, 'SUKUMARAM KASHAYAM', 1, 10, '2019-03-13 14:50:18', '2019-08-13 12:18:54'),
(430, 'THRAYANTHYADI KASHAYAM', 'thrayanthyadi-kashayam', '5d2ec7e4668351563346916.jpg', NULL, 6, 'D37', 0, 0, '10', 'Bacepa monnieri, Terminalia chebula, Terminalia bellerica, Azadirachta indica, Madhuka indica,Operculina turpethum etc.', 'Vidradhi, Rekthapitha, Hridroga, skin diseases, jaundice and associated liver disorders.', NULL, NULL, '200 ml', NULL, 5, 1, 'THRAYANTHYADI KASHAYAM', NULL, 'THRAYANTHYADI KASHAYAM', 1, 12, '2019-03-13 14:52:06', '2019-08-13 12:19:46'),
(431, 'VAJRAKAM KASHAYAM', 'vajrakam-kashayam', '5d2ef7228a2f11563359010.jpg', NULL, 6, 'D53', 0, 0, '9', 'Trichosanthes anguina, Thriphala, Azadirachta indica, Tinospora cordifolia, Solanum indicum, Adhatoda beddomei, Pongamia pinnata.', 'Skin diseases, Vitiligo, Fever, Jaundice.', NULL, NULL, '200 ml', NULL, 5, 1, 'VAJRAKAM KASHAYAM', NULL, 'VAJRAKAM KASHAYAM', 1, 6, '2019-03-13 14:53:39', '2019-08-13 12:20:28');
INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(432, 'GULUCHYADI KASHAYACHOORNAM', 'guluchyadi-kashayachoornam', '', NULL, 24, 'K5', 0, 0, '5', 'Tinospora cordifolia, Caesalpinia sappan, Azadirachta indica, Coriandrum sativum, Pterocarpus santalinus.', 'Fever, Vomitting, Burning sensation, Thirst, Skin disorders, Fainting due to excessive heat.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'GULUCHYADI KASHAYACHOORNAM', NULL, 'GULUCHYADI KASHAYACHOORNAM', 1, 8, '2019-03-13 14:56:07', '2019-08-13 12:56:45'),
(433, 'VARUNADI KASHAYAM', 'varunadi-kashayam', '5d2ef76dbdd5a1563359085.jpg', NULL, 6, 'EMPTY 01', 0, 0, '15', 'Crataeva nurvala, Zingiber officinale, Rotula aquatico, Tribullus terrestris', 'Calculus', NULL, NULL, '200 ml', NULL, 5, 1, 'VARUNADI KASHAYAM', NULL, 'VARUNADI KASHAYAM', 1, 4, '2019-03-13 15:00:06', '2019-08-13 12:31:29'),
(434, 'VASAGULUCHYADI KASHAYAM', 'vasaguluchyadi-kashayam', '5c94b9cf2e9491553250767.jpg', NULL, 6, 'D52', 0, 0, '8', 'Adhatoda beddomei, Tinospora cordifolia, Thriphala, Picrorrhiza, curroa, Solanum indicum, Azadirachta, indica.', 'Jaundice, Anaemia, Hyperbilirubinemia', NULL, NULL, '200 ml.', NULL, 5, 1, 'VASAGULUCHYADI KASHAYAM', NULL, 'VASAGULUCHYADI KASHAYAM', 1, 16, '2019-03-13 15:02:15', '2019-08-13 12:25:45'),
(435, 'VIDARYADI KASHAYAM', 'vidaryadi-kashayam', '5d2ec70e85bb61563346702.jpg', NULL, 6, 'D45', 0, 0, '15', 'Ipomoea mauritiana, Ricinus communis, Boerrhavia diffusa, Cedrus deodara, \r\nDesmodium trifoliatum, Jeevanapanchamoolam, Laghu panchamoolam.', 'Cough, Brohchitis, Cardiac disorders like palpitation and tachycardia, Fatigue, \r\nGeneral debility. Helpful to regain health and body weight.', NULL, NULL, '200 ml', NULL, 5, 1, 'VIDARYADI KASHAYAM', NULL, 'VIDARYADI KASHAYAM', 1, 6, '2019-03-13 15:04:59', '2019-08-13 12:28:21'),
(436, 'INDUKANTHAM KASHAYACHOORNAM', 'indukantham-kashayachoornam', '', NULL, 24, 'K55A', 0, 0, '18', 'Holoptelia integrifolia, Cedrus deodara, Dasamoolam, Shadpalam.', 'Anorexia, Colic, Intermittent fever. Promotes health and strength by improving metabolism.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'INDUKANTHAM KASHAYACHOORNAM', NULL, 'INDUKANTHAM KASHAYACHOORNAM', 1, 5, '2019-03-13 15:05:12', '2019-08-13 13:38:00'),
(437, 'VYOSHADI KASHAYAM', 'vyoshadi-kashayam', '', NULL, 6, 'D28', 0, 0, '15', 'Thrikatu, Apium graveolens, Boerrhavia diffusa, Saccharum officinarum, Iron (processed), Terminalia chebula, Sida cordifolia, Tamarindus indica (rachis), Iron rust (purified), Citrus acida, Curcuma longa, Vitis quadrangularis, Desmodium trifoliatum', 'Anaemia, General weakness, Dropsy, Anorexia.', NULL, '5 - 15 ml kashayam with 60 ml buttermilk b.d. after food or as directed by the physician.', '200 ml', NULL, 5, 1, 'VYOSHADI KASHAYAM', NULL, 'VYOSHADI KASHAYAM', 1, 8, '2019-03-13 15:06:25', '2019-08-13 12:30:23'),
(440, 'MAHARASNADI KASHAYACHOORNAM', 'maharasnadi-kashayachoornam', '', NULL, 24, 'K22', 0, 0, '26', 'Alpinia calcarata, Sida cordifolia, Ricinus communis, Acorus calamus, Boerrhavia diffusa, Tinospora cordifolia, Cassia fistula, Tribulus terrestris, Withania somnifera, Aconitum heterophyllum, Solanum melongena.var. insanum.', 'Rheumatic diseases like Hemiplegia, Brachial palsy, Sciatica etc., Rheumatic fever. Also recommended in Hernia, Elephantiasis, Lassitude, Numbness.', NULL, NULL, '250g., 500g., 1 Kg., 2.5 Kg.', NULL, 5, 1, 'MAHARASNADI KASHAYACHOORNAM', NULL, 'MAHARASNADI KASHAYACHOORNAM', 1, 6, '2019-03-13 15:18:51', '2019-08-13 11:54:33'),
(442, 'VASAGULUCHYADI KASHAYACHOORNAM', 'vasaguluchyadi-kashayachoornam', '', NULL, 24, 'K62', 0, 0, '8', 'Adhatoda beddomei, Tinospora cordifolia, Thriphala, Picrorrhiza kurroa,\r\nSolanum indicum, Azadirachta, indica.', 'Jaundice, Anaemia, Hyperbilirubinemia', NULL, NULL, '250g.,1Kg.', NULL, 5, 1, 'VASAGULUCHYADI KASHAYACHOORNAM', NULL, 'VASAGULUCHYADI KASHAYACHOORNAM', 1, 3, '2019-03-13 15:21:49', '2019-08-13 13:55:30'),
(443, 'VARANADI KASHAYACHOORNAM', 'varanadi-kashayachoornam', '', NULL, 24, 'K27', 0, 0, '17', 'Crataeva religiosa, Strobilanthes ciliatus, Asparagus racemosus, Plumbago \r\nrosea, Semicarpus anacardium.', 'Headache, Carbuncles, Obesity, Anorexia, Rheumatic complaints. Effective in \r\nInflammation and Growths in visceral organs.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg', NULL, 5, 1, 'VARANADI KASHAYACHOORNAM', NULL, 'VARANADI KASHAYACHOORNAM', 1, 4, '2019-03-13 15:23:52', '2019-08-13 12:13:37'),
(444, 'MANJISHTADI KASHAYACHOORNAM', 'manjishtadi-kashayachoornam', '', NULL, 24, 'K23', 0, 0, '9', 'Rubia cordifolia, Thriphala, Solanum indicum, Acorus calamus, Cedrus deodara, Curcuma longa, Tinospora cordifolia, Azadirachta indica.', 'Arthritis, Skin diseases, Diabetes, Chilblain.', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'MANJISHTADI KASHAYACHOORNAM', NULL, 'MANJISHTADI KASHAYACHOORNAM', 1, 4, '2019-03-13 15:24:04', '2019-08-13 11:54:49'),
(445, 'SAPTASARAM KASHAYACHOORNAM', 'saptasaram-kashayachoornam', '', NULL, 24, 'K29', 0, 0, '7', 'Boerrhavia  diffusa,  Aegle  marmelos,  Horse  gram,  Ricinus  communis, \r\nStrobilanthes ciliatus, Dried ginger, Premna serratifolia.', 'Rheumatic  complaints,  Dysmenorrhoea,  Low  back-ache,  Menstrual \r\nirregularities, Indigestion, Ascites, Splenic disorders.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'SAPTASARAM KASHAYACHOORNAM', NULL, 'SAPTASARAM KASHAYACHOORNAM', 1, 5, '2019-03-13 15:27:19', '2019-08-13 12:12:07'),
(446, 'SAHACHARADI KASHAYA CHOORNAM', 'sahacharadi-kashaya-choornam', '', NULL, 24, 'K69', 0, 0, '3', 'Zingiber officinalae, Nilgirianthus ciliates,Cedrus deodara', 'Rheumatic diseases, Vericosity, Lassitude', NULL, '48g powder with 16 times water,boil and reduce to ¼. or as directed by the \r\nphysician', '50g,500g', NULL, 5, 1, 'SAHACHARADI KASHAYA CHOORNAM', NULL, 'SAHACHARADI KASHAYA CHOORNAM', 1, 7, '2019-03-13 15:29:46', '2019-08-13 12:12:30'),
(448, 'RASNEIRANDADI KASHAYACHOORNAM', 'rasneirandadi-kashayachoornam', '', NULL, 24, '14', 0, 0, 'K26', 'Alpinia calcarata, Ricinus communis, Sida cordifolia, Strobilanthes ciliatus,\r\nAsparagus racemosus, Tragia involucrata, Adhatoda beddomei, Tinospora\r\ncordifolia, Cedrus deodara, Aconitum heterophyllum, Cyperus rotundus,\r\nAsteracantha longifolia, Kaempferia galanga, Dried ginger.', 'Rheumatic complaints, Arthritis, Lumbago, Joint pain, Vertigo, Painful Swelling \r\nover joints.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'RASNEIRANDADI KASHAYACHOORNAM', NULL, 'RASNEIRANDADI KASHAYACHOORNAM', 1, 6, '2019-03-13 15:40:00', '2019-08-13 13:53:02'),
(450, 'RASNASAPTHAKAM KASHAYACHOORNAM', 'rasnasapthakam-kashayachoornam', '', NULL, 24, 'K44A', 0, 0, '7', 'Alpinia  calcarata,  Tinospora  cordifolia,  Cassia  fistula,  Cedrus  deodara, \r\nTribulus terrestris, Ricinus communis, Boerrhavia diffusa.', 'Rheumatic fever and related symptoms like joint pain, swelling, body ache \r\nand fever.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'RASNASAPTHAKAM KASHAYACHOORNAM', NULL, 'RASNASAPTHAKAM KASHAYACHOORNAM', 1, 7, '2019-03-13 15:41:39', '2019-08-13 12:10:34'),
(451, 'MUSTHAKARANJADI KASHAYACHOORNAM', 'musthakaranjadi-kashayachoornam', '', NULL, 24, 'K24', 0, 0, '8', 'Cyperus rotundus, Pongamia pinnata, Aconitum heterophyllum, Plumbago rosea, Aegle marmelos, Dried ginger, Piper spp., Holarrhena antidysenterica.', 'Diarrhoea, Dysentery, Colic.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'MUSTHAKARANJADI KASHAYACHOORNAM', NULL, 'MUSTHAKARANJADI KASHAYACHOORNAM', 1, 3, '2019-03-13 15:43:12', '2019-08-13 11:59:39'),
(452, 'PUNARNAVADI KASHAYACHOORNAM', 'punarnavadi-kashayachoornam', '', NULL, 24, 'K19', 0, 0, '8', 'Boerrhavia diffusa, Azadirachta indica, Trichosanthes anguina, Dried ginger, \r\nSolanum  indicum,  Tinospora  cordifoila,  Coscinium  fenestratum,  Terminalia \r\nchebula.', 'Dropsy, Body ache, Fever, Bronchial disturbances, Urinary infection, Productive \r\ncough, Arthritis, Rheumatic complaints, Anaemia.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'PUNARNAVADI KASHAYACHOORNAM', NULL, 'PUNARNAVADI KASHAYACHOORNAM', 1, 7, '2019-03-13 15:43:47', '2019-08-13 12:10:07'),
(453, 'NAYOPAYAM KASHAYACHOORNAM', 'nayopayam-kashayachoornam', '', NULL, 24, 'K9', 0, 0, '3', 'Terminalia chebula, Cuminum cyminum, Dried ginger.', 'Dyspnoea, Hiccup, Flatulence, Nausea, Vomitting.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'NAYOPAYAM KASHAYACHOORNAM', NULL, 'NAYOPAYAM KASHAYACHOORNAM', 1, 4, '2019-03-13 15:45:54', '2019-08-13 11:59:58'),
(455, 'PATHYAKSHADHATHRYADI KASHAYACHOORNAM', 'pathyakshadhathryadi-kashayachoornam', '', NULL, 24, 'K50', 0, 0, '7', 'Thriphala.Solanum indicum, Curcuma longa, Azadirachta indica, Tinospora\r\ncordifolia.', 'Headache, Diseases related with ear, nose and throat, Eye diseases, Neck \r\npain.', NULL, NULL, '250g.,1 Kg.', NULL, 5, 1, 'PATHYAKSHADHATHRYADI KASHAYACHOORNAM', NULL, 'PATHYAKSHADHATHRYADI KASHAYACHOORNAM', 1, 5, '2019-03-13 15:49:20', '2019-08-13 13:51:04'),
(457, 'NEELITHULASYADI KASHAYACHOORNAM', 'neelithulasyadi-kashayachoornam', '', NULL, 24, 'K13', 0, 0, '14', 'Indigofera tinctoria, Ocimum sanctum, Vitex negundo, Hemidesmus indicus, Allium sativum, Thrikatu, Withania somnifera, Santalum album, Glycyrrhiza glabra, Saussurea lappa, Indian valerian, Aristolochia indica.', 'Poisonous bite, Chronic skin diseases.', NULL, NULL, '250 g., 500 g., 1 Kg., 3 Kg.', NULL, 5, 1, 'NEELITHULASYADI KASHAYACHOORNAM', NULL, 'NEELITHULASYADI KASHAYACHOORNAM', 1, 4, '2019-03-13 15:53:06', '2019-08-13 12:03:41'),
(459, 'NISAKATHAKADI KASHAYACHOORNAM', 'nisakathakadi-kashayachoornam', '', NULL, 24, 'K11', 0, 0, '8', 'Curcuma longa, Strychnos potatorum, Emblica officinalis, Ixora coccinea, Symplocos cochinchinensis, Aerva lanata, Salacia fruiticosa, Vetiveria zizanioides.', 'Diabetes mellitus (NIDDM).', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'NISAKATHAKADI KASHAYACHOORNAM', NULL, 'NISAKATHAKADI KASHAYACHOORNAM', 1, 7, '2019-03-13 16:08:25', '2019-08-13 12:05:49'),
(460, 'PRASARANYADI KASHAYACHOORNAM', 'prasaranyadi-kashayachoornam', '', NULL, 24, 'K41A', 0, 0, '6', 'Merrimia tridentata, Phaseolus mungo, Sida cordifolia, Allium sativum, Alpinia \r\ncalcarata, Dried ginger.', 'Brachial palsy and other Rheumatic complaints.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 k', NULL, 5, 1, 'PRASARANYADI KASHAYACHOORNAM', NULL, 'PRASARANYADI KASHAYACHOORNAM', 1, 3, '2019-03-13 16:40:09', '2019-08-13 12:09:09'),
(461, 'PATHYADI KASHAYACHOORNAM', 'pathyadi-kashayachoornam', '', NULL, 24, 'K18', 0, 0, '10', 'Terminalia chebula, Gmelina arborea, Dried ginger, Cyperus rotundus, Acorus \r\ncalamus,  Solanum  indicum,  Coriandrum  sativum,  Cedrus  deodara, \r\nClerodendrum serratum, Oldenlandia corymbosa.', 'Bronchitis, Anorexia, Cough, Flu, Dryness of mouth, Colic.', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'PATHYADI KASHAYACHOORNAM', NULL, 'PATHYADI KASHAYACHOORNAM', 1, 8, '2019-03-13 16:42:15', '2019-08-13 12:08:21'),
(462, 'PADOLADIGANAM KASHAYACHOORNAM', 'padoladiganam-kashayachoornam', '', NULL, 24, 'K30', 0, 0, '6', 'Trichosanthes anguina, Picrorrhiza curroa, Santalum album, Chonemorpha macrophylla, Tinospora cordifolia, Cyclea peltata', 'Acute and Chronic skin diseases, Vomitting, Poisonous bites, Jaundice.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'PADOLADIGANAM KASHAYACHOORNAM', NULL, 'PADOLADIGANAM KASHAYACHOORNAM', 1, 5, '2019-03-13 16:44:23', '2019-08-13 12:06:15'),
(463, 'PHALATHRIKADI KASHAYA CHOORNAM', 'phalathrikadi-kashaya-choornam', '', NULL, 24, 'K71', 0, 0, '6', 'Emblica officinalis, Terminalia chebula, Terminalia bellirica, Picrorrhiza kurroa,\r\nTrichosanthes cucumerina', 'Jaundice, Anaemia,Heart burn', NULL, '48g with sita, madhu, madhuyasti or as directed by the physician', '50g., 500g', NULL, 5, 1, 'PHALATHRIKADI KASHAYA CHOORNAM', NULL, 'PHALATHRIKADI KASHAYA CHOORNAM', 1, 4, '2019-03-13 16:44:32', '2019-08-13 13:43:21'),
(464, 'PANCHATHIKTHAKAM KASHAYACHOORNAM', 'panchathikthakam-kashayachoornam', '', NULL, 24, 'K17', 0, 0, '3', 'Azadirachta indica, Tinospora cordifolia, Adhatoda beddomei, Trichosanthes anguina, Solanum melongena.var.insanum.', 'Blood impurities, Chronic skin diseases and Intermittent fever.', NULL, NULL, '250 g., 500 g., 1 kg., 2 kg.', NULL, 5, 1, 'PANCHATHIKTHAKAM KASHAYACHOORNAM', NULL, 'PANCHATHIKTHAKAM KASHAYACHOORNAM', 1, 7, '2019-03-13 16:46:33', '2019-08-13 12:07:00'),
(465, 'PASHANABHEDADI KASHAYA CHOORNAM', 'pashanabhedadi-kashaya-choornam', '5c94afcddc32c1553248205.jpg', NULL, 24, 'K72', 0, 0, '9', 'Rotula aquatic, Glycyrrhiza glabra, Elettaria cardamomum, Ricinus Communis, \r\nAdhatoda  beddomei,  Tribulus  terrestris,  Terminalia  chebula  Piper  longum, \r\nSugar', 'Urinary  problems  like  burning  sensation,  burning  micturition,  stone,  painful \r\nmicturition', NULL, '48g  powder  with  16  times  water,boil  and  reduce  to  ¼.  Administered  with \r\nsugar,honey and yastichoornam as vehicle, or as directed by the physician', '50g, 500g', NULL, 5, 1, 'PASHANABHEDADI KASHAYA CHOORNAM', NULL, 'PASHANABHEDADI KASHAYA CHOORNAM', 1, 19, '2019-03-13 16:46:48', '2019-08-13 12:06:40'),
(466, 'KUTAJARISHTAM', 'kutajarishtam-2', '5d43de6c85fc91564728940.jpg', NULL, 1, 'A9', 0, 0, '6', 'Holarrhena antidysenterica, Raisins, Madhuca indica (Flower), Gmelina\r\narborea (fruit), Jaggery, Woodfordia fruiticosa', 'Diarrhea and Dysentery. Recommended in Sprue, Colic, Flatulence and\r\nFever associated with diarrhea.', NULL, '15-25 ml. b.d., after food or as directed by the physician', '450 ml', NULL, 5, 1, 'KUTAJARISHTAM', NULL, 'KUTAJARISHTAM', 1, 10, '2019-06-10 10:54:01', '2019-08-09 12:41:49'),
(467, 'MOOLAKASAVAM', 'moolakasavam', '', NULL, 1, 'EMPTY01', 0, 0, '11', 'Raphanus sativus, Azadiracta indica, Thriphala, Glycyrrhiza glabra, milk,\r\nHoney etc.', 'Skin disease etc', NULL, NULL, '450 ml', NULL, 5, 1, 'MOOLAKASAVAM', NULL, 'MOOLAKASAVAM', 1, 7, '2019-06-10 11:10:59', '2019-06-10 11:10:59'),
(468, 'ASANELADI THAILAM', 'asaneladi-thailam', '5d43d4817dbe51564726401.jpg', NULL, 7, 'T5', 0, 0, '34', 'Pterocarpus marsupium, Aegle marmelos, Sida cordifolia, Tinospora cordifolia,\r\nElettaria cardamomum, Kaempferia galanga, Saffron, Commiphora wightii,\r\nGingelly oil, Cow milk.', 'Chronic rhinitis, Scabies, Dandruff, Headache, and diseases related with\r\nhead', NULL, NULL, '200 ml., 450 ml.', 'For external application overhead', 5, 1, 'ASANELADI THAILAM', NULL, 'ASANELADI THAILAM', 1, 20, '2019-06-10 11:24:17', '2019-08-13 16:11:50'),
(469, 'ASANELADI VELICHENNA', 'asaneladi-velichenna', '5d43d456b00461564726358.jpg', NULL, 7, 'T95', 0, 0, '34', 'Pterocarpus marsupium, Aegle marmelos, Sida cordifolia, Tinospora cordifolia,\r\nElettaria cardamomum, Kaempferia galanga, Saffron, Commiphora wightii,\r\nCoconut oil, Cow milk', 'Chronic rhinitis, Scabies, Dandruff, Headache and diseases related with\r\nhead', NULL, NULL, '200 ml., 450 ml.', 'For external application over head.', 5, 1, 'ASANELADI VELICHENNA', NULL, 'ASANELADI VELICHENNA', 1, 15, '2019-06-10 11:30:39', '2019-08-13 16:12:14'),
(471, 'BALA THAILAM', 'bala-thailam', '', NULL, 7, 'T38A', 0, 0, '55', 'Sida cordifolia, Tinospora cordifolia, Alpinia calcarata, Cow milk, Gingelly oil,\r\nButter milk, Coscinium fenestratum, Santalum album, Cyperus rotundus,\r\nOcimum sanctum, Ipomoea mauritiana, Vitex negundo, Eugenia caryophyllata,\r\nAcorus calamus, Asphaltum, Camphor, Saffron.', 'Rheumatic complaints, Mental diseases, Respiratory diseases.', NULL, NULL, '10 ml.', 'Internally: 6-15 drops with Cumin seed water/milk or as directed by the\r\nphysician. May be used externally also.', 5, 1, 'BALA THAILAM', NULL, 'BALA THAILAM', 1, 16, '2019-06-10 11:46:41', '2019-08-13 16:41:32'),
(473, 'DHOORDHOORAPATHRADI VELICHENNA', 'dhoordhoorapathradi-velichenna', '5d43d939ca5b81564727609.jpg', NULL, 7, 'T89', 0, 0, '10', 'Datura stramonium, Cynodon dactylon, Tinospora cordifolia, Coconut oil,\r\nGlycyrrhiza glabra, Coconut juice', 'Dandruff, Alopacia, Scabies over scalp. Promotes growth of hair', NULL, NULL, '200 ml.', 'For external application.', 5, 1, 'DHOORDHOORAPATHRADI VELICHENNA', NULL, 'DHOORDHOORAPATHRADI VELICHENNA', 1, 19, '2019-06-10 12:08:24', '2019-08-13 17:10:30'),
(475, 'LAGHUVISHAGARBHA THAILAM', 'laghuvishagarbha-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '10', 'Dathura metal, Aconitum ferox, Piper nigrum, Gingelly oil etc', 'Hemiplegia, Lock jaw, Tremor', NULL, 'Used externally for abhyanga', '200 ml., 450 ml.', NULL, 5, 1, 'LAGHUVISHAGARBHA THAILAM', NULL, 'LAGHUVISHAGARBHA THAILAM', 1, 9, '2019-06-10 14:08:45', '2019-08-13 17:58:31'),
(476, 'KASISADI THAILAM', 'kasisadi-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '18', 'Ferrous sulphate, rocksalt, Pipper longum, Thevetia neriifolia, Embelia\r\nribesetc', 'Hemorrhoids', NULL, NULL, '50 ml, 200 ml, 450 ml.', 'For external use only', 5, 1, 'KASISADI THAILAM', NULL, 'KASISADI THAILAM', 1, 4, '2019-06-10 14:10:42', '2019-08-13 17:58:53'),
(477, 'SOMARAJI THAILAM', 'somaraji-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '9', 'Psoralia corylifolia, Curcuma longa, Berberris aristata, Pongamia Pinnata etc.', 'Itching, Skin Diseases, Chronic ulcer, Venous ulcer,', NULL, NULL, '200 ml, 450 ml.', 'External application only', 5, 1, 'SOMARAJI THAILAM', NULL, 'SOMARAJI THAILAM', 1, 9, '2019-06-10 14:12:05', '2019-08-13 17:59:08'),
(478, 'GANDHAKADYAMALAHARA', 'gandhakadyamalahara', '', NULL, 7, 'EMPTY01', 0, 0, '5', 'Bees wax, gingelly oil, Sulphur, Borax, Camphor, Naga sindhooram', 'Skin diseases', NULL, NULL, '100 gm, 500 gm.', 'External application only', 5, 1, 'GANDHAKADYAMALAHARA', NULL, 'GANDHAKADYAMALAHARA', 1, 10, '2019-06-10 14:14:39', '2019-08-13 17:59:24'),
(479, 'DHANWANTHARAM MEZHUKPAKAM', 'dhanwantharam-mezhukpakam', '5d43d8bbcd9c71564727483.jpg', NULL, 7, 'EMPTY01', 0, 0, '48', 'Sida cordifolia, Hordeum vulgare, Ziziphus jujuba, Horse gram, Dasamoolam,\r\nAsparagus racemosus, Rubia cordifolia, Withania somnifera, Hemidesmus\r\nindicus, Santalum album, Ipomoea mauritiana, Acorus calamus, Rock salt,\r\nAsphaltum, Boerrhavia diffusa, Thriphala, Chathurjathakam, Gingelly oil,\r\nCow milk.', 'A highly efficacious preparation used in Rheumatic diseases, Fracture, Sprain,\r\nContusions, Emaciation, Mental disorders, Hydrocele, Hernia, Vaginal\r\ndisorders, Menstrual irregularities, Uriogenital disorders. Recommended in\r\nAntenatal as well as Post-natal care.', NULL, NULL, '200 ml., 450 ml.', 'For external application all over body', 5, 1, 'DHANWANTHARAM MEZHUKPAKAM', NULL, 'DHANWANTHARAM MEZHUKPAKAM', 1, 12, '2019-06-10 14:23:33', '2019-08-13 18:00:31'),
(480, 'SAHACHARADI MEZHUKPAKAM', 'sahacharadi-mezhukpakam', '', NULL, 7, 'EMPTY01', 0, 0, '32', 'Strobilanthes ciliatus,. Dasamoolam, Asparagus racemosus, Asphaltum,\r\nGingelly oil, Cow milk.', 'Contractions, Varicose vein, Tremors, Hemiplegia, Muscular pain, Weakness,\r\nConvulsions.', NULL, NULL, '200 ml., 450 ml.', 'Unctuous enema and internally', 5, 1, 'SAHACHARADI MEZHUKPAKAM', NULL, 'SAHACHARADI MEZHUKPAKAM', 1, 5, '2019-06-10 14:30:50', '2019-08-13 18:00:56'),
(481, 'SHADBINDHU THAILAM', 'shadbindhu-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '13', 'Gingelly oil, Milk, Eclipta alba, Ricinus communis, Valleriana vallichi,\r\nAlpinia galanga, Rock salt etc.', 'Tooth problems, improves vision, hair fall', NULL, NULL, '10 ml, 50ml, 200 ml, 450 ml.', 'External application over the body and head, nasal instillation', 5, 1, 'SHADBINDHU THAILAM', NULL, 'SHADBINDHU THAILAM', 1, 6, '2019-06-10 14:33:22', '2019-08-13 18:01:13'),
(482, 'SAINDHAVADI THAILAM', 'saindhavadi-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '8', 'Rock salt, Calotropis gigantea, Piper nigrum, Plumbago zeylanicum,\r\nEclipta alba, gingely oiletc', 'Chronic ulcer, Venous ulcer', NULL, NULL, '50 ml, 100 ml, 450 ml.', 'External application only', 5, 1, 'SAINDHAVADI THAILAM', NULL, 'SAINDHAVADI THAILAM', 1, 7, '2019-06-10 14:35:33', '2019-08-13 18:01:30'),
(483, 'JATHYADI THAILAM', 'jathyadi-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '19', 'Jasminum grandiflorum, Pongamia pinnata, Vitex negundo, Gingelly oil etc', 'Chronic wounds, wounds in vital points, burns, abscess etc.', NULL, NULL, '450 ml', 'For external application', 5, 1, 'JATHYADI THAILAM', NULL, 'JATHYADI THAILAM', 1, 13, '2019-06-10 14:46:50', '2019-08-13 18:01:44'),
(484, 'VILWAMPACHOTYADI THAILAM', 'vilwampachotyadi-thailam', '', NULL, 7, 'EMPTY01', 0, 0, '23', 'Aegle marmelos, Symplocos racemosa, Eclipta alba, Tinospora cordifolia\r\netc', 'Diseases of the head, eyes, ear etc', NULL, NULL, '450 ml', 'For external application over head and neck', 5, 1, 'VILWAMPACHOTYADI THAILAM', NULL, 'VILWAMPACHOTYADI THAILAM', 1, 6, '2019-06-10 14:55:42', '2019-08-13 18:01:59'),
(485, 'SANKHUPUSHPADI ENNA', 'sankhupushpadi-enna', '', NULL, 7, 'EMPTY01', 0, 0, '16', 'Clitoria ternatea, Aegle marmelos, Portulaca oleracea, Ocimum sanctum,\r\nCurcuma longa etc.', 'Chronic sinusitis, rhinitis', NULL, NULL, '200ml, 450 ml', 'For external application over head', 5, 1, 'SANKHUPUSHPADI ENNA', NULL, 'SANKHUPUSHPADI ENNA', 1, 6, '2019-06-10 14:58:04', '2019-08-13 18:02:18'),
(486, 'NONGANADI GHRUTHAM (Sughaprasavada Ghrutham)', 'nonganadi-ghrutham-sughaprasavada-ghrutham', '', NULL, 22, 'EMPTY01', 0, 0, '17', 'Ipomeoa sepiaria, Sida retusa, Boerhavia diffusa, Emblica officinalis, Terminalia chebula, Terminalia bellerica, Cow’s ghee etc.', 'For nourishment during pregnancy & for easy delivery.', NULL, NULL, '200 ml., 450 ml', '10-20 ml bd or as directed by the physician', 5, 1, 'NONGANADI GHRUTHAM (Sughaprasavada Ghrutham)', NULL, 'NONGANADI GHRUTHAM (Sughaprasavada Ghrutham)', 1, 8, '2019-06-10 15:27:41', '2019-08-09 16:55:18'),
(487, 'AMRUTHAPRASAM', 'amruthaprasam', '5d2ecee8e739e1563348712.jpg', NULL, 5, 'H2', 0, 0, '43', 'Jeevaneeyaganam, Dried ginger, Asparagus racemosus, Sida cordifolia, Clerodendrum serratum, Phyllanthus niruri, Laghu panchamoolam, Raisins, Almonds, Dates, Plantain, Cow ghee, Emblica officinalis, Ipomoea mauritiana, Mutton soup, Cow milk, Honey, Sugar, Chathurjathakam, Piper nigrum.', 'Consumption, Fatigue, General weakness, Emaciation.', NULL, '5 - 15 g. b.d., or as directed by the physician', '500 g.', NULL, 5, 1, 'AMRUTHAPRASAM', NULL, 'AMRUTHAPRASAM', 1, 49, '2019-06-10 15:45:35', '2019-08-09 17:06:33'),
(488, 'BRAHMA RASAYANAM', 'brahma-rasayanam', '5d2ecf866c4e71563348870.jpg', NULL, 5, 'EMPTY01', 0, 0, '45', 'Terminalia chebula, Emblica officinalis, Aegle marmelos, Oroxylum indicum, Gmelina arborea, Sugar, cow’sGhee, Gingely oil, Honey', 'Weakness of body and mind,Fever', NULL, '12g with water/milk or as directed by physician', '100g, 500g', NULL, 5, 1, 'BRAHMA RASAYANAM', NULL, 'BRAHMA RASAYANAM', 1, 28, '2019-06-10 15:47:45', '2019-08-09 17:07:52'),
(489, 'DASAMOOLAHAREETHAKI LEHYAM', 'dasamoolahareethaki-lehyam', '5d2ee4cbacab11563354315.jpg', NULL, 5, 'EMPTY01', 0, 0, '20', 'Dasamoolam, Jaggery, Terminalia chebula, Thrikatu, Thrijathakam, Potassium Carbonas Impura, Honey.', 'Dropsy, Urinary disorders, Anaemia, Flatulence, Piles, Obesity, Arthritis, Splenomegaly and Constipation. Recommended as a Hepatocorrective.', NULL, '5 -15 g. b.d., or as directed by the physician.', '200 g., 500 g.', NULL, 5, 1, 'DASAMOOLAHAREETHAKI LEHYAM', NULL, 'DASAMOOLAHAREETHAKI LEHYAM', 1, 25, '2019-06-10 15:50:48', '2019-08-09 17:10:56'),
(490, 'KUTAJA LEHYAM', 'kutaja-lehyam', '5d2eeadd395c01563355869.jpg', NULL, 5, 'H6', 0, 0, '16', 'Holarrhena antidysenterica, Jaggery, Symplocos cochinchinensis, Thrikatu, Cow ghee, Punica granatum.', 'Diarrhoea, Dysentery, Sprue, Bleeding piles, Loss of appetite and Intestinal colic.', NULL, '5 -15 g. b.d., after food or as directed by the physician.', '200 g., 500 g', NULL, 5, 1, 'KUTAJA LEHYAM', NULL, 'KUTAJA LEHYAM', 1, 14, '2019-06-10 16:28:50', '2019-08-09 17:18:45'),
(491, 'PANCHAJEERAKAGUDAM', 'panchajeerakagudam', '5d2ef385bcd061563358085.jpg', NULL, 5, 'H29', 0, 0, '21', 'Cuminum cyminum, Peucedanum graveolens, Carum carvi, Apium graveolens, Nigella sativa, Hyocyamus niger, Piper longum, Ferula asafoetida, Cyperus rotundus, Jaggery, Cow ghee, Cow milk', 'Cough, Loss of appetite. Anorexia, Indigestion and Vaginal disorders. Regulates metabolic activities. Helps to attain body weight. Useful in Post-natal care.', NULL, '5 -15 g. b.d., or as directed by the physician.', '250 g.', NULL, 5, 1, 'PANCHAJEERAKAGUDAM', NULL, 'PANCHAJEERAKAGUDAM', 1, 14, '2019-06-10 16:33:33', '2019-08-09 17:20:51'),
(492, 'KANTAKARYAVALEHAM', 'kantakaryavaleham', '', NULL, 5, 'EMPTY01', 0, 0, '19', 'Solanum surattense, Tinospora cordifolia, Plumbago zeylanica, Cyperus rotundus, Zingiber officinale, Piper nigrum, Piper longum etc', 'Hiccup, Cough, Pain all over the body, Fever', NULL, '6-12gm with milk or Luke warm water, or as directed by the physician', '200 gm.', NULL, 5, 1, 'KANTAKARYAVALEHAM', NULL, 'KANTAKARYAVALEHAM', 1, 8, '2019-06-10 16:48:16', '2019-08-09 17:29:37'),
(493, 'MAHAVILWADI LEHYAM', 'mahavilwadi-lehyam', '', NULL, 5, 'EMPTY01', 0, 0, '14', 'Aegle marmelos, Jaggery, Zingiber officinale, Cuminum cyminum, Elettaria cardamomum etc', 'Vomiting, Cardiac problems, Jaundice, Sinusitis', NULL, '5-15 g b.d or as directed by physician', '200 gm, 500 gm', NULL, 5, 1, 'MAHAVILWADI LEHYAM', NULL, 'MAHAVILWADI LEHYAM', 1, 13, '2019-06-10 16:50:24', '2019-08-09 17:30:25'),
(494, 'CHITHRAKAHAREETHAKI', 'chithrakahareethaki', '', NULL, 5, 'EMPTY01', 0, 0, '24', 'Plumbago zeylanica, dasamoolam etc.', 'Ascites, cold, cough, phthisis, worm infestation etc', NULL, '5-10gm or as directed by physician', '200 gm', NULL, 5, 1, 'CHITHRAKAHAREETHAKI', NULL, 'CHITHRAKAHAREETHAKI', 1, 22, '2019-06-10 16:59:33', '2019-08-09 17:31:59'),
(495, 'AYUSH GHUTTI TABLETS', 'ayush-ghutti-tablets', '', NULL, 3, 'EMPTY01', 0, 0, '7', 'Punica granatum, Mangifera indica, Nelumbo nucifera, Termnalia chebula,\r\nZingiber offinale, Aegle marmelos, Serpentine Stone.', 'Cough, cold, vomiting, diarrhea, Stomatitis & as a general immune tonic for\r\nchildren', NULL, 'Children upto 6 months - 1 tab with mothers milk / with honey once a day. 6\r\nmonths to 1 year - 1 tab twice a day.', '10 x 10 blister of 250 mg tab.', NULL, 5, 1, 'AYUSH GHUTTI TABLETS', NULL, 'AYUSH GHUTTI TABLETS', 1, 22, '2019-06-10 17:05:33', '2019-08-13 12:28:21'),
(496, 'BALARASAYANAM TABLETS', 'balarasayanam-tablets', '', NULL, 3, 'EMPTY01', 0, 0, '7', 'Asparagus racemosus, Embilca officinalis, Tinospora cordifolia, Phyllanthus\r\namarus, Sida retusa, Centella asiatica, Pearl Oyster shell', 'General immune tonic for children', '1 tab twice daily with milk for children upto 1 year of age. 2 tablets twice daily\r\nfor children with 1-5 years of age', NULL, '10 x 10 blister of 250 mg tab.', NULL, 5, 1, 'BALARASAYANAM TABLETS', NULL, 'BALARASAYANAM TABLETS', 1, 15, '2019-06-10 17:07:42', '2019-08-13 12:28:22'),
(497, 'PUNARNAVAMANDOORAM', 'punarnavamandooram', '', NULL, 3, 'EMPTY01', 0, 0, '22', 'Borhoevia diffusa, zingiber officinale, Piper longun, piper nigrum etc.', 'Anaemia, malabsorption syndrome, inter mittent fever, inflammation, splenic\r\ndisorders, diseases of skin, worm infestation.', NULL, '1 - 2 tablets with butter milk or water', '10 x 10 blister.', NULL, 5, 1, 'PUNARNAVAMANDOORAM', NULL, 'PUNARNAVAMANDOORAM', 1, 7, '2019-06-10 17:38:22', '2019-06-10 17:38:22'),
(498, 'CHANDRODAYAVARTI', 'chandrodayavarti', '', NULL, 3, 'EMPTY01', 0, 0, '10', 'Terminalia chebula, Acorus calamus, Piper longum, Piper nigrum, Conch\r\nshell, milk etc.', 'Early stage of Cataract,Growth in layers of eye, Pterygium,\r\nNight blindness,Granular Eyelid disorders, Itching', NULL, NULL, NULL, 'For opthalmic use only', 5, 1, 'CHANDRODAYAVARTI', NULL, 'CHANDRODAYAVARTI', 1, 13, '2019-06-10 17:47:05', '2019-08-13 12:31:35'),
(499, 'SANJEEVANI VATI', 'sanjeevani-vati', '', NULL, 3, 'EMPTY01', 0, 0, '11', 'Embelia ribes, Zingiber officinalis, Thriphala, Acorus calamus etc', 'Indigestion, gastric problems, ascites etc', NULL, '½ tab twice daily or as directed by the physician', '10 x10 blisters', NULL, 5, 1, 'SANJEEVANI VATI', NULL, 'SANJEEVANI VATI', 1, 8, '2019-06-10 17:52:17', '2019-06-10 17:52:17'),
(500, 'LASUNADI VATI', 'lasunadi-vati', '', NULL, 3, 'EMPTY01', 0, 0, '8', 'Allium sativum, Cuminum cyminum, Sulphur, Zingiber officinalis etc.', 'Gastro enteritis with piercing pain, dysentery, sprue.', NULL, '1 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'LASUNADI VATI', NULL, 'LASUNADI VATI', 1, 11, '2019-06-10 17:53:32', '2019-08-13 12:42:37'),
(501, 'PUNARNNAVA GUGGULU', 'punarnnava-guggulu', '', NULL, 3, 'EMPTY01', 0, 0, '20', 'Boerhavia diffusa, Ricinus communis, Zingiber officinalis, Castor oil etc.', 'Rhuematic complaints, inflammation, Sciatica etc.', NULL, '1 tab thrice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'PUNARNNAVA GUGGULU', NULL, 'PUNARNNAVA GUGGULU', 1, 8, '2019-06-10 17:55:27', '2019-06-10 17:55:27'),
(502, 'SAPTHAVIMSHATHIKA GUGGULU', 'sapthavimshathika-guggulu', '', NULL, 3, 'EMPTY01', 0, 0, '14', 'Commiphora mukul, Thrikatu, Curculigo orchioides, Embelia ribes etc.', 'Heart problems, cough, dyspnea, calculus, Rhuematic complaints', NULL, '2 tab Thrice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'SAPTHAVIMSHATHIKA GUGGULU', NULL, 'SAPTHAVIMSHATHIKA GUGGULU', 1, 8, '2019-06-10 17:56:37', '2019-06-10 17:56:37'),
(503, 'SAMSAMANI VATI', 'samsamani-vati', '', NULL, 3, 'EMPTY01', 0, 0, '1', 'Tinospora cordifolia.', 'Fever, chronic fever, phthisis, emaciation, anemia etc', NULL, '1-2 tab twic e daily or as directed by physician', '10 x 10 blisters.', NULL, 5, 1, 'SAMSAMANI VATI', NULL, 'SAMSAMANI VATI', 1, 8, '2019-06-10 17:58:20', '2019-08-13 13:33:05'),
(504, 'CHITRAKADI GULIKA', 'chitrakadi-gulika', '', NULL, 3, 'EMPTY01', 0, 0, '16', 'Plumbago zeylanica, Piper longum, Panchalavana, Zingiber officinalis etc.', 'Indigestion and gastric problems.', NULL, '1 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'CHITRAKADI GULIKA', NULL, 'CHITRAKADI GULIKA', 1, 18, '2019-06-11 09:49:31', '2019-06-11 09:49:31'),
(505, 'LAVANGADI VATI', 'lavangadi-vati', '', NULL, 3, 'EMPTY01', 0, 0, '5', 'Syzygium aromaticum, Piper nigrum, Terminalia chebula, Acacia catechu etc.', 'Cough, Dyspnoea', NULL, '1 tab twice daily or as directed by physician', '10 x 10 blisters.', NULL, 5, 1, 'LAVANGADI VATI', NULL, 'LAVANGADI VATI', 1, 11, '2019-06-11 09:52:06', '2019-06-11 09:52:06'),
(506, 'PRABHAKARA VATI', 'prabhakara-vati', '', NULL, 3, 'EMPTY01', 0, 0, '6', 'Makshikabhasma, Lohabhsama, Abhraka Bhasma etc', 'Heart problems', NULL, '½ tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'PRABHAKARA VATI', NULL, 'PRABHAKARA VATI', 1, 12, '2019-06-11 09:53:56', '2019-06-11 09:53:56'),
(507, 'KUTAJAGHANA VATI', 'kutajaghana-vati', '', NULL, 3, 'EMPTY01', 0, 0, '2', 'Holarrhena antidysenterica, Aconitum heterophyllum', 'Diarrhea, Malabsorption syndrome, Diarrhoea with fever', NULL, '1-2 tab twice daily or as directed by the physician', '10 x 10 blisters', NULL, 5, 1, 'KUTAJAGHANA VATI', NULL, 'KUTAJAGHANA VATI', 1, 13, '2019-06-11 09:55:23', '2019-06-11 09:55:23'),
(508, 'THRIPHALA GUGGULU', 'thriphala-guggulu', '', NULL, 3, 'EMPTY01', 0, 0, '5', 'Thriphala, Commiphora mukul', 'Inflammation, fistula, Haemarrhoids, ascites etc', NULL, '1 tab Thrice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'THRIPHALA GUGGULU', NULL, 'THRIPHALA GUGGULU', 1, 7, '2019-06-11 09:58:05', '2019-06-11 09:58:05'),
(509, 'KANKAYANAM GULIKA', 'kankayanam-gulika', '', NULL, 3, 'EMPTY01', 0, 0, '19', 'Cuminum cyminum, Lepidium sativum, Trachyspermum ammi,\r\nBaliospermum montanum etc', 'Abdominal lump, Helminthiasis/worm infestation, Haemorrhoids,\r\nBleeding disorder, Heart disease', NULL, '1 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'KANKAYANAM GULIKA', NULL, 'KANKAYANAM GULIKA', 1, 7, '2019-06-11 09:59:24', '2019-06-11 09:59:24'),
(510, 'ABHRAKA BHASMAM', 'abhraka-bhasmam', '', NULL, 2, 'EMPTY01', 0, 0, NULL, 'Mica (processed).', 'Diabetes, Skin disorders, Cough, Respiratory disorders. Blood impurity, Sprue, Anaemia, Pthisis, Jaundice.', NULL, '125 - 250 mg. b.d. with honey/Thriphala kashayam/Guluchi swarasa/Ginger juice or as directed by the physician', '5g., 10 g', NULL, 5, 1, 'ABHRAKA BHASMAM', NULL, 'ABHRAKA BHASMAM', 1, 26, '2019-06-11 10:01:09', '2019-08-13 10:54:05'),
(511, 'AVILTHOLADI BHASMAM', 'aviltholadi-bhasmam', '5d43d50693b4a1564726534.jpg', NULL, 2, 'B3', 0, 0, '11', 'Holoptelia integrifolia, Cyathula prostrata, Baliospermum montanum, Ricinus communis, Cassia fistula.', 'Dropsy. Also effective in Anaemia and Ascites.', NULL, 'As directed by the physician. Indicated to prepare gruel with rice and milk/ buttermilk.', '50 g., 100g.', NULL, 5, 1, 'AVILTHOLADI BHASMAM', NULL, 'AVILTHOLADI BHASMAM', 1, 15, '2019-06-11 10:02:52', '2019-08-13 10:55:56'),
(512, 'GODANTHI BHASMAM', 'godanthi-bhasmam', '', NULL, 2, 'EMPTY01', 0, 0, NULL, 'Gypsum', 'Dyspnoea, digestive impairment, headache, chronic fever, cough, etc', NULL, NULL, NULL, '250 mg bd along with Honey, Tulasi Svarasa, ghee, sugar.or as directed by physician.', 5, 1, 'GODANTHI BHASMAM', NULL, 'GODANTHI BHASMAM', 1, 10, '2019-06-11 10:04:23', '2019-08-13 10:57:35'),
(513, 'KALYANAKSHARAM', 'kalyanaksharam', '', NULL, 2, 'EMPTY01', 0, 0, '13', 'Thrikatu, Thriphala, Thrilavanam, Baliospermum montanum, Semicarpus anacardium, Plumbago rosea, cows urine.', 'Piles, Pthisis, Splenic disorders, Ascites, Dropsy, Urolithiasis, Dysuria, Diabetes, Flatulence, Cardiac disorders.', NULL, '1 g. b.d. with honey or ghee or along with food or as directed by the physician.', '250 g', NULL, 5, 1, 'KALYANAKSHARAM', NULL, 'KALYANAKSHARAM', 1, 17, '2019-06-11 10:06:10', '2019-08-13 10:58:28'),
(514, 'SANKHA BHASMAM', 'sankha-bhasmam', '', NULL, 2, 'EMPTY01', 0, 0, NULL, 'Conch shell', 'Dyspepsia, digestive impairment, malabsorption syndrome, Enlargement of liver and spleen, duodenal ulcer.', NULL, NULL, NULL, '125 to 250 mg bd along with lemon juice or Triphala kashaya or as directed by physician.', 5, 1, 'SANKHA BHASMAM', NULL, 'SANKHA BHASMAM', 1, 12, '2019-06-11 10:08:23', '2019-08-09 17:48:51'),
(516, 'BRUHATHYADI KASHAYACHOORNAM', 'bruhathyadi-kashayachoornam', '', NULL, 24, 'K20', 0, 0, '5', 'Desmodium gangeticum, Pseudarthria viscida, Solanum melongena, Aerva lanata, Tribulus terrestris.', 'Urinary infection, Calculi, Dysuria, Haematuria, Urolithiasis.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg.', NULL, 5, 1, 'BRUHATHYADI KASHAYACHOORNAM', NULL, 'BRUHATHYADI KASHAYACHOORNAM', 1, 8, '2019-06-11 10:12:01', '2019-08-13 12:50:54'),
(517, 'BHARANGYADI KASHAYA CHOORNAM', 'bharangyadi-kashaya-choornam', '', NULL, 24, 'EMPTY01', 0, 0, '11', 'Clerodendrum serratum, Tinospora cordifolia, Zingiber officinale, Cyprus rotundus etc', 'Intermittent fever, Cough', NULL, NULL, '250gm, 1kg', NULL, 5, 1, 'BHARANGYADI KASHAYA CHOORNAM', NULL, 'BHARANGYADI KASHAYA CHOORNAM', 1, 7, '2019-06-11 10:13:22', '2019-08-13 12:49:20'),
(518, 'KHADIRADI KASHAYACHOORNAM', 'khadiradi-kashayachoornam', '', NULL, 24, 'K67', 0, 0, '4', 'Murraya koenigii, Terminalia chebula, Trichosanthes anguina, Dried ginger.', 'Diarrhoea, Indigestion, Heart burn, Colic, Flatulence, Amoebic dysentery.', NULL, NULL, '250 g., 500 g., 1 kg., 2 kg.', NULL, 5, 1, 'KHADIRADI KASHAYACHOORNAM', NULL, 'KHADIRADI KASHAYACHOORNAM', 1, 3, '2019-06-11 10:27:22', '2019-08-13 11:53:05'),
(519, 'MAHAMANJISHTADI KASHAYA CHOORNAM', 'mahamanjishtadi-kashaya-choornam', '', NULL, 24, 'EMPTY01', 0, 0, '45', 'Rubia cordifolia, Holorrhena antidysentrica Cyperus rotundus, Tinospara\r\ncardifolia, Zingiber officinale etc.', 'Paralysis, fibriasis, Rheumatic complaints', NULL, NULL, '250 gm, 500 gm, 2.5 kg.', NULL, 5, 1, 'MAHAMANJISHTADI KASHAYA CHOORNAM', NULL, 'MAHAMANJISHTADI KASHAYA CHOORNAM', 1, 2, '2019-06-11 10:30:57', '2019-08-13 10:56:16'),
(520, 'MRUDWEEKADI KASHAYACHOORNAM', 'mrudweekadi-kashayachoornam', '', NULL, 24, 'K34', 0, 0, '4', 'Raisins, Glycyrrhiza glabra, Azadirachta indica, Picorrhiza kurroa.', 'Thirst, Fainting, Alcoholism, Delusions.', NULL, NULL, '250 g., 500 g., 1 kg., 2 kg.', NULL, 5, 1, 'MRUDWEEKADI KASHAYACHOORNAM', NULL, 'MRUDWEEKADI KASHAYACHOORNAM', 1, 2, '2019-06-11 10:33:39', '2019-08-13 11:56:48'),
(521, 'MUSALIKHADIRADI KASHAYACHOORNAM', 'musalikhadiradi-kashayachoornam', '', NULL, 24, 'K25', 0, 0, '6', 'Curculigo orchioides, Acacia catechu, Emblica officinalis, Tribulus terrestris,\r\nAsparagus racemosus, Syzygium cumini.', 'Leucorrhoea, Dysfunctional uterine bleeding, Albuminuria.', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'MUSALIKHADIRADI KASHAYACHOORNAM', NULL, 'MUSALIKHADIRADI KASHAYACHOORNAM', 1, 3, '2019-06-11 10:35:53', '2019-08-13 11:59:18'),
(522, 'NEELIKARANJADI KASHAYACHOORNAM', 'neelikaranjadi-kashayachoornam', '', NULL, 24, 'K14', 0, 0, '10', 'Indigofera tinctoria, Pongamia pinnata, Azadirachta indica, Albizzia lebbeck,\r\nMoringa oleifera, Cyperus rotundus, Acorus calamus, Dried ginger, Cedrus\r\ndeodara, Santalum album.', 'Snake poison', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'NEELIKARANJADI KASHAYACHOORNAM', NULL, 'NEELIKARANJADI KASHAYACHOORNAM', 1, 3, '2019-06-11 10:39:59', '2019-08-13 13:47:05'),
(523, 'NEELIMOOLADI KASHAYACHOORNAM', 'neelimooladi-kashayachoornam', '', NULL, 24, 'K12', 0, 0, '15', 'Indigofera tinctoria, Pongamia pinnata, Azadirachta indica, Alangium lamarckii,\r\nMoringa oleifera, Albizzia lebbeck, Cyperus rotundus, Acorus calamus, Dried\r\nginger, Cedrus deodara, Santalum album, Cuminum cyminum, Withania\r\nsomnifera, Saussurea lappa, Rauwolfia serpentina.', 'Poisonous insect bites, Chronic skin diseases.', NULL, NULL, '250 g, 500 g, 1 kg, 3 kg.', NULL, 5, 1, 'NEELIMOOLADI KASHAYACHOORNAM', NULL, 'NEELIMOOLADI KASHAYACHOORNAM', 1, 3, '2019-06-11 10:41:59', '2019-08-13 12:03:06'),
(524, 'SHADANGAM KASHAYA CHOORNAM', 'shadangam-kashaya-choornam', '', NULL, 24, 'EMPTY01', 0, 0, '6', 'Zingiber officinale, Santalum album, Vetiveria zizanioides', 'Improves digestion, It reduces thirst in fever', NULL, NULL, '50g, 500g', NULL, 5, 1, 'SHADANGAM KASHAYA CHOORNAM', NULL, 'SHADANGAM KASHAYA CHOORNAM', 1, 12, '2019-06-11 11:06:56', '2019-08-13 10:57:05'),
(525, 'VlDARYADI KASHAYACHOORNAM', 'vldaryadi-kashayachoornam', '', NULL, 24, 'K45A', 0, 0, '18', 'Ipomoea mauritiana, Ricinus communis, Boerrhavia diffusa, Cedrus deodara,\r\nDesmodium trifoliatum, Jeevanapanchamoolam, Laghu panchamoolam.', 'Cough, Bronchitis, Cardiac disorders like palpitation and tachycardia, Fatigue,\r\nGeneral debility. Helpful to regain health and body weight.', NULL, NULL, '250 g., 500 g., 1 kg., 2.5 kg', NULL, 5, 1, 'VlDARYADI KASHAYACHOORNAM', NULL, 'VlDARYADI KASHAYACHOORNAM', 1, 8, '2019-06-11 11:10:32', '2019-08-13 12:15:16'),
(526, 'VYAGHRIYADI KASHAYA CHOORNAM', 'vyaghriyadi-kashaya-choornam', '', NULL, 24, 'EMPTY01', 0, 0, '3', 'Tinospora cordifolia, Zingiber officinale, Solanum melongina', 'Fever, Cold, Respiratory tract infection', NULL, NULL, '250gm, 1kg', NULL, 5, 1, 'VYAGHRIYADI KASHAYA CHOORNAM', NULL, 'VYAGHRIYADI KASHAYA CHOORNAM', 1, 6, '2019-06-11 11:12:25', '2019-08-13 12:16:21'),
(527, 'VYOSHADI KASHAYACHOORNAM', 'vyoshadi-kashayachoornam', '', NULL, 24, 'K28', 0, 0, '15', 'Thrikatu, Apium graveolens, Boerrhavia diffusa, Saccharum officinarum, Iron\r\n(processed), Terminalia chebula, Sida cordifolia, Tamarindus indica (rachis),\r\nIron rust (purified), Citrus acida, Curcuma longa, Vitis quadrangularis,\r\nDesmodium trifoliatum', 'Anaemia, General weakness, Dropsy, Anorexia', NULL, NULL, '250 g., 500 g., 1 kg., 3 kg.', NULL, 5, 1, 'VYOSHADI KASHAYACHOORNAM', NULL, 'VYOSHADI KASHAYACHOORNAM', 1, 6, '2019-06-11 11:15:05', '2019-08-13 12:17:53'),
(528, 'ASHTAVARGAM KASHAYAM', 'ashtavargam-kashayam', '5d2ec45f5133b1563346015.jpg', NULL, 6, 'D40', 0, 0, '8', 'Sida cordifolia, Strobilanthes ciliatus, Ricinus communis, Dried ginger, Alpinia calcarata, Cedrus deodara, Vitex negundo, Allium sativum.', 'Rheumatic complaints, Hemiplegia, Numbness, Vertigo, Hypertension.', NULL, NULL, '200 ml.', NULL, 5, 1, 'ASHTAVARGAM KASHAYAM', NULL, 'ASHTAVARGAM KASHAYAM', 1, 24, '2019-06-11 11:20:34', '2019-08-13 11:07:17'),
(529, 'BRUHATHYADI KASHAYAM', 'bruhathyadi-kashayam', '5d2ec42894c6c1563345960.jpg', NULL, 6, 'D20', 0, 0, '5', 'Desmodium gangeticum, Pseudarthria viscida, Solanum melongena, Aerva lanata, Tribulus terrestris.', 'Urinary infection, Calculi, Dysuria, Haematuria, Urolithiasis.', NULL, NULL, '200 ml.', NULL, 5, 1, 'BRUHATHYADI KASHAYAM', NULL, 'BRUHATHYADI KASHAYAM', 1, 10, '2019-06-11 11:23:39', '2019-08-13 11:17:18'),
(533, 'HAMSAPADYADI KASHAYAM', 'hamsapadyadi-kashayam', '', NULL, 6, 'EMPTY01', 0, 0, '5', 'Adiantum phillippens,Tinospora cordifolia, Azadiracta indica, Piper longum, Adathoda beddomie.', 'Thyroid problems', NULL, NULL, '200 ml', NULL, 5, 1, 'HAMSAPADYADI KASHAYAM', NULL, 'HAMSAPADYADI KASHAYAM', 1, 6, '2019-06-11 12:18:03', '2019-08-13 12:30:51'),
(535, 'AMRUTHOTHARAM KASHAYAM TABLET', 'amruthotharam-kashayam-tablet', '', NULL, 23, 'EMPTY01', 0, 0, '3', 'Tinospora cordifolia, Terminalia chebula, Dried ginger', 'Fever, Flu, Cold and Headache. Used as a digestant in Rheumatic fever', NULL, '3 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'AMRUTHOTHARAM KASHAYAM TABLET', NULL, 'AMRUTHOTHARAM KASHAYAM TABLET', 1, 20, '2019-06-11 12:23:05', '2019-08-13 13:58:23'),
(536, 'DASAMOOLAKADUTRAYAM KASHAYAM TABLET', 'dasamoolakadutrayam-kashayam-tablet', '', NULL, 23, 'EMPTY01', 0, 0, '14', 'Dasamoolam, Thrikatu, Adhatoda beddomei.', 'Respiratory disorders, Rhinitis, Cold, Body ache, Headache, Cough, Flu,\r\nFever and Low back-ache.', NULL, '2-3 tab twice daily or as directed by the physician', '10 x 10 blisters', NULL, 5, 1, 'DASAMOOLAKADUTRAYAM KASHAYAM TABLET', NULL, 'DASAMOOLAKADUTRAYAM KASHAYAM TABLET', 1, 18, '2019-06-11 12:32:47', '2019-08-13 10:46:50'),
(537, 'RASNEIRANDADI KASHAYAM TABLET', 'rasneirandadi-kashayam-tablet', '', NULL, 23, 'EMPTY01', 0, 0, '14', 'Alpinia calcarata, Ricinus communis, Sida cordifolia, Strobilanthes ciliatus, Asparagus racemosus, Tragia involucrata, Adhatoda beddomei, Tinospora cordifolia, Cedrus deodara, Aconitum heterophyllum, Cyperus rotundus, Asteracantha longifolia, Kaempferia galanga, Dried ginger', 'Rheumatic complaints, Arthritis, Lumbago, Joint pain, Vertigo, Painful Swelling over joints.', NULL, '2 tab twice daily or as directed by physician', '10 x 10 blisters', NULL, 5, 1, 'RASNEIRANDADI KASHAYAM TABLET', NULL, 'RASNEIRANDADI KASHAYAM TABLET', 1, 15, '2019-06-11 12:34:35', '2019-08-13 10:47:17'),
(538, 'BALADI KASHAYA SOOKSHMA CHOORNAM', 'baladi-kashaya-sookshma-choornam', '', NULL, 4, 'EMPTY01', 0, 0, '1', 'Sida retusa', 'Diseases associated with vata', NULL, NULL, '70g.', NULL, 5, 1, 'BALADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'BALADI KASHAYA SOOKSHMA CHOORNAM', 1, 10, '2019-06-11 12:37:30', '2019-08-13 11:30:57'),
(539, 'PADOLAMOOLADI KASHAYA SOOKSHMA CHOORNAM', 'padolamooladi-kashaya-sookshma-choornam', '', NULL, 4, 'EMPTY01', 0, 0, '8', 'Trichosanthes anguina, Thriphala, Citrullus colocynthis, Bacopa monnieri,\r\nPicrorrhiza curroa, Dried ginger', 'Chronic skin diseases, Vitiligo, Ascites, Psoriasis. An effective purgative.', NULL, NULL, '500g.', NULL, 5, 1, 'PADOLAMOOLADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'PADOLAMOOLADI KASHAYA SOOKSHMA CHOORNAM', 1, 13, '2019-06-11 12:41:18', '2019-08-13 11:32:46'),
(540, 'RASNEIRANDADI KASHAYA SOOKSHMA CHOORNAM', 'rasneirandadi-kashaya-sookshma-choornam', '', NULL, 4, 'K82A', 0, 0, '14', 'Alpinia calcarata, Ricinus communis, Sida cordifolia, Asparagus racemosus,\r\nCedrus deodara, Cyperus rotundus', 'Rheumatic complains, Arthritis, Lumbago, Joint pain, Vertigo, Painful swelling\r\nover joints', NULL, NULL, '250g, 500g, l kg, 2.5kg', NULL, 5, 1, 'RASNEIRANDADI KASHAYA SOOKSHMA CHOORNAM', NULL, 'RASNEIRANDADI KASHAYA SOOKSHMA CHOORNAM', 1, 12, '2019-06-11 12:42:56', '2019-08-13 11:34:39'),
(541, 'APARAJITHADHOOPA CHOORNAM', 'aparajithadhoopa-choornam', '5c94bdbca89271553251772.jpg', NULL, 25, 'C95', 0, 0, '8', 'Bomiphora mukul, Acorus calamus, Azadirachta indica, Calotropis gigantea,\r\nBrassica nigra.', 'For fumigation to prevent spread of contageous diseases.', NULL, NULL, '50g', NULL, 5, 1, 'APARAJITHADHOOPA CHOORNAM', NULL, 'APARAJITHADHOOPA CHOORNAM', 1, 23, '2019-06-11 12:50:46', '2019-08-13 13:43:46'),
(543, 'INTHUPPUKANAM', 'inthuppukanam', '', NULL, 25, 'C4', 0, 0, '4', 'Rock salt, Apium graveolens, Piper longum, Terminalia chebula.', 'Colic and Indigestion.', NULL, '5 -15 g. before food with lukewarm water/buttermilk or as directed by the physician', '50 g., 500 g.', NULL, 5, 1, 'INTHUPPUKANAM', NULL, 'INTHUPPUKANAM', 1, 6, '2019-06-11 12:55:59', '2019-08-13 14:53:39'),
(547, 'AJAMODADI CHOORNAM', 'ajamodadi-choornam', '', NULL, 25, 'EMPTY01', 0, 0, '12', 'Tachyspermum ammi, Embelia ribes, Rocksalt, Cedrus deodara, Plumbago zeylanica etc', 'Rheumatic complaints and associated symptoms, improper movement of vatha, swelling', NULL, '3-6 gm with jaggery or lukewarm water or as directed by the physician', '50 g., 500 g.', NULL, 5, 1, 'AJAMODADI CHOORNAM', NULL, 'AJAMODADI CHOORNAM', 1, 16, '2019-06-11 13:55:03', '2019-08-13 14:18:15'),
(548, 'DASANGALEPA CHOORNAM', 'dasangalepa-choornam', '', NULL, 25, 'EMPTY01', 0, 0, '10', 'Albizia lebbeck, Glycyrrhiza glabra, Valleriana vallichi, Santalum album,\r\nCurcuma longa etc', 'Fever, inflammation, erysipelas, diseases of the skin', NULL, NULL, '50 g., 500 g', 'External application along with ghee or as directed by physician', 5, 1, 'DASANGALEPA CHOORNAM', NULL, 'DASANGALEPA CHOORNAM', 1, 6, '2019-06-11 13:56:23', '2019-08-13 14:19:38'),
(551, 'GULUCHI CHOORNAM', 'guluchi-choornam', '', NULL, 26, 'EMPTY01', 0, 0, NULL, 'Tinospora cordifolia', 'Rheumatoid arthritis, Diabetes mellitus, Jaundice, Skin diseases, Leucorrhoea,\r\nSpermatorrhoea.', NULL, '5 -15 g. with lukewarm water/Honey or as directed by the physician.', '50 g., 500 g.', NULL, 5, 1, 'GULUCHI CHOORNAM', NULL, 'GULUCHI CHOORNAM', 1, 13, '2019-06-11 14:06:28', '2019-08-13 14:25:23'),
(552, 'LAKSHMANA CHOORNAM', 'lakshmana-choornam', '', NULL, 26, 'EMPTY01', 0, 0, NULL, 'Ipomoea sepiaria', 'Burning sensation, hyperdipsia, used in infertility of women, general debility,\r\nas uterine tonic and laxative.', NULL, '5 - 15 g. with luke warm water/Honey or as directed by the physician.', NULL, 'As directed by the physician', 5, 1, 'LAKSHMANA CHOORNAM', NULL, 'LAKSHMANA CHOORNAM', 1, 6, '2019-06-11 14:08:20', '2019-08-13 15:39:56'),
(553, 'MURVA CHOORNAM', 'murva-choornam', '', NULL, 26, 'EMPTY01', 0, 0, NULL, 'Marsdenia tenacissima', 'Fever, obesity, diabetes, diseases of the mouth, worm infestation, thirst, etc', NULL, NULL, NULL, '2-6 gm with suitable anupana or as directed by the physician', 5, 1, 'MURVA CHOORNAM', NULL, 'MURVA CHOORNAM', 1, 6, '2019-06-11 14:15:55', '2019-08-13 15:41:49'),
(554, 'PRISNIPARNI CHOORNAM', 'prisniparni-choornam', '', NULL, 26, 'EMPTY01', 0, 0, NULL, 'Uraria picta', 'Burning sensation, Fever, Asthma, Skin diseases, vomiting, ulcers, Haemarrhoides, eye diseases, fracture etc', '5-15 gm with suitable anupana or as directed by the physician.', NULL, NULL, NULL, 5, 1, 'PRISNIPARNI CHOORNAM', NULL, 'PRISNIPARNI CHOORNAM', 1, 6, '2019-06-11 14:19:21', '2019-08-13 15:46:51'),
(555, 'TRIVRIT CHOORNAM', 'trivrit-choornam', '', NULL, 26, 'C39', 0, 0, NULL, 'Opercilina turperthum', 'A safe purgative. Recommended in Skin diseases, Erysepelas, Dropsy, Ascites.', NULL, '15 - 20 g. for purgation and 5 -10 g. as additive with Honey or decoctions as\r\ndirected by the physician.', '50 g., 500 g.', NULL, 5, 1, 'TRIVRIT CHOORNAM', NULL, 'TRIVRIT CHOORNAM', 1, 8, '2019-06-11 14:24:06', '2019-08-13 15:51:03'),
(556, 'ARJUNA CHOORNAM', 'arjuna-choornam', '', NULL, 26, 'EMPTY01', 0, 0, '1', 'Terminalia arjuna', 'Excess fat, hypercholesterolaemia, ulcers, cardiotonic, emaciation, diabetes,\r\nexcess thirst etc', NULL, '3–6 g. of the drug in powder form or as directed by the physician', NULL, NULL, 5, 1, 'ARJUNA CHOORNAM', NULL, 'ARJUNA CHOORNAM', 1, 20, '2019-06-11 14:35:58', '2019-06-11 14:35:58'),
(557, 'MADANA CHOORNAM', 'madana-choornam', '', NULL, 26, 'EMPTY01', 0, 0, '1', 'Rantia dumetorum', 'Digestive problem', NULL, NULL, '50gm, 500gm', '5 – 10 gm along with honey or as directed by the physician', 5, 1, 'MADANA CHOORNAM', NULL, 'MADANA CHOORNAM', 1, 5, '2019-06-11 14:38:11', '2019-08-13 15:58:25'),
(558, 'VIBHEETHAKA CHOORNAM', 'vibheethaka-choornam', '', NULL, 26, 'EMPTY01', 0, 0, '1', 'Terminalia belerica', 'Cough, Excessive thirst, Vomitting, Eye disorders, hair fall', NULL, '5 – 10 gm along with honey or as directed by the physician', '50gm, 500gm', NULL, 5, 1, 'VIBHEETHAKA CHOORNAM', NULL, 'VIBHEETHAKA CHOORNAM', 1, 3, '2019-06-11 14:39:37', '2019-08-13 15:58:56'),
(559, 'ELANEER KUZHAMBU', 'elaneer-kuzhambu', '', NULL, 10, 'EMPTY01', 0, 0, '9', 'Coscinium fenestratum, Triphala, Glycyrrhiza glabra, Tender coconut water, Rock salt, Camphor, Honey.', 'Cataract, Conjunctivitis, Pain in eyes. Renders cooling effect to eyes', NULL, NULL, '10 g., 25 g', 'For ophthalmic use only.', 5, 1, 'ELANEER KUZHAMBU', NULL, 'ELANEER KUZHAMBU', 1, 17, '2019-06-11 14:41:44', '2019-08-09 15:40:28');
INSERT INTO `product` (`product_id`, `title`, `slug`, `image`, `description`, `category_id`, `model`, `featured`, `show_home`, `total_ingradients`, `main_ingradients`, `indication`, `special_indication`, `dosage`, `presentation`, `p_usage`, `rating`, `tax`, `meta_title`, `meta_description`, `keywords`, `status`, `views`, `date_added`, `date_modified`) VALUES
(561, 'OUSHADHI CHYAVANAPRASAM - CHOCOLATE FLAVOUR', 'oushadhi-chyavanaprasam-chocolate-flavour', '5d2efb3e76a2d1563360062.jpg', NULL, 8, 'EMPTY01', 1, 0, '52', 'Chyavanaprasam, Bacopa monnieri, Acorus calamus, Saffron, Almonds.', 'Emaciation, Respiratory complaints, General debility. Restores immunity power\r\nand youth. Enhances memory power.', NULL, '2 tsp. b.d. after food preferably with milk', '250 g., 500 g.', NULL, 5, 1, 'OUSHADHI CHYAVANAPRASAM', NULL, 'OUSHADHI CHYAVANAPRASAM', 1, 33, '2019-06-11 15:00:49', '2019-08-09 15:49:37'),
(562, 'OUSHADHI MURIVENNA OINTMENT', 'oushadhi-murivenna-ointment', '5d2ec1bdaa6181563345341.jpg', NULL, 8, 'TC2', 1, 0, '3', 'Murivenna, Bee wax, White petroleum jelly', 'Wounds, Sprain, Contusions, Bruises over vital points and Burns', NULL, NULL, '10 g.', 'For external application only', 5, 1, 'OUSHADHI MURIVENNA OINTMENT', NULL, 'OUSHADHI MURIVENNA OINTMENT', 1, 34, '2019-06-11 15:02:39', '2019-08-08 18:32:57'),
(563, 'OUSHADHI RHEUMAJITH OINTMENT', 'oushadhi-rheumajith-ointment', '5d2ec19ba1f621563345307.jpg', NULL, 8, 'T99A', 1, 0, '12', 'Sahacharadi thailam, Dhanwantharam kuzhambu, Prabhanjanavimardanam, Narayana thailam, Kottamchukkadi thailam, Winter grass oil, Mustard oil, Camphor, Bee wax, White petroleum jelly', 'Rheumatic pain, Inflammation, Contusions and Sprain', NULL, 'For external application over the affected area. Gentle massage, followed with fomentation is preferred.', '10 g.', NULL, 5, 1, 'OUSHADHI RHEUMAJITH OINTMENT', NULL, 'OUSHADHI RHEUMAJITH OINTMENT', 1, 28, '2019-06-11 15:04:35', '2019-08-09 15:53:24'),
(566, 'KONNAYILAKADUKADI LEPAM', 'konnayilakadukadi-lepam', '', NULL, 8, 'EMPTY01', 1, 0, '6', 'Cassia fistula, Brassica nigra, curcuma longa, butter milk, cream base etc', 'Itching and fungal infections of skin.', NULL, NULL, '25gm tube', 'External application only', 5, 1, 'KONNAYILAKADUKADI LEPAM', NULL, 'KONNAYILAKADUKADI LEPAM', 1, 51, '2019-06-11 15:15:31', '2019-08-21 14:49:01'),
(567, 'NILAVEMBU KUDINEER', 'nilavembu-kudineer', '', NULL, 11, 'SKC', 0, 0, '9', 'Andrographispaniculata,Vetiveriazizanioides,Coleus vettiveriodes,Santalum album, Trichosanthesdioica,Cyperusrotundus,Zingiberofficinale,Piper nigrum, Oldenlandiacorymbosa', 'Fever (Suram)', NULL, NULL, '100gm,250gm,500gm,2kg', NULL, 5, 1, 'NILAVEMBU KUDINEER', NULL, 'NILAVEMBU KUDINEER', 1, 18, '2019-07-15 09:45:51', '2019-07-15 09:45:51'),
(568, 'NERUNJIL KUDINEER', 'nerunjil-kudineer', '', NULL, 11, 'SKC', 0, 0, '11', 'Tribulusterrestris, Emblicaofficinalis, Terminaliachebula, Terminaliabellerica, Smilax china, Solanumnigrum, Cassia fistula,Foeniculumvulgare,Astercanthalongifolia Curcuma lagenaria, Curcumissativus', 'Diuretic(Neer perukki),Swelling(Veekkam),Odema(Oodhal)', NULL, NULL, '100gm, 250gm, 500gm, 2kg', NULL, 5, 1, 'NERUNJIL KUDINEER', NULL, 'NERUNJIL KUDINEER', 1, 24, '2019-07-15 09:49:05', '2019-07-15 09:49:05'),
(569, 'PITHA JWARA KUDINEER', 'pitha-jwara-kudineer', '', NULL, 11, 'SKC4', 0, 0, '15', 'Vetiveriazizanioides, Tragiainvolucrata,  Aeglemarmelos, Stereospermumcolais, Glyzyrrhizaglabra, Saussurealappa, Hemidesmusindicus, Cassaiaangustifolia Cuminumcyminum, Coriandrumsativum, Leucaszeylanica, Phoenix dactylifera Scindapsusofficinalis, Emblicaofficinalis,Zingiberofficinale', 'All types of fever due to pitha disorders(Azhal suram)', NULL, NULL, '100gm, 250gm, 500gm, 2kg', NULL, 5, 1, 'PITHA JWARA KUDINEER', NULL, 'PITHA JWARA KUDINEER', 1, 16, '2019-07-15 10:26:26', '2019-07-15 10:30:59'),
(570, 'VATHA JWARA KUDINEER', 'vatha-jwara-kudineer', '', NULL, 11, 'SKC3', 0, 0, '20', 'ragiainvolucrata, Piper longum,  Plumbagozeylanica,  Azimatetracantha,  Calotropisgigante, Andrographispaniculata,  Caesalpiniabonduc,   Solanumxanthocarpum, Sidacordifolia, Alpiniacalcarata,  Crataeva magna, Alpiniagalanga,  Anethumgraveolens, Evolvulusalsinoides, Clerodendrumserratum, Zingiberofficinale, Piper brachystachyum, Saussurealappa, Piper longum', 'All types of fever due to Vatha disorders(Vali  suram)', NULL, NULL, '100gm, 250gm, 500gm, 2kg', NULL, 5, 1, 'VATHA JWARA KUDINEER', NULL, 'VATHA JWARA KUDINEER', 1, 16, '2019-07-15 10:30:19', '2019-07-15 10:30:19'),
(571, 'KABA JWARA KUDINEER', 'kaba-jwara-kudineer', '', NULL, 11, 'SKC5', 0, 0, '15', 'Zingiberofficinale, Piper longum, Syyigiumaromaticum, Tragiainvolucrata,  Astercanthalongifolia, Terminaliachebula, Adhatodavasica, Coleus aromaticus Saussurealappa, Tinosporacordifolia, Clerodendrumserratum, Andrographispaniculata Sidacordifolia, Cyperusrotundus', 'All types of fever due to Kapha disorders(Kaba suram)', NULL, NULL, '100gm, 250gm, 500gm, 2kg', NULL, 5, 1, 'KABA JWARA KUDINEER', NULL, 'KABA JWARA KUDINEER', 1, 18, '2019-07-15 11:01:20', '2019-07-15 11:01:20'),
(572, 'ADATHODAI KUDINEER CHOORNAM', 'adathodai-kudineer-choornam', '', NULL, 11, 'SKC2', 0, 0, '4', 'Adathodavasica, Glycyrrhizaglabra, Taxusbaccata, Piper longum', 'cough(Irumal)', NULL, NULL, '100gm, 250gm, 500gm, 2kg', NULL, 5, 1, 'ADATHODAI KUDINEER CHOORNAM', NULL, 'ADATHODAI KUDINEER CHOORNAM', 1, 41, '2019-07-15 11:04:09', '2019-07-15 11:04:09'),
(573, 'AMUKKARA CHURNAM', 'amukkara-churnam', '', NULL, 12, 'SC1', 0, 0, '8', 'Syzygiumaromaticum, Mesuaferrea, Elettariacardamomum, Piper  nigrum, Piper  longum,Zingiberofficinale, Withaniasomnifera', 'All kinds of Megharogangal,Iraippu(Wheezing/spasm),Itching,Vatha disorders,Induces sleep.', NULL, NULL, '50gm, 500gm', NULL, 5, 1, 'AMUKKARA CHURNAM', NULL, 'AMUKKARA CHURNAM', 1, 28, '2019-07-15 11:07:34', '2019-07-15 11:07:34'),
(574, 'NILAVARAI (NILAVAGI) CHURNAM', 'nilavarai-nilavagi-churnam', '', NULL, 12, 'SC4', 0, 0, '5', 'Cassaiaangustifolia, Zingiberofficinale, Piper nigrum, Trachyspermumammi, Embeliaribes', 'constipation(Malakattu)', NULL, NULL, '50gm, 500gm', NULL, 5, 1, 'NILAVARAI (NILAVAGI) CHURNAM', NULL, 'NILAVARAI (NILAVAGI) CHURNAM', 1, 20, '2019-07-15 11:15:26', '2019-07-15 11:15:26'),
(575, 'ELADHI CHURNAM  (S)', 'eladhi-churnam-s', '', NULL, 12, 'SC2', 0, 0, '8', 'Syzygiumaromaticum, Piper longum, Mesuaferrea, Taxusbaccata, Marantaarundinacea, Zingiberofficinale, Elettariacardamoum, Sugar', 'urning sensation of hands and foot,Blacking of skin,itching of skin,Excessive body heat Burning micturation', NULL, NULL, '50gm, 500gm', NULL, 5, 1, 'ELADHI CHURNAM  (S)', NULL, 'ELADHI CHURNAM  (S)', 1, 18, '2019-07-15 11:17:40', '2019-07-15 11:17:40'),
(576, 'PANCHA DEEPAGNI CHURNAM', 'pancha-deepagni-churnam', '', NULL, 12, 'SC3', 0, 0, '5', 'Zingiberofficinale, Piper nigrum, Piper longum, Elettariacardamomum, Cuminumcymimum', 'Abnormal distension of stomach, loss of appetite, indigestion, colic pain', NULL, NULL, '50gm, 500gm', NULL, 5, 1, 'PANCHA DEEPAGNI CHURNAM', NULL, 'PANCHA DEEPAGNI CHURNAM', 1, 20, '2019-07-15 11:19:57', '2019-07-15 11:19:57'),
(577, 'CHUNDAIVATRAL CHURNAM', 'chundaivatral-churnam', '', NULL, 12, 'SC6', 0, 0, '7', 'Solanum thorvum, Murrayakoenigii , Mangiferaindica,  Apiumgraveolens , Emblicaofficinalis,  Punicagranatum, Trigonellafoenum-graecum', 'Indigestion(Agnimaandham),Diarrhoea(Kazhichal/Bedhi),Piles(Moolam),Flatulances', NULL, NULL, '500 g, 50 g', NULL, 5, 1, 'CHUNDAIVATRAL CHURNAM', NULL, 'CHUNDAIVATRAL CHURNAM', 1, 19, '2019-07-15 11:22:12', '2019-07-15 11:22:12'),
(578, 'INJI CHURNAM', 'inji-churnam', '', NULL, 12, 'SC7', 0, 0, '14', 'Zingiberofficinale, Piper longum, Piper nigrum, Piper longum, Elettariacardamomum, Zingiberofficinale, Myristicafragrans, Valerianajatamansi, Cuminumcyminum, Foeniculumvulgare, Saussurealappa, Mesuanagassarium, Cinnamomumzeylanicum,\r\nVetiveriazizanioides, Syyigiumaromaticum, Myristicafragrans', 'Indigestion, Loss of appetite, tastlesness', NULL, NULL, '500g, 50g', NULL, 5, 1, 'INJI CHURNAM', NULL, 'INJI CHURNAM', 1, 21, '2019-07-15 11:23:56', '2019-07-15 11:23:56'),
(579, 'KALARCHI CHURNAM', 'kalarchi-churnam', '', NULL, 12, 'SC10', 0, 0, '2', 'Caesalpiniabonduc, Piper nigrum', 'Scrotal swelling', NULL, NULL, '500g, 50g', NULL, 5, 1, 'KALARCHI CHURNAM', NULL, 'KALARCHI CHURNAM', 1, 33, '2019-07-15 11:26:38', '2019-07-15 11:26:38'),
(580, 'VENGARAPODI', 'vengarapodi', '', NULL, 13, 'SC12', 0, 0, '1', 'Boric acid', 'Itching ,Pun(For washing the ulcers)', NULL, NULL, NULL, NULL, 5, 1, 'VENGARAPODI', NULL, 'VENGARAPODI', 1, 16, '2019-07-15 11:28:24', '2019-07-15 11:28:24'),
(581, 'PARANGIPATTAI CHURNAM', 'parangipattai-churnam', '', NULL, 12, 'SC9', 0, 0, '2', 'Smilax china', 'Skin disoders', NULL, NULL, NULL, NULL, 5, 1, 'PARANGIPATTAI CHURNAM', NULL, 'PARANGIPATTAI CHURNAM', 1, 25, '2019-07-15 11:30:07', '2019-07-15 11:30:07'),
(582, 'THALEESATHI CHURNAM (SIDDHA)', 'thaleesathi-churnam-siddha', '', NULL, 12, 'SC5', 0, 0, '26', 'Taxusbaccata, Cinnamomumzeylanicum, Eletteriacardamomum , Pipernigrum, Zingiberofficinale, Glycyrrhiaglabra, Ferula foetida, Emblicaofficinalis, Sausurealappa, Piper longum, Nigella sativa, Anethumsowa, Trachyspermumammi Cuminumcyminum, Syzygiumaromaticum, Myristicafragrans, Rhus succedanea,\r\nMyristicafragrans, Terminaliabelerica, Terminaliachebula, Nardostachysjatamansi Cinnamomumverum, Micheliachampaca, Embeliaribes, Syzygiumaromaticum, Coriandrumsativum', 'cough, Kabha diseases', NULL, NULL, '50gm, 500gm', NULL, 5, 1, 'THALEESATHI CHURNAM (SIDDHA)', NULL, 'THALEESATHI CHURNAM (SIDDHA)', 1, 18, '2019-07-15 11:32:53', '2019-07-15 11:32:53'),
(583, 'THRIKADUGU CHURNAM  [E.D]', 'thrikadugu-churnam-ed', '', NULL, 12, 'SC12', 0, 0, '3', 'Zingiberofficinale, Piper nigrum,  Piper longum', 'respiratory disorders,dry cough,fever', NULL, NULL, NULL, NULL, 5, 1, 'THRIKADUGU CHURNAM  [E.D]', NULL, 'THRIKADUGU CHURNAM  [E.D]', 1, 20, '2019-07-15 11:34:52', '2019-07-15 11:34:52'),
(584, 'THRIPHALA CHURNAM   [E.D]', 'thriphala-churnam-ed', '', NULL, 12, 'SC11', 0, 0, '3', 'Terminaliachebula, Emblicaofficinalis, Terminaliabelerica', 'Constipation, cleaning of ulcers and wounds, peptic ulcers', NULL, NULL, NULL, NULL, 5, 1, 'THRIPHALA CHURNAM   [E.D]', NULL, 'THRIPHALA CHURNAM   [E.D]', 1, 23, '2019-07-15 11:36:26', '2019-07-15 11:36:26'),
(585, 'SIVATHAI CHURNAM', 'sivathai-churnam', '', NULL, 12, 'SC8', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 'SIVATHAI CHURNAM', NULL, 'SIVATHAI CHURNAM', 1, 22, '2019-07-15 11:39:17', '2019-07-15 11:39:17'),
(586, 'VARANADI KASHAYAM', 'varanadi-kashayam', '', NULL, 6, 'D27', 0, 0, '15', 'Crataeva religiosa, Strobilanthes ciliatus, Asparagus racemosus, Plumbago rosea, Semicarpus anacardium.', 'Headache, Carbuncles, Obesity, Anorexia, Rheumatic complaints. Effective in\r\nInflammation and Growths in visceral organs.', NULL, NULL, '200 ml.', NULL, 5, 1, 'VARANADI KASHAYAM', NULL, 'VARANADI KASHAYAM', 1, 5, '2019-08-13 10:38:16', '2019-08-13 12:21:42'),
(587, 'BHADRADARVADI KASHAYAM', 'bhadradarvadi-kashayam', '', NULL, 6, 'empty01', 0, 0, '11', 'Cedrus deodara, Indian valerian, Saussurea lappa, Dasamoolam, Sida cordifolia', 'Rheumatic diseases.', NULL, NULL, '200 ml.', NULL, 5, 1, 'BHADRADARVADI KASHAYAM', NULL, 'BHADRADARVADI KASHAYAM', 1, 9, '2019-08-13 10:43:58', '2019-08-13 11:13:06'),
(588, 'DASAMOOLAKATUTHRAYAM KASHAYACHOORNAM', 'dasamoolakatuthrayam-kashayachoornam', '', NULL, 24, 'K7', 0, 0, '14', 'Dasamoolam, Thrikatu, Adhatoda beddomei', 'Respiratory disorders, Rhinitis, Cold, Bodyache, Headache, Cough, Flu, Fever and Low back-ache.', NULL, NULL, '250g.,1 Kg.', NULL, 5, 1, 'DASAMOOLAKATUTHRAYAM KASHAYACHOORNAM', NULL, 'DASAMOOLAKATUTHRAYAM KASHAYACHOORNAM', 1, 8, '2019-08-13 11:45:03', '2019-08-13 12:52:04'),
(589, 'YAVA CHOORNAM', 'yava-choornam', '', NULL, 26, 'EMPTY 01', 0, 0, NULL, 'Hordeum vulgare Linn', 'Pramcha, Urusthambha, Improves lipid metabolism, Diseases of throat rhinitis,\r\nCough, Skin diseases, improves complexion.', NULL, '5-15 gm with suitable anupana or as directed by physician.', '50 g., 500 g.', NULL, 5, 1, 'YAVA CHOORNAM', NULL, 'YAVA CHOORNAM', 1, 19, '2019-08-13 14:43:18', '2019-08-13 14:43:18');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `additional_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`additional_image_id`, `product_id`, `image`, `sort_order`, `date_added`) VALUES
(1, 16, '5c7caef3f378b1551675123-tooth-powder.jpg', 1, '2019-03-04 05:52:44'),
(2, 16, '5c7caef40386e1551675124-oushadhi-baby-oil.jpg', 2, '2019-03-04 05:52:44'),
(3, 16, '5c7cb6234816b1551676963-oushadhi-facepack.jpg', 3, '2019-03-04 05:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `price_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_value` varchar(10) NOT NULL,
  `price` bigint(20) NOT NULL,
  `dealer_price` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_price`
--

INSERT INTO `product_price` (`price_id`, `product_id`, `unit_value`, `price`, `dealer_price`) VALUES
(2, 5, '450ml', 38, 38),
(3, 6, '10ml', 18, 18),
(4, 7, '450ml', 41, 41),
(5, 8, '450ml', 44, 44),
(6, 9, '450ml', 41, 41),
(7, 10, '450ml', 39, 39),
(8, 11, '450ml', 67, 67),
(9, 12, '450ml', 310, 310),
(10, 12, '200ml', 139, 139),
(11, 13, '450ml', 196, 196),
(12, 13, '200ml', 89, 89),
(16, 18, '450ml', 49, 49),
(17, 19, '450ml', 67, 67),
(18, 20, '450ml', 45, 45),
(19, 21, '450 ml', 38, 38),
(20, 22, '450ml', 52, 52),
(21, 23, '450ml', 40, 40),
(22, 24, '450ml', 55, 55),
(23, 25, '450ml', 48, 48),
(24, 26, '450ml', 57, 57),
(25, 27, '450ml', 43, 43),
(26, 28, '450ml', 62, 62),
(27, 29, '450ml', 37, 37),
(28, 30, '10ml', 18, 18),
(29, 31, '450 ml', 52, 52),
(31, 33, '450ml', 48, 48),
(32, 34, '450ml', 53, 53),
(33, 35, '450ml', 44, 44),
(34, 36, '450ml', 66, 66),
(35, 37, '450ml', 62, 62),
(36, 38, '450ml', 44, 44),
(37, 39, '450ml', 48, 48),
(38, 40, '450ml', 43, 43),
(39, 41, '450ml', 39, 39),
(40, 42, '450ml', 41, 41),
(41, 43, '450ml', 53, 53),
(42, 44, '10ml', 96, 96),
(43, 45, '450ml', 47, 47),
(44, 46, '450ml', 41, 41),
(45, 47, '450ml', 60, 60),
(46, 48, '450ml', 34, 34),
(47, 49, '450ml', 67, 67),
(48, 50, '50ml', 151, 151),
(49, 50, '10ml', 31, 31),
(50, 51, '450ml', 148, 148),
(51, 51, '200ml', 67, 67),
(52, 52, '200ml', 127, 127),
(53, 52, '50ml', 58, 58),
(59, 55, '450ml', 147, 147),
(60, 55, '200ml', 70, 70),
(61, 56, '450ml', 132, 132),
(62, 56, '200ml', 67, 67),
(63, 57, '200ml', 60, 60),
(64, 58, '450ml', 217, 217),
(65, 58, '200ml', 98, 98),
(66, 59, '450ml', 134, 134),
(67, 59, '200ml', 61, 61),
(68, 60, '450ml', 140, 140),
(69, 60, '200ml', 63, 63),
(70, 61, '450ml', 137, 137),
(71, 61, '200ml', 62, 62),
(72, 62, '450ml', 208, 208),
(73, 62, '200ml', 95, 95),
(74, 63, '10ml', 38, 38),
(75, 64, '450ml', 160, 160),
(76, 64, '200ml', 73, 73),
(77, 65, '450ml', 193, 193),
(78, 65, '200ml', 88, 88),
(79, 66, '450ml', 145, 145),
(80, 66, '200ml', 67, 67),
(81, 67, '450ml', 134, 134),
(82, 67, '200ml', 61, 61),
(83, 68, '10ml', 63, 63),
(84, 69, '10ml', 22, 22),
(85, 70, '10ml', 25, 25),
(86, 72, '10ml', 27, 27),
(87, 73, '10ml', 15, 15),
(88, 74, '450ml', 160, 160),
(89, 74, '200ml', 75, 75),
(90, 75, '450ml', 183, 183),
(91, 75, '200ml', 85, 85),
(92, 76, '450ml', 186, 186),
(93, 76, '200ml', 84, 84),
(96, 78, '450ml', 186, 186),
(97, 78, '200ml', 84, 84),
(98, 79, '450ml', 200, 200),
(99, 79, '200ml', 91, 91),
(100, 80, '10ml', 35, 35),
(101, 81, '450ml', 223, 223),
(102, 81, '50ml', 26, 26),
(103, 82, '450ml', 172, 172),
(104, 82, '200ml', 77, 77),
(105, 83, '500ml', 147, 147),
(106, 83, '50ml', 16, 16),
(107, 84, '500ml', 235, 235),
(108, 84, '50ml', 25, 25),
(109, 85, '450', 154, 154),
(110, 85, '200ml', 70, 70),
(111, 86, '50ml', 30, 30),
(112, 87, '450ml', 175, 175),
(113, 87, '200ml', 80, 80),
(114, 88, '450ml', 145, 145),
(115, 88, '200ml', 66, 66),
(116, 89, '450ml', 172, 172),
(117, 89, '200ml', 77, 77),
(122, 92, '450ml', 204, 204),
(123, 92, '50ml', 25, 25),
(124, 93, '450ml', 137, 137),
(125, 93, '200ml', 62, 62),
(126, 94, '450ml', 160, 160),
(127, 94, '200ml', 73, 73),
(128, 94, '50ml', 39, 39),
(129, 95, '450ml', 148, 148),
(130, 95, '200ml', 67, 67),
(131, 96, '450ml', 145, 145),
(132, 96, '200ml', 66, 66),
(133, 97, '200ml', 137, 137),
(134, 97, '50ml', 62, 62),
(135, 98, '10ml', 51, 51),
(136, 99, '10ml', 27, 27),
(137, 100, '10ml', 35, 35),
(138, 101, '10ml', 40, 40),
(139, 102, '10ml', 20, 20),
(140, 103, '450ml', 165, 165),
(141, 103, '200ml', 75, 75),
(142, 104, '450ml', 178, 178),
(143, 104, '200ml', 81, 81),
(146, 106, '450ml', 178, 178),
(147, 106, '200ml', 81, 81),
(148, 107, '450ml', 202, 202),
(149, 107, '200ml', 91, 91),
(150, 108, '450ml', 172, 172),
(151, 108, '200ml', 77, 77),
(152, 109, '450ml', 206, 206),
(153, 109, '200ml', 93, 93),
(154, 110, '450ml', 290, 290),
(155, 110, '200ml', 130, 130),
(156, 111, '500 gm', 95, 95),
(157, 111, '200 gm', 39, 39),
(160, 113, '400gm', 114, 114),
(163, 115, '400 gm', 85, 85),
(166, 117, '200 gm', 46, 46),
(167, 117, '500 gm', 107, 107),
(168, 118, '200 gm', 44, 44),
(169, 118, '500 gm', 107, 107),
(170, 119, '100 gm', 88, 88),
(171, 120, '450ml', 247, 247),
(172, 120, '200ml', 111, 111),
(175, 122, '450ml', 285, 285),
(176, 122, '200ml', 127, 127),
(177, 123, '200 gm', 70, 70),
(178, 123, '500 gm', 168, 168),
(179, 124, '50 gm', 48, 48),
(180, 125, '200 gm', 45, 45),
(181, 125, '500 gm', 109, 109),
(182, 126, '450ml', 148, 148),
(183, 126, '200ml', 67, 67),
(184, 127, '50 gm', 16, 16),
(185, 128, '200 gm', 43, 43),
(186, 128, '500 gm', 102, 102),
(187, 129, '450ml', 133, 133),
(188, 129, '200ml', 60, 60),
(189, 130, '200 gm', 36, 36),
(190, 130, '500 gm', 88, 88),
(191, 131, '20g', 57, 57),
(192, 131, '10g', 30, 30),
(193, 131, '5g', 15, 15),
(194, 132, '200gm', 46, 46),
(195, 132, '500 gm', 116, 116),
(196, 133, '450ml', 159, 159),
(197, 133, '200ml', 72, 72),
(198, 133, '50ml', 39, 39),
(201, 135, '200 gm', 41, 41),
(202, 135, '500 gm', 98, 98),
(203, 136, '450ml', 205, 205),
(204, 136, '200ml', 92, 92),
(205, 137, '200 gm', 55, 55),
(206, 138, '450ml', 162, 162),
(207, 138, '200ml', 74, 74),
(208, 139, '100g', 139, 139),
(209, 139, '50g', 71, 71),
(210, 140, '450ml', 247, 247),
(211, 140, '200ml', 111, 111),
(215, 143, '100 gm', 47, 47),
(216, 144, '200 gm', 53, 53),
(217, 144, '500 gm', 126, 126),
(218, 145, '450ml', 154, 154),
(219, 145, '200ml', 70, 70),
(220, 146, '200gm', 48, 48),
(221, 146, '500gm', 199, 199),
(222, 147, '25g', 95, 95),
(223, 147, '10g', 39, 39),
(224, 147, '5g', 20, 20),
(225, 148, '200 gm', 57, 57),
(226, 149, '450ml', 217, 217),
(227, 149, '200ml', 98, 98),
(228, 149, '50ml', 51, 51),
(229, 150, '450ml', 201, 201),
(230, 150, '200ml', 91, 91),
(231, 151, '10g', 48, 48),
(232, 151, '5g', 25, 25),
(233, 152, '200 gm', 71, 71),
(234, 152, '500 gm', 168, 168),
(235, 153, '450ml', 148, 148),
(236, 153, '200ml', 67, 67),
(239, 155, '450ml', 205, 205),
(240, 155, '200ml', 92, 92),
(241, 156, '25g', 126, 126),
(242, 156, '10g', 52, 52),
(243, 156, '5g', 27, 27),
(244, 157, '450ml', 174, 174),
(245, 157, '200ml', 79, 79),
(246, 158, '450ml', 137, 137),
(247, 158, '200ml', 62, 62),
(248, 159, '20g', 154, 154),
(249, 159, '10g', 81, 81),
(250, 160, '450ml', 146, 146),
(251, 160, '200ml', 67, 67),
(252, 161, '100g', 43, 43),
(253, 161, '50g', 23, 23),
(254, 162, '450ml', 155, 155),
(255, 162, '200ml', 71, 71),
(256, 163, '450ml', 174, 174),
(257, 163, '200ml', 79, 79),
(258, 164, '20', 462, 462),
(259, 164, '10', 238, 238),
(260, 164, '5', 123, 123),
(261, 165, '100 nos', 146, 146),
(262, 166, '450ml', 286, 286),
(263, 166, '200ml', 129, 129),
(264, 167, '450ml', 139, 139),
(265, 167, '200ml', 63, 63),
(266, 168, '500g', 93, 93),
(267, 168, '50g', 21, 21),
(268, 169, '450ml', 176, 176),
(269, 169, '200ml', 80, 80),
(270, 170, '100 nos', 413, 413),
(271, 171, '500g', 98, 98),
(272, 171, '250g', 11, 11),
(273, 172, '500g', 333, 333),
(274, 172, '250g', 35, 35),
(275, 173, '450ml', 168, 168),
(276, 173, '200ml', 76, 76),
(279, 175, '450ml', 119, 119),
(280, 175, '200ml', 55, 55),
(281, 176, '500g', 175, 175),
(282, 176, '250g', 18, 18),
(283, 177, '500g', 161, 161),
(284, 177, '250g', 17, 17),
(285, 178, '450ml', 161, 161),
(286, 178, '200ml', 74, 74),
(287, 179, '450ml', 141, 141),
(288, 179, '200ml', 64, 64),
(289, 180, '450ml', 172, 172),
(290, 180, '200ml', 77, 77),
(291, 181, '450ml', 202, 202),
(292, 181, '200ml', 97, 97),
(295, 183, '500g', 168, 168),
(296, 183, '250g', 18, 18),
(297, 184, '450ml', 172, 172),
(298, 184, '200ml', 77, 77),
(299, 185, '450ml', 155, 155),
(300, 185, '200ml', 71, 71),
(301, 186, '450ml', 160, 160),
(302, 186, '200ml', 73, 73),
(303, 187, '450ml', 139, 139),
(304, 187, '200ml', 63, 63),
(305, 188, '250g', 18, 18),
(306, 189, '250g', 8, 8),
(307, 189, '200g', 2, 2),
(308, 190, '450ml', 128, 128),
(309, 190, '200ml', 58, 58),
(310, 191, '450ml', 157, 157),
(311, 191, '200ml', 71, 71),
(312, 192, '450ml', 231, 231),
(313, 192, '200ml', 106, 106),
(314, 193, '500g', 259, 259),
(315, 193, '250g', 27, 27),
(316, 193, '200g', 6, 6),
(317, 194, '450ml', 235, 235),
(318, 194, '200ml', 106, 106),
(319, 195, '450ml', 231, 231),
(320, 195, '200ml', 105, 105),
(321, 196, '450ml', 256, 256),
(322, 196, '200ml', 116, 116),
(323, 197, '500g', 266, 266),
(324, 197, '250g', 27, 27),
(325, 198, '450ml', 231, 231),
(326, 198, '200ml', 104, 104),
(327, 199, '450ml', 210, 210),
(328, 199, '200ml', 95, 95),
(329, 200, '450ml', 315, 315),
(330, 200, '200ml', 142, 142),
(331, 201, '500g', 217, 217),
(332, 201, '250g', 22, 22),
(333, 201, '200g', 6, 6),
(334, 202, '450ml', 230, 230),
(335, 202, '200ml', 104, 104),
(336, 203, '450ml', 231, 231),
(337, 203, '200ml', 105, 105),
(338, 204, '450ml', 1036, 1036),
(339, 204, '200ml', 466, 466),
(340, 205, '450ml', 231, 231),
(341, 205, '200ml', 105, 105),
(342, 206, '450ml', 260, 260),
(343, 206, '200ml', 117, 117),
(344, 207, '500g', 216, 216),
(345, 207, '250g', 22, 22),
(346, 208, '100 gm', 32, 32),
(347, 209, '500g', 1505, 1505),
(348, 209, '250g', 154, 154),
(349, 210, '200 gm', 58, 58),
(350, 210, '500 gm', 144, 144),
(351, 211, '200 gm', 50, 50),
(352, 211, '500 gm', 121, 121),
(353, 212, '500g', 454, 454),
(354, 212, '250g', 49, 49),
(355, 213, '100 gm', 15, 15),
(356, 214, '200 gm', 63, 63),
(357, 214, '500 gm', 140, 140),
(358, 215, '500g', 210, 210),
(359, 215, '250g', 22, 22),
(360, 216, '200 gm', 175, 175),
(361, 216, '500 gm', 420, 420),
(362, 217, '500g', 285, 285),
(363, 217, '250g', 31, 31),
(364, 218, '500g', 188, 188),
(365, 218, '250g', 20, 20),
(366, 219, '50 gm', 9, 9),
(367, 219, '500 gm', 88, 88),
(368, 220, '50 gm', 9, 9),
(369, 220, '500 gm', 84, 84),
(370, 221, '500g', 378, 378),
(371, 221, '250g', 39, 39),
(372, 222, '50 gm', 12, 12),
(373, 222, '500 gm', 126, 126),
(374, 223, '500g', 175, 175),
(375, 223, '250g', 18, 18),
(376, 224, '50 gm', 11, 11),
(377, 224, '500 gm', 99, 99),
(378, 225, '500g', 161, 161),
(379, 225, '250g', 17, 17),
(380, 226, '50 gm', 9, 9),
(381, 226, '500 gm', 84, 84),
(382, 227, '500g', 315, 315),
(383, 227, '250g', 32, 32),
(386, 229, '50 gm', 11, 11),
(387, 229, '500 gm', 105, 105),
(388, 230, '50 gm', 12, 12),
(389, 230, '500 gm', 112, 112),
(390, 231, '50 gm', 11, 11),
(391, 231, '500 gm', 105, 105),
(392, 232, '500g', 231, 231),
(393, 232, '50g', 24, 24),
(396, 234, '200g', 81, 81),
(399, 236, '500g', 441, 441),
(400, 236, '50g', 45, 45),
(401, 237, '50g', 28, 28),
(402, 238, '200 ml', 113, 113),
(403, 238, '450 ml', 252, 252),
(404, 239, '200 ml', 139, 139),
(405, 239, '450 ml', 309, 309),
(406, 240, '200 ml', 133, 133),
(407, 240, '450 ml', 288, 288),
(408, 241, '200 ml', 105, 105),
(409, 241, '450 ml', 231, 231),
(411, 243, '200 ml', 125, 125),
(412, 243, '450 ml', 275, 275),
(413, 244, '200 ml', 125, 125),
(414, 244, '450 ml', 277, 277),
(415, 245, '500g', 119, 119),
(416, 245, '50g', 13, 13),
(417, 246, '200 ml', 259, 259),
(418, 246, '450 ml', 578, 578),
(419, 247, '500g', 294, 294),
(420, 247, '50g', 30, 30),
(421, 248, '200g', 49, 49),
(422, 248, '50g', 13, 13),
(423, 249, '200g', 53, 53),
(424, 250, '500g', 133, 133),
(425, 250, '50g', 14, 14),
(426, 251, '500g', 189, 189),
(427, 251, '50g', 20, 20),
(428, 252, '500g', 154, 154),
(429, 252, '50g', 16, 16),
(430, 253, '500g', 84, 84),
(431, 253, '50g', 7, 7),
(432, 254, '200g', 104, 104),
(433, 254, '50g', 27, 27),
(434, 255, '500g', 161, 161),
(435, 255, '50g', 15, 15),
(436, 256, '50g', 36, 36),
(437, 257, '500g', 203, 203),
(438, 257, '50g', 21, 21),
(439, 258, '500g', 203, 203),
(440, 258, '50g', 21, 21),
(441, 259, '500g', 245, 245),
(442, 260, '50g', 25, 25),
(443, 261, '500g', 609, 609),
(444, 261, '50g', 63, 63),
(445, 262, '500g', 147, 147),
(446, 262, '50g', 15, 15),
(447, 263, '500g', 116, 116),
(448, 263, '50g', 14, 14),
(449, 264, '500g', 91, 91),
(450, 264, '50g', 10, 10),
(451, 265, '500g', 273, 273),
(452, 265, '50g', 28, 28),
(453, 266, '500g', 252, 252),
(454, 266, '50g', 26, 26),
(455, 267, '500g', 245, 245),
(456, 267, '50g', 25, 25),
(457, 268, '200ml', 210, 210),
(458, 269, '450ml', 224, 224),
(459, 269, '200ml', 102, 102),
(460, 270, '450ml', 231, 231),
(461, 270, '200ml', 105, 105),
(462, 271, '450ml', 256, 256),
(463, 271, '200ml', 115, 115),
(464, 272, '450ml', 194, 194),
(465, 272, '200ml', 88, 88),
(466, 267, '10g', 6, 6),
(467, 274, '500g', 469, 469),
(468, 274, '50g', 49, 49),
(469, 275, '500g', 539, 539),
(470, 275, '50g', 56, 56),
(471, 276, '450ml', 263, 263),
(472, 276, '200ml', 119, 119),
(473, 277, '500g', 315, 315),
(474, 277, '50g', 34, 34),
(475, 278, '450ml', 228, 228),
(476, 278, '200ml', 103, 103),
(477, 279, '450ml', 206, 206),
(478, 279, '200ml', 92, 92),
(480, 281, '50g', 43, 43),
(481, 282, '500g', 312, 312),
(482, 282, '50g', 32, 32),
(483, 283, '450ml', 301, 301),
(484, 283, '200ml', 135, 135),
(485, 284, '500g', 210, 210),
(486, 284, '50g', 22, 22),
(487, 285, '450ml', 249, 249),
(488, 285, '200ml', 113, 113),
(489, 286, '500g', 210, 210),
(490, 286, '50g', 22, 22),
(491, 287, '450ml', 210, 210),
(492, 287, '200ml', 95, 95),
(493, 288, '500g', 189, 189),
(494, 288, '250g', 20, 20),
(495, 289, '450ml', 174, 174),
(496, 289, '200ml', 78, 78),
(497, 290, '500g', 139, 139),
(498, 290, '250g', 15, 15),
(501, 292, '500g', 217, 217),
(502, 292, '250g', 22, 22),
(503, 292, '10g', 6, 6),
(504, 293, '450ml', 341, 341),
(505, 293, '200ml', 154, 154),
(506, 294, '450ml', 240, 240),
(507, 294, '200ml', 109, 109),
(508, 295, '500g', 417, 417),
(509, 295, '250g', 43, 43),
(512, 297, '500g', 259, 259),
(513, 297, '250g', 27, 27),
(514, 298, '500g', 126, 126),
(515, 298, '250g', 13, 13),
(516, 299, '500g', 175, 175),
(517, 299, '250g', 18, 18),
(518, 300, '500g', 210, 210),
(519, 300, '250g', 22, 22),
(520, 301, '500g', 203, 203),
(521, 301, '250g', 21, 21),
(522, 302, '500g', 298, 298),
(523, 302, '250g', 31, 31),
(524, 303, '500g', 168, 168),
(525, 303, '250g', 18, 18),
(526, 304, '500g', 210, 210),
(527, 304, '250g', 22, 22),
(528, 305, '500g', 294, 294),
(529, 305, '250g', 30, 30),
(530, 306, '100 ml', 34, 34),
(531, 307, '25 gm', 20, 20),
(532, 308, '100 nos.', 116, 116),
(535, 311, '100 nos.', 158, 158),
(536, 312, '50 gm', 13, 13),
(537, 313, '100 nos.', 83, 83),
(538, 314, '100 nos.', 350, 350),
(539, 315, '100 nos.', 83, 83),
(540, 316, '100 nos.', 118, 118),
(541, 317, '100 nos.', 76, 76),
(542, 318, '100 nos.', 105, 105),
(543, 319, '100 nos.', 525, 525),
(544, 320, '100 nos.', 151, 151),
(545, 321, '100 nos.', 214, 214),
(546, 322, '100 nos.', 161, 161),
(547, 323, '100 nos.', 175, 175),
(548, 324, '100 nos.', 210, 210),
(549, 325, '100 nos', 70, 70),
(550, 326, '100 nos.', 357, 357),
(551, 327, '100 nos.', 115, 115),
(552, 328, '100 nos.', 98, 98),
(553, 329, '100 nos.', 525, 525),
(554, 329, '10 nos.', 60, 60),
(555, 330, '100 nos.', 182, 182),
(556, 331, '10 gm', 19, 19),
(557, 332, '100 nos.', 175, 175),
(559, 334, '100 nos.', 81, 81),
(560, 335, '100 nos.', 196, 196),
(561, 336, '100 ml', 67, 67),
(562, 337, '100 nos.', 270, 270),
(563, 338, '100 nos.', 330, 330),
(564, 339, '25 gm', 16, 16),
(565, 340, '100 nos.', 112, 112),
(566, 341, '100 nos.', 119, 119),
(567, 342, '100 nos.', 221, 221),
(568, 343, '10 nos.', 57, 57),
(569, 344, '50 gm', 20, 20),
(571, 346, '100 ml', 39, 39),
(572, 347, '100 nos.', 86, 86),
(573, 348, '100 nos.', 89, 89),
(574, 349, '100 nos.', 90, 90),
(575, 350, '100 nos.', 96, 96),
(576, 351, '100 nos.', 203, 203),
(577, 352, '100 nos.', 86, 86),
(578, 353, '10 gm', 19, 19),
(579, 354, '100 nos.', 175, 175),
(580, 355, '100 nos.', 160, 160),
(582, 357, '100 nos.', 588, 588),
(583, 358, '100 gm', 67, 67),
(584, 359, '100 nos.', 131, 131),
(585, 360, '100 nos', 175, 175),
(588, 362, '50 gm', 31, 31),
(589, 363, '200 ml', 53, 53),
(592, 366, '100 ml', 55, 0),
(594, 368, '400 gm', 99, 99),
(595, 369, '200 ml.', 48, 48),
(596, 370, '100 nos', 165, 165),
(597, 371, '200 ml', 50, 50),
(600, 374, '100 nos', 87, 87),
(601, 375, '60 nos', 266, 266),
(602, 376, '200 ml', 74, 74),
(603, 377, '100 nos', 147, 147),
(604, 378, '100 ml', 46, 46),
(605, 379, '100 nos', 112, 112),
(606, 380, '200 ml', 63, 63),
(608, 382, '50 gm', 67, 67),
(609, 383, '200 ml', 72, 72),
(610, 384, '200 ml', 71, 71),
(611, 385, '200 ml', 70, 70),
(612, 386, '200ml', 73, 73),
(613, 387, '200 ml.', 59, 59),
(614, 388, '200 ml', 53, 53),
(615, 389, '200 ml', 94, 94),
(616, 390, '200ml', 77, 77),
(617, 391, '200 ml', 55, 55),
(618, 392, '200 ml', 49, 49),
(619, 393, '200 ml', 55, 55),
(621, 395, '200 ml', 80, 80),
(622, 396, '200 ml', 90, 90),
(623, 397, '200 ml', 93, 93),
(624, 398, '200 ml', 76, 76),
(625, 399, '200 ml', 59, 59),
(626, 400, '200 ml', 69, 69),
(627, 401, '1kg', 138, 138),
(628, 401, '500g', 70, 70),
(629, 401, '250g', 36, 36),
(630, 402, '500g', 71, 71),
(631, 403, '1kg', 143, 143),
(632, 403, '500g', 72, 72),
(633, 403, '250g', 37, 37),
(637, 405, '200 ml', 73, 73),
(638, 406, '200 ml', 49, 49),
(639, 407, '1kg', 216, 216),
(640, 407, '500g', 109, 109),
(641, 407, '250g', 55, 55),
(642, 408, '200 ml', 53, 53),
(644, 410, '1kg', 160, 160),
(645, 410, '500g', 81, 81),
(646, 410, '250g', 41, 41),
(647, 411, '200 ml', 55, 55),
(648, 412, '1kg', 146, 146),
(649, 412, '500g', 75, 75),
(650, 412, '250g', 39, 39),
(651, 413, '200 ml', 123, 123),
(652, 414, '1kg', 476, 476),
(653, 414, '500g', 239, 239),
(654, 414, '250g', 120, 120),
(655, 415, '200 ml', 67, 67),
(656, 416, '1kg', 308, 308),
(657, 416, '500', 155, 155),
(658, 416, '250', 78, 78),
(659, 417, '200 ml', 59, 59),
(660, 418, '200 ml', 57, 57),
(661, 419, '1kg', 143, 143),
(662, 419, '500g', 72, 72),
(663, 419, '250g', 37, 37),
(664, 420, '200 ml', 52, 52),
(665, 421, '200 ml', 53, 53),
(666, 422, '200 ml', 56, 56),
(667, 423, '200 ml', 94, 94),
(668, 424, '200 ml', 104, 104),
(669, 425, '200 ml', 52, 52),
(670, 426, '1kg', 224, 224),
(671, 426, '500g', 113, 113),
(672, 426, '100g', 20, 20),
(673, 427, '200 ml', 62, 62),
(674, 428, '500g', 105, 105),
(675, 429, '200 ml', 62, 62),
(676, 430, '200 ml', 92, 92),
(677, 431, '200 ml', 54, 54),
(678, 432, '1kg', 129, 129),
(679, 432, '500g', 65, 65),
(680, 432, '250g', 34, 34),
(681, 433, '200 ml', 50, 50),
(682, 434, '200 ml', 50, 50),
(683, 435, '200 ml', 59, 59),
(684, 436, '500g', 68, 68),
(685, 437, '200 ml', 64, 64),
(692, 440, '1kg', 204, 204),
(693, 440, '500g', 103, 103),
(694, 440, '250g', 53, 53),
(696, 442, '1kg', 395, 395),
(697, 442, '500g', 197, 197),
(698, 442, '250g', 21, 21),
(699, 443, '1kg', 129, 129),
(700, 443, '500g', 65, 65),
(701, 443, '250g', 34, 34),
(702, 444, '1kg', 162, 162),
(703, 444, '500g', 82, 82),
(704, 444, '250g', 42, 42),
(705, 445, '1kg', 134, 134),
(706, 445, '500g', 68, 68),
(707, 445, '250g', 35, 35),
(708, 446, '1kg', 197, 197),
(709, 446, '500g', 99, 99),
(710, 446, '50g', 11, 11),
(714, 448, '1kg', 190, 190),
(715, 448, '500g', 96, 96),
(716, 448, '250g', 49, 49),
(720, 450, '500g', 70, 70),
(721, 451, '1kg', 171, 171),
(722, 451, '500g', 86, 86),
(723, 451, '250g', 44, 44),
(724, 452, '1kg', 151, 151),
(725, 452, '500g', 76, 76),
(726, 452, '250g', 39, 39),
(727, 453, 'ing', 199, 199),
(728, 453, '500g', 100, 100),
(729, 453, '250g', 51, 51),
(733, 455, '1kg', 105, 105),
(734, 455, '500g', 53, 53),
(738, 457, '1kg', 258, 258),
(739, 457, '500g', 130, 130),
(740, 457, '250g', 66, 66),
(744, 459, '1kg', 143, 143),
(745, 459, '500g', 72, 72),
(746, 459, '250g', 37, 37),
(747, 460, '500g', 125, 125),
(748, 461, '1kg', 134, 134),
(749, 461, '500g', 68, 68),
(750, 461, '250g', 35, 35),
(751, 462, '1kg', 720, 720),
(752, 462, '500g', 361, 361),
(753, 462, '250g', 181, 181),
(754, 463, '1kg', 273, 273),
(755, 463, '500g', 137, 137),
(756, 463, '100g', 28, 28),
(757, 464, '1kg', 141, 141),
(758, 464, '500g', 72, 72),
(759, 464, '250g', 37, 37),
(760, 465, '1kg', 218, 218),
(761, 465, '500g', 109, 109),
(762, 465, '100g', 22, 22),
(763, 567, '100 g', 106, 106),
(764, 567, '250 g', 259, 259),
(765, 567, '500 g', 504, 504),
(766, 567, '2 kg', 1995, 1995),
(767, 568, '500 g', 343, 343),
(768, 568, '250 g', 175, 175),
(769, 568, '100 g', 72, 72),
(770, 568, '2 kg', 1341, 1341),
(771, 569, '500 g', 219, 219),
(772, 569, '250 g', 114, 114),
(773, 569, '100 g', 48, 48),
(774, 569, '2 kg', 844, 844),
(775, 570, '500 g', 259, 259),
(776, 570, '250 g', 134, 134),
(777, 570, '100 g', 58, 58),
(778, 570, '2.5 kg', 1007, 1007),
(779, 571, '500 g', 289, 289),
(780, 571, '250 g', 149, 149),
(781, 571, '100 g', 62, 62),
(782, 571, '2 kg', 1124, 1124),
(783, 572, '2 kg', 1047, 1047),
(784, 572, '500 g', 269, 269),
(785, 572, '250 g', 139, 139),
(786, 572, '100 g', 57, 57),
(787, 573, '500 g', 353, 353),
(788, 573, '50 g', 38, 38),
(789, 574, '500 g', 246, 246),
(790, 574, '50 g', 28, 28),
(791, 575, '500 g', 537, 537),
(792, 575, '50 g', 56, 56),
(793, 576, '500 g', 386, 386),
(794, 576, '50 g', 42, 42),
(795, 577, '500 g', 191, 191),
(796, 577, '50 g', 22, 22),
(797, 578, '500 g', 392, 392),
(798, 578, '50 g', 41, 41),
(799, 579, '500 g', 712, 712),
(800, 579, '50 g', 74, 74),
(801, 580, '50 g', 14, 14),
(802, 581, '500 g', 737, 737),
(803, 581, '50 g', 77, 77),
(804, 582, '500 g', 328, 328),
(805, 582, '50 g', 36, 36),
(806, 583, '500 g', 542, 542),
(807, 583, '50 g', 57, 57),
(808, 584, '500 g', 157, 157),
(809, 584, '50 g', 19, 19),
(810, 585, '500 g', 449, 449),
(811, 585, '50 g', 48, 48),
(812, 358, '250g', 161, 161),
(813, 336, '200 ml', 126, 126),
(814, 529, '200 ml', 53, 53),
(815, 586, '200 ml', 50, 50),
(816, 587, '200 ml', 0, 0),
(817, 540, '50gm', 11, 11),
(818, 540, '50gm', 99, 99),
(819, 516, '1kg', 115, 115),
(820, 516, '500g', 58, 58),
(821, 516, '250g', 30, 30),
(822, 588, '1kg', 160, 160),
(823, 588, '500g', 81, 81),
(824, 588, '250g', 41, 41),
(825, 518, '1kg', 171, 171),
(826, 518, '500 g', 86, 86),
(827, 518, '250 g', 44, 44),
(828, 520, '1kg', 203, 203),
(829, 520, '500g', 103, 103),
(830, 520, '250g', 53, 53),
(831, 521, '1kg', 118, 118),
(832, 521, '500g', 60, 60),
(833, 521, '250g', 31, 31),
(834, 522, '1kg', 437, 437),
(835, 522, '500g', 219, 219),
(836, 522, '250g', 111, 111),
(837, 523, '1kg', 367, 367),
(838, 523, '500g', 184, 184),
(839, 523, '250g', 93, 93),
(840, 525, '500g', 79, 79),
(841, 527, '1kg', 168, 168),
(842, 527, '500g', 85, 85),
(843, 527, '250g', 43, 43),
(844, 543, '500g', 238, 238),
(845, 543, '50g', 25, 25),
(846, 555, '500g', 133, 133),
(847, 555, '250g', 14, 14),
(848, 469, '450ml', 177, 177),
(849, 469, '200ml', 186, 186),
(850, 469, '50ml', 84, 84),
(851, 468, '450ml', 154, 154),
(852, 468, '200ml', 81, 81),
(853, 473, '450ml', 186, 186),
(854, 473, '200ml', 84, 84);

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE `product_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) DEFAULT NULL,
  `text` text,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotations`
--

CREATE TABLE `quotations` (
  `quotation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_original` double NOT NULL,
  `total` double NOT NULL,
  `status` tinyint(1) NOT NULL,
  `quotation_status` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotations_copy`
--

CREATE TABLE `quotations_copy` (
  `quotation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_original` double NOT NULL,
  `total` double NOT NULL,
  `status` tinyint(1) NOT NULL,
  `quotation_status` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE `quotation_details` (
  `details_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `unit_value` varchar(50) DEFAULT NULL,
  `qty_original` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `uprice` double NOT NULL,
  `utax` double NOT NULL,
  `price` double NOT NULL,
  `total_uprice` double NOT NULL,
  `total_utax` double NOT NULL,
  `total_original` double NOT NULL,
  `total` double NOT NULL,
  `admin_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details_copy`
--

CREATE TABLE `quotation_details_copy` (
  `details_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `unit_value` varchar(50) DEFAULT NULL,
  `qty_original` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `uprice` double NOT NULL,
  `utax` double NOT NULL,
  `price` double NOT NULL,
  `total_uprice` double NOT NULL,
  `total_utax` double NOT NULL,
  `total_original` double NOT NULL,
  `total` double NOT NULL,
  `admin_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_history`
--

CREATE TABLE `quotation_history` (
  `quotation_history_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `comment` text,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_status`
--

CREATE TABLE `quotation_status` (
  `status_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `color_code` varchar(9) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation_status`
--

INSERT INTO `quotation_status` (`status_id`, `name`, `color_code`, `status`) VALUES
(1, 'Pending', '#00c0ef', 1),
(2, 'Reviewed by Admin', '#3c8dbc', 1),
(3, 'Processing', '#f39c12', 1),
(4, 'Delivered', '#00a65a', 1),
(5, 'Cancelled', '#dd4b39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `raw_materials`
--

CREATE TABLE `raw_materials` (
  `raw_material_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_en` varchar(500) DEFAULT NULL,
  `title_hn` varchar(500) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `size` varchar(100) DEFAULT NULL,
  `botanical_name` varchar(100) DEFAULT NULL,
  `quantity` varchar(15) DEFAULT NULL,
  `raw_type` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raw_materials`
--

INSERT INTO `raw_materials` (`raw_material_id`, `title`, `title_en`, `title_hn`, `image`, `size`, `botanical_name`, `quantity`, `raw_type`, `status`, `date_added`, `date_modified`) VALUES
(1, 'ഉഴിഞ്ഞ സമൂലം (ഉണക്ക)', 'Ballon Vine, Heart’s pea (Dried)', 'Kanphuti,Kapalphuti', '5d4bf0672f4a11565257831.jpg', NULL, 'Cardiospermum  halicacabum', '150', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:06:02'),
(2, 'ഉഴിഞ്ഞ സമൂലം (പച്ച)', 'Ballon Vine, Heart’s pea (Fresh)', 'Kanphuti,Kapalphuti', '5d8071de48d841568698846.jpg', NULL, 'Cardiospermum  halicacabum', '1200', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:10:46'),
(3, 'എരുമക്കള്ളി സമൂലം (പച്ച)', 'Mexican poppy, Prickly pappy (Fresh)', NULL, '5d8071ec46bd71568698860.jpg', NULL, 'Agremone  mexicana', '50', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:11:00'),
(4, 'എലിച്ചെവിയന്‍ സമൂലം (ഉണക്ക)', '(dried)', NULL, '5d4401b2a64991564737970.jpg', NULL, 'Emilia  sonchifolia', '125', 1, 1, '2019-06-12 11:23:49', '2019-08-02 14:56:10'),
(5, 'കണ്ടകാരി സമൂലം (ഉണക്ക)', '(Dried)', '‎Choti Kateri, Ringni', '5d44026f0bb3c1564738159.jpg', NULL, 'Solanum  surattense', '1800', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:14:39'),
(6, 'കരിം കുറിഞ്ഞി സമൂലം (പച്ച)Wood size up to 1\"(1 Bundle below 50 Kg.)', '(Fresh)', 'Karvi', '5d440352ad4501564738386.jpg', NULL, 'Strobilanthes  ciliatus', '24000', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:15:54'),
(7, 'കരിന്തുമ്പ സമൂലം  (പച്ച)', 'Malabar catmint (Fresh)', 'Kala bhangra, Gobara', '5d8072b0273651568699056.jpg', NULL, 'Anisomeles  malabarica', '5', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:14:16'),
(8, 'കരിന്തുമ്പ സമൂലം (ഉണക്ക)', 'Malabar catmint (dried)', 'Kala bhangra, codhara', '5d4566dc785ea1564829404.jpg', NULL, 'Anisomeles  malabarica', '125', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:16:58'),
(9, 'കാട്ടുപടവലം സമൂലം ഉണക്ക', 'Wild snake gourd', 'Jangli padvel', '5d4566c59f4e41564829381.jpg', NULL, 'Trichosanthes lobata', '11000', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:17:14'),
(10, 'കിരിയാത്ത് സമൂലം  (ഉണക്ക)', 'Green chiretta, creat (dried)', 'kalamegh, kalpaaath', '5d4403a7796341564738471.jpg', NULL, 'Andrographis paniculata/swertia Chirata', '3500', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:18:00'),
(11, 'കീഴാര്‍നെല്ലി സമൂലം  (ഉണക്ക)', '(Dried)', 'Jangli amala', '5d4403d19af531564738513.jpg', NULL, 'Phyllanthus  niruri', '800', 1, 1, '2019-06-12 11:23:49', '2019-08-26 11:18:12'),
(12, 'കീഴാര്‍നെല്ലി സമൂലം  (പച്ച)', '(Fresh)', 'Jar Amla', '5d4403e28d5a61564738530.jpg', NULL, 'Phyllanthus  niruri', '2800', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:05:32'),
(13, 'കുഴിമുണ്ടന്‍ സമൂലം (പച്ച)', '(Fresh)', 'Pratanika  sampoorn poudha thaza', '5d4bf1c403a831565258180.jpg', NULL, 'Orthosiphon glabratus', '30', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:06:27'),
(14, 'കൊഴുപ്പ സമൂലം(ഉണക്ക)', 'Common  Purslane', 'Luniya sampoorn poudha sookha', '5d80737ddf05c1568699261.jpg', NULL, 'Portulaca oleracea', '20', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:17:41'),
(15, 'ചതുര മുല്ല സമൂലം (പച്ച)', NULL, 'Chamely sampoorn poudha thaza', '5d4bf1d1d7acf1565258193.jpg', NULL, 'myxopyrum  smilacifolium', '10', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:06:53'),
(16, 'ചെറുകടലാടി സമൂലം  (ഉണക്ക)', 'Small prickly chaff-flower plant (dried)', 'lal chirchita', '5d4405632306a1564738915.jpg', NULL, 'Cyathula  prostrata', '75', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:07:08'),
(17, 'ചെറുകടലാടി സമൂലം  (പച്ച)', 'Small prickly chaff-flower plant (Fresh)', 'lal chirchita', '5d44058f755a51564738959.jpg', NULL, 'Cyathula  prostrata', '2200', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:09:10'),
(18, 'ചെറുചീര സമൂലം (പച്ച)', 'White goosefoot, Lamp’s queeterts (Fresh)', 'bathua or bathuwa', '5d456736431e31564829494.jpg', NULL, 'Chenopodium  album', '3', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:09:36'),
(19, 'ചെറൂള സമൂലം  (പച്ച)', '(Fresh)', 'Gorakhbuti or Kapuri jadi', '5d4405f4308a21564739060.jpeg', NULL, 'Aerva  lanata', '2200', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:12:07'),
(20, 'ഞൊട്ടാഞൊടിയന്‍ സമൂലം  (പച്ച)', 'Country  gooseberry  (Fresh)', 'Rasbhari, Ban Tipariya, Chirpati', '5d44061437c251564739092.jpg', NULL, 'Physalis  minima', '2100', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:13:19'),
(21, 'തഴുതാമ സമൂലം  (പച്ച)', 'Hogweed, Pig weed (Fresh)', 'Lal Punarnava, Santh', '5d44062c4c5071564739116.jpg', NULL, 'Boerrhavia  diffusa', '1900', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:15:23'),
(22, 'താര്‍താവല്‍ സമൂലം  (ഉണക്ക)', '(Dried)', 'Madanaghanti sampoorn poudha sookha', '5d4bf2f5cdeaf1565258485.jpg', NULL, 'Spermacoce  hispida', '100', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:17:02'),
(23, 'താര്‍താവല്‍ സമൂലം  (പച്ച)', '(Fresh)', 'Madanaghanti sampoorn poudha thaza', '5d44068015a731564739200.jpg', NULL, 'Spermacoce  hispida', '15000', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:17:22'),
(24, 'നിലമ്പരണ്ട സമൂലം   (ഉണക്ക)', '(Dried)', 'kuddalia', '5d4406f9cd4ac1564739321.jpg', NULL, 'Desmodium  triflorum', '500', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:17:35'),
(25, 'നിലമ്പരണ്ട സമൂലം (പച്ച)', '(Fresh)', 'kuddalia', '5d440738b49231564739384.jpg', NULL, 'Desmodium  triflorum', '10', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:20:42'),
(26, 'നീല ഉമ്മത്ത് സമൂലം (പച്ച)', 'Thorn apple (Fresh)', 'Dhatura', '5d4407631a79a1564739427.jpg', NULL, 'Datura  stramonium', '350', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:20:58'),
(27, 'പര്‍പ്പടകപ്പുല്ല് സമൂലം (ഉണക്ക)', '(Dried)', 'Daman pappar, Pitpapra', '5d4407917e28c1564739473.jpg', NULL, 'Hedyotis  corymbosa', '3500', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:21:26'),
(28, 'പ്രസാരണി സമൂലം (ഉണക്ക)', '(Dried)', 'Jhamar bel sampoorn poudha sookha', '5d4407e3062281564739555.png', NULL, 'Merremia tridentata', '500', 1, 1, '2019-06-12 11:23:49', '2019-08-26 12:21:47'),
(29, 'പ്രസാരണി സമൂലം (പച്ച)', '(Fresh)', 'Jhamar bel sampoorn poudha thaza', '5d44082d8d7e11564739629.jpg', NULL, 'Merremia tridentata', '4000', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:51:36'),
(30, 'പുലിച്ചുവടി സമൂലം (ഉണക്ക)', 'Cupids  flower  (Dried)', 'Panchpatia', '5d440859196971564739673.jpg', NULL, 'Ipomoea pestigrids', '150', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:51:49'),
(31, 'പുളിയാറല്‍ സമൂലം (പച്ച)', 'Indian sorrel (Fresh)', 'Tinputiy, Amrul', '5d4408ab277931564739755.jpg', NULL, 'Oxalis  corniculata', '15', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:52:01'),
(32, 'മുക്കുറ്റി സമൂലം   (ഉണക്ക)', '(Dried)', 'lajalu', '5d44087f9362e1564739711.jpg', NULL, 'Biophytum  sensitivum', '125', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:52:14'),
(33, 'മുക്കുറ്റി സമൂലം  (പച്ച)', '(Fresh)', 'lajalu', '5d8073eb432311568699371.jpg', NULL, 'Biophytum  sensitivum', '4000', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:19:31'),
(34, 'മുത്തിള്‍/കുടങ്ങന്‍ സമൂലം (ഉണക്ക)', '(Dried)', 'Brahma-manduki, Khulakhudi, Mandookaparni', '5d4408e29a2401564739810.png', NULL, ',,                 ,,', '3', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:53:31'),
(35, 'മുത്തിള്‍/കുടങ്ങന്‍ സമൂലം (പച്ച)', '(Fresh)', 'Brahma-manduki, Khulakhudi, Mandookaparni', '5d44090cd5a3b1564739852.jpg', NULL, 'Hydrocotyle   asiatica', '20', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:53:53'),
(36, 'മുശുമുശുക്ക് സമൂലം (മുക്കാപ്പീരം)(പച്ച)', '(Fresh)', 'Bilari, Agumaki', '5d44094e8ba091564739918.JPG', NULL, 'Mukkia  maderaspatana', '300', 1, 1, '2019-06-12 11:23:49', '2019-08-26 15:59:45'),
(37, 'ശംഖുപുഷ്പം സമൂലം (പച്ച)', 'Clitoria  (Fresh)', 'Aparajita', '5d8074148ce901568699412.jpg', NULL, 'Clitoria ternatea', '50', 1, 1, '2019-06-12 11:23:49', '2019-09-17 11:20:12'),
(39, 'അക്കിക്കറുക', 'Pelletary  Roots', 'Akarkara', '5d807431d94831568699441.jpg', NULL, 'Anacyclus  pyrethrum', '15', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:20:41'),
(40, 'അങ്കോലത്തിന്‍ വേര് (ഉണക്ക)  Size 18\"length & 11\" Diameter', 'Sage-Leaved  Aiungium  (dried)', 'Angol, Dhera, Dhela', '5d4409b290f161564740018.jpg', NULL, 'Alangium salvifolium', '15', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:00:57'),
(41, 'അടയ്ക്കാമണിയന്‍ വേര് (ഉണക്ക)', 'Junippe Berry (Dried)', 'mundi, gorkhmundi,', '5d4409c6a796f1564740038.jpg', NULL, 'Sphaeranthus  indicus', '900', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:01:42'),
(42, 'അമല്‍പൊരി വേര് (ഉണക്ക)', 'Red Serpentina [dried]', 'Sarpagandha mool sookha', '5d4409e7a292c1564740071.jpg', NULL, 'Rauwolfia serpentina', '300', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:02:31'),
(43, 'അമവേര്  (ഉണക്ക)', 'Pinreed Grass (Dried)', 'Sarkanda mool sookha', '5d47e5a073b471564992928.jpg', NULL, 'Saccharem Bengalens', '75', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:02:43'),
(44, 'അമുക്കുരം', 'Winter Cherry', 'Aswagandha', '5d440a0ab09ba1564740106.png', NULL, 'Withania  somnifera', '38000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:02:54'),
(45, 'ആട്ടുകൊട്ടപ്പാലവേര് (ഉണക്ക)(ആടുതൊടാപാല വേര്)', 'Worm  killer,  Bractealied Birthwort  (Dried)', 'Isvarmul', '5d440a238c28f1564740131.jpg', NULL, 'Aristolochia bracteolata', '1000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:05:53'),
(46, 'ആടുനാറിവേളവേര് (ഉണക്ക)', 'Wild mustard, Dog mustard (dried)', 'Hulhul, Hurhur', '5d440a44a36ad1564740164.jpg', NULL, 'Cleome gynandra', '300', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:06:19'),
(47, 'ആര്യവേപ്പിന്‍വേര് (ഉണക്ക)', 'The Creat   (dried)', 'Neem mool sookha', '5d440b9ee656e1564740510.jpg', NULL, 'Azadirachta  Indica', '3', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:06:42'),
(48, 'ആറ്റുദര്‍ഭ വേര് (ഉണക്ക)', 'Dhub  Grass  (Dried)', 'Dhub, Hariali', '5d440b618e0301564740449.jpeg', NULL, 'Typha elephantima', '50', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:06:59'),
(49, 'ആറ്റുവഞ്ചി വേര് (ഉണക്ക)', '(Dried)', 'Sherni', '5d8074c727b3b1568699591.jpg', NULL, 'Homonoia riparia, Lour', '2', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:23:11'),
(50, 'ആവണക്കിന്‍ വേര് (ഉണക്ക)', 'Castor Oil tree (dried)', 'Eranda mool sookha', '5d440ba963b901564740521.jpg', NULL, 'Ricinus Communis, Linn', '30000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:10:24'),
(51, 'ഇരട്ടിമധുരം', 'Liquorice Root', 'Mulhathi, Jethi-madh', '5d440bdad29da1564740570.jpg', NULL, 'Glycyrrhiza glabra', '10000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:10:34'),
(52, 'ഉങ്ങിന്‍ വേര്  (ഉണക്ക)18” length & 11” diameter', 'Indian reech, pongamal tree (dried)', 'kanji, karanj, karanja, papar', '5d440bfa74ddb1564740602.jpg', NULL, 'Pongamia pinnata', '600', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:10:44'),
(53, 'എരുക്കിന്‍ വേര് (ഉണക്ക)', 'Calotropis  (dried)', 'Akada', '5d440c11d59141564740625.jpg', NULL, 'Calotropis Procera', '9500', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:12:09'),
(54, 'എശങ്കിന്‍ വേര് (ഉണക്ക)', 'Asank moola (Dried)', 'Kanda-gur-kamay', '5d440c356b45c1564740661.jpg', NULL, 'Azima tetracantha, Linn', '100', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:12:19'),
(55, 'ഏകനായകവേര്  ഉണക്ക (Size 5\" )', 'Salacia oblonga', 'Saptarangi', '5d440c48514681564740680.jpg', NULL, 'Salacia oblonga', '16000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:12:31'),
(56, 'ഓരിലവേര് ഉണക്ക  (Size 22\" Length)', 'Trick Tree dried', 'Sarivan', '5d440c6b6249d1564740715.jpg', NULL, 'Uraria Picta', '14000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:12:43'),
(57, 'കടമ്പിന്‍ വേര് (ഉണക്ക)', 'Careys tree (dried)', 'Kadamb', '5d440c79284d61564740729.jpg', NULL, 'Neolamarckia  cadamba', '100', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:14:56'),
(58, 'കടുകുരോഹിണി', 'Black Hellebore', 'Katuka, Kutki', '5d440cb35eb9e1564740787.png', NULL, 'Picrorrhiza kurroa', '2700', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:15:18'),
(59, 'കന്നികൊട്ടകത്തിന്‍ വേര്  (ഉണക്ക)', 'Kontamool (dried)', NULL, '5d47e60ac1b3c1564993034.jpg', NULL, NULL, '100', 2, 1, '2019-06-12 11:23:49', '2019-08-05 13:47:14'),
(60, 'കമുകിന്‍ വേര്  (ഉണക്ക)', 'Areca  Palm  (Dried)', 'Pug mool ,Supari mool,Khapur  mool', '5d440d08cc6661564740872.jpg', NULL, 'Areca  Catechu', '20', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:21:21'),
(61, 'കര്‍പ്പൂരതുളസി വേര് (പച്ച)', '(fresh)', 'Karpoor tulsi mool taaza', '5d440cf8b4b9a1564740856.jpg', NULL, 'Ocimum  kilimandscharicum', '10', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:21:49'),
(62, 'കരവീരത്തിന്‍ വേര്  (ഉണക്ക)', '(dried)', 'Kaner mool ,Karuvira mool', '5d440d335564b1564740915.jpg', NULL, 'Nerium odorum', '20', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:23:42'),
(63, 'കരിംകുറിഞ്ഞി വേര്  (ഉണക്ക)', 'Common Yellow Nail Dye Plant  (dried)', ' Karvi', '5d440d680c2f71564740968.jpg', NULL, 'Strobilanthes ciliatus', '13000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:23:55'),
(64, 'കരിനൊച്ചിവേര്  (ഉണക്ക) 18” Length   & 11 “ Diameter', 'Five Leaved Chaste Tree (dried)', 'Mewri, Nirgundi, Nisinda', '5d440da56d9de1564741029.jpg', NULL, 'Vitex negundo', '1600', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:24:25'),
(65, 'കരിമ്പിന്‍ വേര്  (ഉണക്ക)', 'Sugarcane  (dried)', 'Eekh', '5d807548833da1568699720.jpg', NULL, 'Saccharum  officinarum', '900', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:25:20'),
(66, 'കരിമ്പിന്‍തടി (പച്ച)', 'Sugarcane', 'Eekh', '5d807570ddf371568699760.jpg', NULL, 'Saccharum  officinarum', '700', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:26:00'),
(67, 'കല്ലൂര്‍വഞ്ചി വേര്  (ഉണക്ക)', 'Country Borage (Dried)', 'pashanabhed', '5d440dfbf27c51564741115.png', NULL, 'Rotula  aquatica', '500', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:25:35'),
(68, 'കള്ളി വേര്  (ഉണക്ക) 18” length & 11” diameter', 'Thorny Milk Hedge Plant (dried)', 'Sehunda, Dhusar', '5d8075c516a561568699845.jpg', NULL, 'Euphorbia ligularia', '100', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:27:25'),
(69, 'കഴഞ്ചി വേര് (ഉണക്ക)upto 22” length', 'Fever nut, bonduc nut (dried)', 'Kantkeraj, Kantikaranja', '5d440e2bc1f691564741163.jpg', NULL, 'Caesalpina bonduc', '200', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:25:59'),
(70, 'കാക്കതൊണ്ടിവേര് (ഉണക്ക)', '(Dried)', 'Indrayan, Mahakal', '5d440e49576c71564741193.jpg', NULL, 'Trichosanthes  tricuspidata', '50', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:26:16'),
(71, 'കാട്ടത്തി വേര് ( ഉണക്ക)', '(Dried)', 'Katgularia, kala umbar, phalgu, gobla,kagsha', '5d440e6db52d31564741229.jpg', NULL, 'Ficus  hispida', '2', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:27:42'),
(72, 'കാട്ടപ്പവേര് (പച്ച)', '(fresh)', 'Jangali pudina mool,Bhakumbar mool thaza', '5d440e7d023a31564741245.jpg', NULL, 'Ageratum conyzoides', '10', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:28:53'),
(73, 'കാട്ടപ്പവേര്(ഉണക്ക)', '(dried)', 'Jangali pudina mool,Bhakumbar mool sookha', '5d440eca07c8f1564741322.jpg', NULL, 'Ageratum conyzoides', '5', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:29:46'),
(74, 'കാട്ടുതിപ്പലി വേര് (ഉണക്ക)', 'Pippalmoolam (dried)', 'Pipal, Pipar', '5d440ef40d0f61564741364.jpg', NULL, 'Piper longum', '3500', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:29:59'),
(75, 'കാട്ടുതുളസി വേര് (ഉണക്ക)up to 22\" length', '(Dried)', 'Jangali thulsi mool sookha', '5d47e62706c901564993063.jpg', NULL, 'Ocimum  malabaricum', '175', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:30:19'),
(76, 'കാട്ടുപയറിന്‍ വേര്  (ഉണക്ക)', 'Pillipesara (dried)', 'Mugani,Mungan', '5d47e65de601c1564993117.jpg', NULL, 'Vigna trilobata', '800', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:37:56'),
(77, 'കാട്ടുമുളകിന്‍ വേര് (ഉണക്ക)', 'Piper Chaba (dried)', 'Pipal, Pipar', '5d440f1bf20241564741403.jpg', NULL, 'Piper chaba', '4300', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:38:12'),
(78, 'കാട്ടുഴുന്നിന്‍ വേര്  (ഉണക്ക)', 'Vigna radiata (dried)', 'Jangali  mungo mool', '5d440f7f574c31564741503.jpg', NULL, 'Vigna radiata var, sublobata', '750', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:38:24'),
(79, 'കാട്ടുവെള്ളരി', 'Colocynth', 'lndrayan', '5d440ff4cf7bd1564741620.jpg', NULL, 'Citrullus  colocynthis', '1500', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:38:35'),
(80, 'കാര്‍തൊട്ടി വേര് (ഉണക്ക)', 'Ceylon Caper (dried)', 'Kamsamara mool sookha', '5d47e6623e0cb1564993122.jpg', NULL, 'Hugonia mystax', '1200', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:38:48'),
(81, 'കുമിഴിന്‍ വേര് (ഉണക്ക)18” length & 11” diameter', 'White Teak (dried)', 'Gamhar', '5d4419d14b4cb1564744145.jpg', NULL, 'Gmelina  arborea', '15000', 2, 1, '2019-06-12 11:23:49', '2019-08-26 16:39:01'),
(82, 'കുരുക്കുത്തിമുല്ല വേര് (പച്ച)', '(fresh)', 'Balini', NULL, NULL, 'Jasminum  ssp', '10', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:00:57'),
(83, 'കുറുന്തോട്ടിവേര് ഉണക്ക ( Size 22\" length)', 'Country Mallon (dried)', 'Bariyar, Kharethi', '5d441ae99c9681564744425.jpg', NULL, 'Sida rhombifolia ssp retusa, SP cordifolia', '68000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:01:24'),
(84, 'കുഴൽകൊന്ന', 'Turpeth Root', 'Nishoth, Tarbut', '5d441b2bda8f11564744491.jpg', NULL, 'loperculina turpethum', '19000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:01:37'),
(85, 'കുശവേര് (ഉണക്ക)', 'Thatch Grass   (Dried)', 'Kaans', '5d8076454bf261568699973.jpg', NULL, 'Saccharum  spontaneum', '75', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:29:33'),
(86, 'കൂവളവേര് ഉണക്ക(Size 18\" Length & 11\" Diameter)', 'Holy fruit tree, Bael tree(dried)', 'Bilva,Bel', '5d441c5c956e21564744796.jpg', NULL, 'Aegle marmelos', '44000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:01:57'),
(87, 'കൃഷ്ണതുളസി വേര് (പച്ച)', 'Sacred  basil  (fresh)', 'Tulsi, Baranda,Kala tulsi', '5d441cbd70abf1564744893.jpg', NULL, 'Ocimum tenuiflorum', '10', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:07:52'),
(88, 'കൊടുതൂവ വേര്  (ഉണക്ക)', 'Climbing Nettle   (dried)', 'barhanta', '5d441cc147eed1564744897.jpg', NULL, 'Tragia involucrata', '13500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:08:03'),
(89, 'കോല്‍മാഞ്ചി', 'Musk Root', 'Jatamansi', '5d441cc54ccac1564744901.jpg', NULL, 'Nardostachys grandiflora', '2300', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:08:13'),
(90, 'കോഴിക്കാലിന്‍ വേര് (പച്ച)', '(fresh)', 'Kalijhamp thaza', '5d4bf36ce54481565258604.jpg', NULL, 'Adiantum   lanatum', '2', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:08:23'),
(91, 'ഗരുഡക്കൊടി വേര് (കരളകം)(ഉണക്ക)', 'Indian birthwort (Dried)', 'Hooka-bel', '5d441d2a3ffdc1564745002.jpg', NULL, 'Aristolochia  indica', '25', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:08:36'),
(92, 'ചിറ്റീന്തല്‍ വേര് (ഉണക്ക)', 'Small wild date palm (dried)', 'khajur', '5d441d570c8741564745047.jpg', NULL, 'Phoenix  pusilla', '750', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:08:49'),
(93, 'ചുവന്നരത്ത', 'Lesser Galangal', 'Kulanjan, Kulinjan', '5d4bf3736b14e1565258611.jpg', NULL, 'Alpinia galanga', '20000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:09:27'),
(94, 'ചെറിയ ആടലോടക വേര്  ഉണക്ക (Size 5\")', 'Vasak (dried)', 'Arusa, Bansa', '5d441db022d7d1564745136.jpg', NULL, 'Adhatoda  beddomei/Adhatoda  vassica', '24000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:09:37'),
(95, 'ചെറുതേക്കിന്‍ വേര് (ഉണക്ക)', 'Beetle Killer Root (dried)', 'Bharangi, Babhanaiti', '5d441dd29a4f21564745170.jpg', NULL, 'Clerodendrum serratum', '2500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:09:47'),
(96, 'ചെറുവഴുതിന വേര്  (ഉണക്ക)', 'India night shade (dried)', 'Mokoi मोकोय', '5d450651b17f21564804689.jpg', NULL, 'solanum melongena', '21000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:10:03'),
(97, 'ചെറൂള വേര്   ഉണക്ക  (Size 22\" Length)', 'Mountain knot grass(dried)', 'Gorakhbuti, Kapuri jadi', '5d4506bfc23c11564804799.jpg', NULL, 'Aerva lanata', '18000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:10:14'),
(98, 'ജഡാമാഞ്ചി', NULL, 'Jatamansi', '5d4506d2f33251564804818.jpg', NULL, 'Nardostachys   Jatamansi', '300', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:10:49'),
(99, 'തകരം (പഞ്ചാബ്)', 'Foetid Cassia', 'balchhari, mansi, nihani, smak, sumaya, tagar', '5d450718df01b1564804888.jpg', NULL, 'Valeriana jatamansi/Valeriana wallichii', '2500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:10:59'),
(100, 'തഴുതാമവേര്  ഉണക്ക', 'Spreading Hogweed (dried)', 'Punarnava, Gadahpurna', '5d45072f733c31564804911.jpg', NULL, 'Boerrhavia diffusa', '23000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:11:09'),
(101, 'താന്നി വേര് (പച്ച)', 'Belliric myrobalan (fresh)', 'Bahera', '5d450752b52801564804946.jpg', NULL, 'Terminalia bellirica', '300', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:11:21'),
(102, 'തുളസി വേര് (ഉണക്ക)', 'Holy Basil (Dried)', 'Tulsi', '5d45076fd6fb71564804975.jpg', NULL, 'Ocimum sanctum', '100', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:12:40'),
(103, 'തൂതുവിള വേര്  (ഉണക്ക)', '(dried)', 'Alarka mool sookha', '5d45078b645b91564805003.jpg', NULL, 'Solanum trilobatum', '0.5', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:13:20'),
(104, 'തെങ്ങിന്‍ വേര്  (ഉണക്ക)', 'Coconut  Palm  (Dried)', 'Nariyal mool sookha', '5d8076b4106eb1568700084.jpg', NULL, 'Cocos  nucifera', '20', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:31:24'),
(105, 'തെച്ചി വേര് (ഉണക്ക) 22\" length', 'Wild Date Fruit (dried)', 'Rugmini', '5d4507f644eee1564805110.jpg', NULL, 'Ixora  coccinea', '6000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:13:45'),
(106, 'തേക്കട വേര് (ഉണക്ക) 22\"length', 'Indian turnsole (dried)', 'Siriyari,Hattasura', '5d4507ed07e401564805101.JPG', NULL, 'Heliotropium  indicum', '50', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:14:35'),
(107, 'ദര്‍ഭ വേര് (ഉണക്ക)', 'Sacritical Grass Root (dried)', 'Daabh, Davoli, Durva', '5d45082c477211564805164.jpg', NULL, 'Desmostachya  bipinnata', '300', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:14:51'),
(108, 'നാഗദന്തി വേര് (ഉണക്ക)', 'Wild   croton   (dried)', 'Danti', '5d450b1da3bd31564805917.jpg', NULL, 'Baliospermum  montanum', '500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:15:06'),
(109, 'നായ്ക്കുരണ വേര് (ഉണക്ക)', 'Cowhage Planet (dried)', 'Kaunch, Kevanch', '5d450b43f16c01564805955.jpg', NULL, 'Mucuna  pruriens', '500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:15:17'),
(110, 'നീര്‍മാതളത്തിന്‍ വേര് (ഉണക്ക) 18\" length & 11\"diameter', 'Three leaved caper (dried)', 'Barun,Barna', '5d450b601e2951564805984.JPG', NULL, 'Crataeva  nurvala', '1800', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:15:29'),
(111, 'നീലയമരി വേര് (ഉണക്ക)', 'True Indigo (dried)', 'neel ka mool sookha', '5d450b82e63c01564806018.jpg', NULL, 'Indigofera tinctoria', '50', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:15:39'),
(112, 'നെന്മേനി വാക വേര്  (ഉണക്ക)18” length & 4” diameter', '(dried)', 'Saras mool sookha', '5d450b9b132c61564806043.jpg', NULL, 'Albizzia lebbeck', '5', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:16:09'),
(113, 'പടര്‍ച്ചുണ്ട വേര് (ഉണക്ക)', '(dried)', 'Ptwa ghas mool sookha', '5d80774eec7c41568700238.jpg', NULL, 'Cassia  mimosodies', '750', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:33:58'),
(114, 'പലകപയ്യാനി വേര്  (ഉണക്ക) 18” length & 11” diameter', 'Indian trumpet flowerbark (dried)', 'Sonapatha, Shyonaka', '5d450bd4a60a31564806100.jpg', NULL, 'Oroxylum  indicum', '14000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:16:37'),
(115, 'പാതിരി വേര് (ഉണക്ക)18” length & 11” diameter', '(dried)', 'Pdhal ,podal mool sookha', '5d450be92b1f71564806121.jpg', NULL, 'Stereospermum  suaveolens', '14000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:16:51'),
(116, 'പിച്ചകത്തിന്‍ വേര്  (ഉണക്ക)', '(dried)', 'jathi mool sookha', '5d450c0161b3a1564806145.jpg', NULL, 'Jasminum officinale', '3', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:30:46'),
(117, 'പുത്തരിചുണ്ടവേര് ഉണക്ക (Size 5\")', 'The creat(dried)', 'Barhanta', '5d450c217adc31564806177.jpg', NULL, 'Solanum anguivi', '10500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:30:58'),
(118, 'പുഷ്കരമൂലം', 'Pushkaramool', 'Pushkaramool', '5d450c46ad4af1564806214.jpg', NULL, 'Inula racemosa', '4700', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:31:11'),
(119, 'പൂക്കൈതവേഡ് (ഉണക്ക)', 'Fragrant  Screw  Hemp(dried)', 'Ketaka proproot sookha', '5d80779da94ea1568700317.jpg', NULL, 'Pandanus Tectorius', '175', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:35:17'),
(120, 'പൂക്കൈതവേഡ് (പച്ച)', 'Fragrant  Screw  Hemp(fresh)', 'Ketaka proproot thaza', '5d451304466de1564807940.jpg', NULL, 'Pandanus Tectorius', '2000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:32:12'),
(121, 'പൂവത്ത് (മഞ്ചട്ടി , മഞ്ജിഷ്ട )', 'Madder Root', 'Manjith, Majith', '5d4513262f5e91564807974.jpg', NULL, 'Rubia cordifolia', '5700', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:32:32'),
(122, 'പെരുംകുറുമ്പ വേര്  (ഉണക്ക)', 'Bow-Screw  Hemp  (dried)', 'Moorva mool sookha', '5d45135de0b2e1564808029.jpg', NULL, 'Chonemorpha  fragrans', '2000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:36:09'),
(123, 'പൊന്നാവീരത്തിന്‍ വേര്  (ഉണക്ക)', 'Negrao coftee Plant (Fresh)', 'Kasunda mool sookha', '5d45138e9ded91564808078.jpg', NULL, 'Cassia  occidentalis', '15', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:36:18'),
(124, 'പേരാല്‍ വേഡ് (ഉണക്ക)', 'Banyan Tree (dried)', 'Bargad', '5d80780187dd51568700417.jpg', NULL, 'Ficus benghalensis', '10', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:36:57'),
(125, 'പേരാല്‍ വേഡ് (പച്ച))', 'Banyan Tree (Fresh)', 'Bargad', '5d80780d430df1568700429.jpg', NULL, 'Ficus  benghalensis', '20', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:37:09'),
(126, 'മണിത്തക്കാളി വേര് (പച്ച)', 'Black night -shade (fresh)', 'Mokoi मोकोय mool  thaza', '5d451480945331564808320.jpg', NULL, 'Solanum  nigrum', '10', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:37:09'),
(127, 'മുഞ്ഞ വേര് (ഉണക്ക)18” length & 11” diameter', 'Wind Killer   (dried)', 'Arani, Ganiyari', '5d8078329941d1568700466.jpg', NULL, 'Premna  serratifolia', '16000', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:37:46'),
(128, 'മൂവിലവേര് ഉണക്ക (Size 22\" Length)', 'Viseid pseudardhria dried', 'Pidhavan, Deerkhamoola, Ekamoola', '5d45150ae1c2b1564808458.jpg', NULL, 'Desmodium  gangelium', '14000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:37:37'),
(129, 'രാമച്ചം  ഉണക്ക', 'Cuscus Grass (dried)', 'Khas', '5d45151a9292f1564808474.jpg', NULL, 'Vetiveria zizanioides', '11000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:38:42'),
(130, 'വയമ്പ്', 'Sweet Flag Root', 'Bach', '5d45152b3e0311564808491.jpg', NULL, 'Acorus calamus', '19500', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:38:53'),
(131, 'വയല്‍ ചുള്ളി വേര്  (ഉണക്ക)', 'longleaved Barleria (dried)', 'Talim khana', '5d451557a2e701564808535.JPG', NULL, 'Hygrophila  auriculata', '5000', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:39:15'),
(132, 'വലിയ കടലാടി വേര് (ഉണക്ക)', 'Rough Chapp Tree (dried)', 'Chirchra', '5d45157ca2b581564808572.jpg', NULL, 'Achyranthes aspera, Linn', '400', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:40:25'),
(133, 'ശംഖുപുഷ്പത്തിന്‍ വേര്  (ഉണക്ക)', 'Clitoria (Dried)', 'Koyala', '5d4515812935f1564808577.jpg', NULL, 'Clitoria ternatea', '1300', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:40:35'),
(134, 'ശീമ ഇരുവേലി  (ഉണക്ക)', 'Fragrant Silky Mallow (Dried)', 'Sugandhabala', '5d4515a4764a21564808612.JPG', NULL, 'Coleus  Zeylanicus', '4200', 2, 1, '2019-06-12 11:23:49', '2019-08-27 10:40:45'),
(135, 'ശീമക്കൊട്ടം', 'Costus Root', 'Kuth, Kur', '5d80786899ee31568700520.jpg', NULL, 'Saussurea lappa', '7000', 2, 1, '2019-06-12 11:23:49', '2019-09-17 11:38:40'),
(137, 'അടപതിയൻ കിഴങ്ങ് ഉണക്ക', 'Holostemma creeper(dried)', 'Chhirvel', '5d47e83b79fd51564993595.jpg', NULL, 'Holostemma ada-kodien', '1400', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:41:13'),
(138, 'അതിവിടയം', 'Indian Atis', 'Atis, Atvika', '5d4515f8d9d011564808696.jpg', NULL, 'Aconitum heterophyllum', '600', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:41:27'),
(139, 'ആമ്പല്‍ക്കിഴങ്ങ് (ഉണക്ക)', 'Water Lily Rhizome (dried)', 'Kanwal, Kokka', '5d45161a113d11564808730.jpg', NULL, 'Nymphaea alba', '75', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:41:39'),
(140, 'ഇഞ്ചി', 'Ginger', 'Adrak', '5d45162a1f2671564808746.jpg', NULL, 'Zingiber officinale', '2000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:41:51'),
(141, 'ഓരിലത്താമരക്കിഴങ്ങ് (പച്ച)', '(fresh)', 'Sthalapadma ', '5d8078bc0339c1568700604.jpg', NULL, 'Nervilia aragoana', '5', 3, 1, '2019-06-12 11:23:49', '2019-09-17 11:40:04'),
(142, 'കച്ചൂരിക്കിഴങ്ങ്   ഉണക്ക', 'Long Zedor(dried)', 'Chandramoola', '5d45165edc6b71564808798.jpg', NULL, 'Hedychium spicatum', '8700', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:42:15'),
(143, 'കദളി വാഴക്കിഴങ്ങ് (പച്ച) cut in to pieces (4” x4”   square )', 'Plantain  (fresh)', 'Kadali prakand', '5d451682cecba1564808834.jpg', NULL, 'Musa paradisica', '700', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:43:55'),
(144, 'കരിംകൂവളക്കിഴങ്ങ്  (ഉണക്ക)', '(dried)', 'Neelolpal', '5d45169eb24771564808862.jpg', NULL, 'Monochoria vaginalis', '75', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:44:04'),
(145, 'കസ്തൂരി മഞ്ഞള്‍', 'Aromatic Turmeric  (Dried)', 'Jangali haldi', '5d4516b13e90d1564808881.jpg', NULL, 'Curcuma aromatica', '200', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:44:16'),
(146, 'കാട്ടുചേന', 'Elephant Foot yarm (fresh)', 'Suran, Jamikand.', '5d4516c90ffc61564808905.jpeg', NULL, 'Amorphophallus canpaulatus', '1200', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:44:29'),
(147, 'കൊടുവേലി കിഴങ്ങ് (ഉണക്ക)', 'Lead Wort (dried)', 'Chitraka', '5d4516e005e091564808928.jpg', NULL, 'plumbago zeylanica', '3300', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:44:45'),
(148, 'കോവല്‍ക്കിഴങ്ങ് (ഉണക്ക)', 'Ivy gourd', 'कुन्द्रू Kunduru', '5d4516fbdf9881564808955.jpg', NULL, 'Coccinia Indica', '5', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:44:57'),
(149, 'ചീനപ്പാവ്', 'China Root', 'Chopchini', '5d451713ecee91564808979.jpg', NULL, 'Smilax china', '50', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:45:09'),
(150, 'ചുക്ക്', 'Dried Ginger', 'Adrak, Sonth', '5d451724356191564808996.jpeg', NULL, 'Zingiber officinalie', '60000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:45:20'),
(151, 'ചുരുട്ടുരോഹിണി (ഉണക്ക)', '(dried)', 'Lal chitrak', '5d45187cb5d2d1564809340.jpg', NULL, 'Soymida febrifuga', '125', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:45:31'),
(152, 'ചുവന്നുള്ളി', 'Onion', 'प्याज़ Pyaz', '5d451989eb3921564809609.jpg', NULL, 'Allium cepa', '15500', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:45:45'),
(153, 'ചെങ്ങഴിനീര്‍ക്കിഴങ്ങ്  (ഉണക്ക)', 'Indian crocus (dried)', 'Bhuyichampa', '5d4519ce14d621564809678.jpg', NULL, 'Kaempferia rotunda', '1000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:46:05'),
(154, 'ഞെരിഞ്ഞാംപുളിക്കിഴങ്ങ് (ഉണക്ക)', '(dried)', 'Amanthamul Sookha', '5d47e85b70a311564993627.jpg', NULL, 'Solena amplexicaulis', '700', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:47:45'),
(155, 'ഞെരിഞ്ഞാംപുളിക്കിഴങ്ങ് (പച്ച)upto 1.5 mtr length', '(fresh)', 'Amanthamul thaza', NULL, NULL, 'Solena amplexicaulis', '1200', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:47:56'),
(156, 'താമരക്കിഴങ്ങ്  (ഉണക്ക)', 'Lotus (dried)', 'Kamalkhand', '5d807916477a01568700694.jpg', NULL, 'Nelumbo nucifera', '75', 3, 1, '2019-06-12 11:23:49', '2019-09-17 11:41:34'),
(157, 'നറുചണ്ണക്കിഴങ്ങ് (ഉണക്ക)', '(dried)', 'Ketha khand', '5d807956139e41568700758.jpg', NULL, 'Costus  speciosus', '50', 3, 1, '2019-06-12 11:23:49', '2019-09-17 11:42:38'),
(158, 'നറുനീണ്ടി കിഴങ്ങ്  ഉണക്ക', 'Indian Sarasaparilla(dried)', 'Anantamul, Sariva', '5d451a909ec741564809872.jpg', NULL, 'Hemidesmus indicus', '5000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:48:31'),
(159, 'നിലപ്പനകിഴങ്ങ്  ഉണക്ക', 'Black Musali(dried)', 'Kali Musli, Kali Musali, Musali kand.', '5d451aabf1e371564809899.jpg', NULL, 'Curculigo orchioides', '8500', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:48:41'),
(160, 'നൈതല്‍ക്കിഴങ്ങ് (ഉണക്ക)', 'Indian blue water Fily (dried)', 'Kumuda', '5d47e8ac375aa1564993708.jpg', NULL, 'Nymphaea stellata', '50', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:48:56'),
(161, 'പച്ചമഞ്ഞൾ', 'Turmeric', 'Haldi', '5d451ad599d111564809941.jpg', NULL, 'Curcuma longa', '61000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:49:07'),
(162, 'പാടക്കിഴങ്ങ് ഉണക്ക', 'Flase Pareirabrave (dried)', 'Path, Pathi', '5d451b34a17901564810036.jpg', NULL, 'Cyclea peltata', '4200', 3, 1, '2019-06-12 11:23:49', '2019-08-27 10:49:18'),
(163, 'പാര്‍വള്ളിക്കിഴങ്ങ് (ഉണക്ക)', 'Black Sarasaparilla dried', 'कालीदूधी Kalidudhi, श्यामलता Shyamalata', '5d451b5231e9b1564810066.jpg', NULL, 'Ichnocarpus  frutiscenes', '200', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:14:21'),
(164, 'പാല്‍മുതുക്ക് (പച്ച)', 'French Honey Suckle Tuber (fresh)', 'Vidarikhand thaza', '5d807a4284d811568700994.jpg', NULL, 'Ipomoea digitata', '700', 3, 1, '2019-06-12 11:23:49', '2019-09-17 11:46:34'),
(165, 'പാല്‍മുതുക്ക്(ഉണക്ക)', 'French Honey Suckle Tuber (dried)', 'Vidarikhand sookha', '5d451ba59f57b1564810149.jpg', NULL, 'Ipomoea digitata', '700', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:14:43'),
(166, 'പീതരോഹിണി', NULL, 'Mamira', '5d451bc03b5351564810176.jpg', NULL, 'Picrorrhiza  Ssp.', '10', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:14:55'),
(167, 'മുത്തങ്ങക്കിഴങ്ങ് ഉണക്ക', 'Nut grass (dried)', 'Motha, Mutha', '5d451bdf1980e1564810207.jpg', NULL, 'Cuperus rotundus', '27000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:15:10'),
(168, 'മൂലേരിക്കിഴങ്ങ് (പച്ച)', 'Raddish   (Fresh)', 'Mooli', '5d451c001a6e01564810240.jpg', NULL, 'Raphanus sativus', '900', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:15:21'),
(169, 'വത്സനാഭി', 'Indian Aconite', 'Bachang,Meetha vish', '5d807adbb776d1568701147.jpg', NULL, 'Aconitum  ferox', '400', 3, 1, '2019-06-12 11:23:49', '2019-09-17 11:49:07'),
(170, 'വരട്ടുമഞ്ഞൾ', 'Turmeric', 'Haldi, Halada', '5d451c2ace1ff1564810282.jpg', NULL, 'Curcuma longa', '18500', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:15:51'),
(171, 'വെള്ളുള്ളി (ഉണക്ക)', 'Garlic [dried]', 'Lehasun', '5d451c4d516b51564810317.jpg', NULL, 'Allium  sativum', '800', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:16:01'),
(172, 'വെള്ളുള്ളി(പച്ച)', 'Garlic  [Fresh]', 'Lehasun', '5d451c698b8431564810345.jpg', NULL, 'Allium  sativum', '11000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:16:14'),
(173, 'ശതാവരികിഴങ്ങ് ഉണക്ക', 'Wild aspara gus (dried)', 'Satavar, Satmuli', '5d451c7a9fa9e1564810362.jpg', NULL, 'Asparagus racemosus', '2500', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:17:52'),
(174, 'ശതാവരികിഴങ്ങ് പച്ച   (Bag  wt. 50 Kg)', 'Wild aspara gus (fresh)', 'Satavar, Satmuli', '5d451ca6bbc141564810406.jpg', NULL, 'Asparagus racemosus', '80000', 3, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:02'),
(176, 'കോവല്‍വള്ളി (പച്ച)', 'Ivy gourd (fresh)', 'कुन्द्रू Kunduru', '5d451cd1c85671564810449.JPG', NULL, 'Coccinia grandis', '300', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:12'),
(177, 'ചിറ്റമൃത് (പച്ച) 12” to 18” length', 'Heart Leaved Moon Seed (fresh)', 'Giloy,Gurcha', '5d451d01326971564810497.jpg', NULL, 'Tinospora cordifolia', '6000', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:22'),
(178, 'ചിറ്റമൃത് ഉണക്ക', 'Heart Leaved Moon Seed (dried)', 'Giloy,Gurcha', '5d451d2dbef741564810541.jpg', NULL, 'Tinospora cordifolia', '20000', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:32'),
(179, 'താമരവളയം (ഉണക്ക)', 'Stalk of the Lotus (dried)', 'Kanwal', '5d451db8e9e3a1564810680.jpg', NULL, 'Nelumbo nucifera', '725', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:42'),
(180, 'തിരുതാളി (പച്ച)', 'Mandrake (fresh)', 'Bankalmi', '5d451dcc311111564810700.jpg', NULL, 'Ipomoea sepiaria', '50', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:18:53'),
(181, 'പരുവക്കൊടി (പച്ച)', '(fresh)', 'Vrikshadani', '5d451de2529e71564810722.jpg', NULL, 'Pothos scandens', '1500', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:19:06'),
(182, 'പർപ്പടക പുല്ല് പച്ച', 'Old world  diamon flower (fresh)', 'Daman pappad', '5d451dfd935621564810749.jpg', NULL, 'Hedyotis corymbosa', '54000', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:19:16'),
(183, 'ബ്രഹ്മി (ഉണക്ക)', 'Indian penny Wort (dried)', 'Brahmi', '5d807b1bd17a21568701211.jpg', NULL, 'Bacopa monnieri', '200', 4, 1, '2019-06-12 11:23:49', '2019-09-17 11:50:11'),
(184, 'ബ്രഹ്മി (പച്ച)', 'Indian penny Wort (fresh)', 'Brahmi', '5d451e3405a4c1564810804.jpg', NULL, 'Bacopa monnieri', '3200', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:36:42'),
(185, 'വെറ്റിലക്കൊടി (കൊടിഞ്ഞാലി) (പച്ച)', 'Betel Leaf (fresh)', 'pan', '5d807b40ddca71568701248.jpg', NULL, 'Pipper betel', '4300', 4, 1, '2019-06-12 11:23:49', '2019-09-17 11:50:48'),
(186, 'സോമവല്ലി (സോമലത) (പച്ച)', 'Moon creeper moon plant (fresh)', 'Somlata', '5d807b5f4177c1568701279.jpg', NULL, 'Sarcostemma acidum', '25', 4, 1, '2019-06-12 11:23:49', '2019-09-17 11:51:19'),
(188, 'അമ്പഴത്തില', 'Wild mangom Hogplum (dried)', 'Jangli aam, Amra', '5d45232884a481564812072.JPG', NULL, 'Spondias  pinnata', '1300', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:39:03'),
(189, 'ആര്യവേപ്പില(പച്ച)', 'Indian lilac, Margora tree [fresh]', 'नीम Neem', '5d452353874211564812115.jpg', NULL, 'Azadirachta indica', '1600', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:39:16'),
(190, 'എരുക്കില (പച്ച)', 'Gigantic Swallow wort Mudar [fresh]', 'Aak, Madar', '5d45236ae06511564812138.jpg', NULL, 'Calotropis Procera', '75', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:39:28'),
(191, 'കഞ്ഞുണ്ണി (ഉണക്ക)', 'Traling Eclipta [dried]', 'Bhringaraj Sookha', '5d4523822550d1564812162.jpg', NULL, 'Eclipta  prostrata', '175', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:39:39'),
(192, 'കഞ്ഞുണ്ണി (പച്ച)', 'Traling Eclipta [fresh]', 'Bhangra, Mochkand, Babri', '5d4523a8445711564812200.jpg', NULL, 'Eclipta prostrata', '29000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:39:50'),
(193, 'കണികൊന്നയില (പച്ച)', 'Purging fistula, Indian laburom', 'Amaltas pathra thaza', '5d4523debb5a41564812254.jpg', NULL, 'Cassia fistula', '25', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:40:01'),
(194, 'കരിനൊച്ചിയില (പച്ച)', 'Five  leaved  Chast Tree  [fresh]', 'Sambhalu', '5d45242e38ded1564812334.jpg', NULL, 'Vitex  negundo', '6500', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:40:16'),
(195, 'കരിമുരുക്ക് (പച്ച)', 'Indian coral tree [fresh]', 'Pangaar पंगार', '5d452451df2941564812369.jpg', NULL, 'Erythrina  indica', '4000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:41:20'),
(196, 'കറ്റുവാഴത്തണ്ട് (പച്ച)', 'Aloes  (fresh)', 'gheekumari ,Ghi-kuvar,Gvar patha', '5d452491c8bd41564812433.jpg', NULL, 'Aloe  barbadesis', '73000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:41:30'),
(197, 'കറിവേപ്പില (പച്ച)', 'Curry Leaf Tree [fresh]', 'karipatta', '5d4524bb77e1e1564812475.jpg', NULL, 'Murraya  koenigii', '6200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:41:40'),
(198, 'കറിവേപ്പില(ഉണക്ക)', 'Curry Leaf Tree [dried]', 'karipatta', '5d807f6ec7f631568702318.jpg', NULL, 'Murraya  koenigii', '5', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:08:38'),
(199, 'കറുക (ഉണക്ക)', 'Creeping Cynodon [dried]', 'Dhoob, Hariali', '5d45251cda48c1564812572.jpg', NULL, 'Cynodon  dactylon', '10', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:42:04'),
(200, 'കറുക (പച്ച)', 'Creeping Cynodon [fresh]', 'Dhoob, Hariali', '5d45254dadee61564812621.jpg', NULL, 'Cynodon  dactylon', '7400', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:43:12'),
(201, 'കള്ളിക്കഴുത്ത് (ഉണക്ക)', 'Thorny Milk Hedge Plant [dried]', 'Vajra vriksha', '5d45255fa1f561564812639.jpg', NULL, 'Euphorbia trigona', '75', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:43:14'),
(202, 'കള്ളിയില', 'Common  mille  hedge  [fresh]', 'Sehunda, Dhusar', '5d80800946ec41568702473.jpg', NULL, 'Euphorbia derifolia', '20', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:11:13'),
(203, 'കാഞ്ഞിരത്തില (പച്ച)', 'NUX - Vomica tree leaf', 'Kajra,kuchila', '5d807fa2df2441568702370.jpg', NULL, 'Strychnos nux - vomica', '5', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:09:30'),
(204, 'കാട്ടുപടവലത്തില (പച്ച)', 'Wild Snake gound [fresh]', 'Jungli chichinda, Paraval', '5d4525b8bf4941564812728.jpg', NULL, 'Trichosanthes cucumerina', '150', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:43:49'),
(205, 'കുടപ്പുളിയില(പച്ച)', 'Indian gamboge [fresh]', 'Bilatti amli', '5d4525d7ccebd1564812759.jpg', NULL, 'Garcinia gummi-gutta', '600', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:56:46'),
(206, 'കൂവളത്തില (പച്ച)', 'Bael Tree   [fresh]', 'Bel', '5d4525ece7c131564812780.jpg', NULL, 'Aegle marmelos', '5600', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:56:56'),
(207, 'കൃഷ്ണതുളസിയില (പച്ച)', 'Sacred  Basil  [fresh]', 'Tulsi, Baranda,Kala tulsi', '5d45261070c241564812816.jpg', NULL, 'Ocimum sanctum', '6000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:57:08'),
(208, 'കൊഴുപ്പയില (പച്ച)', 'Prickly leaved elephants foot [fresh]', 'Gobhi', '5d45261eec3b71564812830.jpg', NULL, 'Elephantopus scaber', '150', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:57:22'),
(209, 'ചക്കരകൊല്ലിയില (ഉണക്ക)', 'Periploca of woods', 'Gurmar pathra sookha', '5d808036b429e1568702518.jpg', NULL, 'Gymnema  sylvestre', '200', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:11:58'),
(210, 'ചങ്ങലമ്പരണ്ട (ഉണക്ക)', '(dried)', 'Hadjod', '5d452655eb5701564812885.jpg', NULL, 'Cissus quandrangularis', '150', 4, 1, '2019-06-12 11:23:49', '2019-08-27 11:57:48'),
(211, 'ചുരയില(പച്ച)', '[fresh]', 'Lauki, Doodhi Bhopala, Ghia and Kaddu', '5d45267ea36cc1564812926.jpg', NULL, 'Curcuma lagenaria', '2200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:58:00'),
(212, 'ചെമ്പരത്തിയില (പച്ച)', 'Shoe Flower [fresh]', 'Japapushpa', '5d8080ab39da01568702635.jpg', NULL, 'Hibiscus rosa-sinensis', '4300', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:13:55'),
(213, 'ചെറിയ ആടലോടകത്തില (പച്ച)', '[fresh]', 'Arusa pathra Thaza', '5d45281dc7c651564813341.jpg', NULL, 'Adhatoda beddomei', '30', 5, 1, '2019-06-12 11:23:49', '2019-08-27 11:58:46'),
(214, 'ചെറുപ്പിച്ചകത്തില (പച്ച) 1m-1.5m length', 'Jasmine  [fresh]', 'Jati', '5d8080ec6861e1568702700.jpg', NULL, 'Jasminum grandiflorum', '5000', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:15:00'),
(215, 'ചോനകപ്പുല്ല് (ഉണക്ക)', 'West Indian lemen grass [dried]', 'गंधत्रिण Gandhatrina', '5d4528536453e1564813395.JPG', NULL, 'Cymbopogan citratus', '500', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:05:11'),
(216, 'ഞാറകുരുന്ന് (പച്ച)', 'Jamoon  [fresh]', 'duhat, jam, jaman, jamun', '5d45286c0a1e51564813420.jpg', NULL, 'Syzygium cumini', '15', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:05:24'),
(217, 'താന്നിയില (പച്ച)', 'Belleric Myrobalan [fresh]', 'Bahera', '5d45287c4848c1564813436.jpg', NULL, 'Terminalia bellirica', '600', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:05:37'),
(218, 'താലീസപത്രം', 'Himalayam Silver Fir Leaves', 'Talispatra', '5d45288aa66761564813450.jpg', NULL, 'Abies webbiana', '1400', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:05:49'),
(219, 'തേക്കിന്‍ കുരുന്ന് (കൂമ്പ്) (പച്ച)', 'Teak Tree [fresh]', 'Sagon, Sagwan', '5d4528b04ad4c1564813488.jpg', NULL, 'Tectona grandis', '1100', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:06:00'),
(220, 'ദന്തപ്പാലയില', 'Pala indigo plant', 'कपार Kapar, Dudhi दुधी', '5d4528c0bbc4c1564813504.jpg', NULL, 'Wrightia tinctoria', '3800', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:06:17'),
(221, 'ദര്‍ഭപ്പുല്ല് (ഉണക്ക)', 'Sacrificial  grass  [dried]', 'Dab, Davoli', '5d4528dd483fd1564813533.jpg', NULL, 'Desmostachya bipinnata', '200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:06:28'),
(222, 'നാന്‍മുഖപ്പുല്ല് (ഉണക്ക)', 'Peacock’s tail [dried]', 'Mayursikha, Morpamkhi', '5d4528fe339ba1564813566.JPG', NULL, 'Actiniopteris dichotoma', '200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:48:49'),
(223, 'നീല ഉമ്മത്തിന്‍ ഇല (പച്ച)', 'Thorn apple [fresh]', 'Dattura', '5d45291eac5161564813598.jpg', NULL, 'Datura stramonium, Linn', '2200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 12:48:59'),
(224, 'നീലയമരിയില പച്ച', 'True Indigo (fresh)', 'Neeli pathra thaza', '5d8084ca707a51568703690.jpg', NULL, 'Indigofera tinctoria', '6500', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:31:30'),
(225, 'പനിക്കൂര്‍ക്കയില (പച്ച)', '[fresh]', 'Patharchur', '5d80851920b491568703769.jpg', NULL, 'Coleus aromatics', '700', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:32:49'),
(226, 'പാച്ചോറ്റിയില (പച്ച)', 'Lodh Tree [fresh]', 'Bholiya, Sodha', '5d45299194c211564813713.jpg', NULL, 'Symplocos  cochinchinensis', '100', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:55:08'),
(227, 'പുല്ലാനിയില (പച്ച)', '[fresh]', 'kokkarai', '5d808531c7eb41568703793.jpg', NULL, 'Calycopteris floribunda', '2200', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:33:13'),
(228, 'പുളിഞെരമ്പ് ഉണക്ക', 'Tamarind Tree (dried)', 'imli (इमली)', '5d4529e1c7bb01564813793.jpg', NULL, 'Tamarindus indicus', '3200', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:55:37'),
(229, 'പുളിയില പച്ച ( leaves only)', 'Tamarind (fresh)', 'imli (इमली)', '5d4529ef514f51564813807.jpg', NULL, 'Tamarindus indicus', '46000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:55:49'),
(230, 'പൂതണക്കപ്പുല്ല് (സംഭാര പുല്ല്)', 'Rusha  grass', 'Gandhabel', '5d452a150d8741564813845.jpg', NULL, 'Cymbopogon martinii', '10', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:56:09'),
(231, 'മാതളനാരകത്തില (പച്ച)', 'Pomegranate  [fresh]', 'Anaar, Dadimb', '5d8086610967e1568704097.jpg', NULL, 'Punica granatum', '600', 5, 1, '2019-06-12 11:23:49', '2019-09-17 12:38:17'),
(232, 'മാവില (ഉണക്ക)', 'Mango tree (dried)', 'Aam ka patha', '5d47eb38071d11564994360.jpg', NULL, 'Mangifera indica', '20', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:56:36'),
(233, 'മുരിങ്ങയില (പച്ച)', 'Drum Stick Tree [fresh]', 'Sahjan  (सहजन)', '5d452a368313f1564813878.jpg', NULL, 'Moringa oleifera', '15000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:56:50'),
(234, 'വെറ്റില പച്ച', 'Betel Leaf (fresh)', 'Pan ,Nagar-bel pathra', '5d452a61c87b61564813921.JPG', NULL, 'Piper betle', '15000', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:58:04'),
(235, 'ശീമപ്പച്ചില', 'Cassia  Cinnomon', 'Tej Patta (तेजपत्ता)', '5d452a6d726281564813933.jpg', NULL, 'Cinnamomum  tamala', '3500', 5, 1, '2019-06-12 11:23:49', '2019-08-27 14:58:18'),
(237, 'അത്തിമൊട്ട്  (ഉണക്ക)', 'Country Fig Tree [dried]', 'Gular ,umar', '5d452b8c28ac21564814220.JPG', NULL, 'Ficus racemosa', '2', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:03:50'),
(238, 'അരയാല്‍മൊട്ട്  (ഉണക്ക)', 'Sacred Fig [dried]', 'Ashwattha', '5d452bd2035621564814290.jpg', NULL, 'Ficus religiosa', '2', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:14:51'),
(239, 'ഇത്തിമൊട്ട്  (ഉണക്ക)', 'Ficus Lacor [dried]', 'Pakar,pakdi', '5d452be6e38181564814310.jpg', NULL, 'Ficus microcarpa', '2', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:15:06'),
(240, 'ഇരുപ്പപ്പൂവ് (ഉണക്ക)', 'Mahua Tree [dried]', 'महुआ', '5d452af12e1de1564814065.jpg', NULL, 'Madhuca longifolia', '1800', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:15:19'),
(241, 'ഇലഞ്ഞിപ്പൂവ് (ഉണക്ക)', 'Indian Medler [dried]', 'Maulsari मौलसरी', '5d452afe31e7a1564814078.jpg', NULL, 'Mimusops elengi', '5', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:15:30'),
(242, 'കരയാമ്പൂവ്', 'Cloves', 'Laung', '5d452b124e9291564814098.jpg', NULL, 'Syzygium aromaticum', '3000', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:15:43'),
(243, 'കുങ്കുമപൂവ്', 'Saffron', 'Kumkuma, Kashmira, Kesar, Zafran', '5d452c019db271564814337.jpg', NULL, 'Crocus sativus', '15', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:15:54'),
(244, 'ചെമ്പകമൊട്ട് (ഉണക്ക)', 'Champak,  golden  champa  [dried]', 'चम्पा', '5d452c187a65b1564814360.jpg', NULL, 'Michelia champaca', '3', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:16:06'),
(245, 'ചെമ്പരത്തിമൊട്ട് (പച്ച)', 'Hibiscus  rosasinensis  [fresh]', 'Japapushpa', '5d452c275e0271564814375.jpg', NULL, 'Hibiscus rosa-sinensis', '20', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:16:25'),
(246, 'ജാതിപത്രിക', 'Mace', 'जातीफलस्यत्वक्', '5d452c3b5bcd21564814395.jpg', NULL, 'Myristica  fragrans', '250', 6, 1, '2019-06-12 11:23:49', '2019-08-27 15:19:07'),
(247, 'ഞാഴല്‍ പൂവ്', 'Black  Plum', 'Priyangu, Daiya', '5d452c46190181564814406.png', NULL, 'Callicarpa  macrophylla', '1500', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:41:18'),
(248, 'തക്കോലപൊട്ടില്‍', 'Star Anise Fruit', 'badian khatai', '5d452c595fbbe1564814425.jpg', NULL, 'Illicium  verum', '700', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:41:33'),
(249, 'താതിരിപൂവ്', 'Fulses Flower (Dried)', 'Dawi, Santha, Dhaura', '5d452c860f5601564814470.jpg', NULL, 'Woodfordia fruiticosa', '15000', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:41:50'),
(250, 'താമരപ്പൂവ് (പച്ച)', 'Lotus flower [fresh]', 'Kanwal', '5d452c936885b1564814483.jpg', NULL, 'Nelumbo nucifera', '15', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:42:03'),
(251, 'താമരയല്ലി', 'Lotus Stamens', 'कमल Kamal', '5d452ca122c401564814497.jpg', NULL, 'Nelumbo nucifera', '800', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:42:21'),
(252, 'തുമ്പപ്പൂവ് (പച്ച)', 'Phlomis  [fresh]', 'Gophaa', '5d452cbf1d5a11564814527.jpg', NULL, 'Leucas aspera', '5', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:42:29'),
(253, 'തുളസിക്കതിര്‍ (ഉണക്ക)', 'Holy Basil [dried]', 'Tulsi', '5d808648559fc1568704072.jpg', NULL, 'Ocimum sanctum', '700', 6, 1, '2019-06-12 11:23:49', '2019-09-17 12:37:52'),
(254, 'തെങ്ങിന്‍ പൂക്കുല (പച്ച-മൂപ്പ് കുറഞ്ഞത്)', '[fresh]', 'नारियल Naariyal', '5d452cedeb10d1564814573.jpg', NULL, 'Cocos nucifera', '1700', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:42:46'),
(255, 'തെച്ചിപ്പൂവ് (പച്ച)', 'Wild Date Fruit [fresh]', 'Rugmini रुग्मिनी', '5d80868adb8ae1568704138.jpg', NULL, 'Ixora coccinea', '4500', 6, 1, '2019-06-12 11:23:49', '2019-09-17 12:38:58'),
(256, 'പനംകുല (പച്ച)', 'Fish tail palm, Elephant’s palm [fresh]', 'Mari', '5d452d2c4a0ec1564814636.jpg', NULL, 'Carvota urens', '600', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:43:23'),
(257, 'പനവാഴയ്ക്ക (ഉണക്ക)', 'Palmira Palm [dried]', 'Tila', '5d8086b26708d1568704178.jpg', NULL, 'Borassue formis', '75', 6, 1, '2019-06-12 11:23:49', '2019-09-17 12:39:38'),
(258, 'പശുപാശി', 'Pasupasi', 'Rampatri', '5d452d863b1291564814726.jpg', NULL, 'Myristica malabarica', '5', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:43:47'),
(259, 'പിച്ചകമൊട്ട് (പച്ച)', 'Jasmine  [fresh]', 'Jati', '5d452d96ad4441564814742.jpg', NULL, 'Jasminum grandiflorum', '50', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:43:56'),
(260, 'പുന്നപ്പൂവ്  (ഉണക്ക)', 'Alexandrian laurel [dried]', 'सुलतान चम्पा Sultan Champa', '5d452da6347ec1564814758.jpg', NULL, 'Calophyllum inophyllum', '150', 6, 1, '2019-06-12 11:23:49', '2019-08-28 16:44:07'),
(261, 'പേരാല്‍ മൊട്ട്  (ഉണക്ക)', 'Banyan Tree [dried]', 'Barh', '5d8086eee71b31568704238.jpg', NULL, 'Ficus benghalensis', '2', 6, 1, '2019-06-12 11:23:49', '2019-09-17 12:40:38'),
(262, 'ശീമ നാഗ പൂവ്', 'Lotus stamens', 'Naga champa', '5d8087078f6ec1568704263.jpg', NULL, 'Mesua ferea', '2800', 6, 1, '2019-06-12 11:23:49', '2019-09-17 12:41:03'),
(264, 'അക്ലാരി തേങ്ങ', 'Double  Coconut', 'Darya-Ka-Naryal, दिरया का नािरयल', '5d452e1e0e21e1564814878.jpg', NULL, 'Lodoicea  maldivica', '2', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:50:07'),
(265, 'അഗശിക്കുരു', 'Linseed/Flax  seed', 'अलसी Alsi', '5d452ec7d42981564815047.jpg', NULL, 'Linum usitatissimum', '900', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:54:58'),
(266, 'അത്തിത്തിപ്പലി', 'Elephant Piper', 'Sphodyabhujanga', '5d452e498f1f51564814921.jpg', NULL, 'Rhaphidophora pertusa', '1000', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:55:13');
INSERT INTO `raw_materials` (`raw_material_id`, `title`, `title_en`, `title_hn`, `image`, `size`, `botanical_name`, `quantity`, `raw_type`, `status`, `date_added`, `date_modified`) VALUES
(267, 'അയമോദകം', 'Bishopweed', 'अजमॊड़ Ajmod', '5d4bf40a93c9e1565258762.jpg', NULL, 'Apium Leptophyllum', '12000', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:55:51'),
(268, 'അരേണുകം', NULL, 'Myrole', '5d4531d566ff51564815829.jpg', NULL, 'Piper  aurantiacum', '350', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:56:03'),
(269, 'അവില്‍', 'Flattened  Rice', 'Poha', '5d4531f45dcc31564815860.jpg', NULL, NULL, '100', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:56:14'),
(270, 'ആര്യവേപ്പിന്‍ കായ', 'Neem  Seed', 'Neem', '5d45320b57bd01564815883.jpg', NULL, 'Azadirachta Indica', '20', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:56:29'),
(271, 'ആശാളി', 'Creess  Seeds', 'hansur, Halim, Haloon, Chandrasur', '5d45323a1972b1564815930.jpg', NULL, 'Lepidium sativum', '400', 7, 1, '2019-06-12 11:23:49', '2019-08-28 16:58:06'),
(272, 'ഇളനീര്‍(Nos) (Min.1.750Kg)', 'Tender  Coconut  (Nos.)', 'नारियल Naariyal', '5d453260734d01564815968.jpg', NULL, 'Cocosnucifera', '8600', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:05:36'),
(273, 'ഈന്തപ്പഴം', 'Edible Date', 'Khajoor', '5d80873e2f6241568704318.jpg', NULL, 'Phoenix dactylifera', '5', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:41:58'),
(274, 'ഉങ്ങിന്‍ കുരു', 'Indian Beach', 'Karanj', '5d47eae0881911564994272.jpg', NULL, 'Pongamia pinnata', '75', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:06:04'),
(275, 'ഉങ്ങിന്‍ കുരു പരിപ്പ്', 'Indian Beach', 'Karanj', '5d47eaf109e6f1564994289.jpg', NULL, 'Pongamia pinnata dried coty ledon', '500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:06:21'),
(276, 'ഉണക്കലരി', 'Kanjikam', 'Dhan, Chaval ', '5d80875717dbc1568704343.jpg', NULL, 'Oriza sativa', '325', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:42:23'),
(277, 'ഉലുവ', 'Fenu Greek Seed', 'methi', '5d45343255cd91564816434.jpg', NULL, 'Trigonella  foenum-graeceum', '4500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:07:08'),
(278, 'ഉഴുന്ന്', 'Black  gram', 'urad dal', '5d4bf4410e6621565258817.jpg', NULL, 'Vigna  mungo', '5500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:07:19'),
(279, 'എള്ള് (നല്ലയിനം കറുത്തത്)', 'Seasamum', 'Tila', '5d45335a19dea1564816218.jpg', NULL, 'Seasamum indicum', '75', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:07:33'),
(280, 'ഏലക്കായ', 'Cardamom', 'Chhoti elaichi', '5d4534875914e1564816519.jpeg', NULL, 'Elettaria cardamomum', '7000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:07:44'),
(281, 'ഏലാവാലുകം', 'Bird  cherry', 'paṭmakāṭh, paṭmākh, paḍḍam', '5d47ec1f5bd7f1564994591.jpg', NULL, 'Prunus avium', '600', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:08:01'),
(282, 'കടുക്ക', 'Chebulic Myrobalan', 'Harad', '5d4534aadc2a81564816554.jpg', NULL, 'Terminalia chebula', '22000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:08:15'),
(283, 'കടുക്കത്തോട്', 'Chebulic Myrobalan', 'Harad', '5d4534d14d2301564816593.jpg', NULL, 'Terminalia chebula  (Fruit rind)', '84000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:08:27'),
(284, 'കദളിപ്പഴം', 'Plantain', 'केला जंगली', '5d80879208b631568704402.jpg', NULL, 'Musa paradisica', '100', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:43:22'),
(285, 'കര്‍ക്കിടക ശൃംഗി', 'Galls', 'Kakra sringi', '5d808786073de1568704390.jpg', NULL, 'Pistaacia  Chinensis', '500', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:43:10'),
(286, 'കരിംജീരകം', 'Black Cumine', 'मंगरैल maṃgarail', '5d453536952311564816694.jpg', NULL, 'Nigella sativa', '5500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 11:11:07'),
(287, 'കറുത്തകടുക്', 'Mustard Black', 'Rai', '5d453559bf9d31564816729.jpg', NULL, 'Brassica juncea', '10000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:18:18'),
(288, 'കറുത്തമുന്തിരിങ്ങപഴം', 'Black Grapes (With seed & sweet taste)', 'Draksha', '5d4535763a3a11564816758.jpg', NULL, 'Vitis vinifera', '29000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:18:28'),
(289, 'കഴഞ്ചിക്കുരു പരിപ്പ്', 'Bondue Nuts', 'Kandkeraj, Kantikaranja', '5d453585cb3f71564816773.jpg', NULL, 'Caesalpinia bonduc', '10', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:18:46'),
(290, 'കാഞ്ഞിരക്കുരു', 'Nux Vomica', 'Kuchala', '5d4535c5d9bed1564816837.png', NULL, 'Strychnos nux Vomica', '5', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:19:02'),
(291, 'കാട്ടുജീരകം', 'Purple fleebane', 'Kaleejeera, Bakshi', '5d4535d55afd41564816853.jpg', NULL, 'Centratherum anthelminticum', '3300', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:19:15'),
(292, 'കാര്‍കോകിലരി', 'Psoralea  Seeds', 'Bemchi, Bawchi, Babachi.', '5d4535e6e32531564816870.jpg', NULL, 'Psoralea  corylifolia', '1500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:19:28'),
(293, 'കുടകപാലയരി', 'Kurchi', 'Kura, Kora, Kureya, Kurchi', '5d8087d236cfc1568704466.jpg', NULL, 'Holarrhena antidysenterica', '2500', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:44:26'),
(294, 'കുന്നിക്കുരു', 'Red Beed Tree', 'गुंजा', '5d453635bf9b31564816949.jpg', NULL, 'Abrus precatorious', '125', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:22:17'),
(295, 'കുമ്പളങ്ങ (ഒരു കൊല്ലം പഴകിയത്)', 'Ash Guard [one year old]', 'पेठा Petha, पेठाकद्दू', '5d4bf478666c71565258872.jpg', NULL, 'Benincasa cerifera', '4000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:22:34'),
(296, 'കുമിഴിന്‍ പഴം', 'White Teak', 'gamhar, khamara, khumbhari, sewan', '5d45369fbcbd81564817055.jpg', NULL, 'Gmelina arborea', '400', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:22:48'),
(297, 'കുരുമുളക്', 'Black Pepper', 'Kalimirch', '5d4536b3535d31564817075.jpg', NULL, 'Piper nigrum', '15000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:23:40'),
(298, 'കുറാശ്ശാണി', 'Henbane', 'Khurasani ajavayan', '5d4536c8092c41564817096.jpg', NULL, 'Hyoscyamus  niger', '10', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:23:52'),
(299, 'കൂവളക്കായ ചോറ് (പച്ച ഇളയത്)', 'Bael Tree [fresh]', 'Bel', '5d4536dd745c71564817117.jpg', NULL, 'Aegle marmelos', '5', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:24:04'),
(300, 'കൊത്തമ്പാലയരി (മല്ലി)', 'Coriander Seeds', 'धनिया Dhaniya', '5d45370c92a621564817164.jpg', NULL, 'Coriandrum sativum', '12000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:24:18'),
(301, 'ചണം പയര്‍ (മസൂര്‍ പരിപ്പ്)', 'Masoor dal', 'Masoor', '5d453726643ef1564817190.jpg', NULL, 'Lensculinaris', '125', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:24:29'),
(302, 'ചീനത്തിപ്പല്ലി', 'Long Pepper', 'Pipal, Pipar', '5d453742c67161564817218.jpg', NULL, 'Piper longum', '27500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:24:42'),
(303, 'ചെറുത്തിപ്പല്ലി', 'Long pepper', 'Pippali', '5d4537540c1411564817236.jpg', NULL, 'Piper longum', '3600', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:24:52'),
(304, 'ചെറുനാരങ്ങ (Nos) Min.75 gm', 'Lemon (nos) Min. 75 gm', 'Turanj', '5d453773aac161564817267.jpg', NULL, 'Citrus limon', '240000', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:25:10'),
(305, 'ചെറുനാരങ്ങ (ഉണക്ക)(കി.ഗ്രാം)', 'Lemon [dried] Kg.', 'Turanj', '5d453784c7d3c1564817284.jpg', NULL, 'Citrus limon', '25', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:25:22'),
(306, 'ചെറുപയര്‍', 'Greem  Gram', 'मूँग MUNG', '5d4537b8b283b1564817336.jpg', NULL, 'Phaseolus   radiatus', '500', 7, 1, '2019-06-12 11:23:49', '2019-08-29 16:25:33'),
(307, 'ചെറുപുന്നയരി', 'Balloon  Wine', 'Kondgaidh, Malkangni, Sankhu', '5d4537b5cb1701564817333.jpg', NULL, 'Celastrus paniculatus', '400', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:48:58'),
(308, 'ചേര്‍ക്കുരു', 'Marking Nut', 'Belathak', '5d808810ef6141568704528.jpg', NULL, 'Semicarpus anacardium', '30', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:45:28'),
(309, 'ജാതിയ്ക്ക', 'Nutmeg', 'Jaiphal, Jayapatri, Javitri', '5d4537f28e7171564817394.jpg', NULL, 'Myristica  fragrans', '900', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:50:04'),
(310, 'ജീരകം', 'Cummin seeds', 'Jeera', '5d453835c07401564817461.jpg', NULL, 'Cuminum cyminum', '18000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:50:16'),
(311, 'ഞാവല്‍ക്കുരു പരിപ്പ്', 'Black  plum', 'JAMAN', '5d453835918191564817461.jpg', NULL, 'Syzygium cumini', '750', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:50:27'),
(312, 'ഞെരിഞ്ഞിൽ', 'Small Caltrops', 'Gokhru', '5d4538663c6201564817510.jpg', NULL, 'Tribulus terrestris', '24000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:50:39'),
(313, 'താന്നിക്കുരു പരിപ്പ്', 'Belleric Myrobalan (Seed)', 'Bahera', '5d4538410045f1564817472.jpg', NULL, 'Terminalia bellirica', '10', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:50:52'),
(314, 'താന്നിക്കത്തോട് (തൊണ്ട്)', 'Bellerica Myrobalan', 'Bahera', '5d45386f3d4dc1564817519.jpg', NULL, 'Terminalia bellirica              (Fruit rind)', '31000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:51:06'),
(315, 'താമര കുരു പരിപ്പ്', 'Sacred lotus nut', 'Kamala beej', '5d4538897febd1564817545.jpg', NULL, 'Nelumbo nucifera', '5', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:51:16'),
(316, 'തിനയരി', 'Great Millet', 'Kala kangini', '5d453fb9c558e1564819385.jpg', NULL, 'Setaria italica', '100', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:51:26'),
(317, 'തുമ്പൂണലരി', NULL, 'Ishadgola', '5d453fd33612e1564819411.jpg', NULL, 'Zanthoxylum armatum', '100', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:51:39'),
(318, 'തുവരപരിപ്പ്', 'Pegion Pea', 'Arhar or Rahar  or Tur  or Tuar', '5d453feef18211564819438.jpg', NULL, 'Cajanus  cajan', '2', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:53:19'),
(319, 'തേറ്റാമ്പരൽ', 'Clearing Nut', 'Nirmali', '5d454008d11941564819464.jpg', NULL, 'Strychnos potatorum', '6200', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:53:30'),
(320, 'നായ് കുരണ പരിപ്പ്', 'velvet bean', 'Kiwanch or Konch', '5d454031e0a711564819505.jpg', NULL, 'Mucuna pruriens', '25', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:53:55'),
(321, 'നാളികേരം(പഴുത്തത്)  Wt500GM  (NOS)', 'Coconut (Min.500g wt) Nos', 'NARIYAL', '5d45404c1ccab1564819532.jpg', NULL, 'Cocos nucifera', '56000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:54:18'),
(322, 'നീല ഉമ്മത്തിന്‍ കുരു', 'Thorn apple seed', 'Nili', '5d4540783997e1564819576.jpg', NULL, 'Daturs metal seed', '60', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:54:30'),
(323, 'നെല്ല്', 'Paddy', 'CHAVAL', '5d8088639f4bf1568704611.jpg', NULL, 'Oryza sativa', '3', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:46:51'),
(324, 'നെല്ലിക്കാത്തോട്', 'Emblic Myrobalan', 'aamla (आमला)', '5d4540b2575851564819634.jpg', NULL, 'Emblica Officinails', '58000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:55:22'),
(325, 'പച്ചനെല്ലിക്ക', 'Emblic Myrobalans(fresh)', 'Amla', '5d4540c5e3bd11564819653.jpg', NULL, 'Emblica officinalis', '41000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:55:35'),
(326, 'പരുത്തിക്കുരു', 'Indian Cotton Seed', 'Karpasa', '5d4540de4dfac1564819678.jpg', NULL, 'Gossypium  herbaceum', '3100', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:55:49'),
(327, 'പഴയ മുതിര', 'Horse grain', 'Kulath', '5d454113423581564819731.jpg', NULL, 'Dolichos   biflorus', '7300', 7, 1, '2019-06-12 11:23:49', '2019-08-30 09:59:12'),
(328, 'പഴയപുളി', 'Old Tamarind', 'Imli', '5d808887d1c031568704647.jpg', NULL, 'Tamarindus indicus', '250', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:47:27'),
(329, 'പഴുക്കടക്ക (മൊരി കളഞ്ഞത്)', 'Arecanut  Palm', 'Supari', '5d8088f32076a1568704755.jpg', NULL, 'Areca catechu', '500', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:49:15'),
(330, 'പുല്ലാനിക്കായ', NULL, 'Kokkarai', '5d4541544f10d1564819796.jpg', NULL, 'Calycoteris florbunda', '5', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:00:08'),
(331, 'പെരുംജീരകം', 'Fennel', 'Sanuf, Bari-Sanuf', '5d454171e68fb1564819825.jpg', NULL, 'Foeniculum vulgare', '300', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:00:23'),
(332, 'പൊന്നാന്തകരക്കുരു', 'Foetid  Cassia', 'Charota,Chakvad,Chakavat.', '5d45418e037911564819854.jpg', NULL, 'Cassia tora', '3', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:00:36'),
(333, 'പേരേലം', 'Large  Cardamom', 'Badi ilyachi, Bari ilyachi', '5d454196b353a1564819862.jpg', NULL, 'Amomum  subulatum', '175', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:00:47'),
(334, 'ബദാം പരിപ്പ്', 'Sweer Almonds', 'BADAM', '5d4541a2ca4b21564819874.jpg', NULL, 'Prunus dulcis', '5', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:00:57'),
(335, 'ബ്ലാങ്കായപ്പരിപ്പ് (ചോറുണങ്ങിയത്)', 'Wood Apple', 'Kowit', '5d808916544f31568704790.jpg', NULL, 'Feronia elephantum', '3000', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:49:50'),
(336, 'മലങ്കാരപരിപ്പ്', 'Emetic  Nut', 'Mainphal, Madan', '5d45436979aee1564820329.jpg', NULL, 'Randia  dumetorum', '250', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:01:45'),
(337, 'മലര്‍', 'Parched  Paddy', 'खील, भुना हुआ चावल', '5d454292f0b9b1564820114.jpg', NULL, NULL, '300', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:03:13'),
(338, 'മാങ്ങയണ്ടിപരിപ്പ് (ഉണക്ക)', 'Mango Seed Kernal [dried]', 'Aam beej', '5d454286a48851564820102.jpg', NULL, 'Mangifera indica', '1000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:03:24'),
(339, 'മാതള നാരങ്ങ (ഉണക്ക)', 'Pomegranate  [dried]', 'Anaar, Dadimb', '5d4544d6540101564820694.jpg', NULL, 'Punica granatum', '125', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:03:34'),
(340, 'മാതള നാരങ്ങ (പഴുത്തത്)', 'Pomegranate', 'Anaar, Dadimb', '5d4544eda7a901564820717.jpg', NULL, 'Punica granatum', '3300', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:03:44'),
(341, 'മാതളനാരങ്ങത്തോട്', 'Pomegranate', 'Anar', '5d4544f1ddfe71564820721.jpg', NULL, 'Punica  granatum', '1200', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:03:53'),
(342, 'മായാക്ക്', 'Oak Galls', 'Majuphal', '5d454510b6e751564820752.jpg', NULL, 'Quercus infectoria', '10', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:04:05'),
(343, 'മുരിങ്ങക്കുരു (ഉണക്ക)', 'Drum Stick Tree [dried]', 'Sahjan  (सहजन)', '5d45459abea001564820890.jpg', NULL, 'Moringa olefera', '125', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:04:15'),
(344, 'യവം', 'Barley', 'Jav, Jau', '5d4bf4a7bf55d1565258919.jpeg', NULL, 'Hordeum vulgare', '21000', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:04:24'),
(345, 'രുദ്രാക്ഷം', 'Rosery Nut', 'Rudarki, Rudraks', '5d4545a623b581564820902.jpg', NULL, 'Elaeocarpus sphaericus', '75', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:04:33'),
(346, 'ലന്തക്കുരു', 'Jujube Fruit', 'बेर ber', '5d4545b7224b51564820919.jpg', NULL, 'Zizyphus  mauritiana', '2800', 7, 1, '2019-06-12 11:23:49', '2019-08-30 10:04:44'),
(347, 'വട്ടത്തകര കുരു', 'Sickle senna, wild senna', 'Chakavad ,Pavanad beej', '5d4546041c41b1564820996.png', NULL, 'Cassiafora', '15', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:36:32'),
(348, 'വട്ടപ്പൂന്താളിയരി', 'purple moonflower', 'Kaladana', '5d45460fc01cc1564821007.jpg', NULL, 'Acalypha hispida', '15', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:36:52'),
(349, 'വന്‍കൊട്ടക്കിഴങ്ങ്/കായ', 'Water  chestnut', 'Singhara', '5d808a841350c1568705156.jpg', NULL, 'Trapanatans', '5', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:55:56'),
(350, 'വയല്‍ ചുള്ളി വിത്ത്', 'Marsh Barbel', 'Talimakhana', '5d808a8a009ec1568705162.jpg', NULL, 'Hygrophila auriculata', '15', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:56:01'),
(351, 'വരകിനരി', 'Common  Millet', 'Khododhan', '5d45465a0eab31564821082.jpg', NULL, 'Paspalum scrobiculatum', '100', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:38:07'),
(352, 'വാൽമുളക്‌', 'cubeb Pebber', 'Kabab chini', '5d45467aa6f671564821114.jpg', NULL, 'Piper cubeba', '500', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:38:18'),
(353, 'വിഴാലരി', 'Babrieng', 'Baberang, Vayvidang', '5d4bf4aba91c31565258923.jpg', NULL, 'Embelia ribes', '4600', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:38:29'),
(354, 'വെള്ള കശകശ', 'Opium  poppy  seeds', 'Khash-Khash', '5d808ae06b4681568705248.jpg', NULL, 'Papaver somniferum', '1', 7, 1, '2019-06-12 11:23:49', '2019-09-17 12:57:28'),
(355, 'വെളുത്ത കടുക്', 'White  Mustard', 'Safed Rai', '5d45471d8c57e1564821277.jpg', NULL, 'Brassica  alba', '325', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:38:51'),
(356, 'ശതകുപ്പ', 'Dilseed', 'Sotapa, Sowa', '5d45472fdd5ac1564821295.jpg', NULL, 'Anethum  Graveolens', '5200', 7, 1, '2019-06-12 11:23:49', '2019-08-30 11:39:23'),
(358, 'അത്തിത്തൊലി (ഉണക്ക)', 'Country Fig Tree [dried]', 'Rumbodo', '5d454746de27f1564821318.jpg', NULL, 'Ficus glomerata', '600', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:18:00'),
(359, 'അമ്പഴത്തൊലി (ഉണക്ക)', 'Wild Mango [dried]', 'Jangli aam, Amra', '5d454764bfebc1564821348.jpg', NULL, 'Spondias pinnata', '250', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:24:32'),
(360, 'അരയാല്‍ത്തൊലി (ഉണക്ക)', 'Sacred Fig [dried]', 'Peepal', '5d45478d94af51564821389.jpg', NULL, 'Ficus religiosa', '600', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:24:48'),
(361, 'അശോകത്തൊലി (ഉണക്ക)', 'Ashok Tree [dried]', 'Ashok', '5d4547972dd191564821399.jpg', NULL, 'Saraca asoka', '4300', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:01'),
(362, 'ആര്യവേപ്പിന്‍ തൊലി (ഉണക്ക)5” Width & 36” length', 'Indian liac Margosa tree', 'Nim, Nimb', '5d4547a4efa1f1564821412.jpg', NULL, 'Azadirachta indica', '30000', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:11'),
(363, 'ആവല്‍ പട്ട (ഉണക്ക)5” Width & 36” length', 'Indianelm, Kanju [dried]', 'Kanju, Papri, Banchilla, Chilbil, Dhamna', '5d4547e679e4a1564821478.jpg', NULL, 'Holoptelea integrifolia', '12000', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:22'),
(364, 'ഇത്തിത്തൊലി (ഉണക്ക)', 'Ficus Laco [dried]', 'Pakar,Pakadi', '5d4547f4ab9751564821492.jpg', NULL, 'Ficus microcarpa', '700', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:33'),
(365, 'ഉങ്ങിൻ തൊലി പച്ച (Size 5\" )', 'Indian Beach (fresh)', 'Karanj', '5d4548126e2151564821522.jpg', NULL, 'Pongamia glabra', '15000', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:44'),
(366, 'ഉങ്ങിന്‍ത്തൊലി (ഉണക്ക)', 'Indian Beach [dried]', 'Shaitanki,Chatwan', '5d45482cc34f51564821548.jpg', NULL, 'Pongamia pinnata', '1500', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:25:55'),
(367, 'എഴിലംപാലത്തൊലി (ഉണക്ക)', 'Dita Birk [dried]', 'Satvan,Chativan', '5d454841bf38d1564821569.jpg', NULL, 'Alstonia scholaris', '175', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:26:06'),
(368, 'കണിക്കൊന്ന തൊലി ഉണക്ക', 'Purging fistula (dried)', 'Amaltas, Bandarlauri', '5d4548bbdf6441564821691.jpg', NULL, 'Cassia fistula', '5500', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:32:02'),
(369, 'കമ്പിപ്പാലത്തൊലി (ഉണക്ക)', 'Kamela  Rottlera[dried]', 'Kamala, Kampillaka, Kapila', '5d47ecdb7df751564994779.jpg', NULL, 'Mallotus philippinensis', '25', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:32:13'),
(370, 'കരവീരത്തിന്‍ത്തൊലി (ഉണക്ക)', 'Oleander [dried]', 'কবীৰৈ Kabirei', '5d47ece6ddf1e1564994790.jpg', NULL, 'Nerium oleander', '20', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:32:25'),
(371, 'കരിവേലപ്പട്ട (ഉണക്ക)', 'Black babool, Indian gum arabic tree [dried]', 'Babool', '5d809f159acd31568710421.jpg', NULL, 'Acacia nilotica', '13000', 8, 1, '2019-06-12 11:23:49', '2019-09-17 14:23:41'),
(372, 'കുടകപ്പാലത്തൊലി (ഉണക്ക)', 'Kurchi [dried]', 'Kura, Kora, Kureya, Kurchi', '5d4549084ebc51564821768.jpg', NULL, 'Holarrhena antidysenterica', '1700', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:06'),
(373, 'ചുവന്ന മന്ദാരത്തിന്‍ത്തൊലി (ഉണക്ക)', 'Orkid Tree [dried]', 'Kachnar', '5d47ed175fa341564994839.jpg', NULL, 'Bauhinia variegata', '75', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:15'),
(374, 'ഞാറത്തൊലി (ഉണക്ക)5” Width & 18” length', 'Jamoon [dried]', 'jamun', '5d454a112137f1564822033.jpeg', NULL, NULL, '1100', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:25'),
(375, 'ഞാവല്‍ത്തൊലി (ഉണക്ക)5” Width & 18” length', 'Black plum [dried]', 'जामुन Jamun', '5d454a2e2f1481564822062.jpg', NULL, 'Syzygium cumini', '1000', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:37'),
(376, 'തകിട്ടുവെമ്പാട തൊലി (ഉണക്ക)', 'Alkanet', 'Ratanjot, Laljari', '5d454a59bd3741564822105.jpeg', NULL, 'Ventilago maderaspatna', '400', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:47'),
(377, 'താന്നിത്തൊലി (പച്ച)', 'Belliric myrobulan [fresh]', 'Bahera', '5d454a7d5b1e31564822141.jpg', NULL, 'Terminalia bellirica', '300', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:34:58'),
(378, 'തേക്കിന്‍ത്തൊലി (ഉണക്ക)5” Width & 18” length', 'Teak Tree [dried]', 'Sagon, Sagwan', '5d454aae95a4f1564822190.jpg', NULL, 'Tectona grandis', '200', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:35:09'),
(379, 'നീര്‍മരുതിന്‍ത്തൊലി (ഉണക്ക)', 'Terminalia arjuna [dried]', 'Arjun, Kahu', '5d454abd8fb321564822205.png', NULL, 'Terminalia arjuna', '4800', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:35:26'),
(380, 'നീര്‍മാതളത്തൊലി (ഉണക്ക)', 'Three leaved caper [dried]', 'Barun,Barna', '5d454afe619e51564822270.jpg', NULL, 'Crataeva nurvala', '100', 8, 1, '2019-06-12 11:23:49', '2019-08-30 12:35:36'),
(381, 'നെന്‍മേനിവാകത്തൊലി (ഉണക്ക)5” Width & 18” length', 'Sirisa Tree [dried]', 'Saras सरस', '5d47ed4e12cef1564994894.jpg', NULL, 'Albizia lebbeck', '200', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:20:59'),
(382, 'പറങ്കിപ്പട്ട', 'Parangi Bark', 'काजू Kaju', '5d47ed68cdf4d1564994920.jpg', NULL, 'Anacardium occidentale', '2', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:21:10'),
(383, 'പ്ലാശിന്‍ തൊലി  (ഉണക്ക)1” width & 18” length', 'Bastard Teak [dried]', 'Dhak, plash', '5d47ee0f880aa1564995087.jpg', NULL, 'Butea monosperma', '325', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:21:19'),
(384, 'പാച്ചോറ്റിതൊലി ഉണക്ക', 'Lodh Tree (dried)', 'Bholiya, Sodha', '5d454b859bf6e1564822405.jpg', NULL, 'Symplocos racemosa', '11000', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:21:31'),
(385, 'പുളിത്തൊലി  (ഉണക്ക)', 'Tamarind [dried]', 'imli', '5d454c2edb6021564822574.jpg', NULL, 'Tamarindus indicus', '100', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:21:47'),
(386, 'പേരാലിന്‍ത്തൊലി  (ഉണക്ക)5” width & 18” length', 'Banyan Tree [dried]', 'Bargad', '5d454c52416441564822610.jpg', NULL, 'Ficus benghalensis', '700', 8, 1, '2019-06-12 11:23:49', '2019-08-30 14:22:01'),
(387, 'മരമഞ്ഞൾതൊലി ഉണക്ക', 'Indian Barberry', 'Jhar Haldi', '5d454c6a8d26c1564822634.jpg', NULL, 'Coscinium fenestratum', '10500', 8, 1, '2019-06-12 11:23:49', '2019-08-30 15:09:45'),
(388, 'മലയകത്തിത്തൊലി  (ഉണക്ക)', '[dried]', 'Kachnar, Kaniar', '5d454c9059d2c1564822672.jpg', NULL, 'Bauhinia variegata', '5', 8, 1, '2019-06-12 11:23:49', '2019-08-30 15:13:01'),
(389, 'മാവിന്‍തൊലി (ഉണക്ക)', 'Mangifera indica [dried]', 'Aam twak', '5d809f681d4901568710504.jpg', NULL, 'Mangifera indica', '3', 8, 1, '2019-06-12 11:23:49', '2019-09-17 14:25:04'),
(390, 'മുരിങ്ങപ്പട്ട  (ഉണക്ക)', 'Drum Stick [dried]', 'Senjana सेंजन', '5d809f6ebd5bd1568710510.jpg', NULL, 'Moringa oleifera Lam', '2500', 8, 1, '2019-06-12 11:23:49', '2019-09-17 14:25:10'),
(391, 'മുരുക്കിന്‍പട്ട (ഉണക്ക)', 'Indian Coral Tree [dried]', 'Pangara  sookha पंगार', '5d454ce1156b51564822753.jpg', NULL, 'Erythrina variegata', '50', 8, 1, '2019-06-12 11:23:49', '2019-08-30 15:13:55'),
(392, 'മുരുക്കിന്‍പട്ട (പച്ച)', 'Indian Coral Tree [fresh]', 'Pangara पंगार', '5d4bf4f3c10971565258995.jpg', NULL, 'Erythrina variegata', '150', 8, 1, '2019-06-12 11:23:49', '2019-08-30 15:14:05'),
(393, 'ശീമഇലവംഗതൊലി ഉണക്ക', 'Silk Cotton Tree', 'Dalchini', '5d80a04216ee61568710722.jpg', NULL, 'Cinnamomum Zeylanicum', '4600', 8, 1, '2019-06-12 11:23:49', '2019-09-17 14:28:42'),
(395, 'ആര്യവേപ്പിന്‍ കാതല്‍ (ഉണക്ക)18” length & 11” diameter', 'Margosa Tree, Indian lilac [dried]', 'Nim, Nimb', '5d47f3fe76d321564996606.jpg', NULL, 'Azadirachta indica', '50', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:14:26'),
(396, 'ഇരവൂളിന്‍ കാതല്‍ 18” length & 11” diameter', NULL, 'Jambu', '5d454d30d431a1564822832.jpg', NULL, 'Xylia  xylocarpa', '100', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:14:42'),
(397, 'ഇരുപ്പ കാതല്‍', 'Mahua Tree', 'महुआ', '5d47f583368891564996995.jpg', NULL, 'Madhuka  longifolia', '1500', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:14:52'),
(398, 'കരിങ്ങാലിക്കാതല്‍18” length & 11” diameter', 'Catechu Tree', 'Khair', '5d454d66c1b241564822886.jpg', NULL, 'Acacia catechu', '15500', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:15:04'),
(399, 'കാരകില്‍', 'Eagle  Wood', 'Agar', '5d454e9689cc91564823190.jpg', NULL, 'Aquilaria agallocha', '10000', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:16:55'),
(400, 'ചരളം 18” length & 11” diameter', 'Longleaved Pine', 'Chir', '5d454e9c61a141564823196.jpg', NULL, 'Pinus  roxburghie', '1400', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:17:07'),
(401, 'ദേവതാരം', 'Deodar', 'Dedwar, Deodar', '5d454eab911f11564823211.jpg', NULL, 'Cedrus deodara', '39000', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:17:38'),
(402, 'പതിമുഖം കാതല്‍ (ഉണക്ക)18” length & 11” diameter', 'Sappan wood, Bazil wood [dried]', 'Baka, Mal', '5d454ebb6ba2d1564823227.jpg', NULL, 'Caesalpinia Sappan', '8800', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:18:13'),
(403, 'പീനാറി (ഉണക്ക)  18” length & 11” diameter', 'Pinari [dried]', 'jamglibadam', '5d47e45a3faad1564992602.jpg', NULL, 'Sterculia foetida', '500', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:18:41'),
(404, 'പൈന്‍വൃക്ഷ കാതല്‍ (ഉണക്ക)', 'White Damat Tree [dried]', 'Badasal', '5d47e47799c3a1564992631.jpg', NULL, 'Vateria indica', '50', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:18:53'),
(405, 'രക്തചന്ദനം   Upto 5\" Length', 'Red Sandal Wood', 'Lalchandan', '5d454ef58b1271564823285.JPG', NULL, 'Pterocarpus santalinus', '20000', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:19:03'),
(406, 'വേങ്ങാക്കാതല്‍ (ഉണക്ക)18” length & 11” diameter', 'Indian Kino Tree [dried]', 'Vijaysar', '5d454f17d9cb91564823319.jpg', NULL, 'Pterocarpusmarsupium', '9500', 9, 1, '2019-06-12 11:23:49', '2019-08-30 15:19:14'),
(408, 'കന്മദം', 'Asphaltum', 'Shilajit', '5d45503a555091564823610.jpg', NULL, 'Asphaltum', '2200', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:19:26'),
(409, 'കാത്ത്', 'Kath', 'Khair', '5d4550bbdbb2b1564823739.jpg', NULL, 'Acacia catechu (extract)', '20', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:19:36'),
(410, 'കൂവ്വനൂറ് ഉണങ്ങിയത്', 'Arrow Root Powder', 'Tikhor, Tikkor', '5d45516d52da71564823917.jpg', NULL, 'Arrow Root', '5000', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:20:32'),
(411, 'കൊക്കോപൊടി (ISI  BRAND)', NULL, 'Cocoa', '5d455160b3ea41564823904.jpg', NULL, 'Coco Powder', '10', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:20:47'),
(412, 'ചെഞ്ചല്യം', 'Sal Tree Resin', 'साल Sal', '5d4551ea8e4dd1564824042.jpg', NULL, 'Shorea robusta', '11000', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:20:55'),
(413, 'മുളമുത്ത്', 'Bamboo Manna', 'Bambu,Baans', '5d45516e8ccf21564823918.jpg', NULL, 'Bambusa  arundiacum', '500', 10, 1, '2019-06-12 11:23:49', '2019-08-30 15:21:03'),
(415, 'ഇരുവിക്കറ', 'Hirvi', NULL, '5d4550fabd0f61564823802.jpg', NULL, NULL, '2', 11, 1, '2019-06-12 11:23:49', '2019-08-03 14:46:42'),
(416, 'ഇലവിന്‍ പശ', 'Silk Cotton, Trees, Gum', 'Salmali dhoona', '5d455181303461564823937.jpg', NULL, 'Bombax malabaricum', '800', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:21:21'),
(417, 'കോലരക്ക്', 'Lacca', 'Lakh', '5d4551cd16ef41564824013.jpg', NULL, 'Coccus  lacca', '1700', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:21:31'),
(418, 'ഗുൽഗുലു', 'Guggul resin', 'Shatawari, Shatamuli', '5d4551d9010761564824025.jpg', NULL, 'Commiphora wightii', '12000', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:21:40'),
(419, 'ചെന്നിനായകം', 'Ext. Aloe', 'घृतकुमारी,Dridhkumari', '5d4551471f2dc1564823879.jpg', NULL, 'Aloe vera', '1000', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:21:53'),
(420, 'തിരുവട്ട പ്പശ', NULL, 'Chir dhoona', '5d45516a6f0e81564823914.jpg', NULL, 'Pinus roxburghii', '400', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:22:09'),
(421, 'തേന്‍മെഴുക്', 'Bee Wax', 'मधु मोम, madhu-mom', '5d45524f92a511564824143.jpg', NULL, NULL, '5800', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:02'),
(422, 'തേന്‍മെഴുക്(വെള്ള)', 'Bees  Wax  (White)', 'मधु मोम, madhu-mom', '5d4552358a1b11564824117.jpg', NULL, NULL, '1000', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:11'),
(423, 'നറുംപശ', 'Myrrha', 'Myrrh', '5d455233df4de1564824115.jpg', NULL, 'Balsmodendron myrrha', '350', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:22'),
(424, 'നവച്ചാൽക്കായം', 'Asafoetida', 'heeng', '5d455233e08d31564824115.jpg', NULL, 'Ferula asafoetida', '4000', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:35'),
(425, 'മണികുന്തിരിക്കം', 'Oil  banam', 'Semul or Simul', '5d455268d90bf1564824168.jpg', NULL, 'Boswellia  Serratum', '1800', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:45'),
(426, 'ശോധിത ഗുൽഗുലു (ശുദ്ധ ഗുൽഗുലു)', 'Sudh Guggul resin', 'Guggulu Sudh', '5d455273dffb41564824179.jpg', NULL, 'Commiphora mukul', '8400', 11, 1, '2019-06-12 11:23:49', '2019-08-30 15:23:55'),
(428, 'അഞ്ജനക്കല്ല്', 'Galena', 'सीसाभश्म', '5d455295c8db31564824213.jpg', NULL, NULL, '700', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:24:07'),
(429, 'അന്നഭേദി', 'Green Viterol', 'Hara tutia', '5d45528a4fc611564824202.jpg', NULL, NULL, '700', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:24:22'),
(430, 'അയസ്ക്കാന്തം', 'Load Stone', NULL, '5d4552c8b55fb1564824264.jpg', NULL, NULL, '10', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:54:24'),
(431, 'അരിതാലം', 'Yellow  orpiment', NULL, '5d4552cd8d3771564824269.jpg', NULL, NULL, '35', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:54:29'),
(432, 'ഇന്തുപ്പ്', 'Rock  Salt', 'Sendha Namak', '5d455339aff041564824377.jpg', NULL, NULL, '4500', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:25:26'),
(433, 'ഉരുക്കുപൊടി', 'Iron Powder', NULL, '5d4553457523a1564824389.jpg', NULL, NULL, '2500', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:56:29'),
(434, 'കന്നാരം', 'Kannaram', NULL, '5d45534f223901564824399.jpg', NULL, NULL, '150', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:56:39'),
(435, 'കറുത്തഭ്രം', 'Mica (black)', 'अभरक', '5d45535a09e3f1564824410.jpg', NULL, NULL, '5', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:25:44'),
(436, 'കല്ലുപ്പ്', 'Common  Salt', 'namak', '5d45537d1fbf11564824445.jpg', NULL, 'Sodium Chloride', '200', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:25:56'),
(437, 'കാരുപ്പ്', 'Black Salt', 'Kala Namak or Sanchal', '5d4553c0531ae1564824512.jpg', NULL, NULL, '525', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:26:04'),
(438, 'കാവിമണ്ണ്', 'Red  Ochre', NULL, '5d4553922b8111564824466.jpg', NULL, 'Red Ochre', '1800', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:57:46'),
(439, 'ഗൗരീപാഷാണം', 'Arsenic  Pentasulphide', NULL, '5d4553aa253f41564824490.jpg', NULL, NULL, '5', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:58:10'),
(440, 'ചവര്‍ക്കാരം', 'Pottassium  Carbonate', 'Jawakhar', '5d4553baa50bb1564824506.jpg', NULL, NULL, '500', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:26:38'),
(441, 'ചായില്യം (കുപ്പിചായില്യം)', 'Cinnabar', 'सिगरफ', '5d4553e5cdf111564824549.jpg', NULL, NULL, '325', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:26:49'),
(442, 'ചെമ്പുപൊടി', 'Copper  Powder', 'तांबा पाउडर', '5d4553ec3593b1564824556.jpg', NULL, NULL, '0.1', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:27:41'),
(443, 'ജഹര്‍മോഹ്റാപിഷ്ടി/നാഗപാഷണം', 'Serpentine  stone', 'Jaharmohara pihti', '5d4553febc7df1564824574.jpg', NULL, NULL, '5', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:27:53'),
(444, 'തുരിശ്', 'Copper  Sulphate', 'तूतिया  tuutiyaa', '5d45540a4f5d21564824586.jpg', NULL, 'Copper Sulphate', '175', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:28:03'),
(445, 'തുവര്‍ച്ചിലക്കാരം', NULL, NULL, '5d45541727a811564824599.jpg', NULL, NULL, '400', 12, 1, '2019-06-12 11:23:49', '2019-08-03 14:59:59'),
(446, 'നെല്ലിക്കാഗന്ധകം', 'Purified Sulphur', 'गंधक gandhak', '5d455440a21d01564824640.jpg', NULL, 'Sulphur', '75', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:28:28'),
(447, 'പച്ചക്കർപ്പൂരം', 'Camphor', 'कपूर', '5d45544bd54cd1564824651.jpg', NULL, 'Cinnamomum Camphora', '4800', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:28:44'),
(448, 'പാല്‍തുത്ഥം', 'Sulphate of Zinc', NULL, '5d455452850ce1564824658.jpg', NULL, 'Zinci Sulphidum', '2', 12, 1, '2019-06-12 11:23:49', '2019-08-03 15:00:58'),
(449, 'പുരാണകിട്ടം', 'Dress  Iron', 'Loha ka zang', '5d4554606f9341564824672.jpg', NULL, 'Ferroso-ferric oxide', '1300', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:29:03'),
(450, 'പൊങ്കാരം', NULL, 'सुहागा suhaga', '5d45546bdfcdb1564824683.jpg', NULL, 'Borax', '400', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:29:18'),
(451, 'മനയോല', 'Red  Orpiment', 'Lal hartal', '5d455481b00061564824705.jpg', NULL, NULL, '100', 12, 1, '2019-06-12 11:23:49', '2019-08-30 15:29:29'),
(452, 'മാക്കീരക്കല്ല്', 'Iron Pyrites Calcined', NULL, '5d4554b103f931564824753.jpg', NULL, NULL, '100', 12, 1, '2019-06-12 11:23:49', '2019-08-03 15:02:33'),
(453, 'മുള്‍ട്ടാണി മിട്ടി', 'Fullers  Earth', 'Multani mitti', '5d4554b177b0f1564824753.jpg', NULL, NULL, '10', 12, 1, '2019-06-12 11:23:49', '2019-08-31 11:18:22'),
(454, 'രസം', 'Mercury', NULL, '5d4554bc6a69b1564824764.jpg', NULL, NULL, '50', 12, 1, '2019-06-12 11:23:49', '2019-08-03 15:02:44'),
(455, 'വിളയുപ്പ്', NULL, NULL, '5d4554cec1aab1564824782.jpg', NULL, NULL, '600', 12, 1, '2019-06-12 11:23:49', '2019-08-03 15:03:02'),
(456, 'വെള്ളിപ്പൊടി', 'Silver  Powder', 'चांदी पाउडर', '5d4554dd4c2051564824797.jpg', NULL, NULL, '1', 12, 1, '2019-06-12 11:23:49', '2019-08-31 11:19:16'),
(457, 'സ്ഫടികം', 'Quartz', 'बिल्लौर', '5d4554e8bdb6f1564824808.jpg', NULL, NULL, '0.1', 12, 1, '2019-06-12 11:23:49', '2019-08-31 11:19:29'),
(458, 'സള്‍ഫര്‍ ഐ.പി (നെല്ലിക്കാഗന്ധകം)', 'Purified Sulpher', 'Jandhak sudh', '5d45551ca44bb1564824860.jpg', NULL, 'Sulpher', '100', 12, 1, '2019-06-12 11:23:49', '2019-08-31 11:19:39'),
(459, 'സ്വർണം 24 CT', 'GOLD 24 CT', 'Sona', '5d4555271a7af1564824871.jpg', NULL, NULL, '1', 12, 1, '2019-06-12 11:23:49', '2019-08-31 11:19:54'),
(460, 'സഹസ്രവേദി', 'Sahasravedhi', NULL, '5d45556039b8a1564824928.jpg', NULL, NULL, '500', 12, 1, '2019-06-12 11:23:49', '2019-08-03 15:05:28'),
(462, 'ആട്ടിന്‍ കൊമ്പ്', 'Goat’s Horn', 'Bakari ka pyaala', '5d45558e6b6421564824974.jpg', NULL, NULL, '5', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:20:19'),
(463, 'കടല്‍നുര', 'Cuttle Fish Bone', 'Samudraphen', '5d4555fcb84e41564825084.jpg', NULL, NULL, '500', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:20:30'),
(464, 'ചുണ്ണാമ്പ്', 'Lime', 'Choona', '5d45582c7172b1564825644.jpg', NULL, NULL, '300', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:20:45'),
(465, 'പവിഴക്കൊമ്പ്', 'Red  Coral', 'Moong rathn', '5d45586c6512c1564825708.jpg', NULL, NULL, '10', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:21:35'),
(466, 'പശുവിന്‍ കൊമ്പ്', 'Cow’s  Horn', 'Gaay ka pyaala', '5d4558fba53e11564825851.jpg', NULL, NULL, '1', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:21:45'),
(467, 'പോത്തിന്‍കൊമ്പ്', 'Buffalo Horn', 'भैंस ka pyaala', '5d4558912b1b41564825745.jpg', NULL, NULL, '2', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:21:57'),
(468, 'മുത്തുച്ചിപ്പി', 'Calx of Pearl oyster Shells', 'Miukthasukthi', '5d45588a79aa51564825738.jpg', NULL, NULL, '150', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:22:08'),
(469, 'മുത്തുചിപ്പി ഭസ്മം', 'Pearl oyster', 'Miukthasukthi bhasma', '5d6a0b20e3b4e1567230752.jpg', NULL, NULL, '5', 13, 1, '2019-06-12 11:23:49', '2019-08-31 11:22:32'),
(470, 'ശംഖ്', 'Congue', NULL, '5d4558e4863a51564825828.jpg', NULL, NULL, '15', 13, 1, '2019-06-12 11:23:49', '2019-08-03 15:20:28'),
(472, 'ആട്ടിന്‍ മൂത്രം (ലിറ്റര്‍)', 'Goat’s Urine', 'Bakari पेशाब', NULL, NULL, NULL, '11500', 14, 1, '2019-06-12 11:23:49', '2019-08-31 11:22:56'),
(473, 'കണ്ടിവെണ്ണ ഒന്നാതരം', 'Kandivenna', NULL, '5d455c51dac821564826705.jpg', NULL, NULL, '200', 14, 1, '2019-06-12 11:23:49', '2019-08-03 15:35:05'),
(474, 'ഗോമൂത്രം (ലിറ്റര്‍)', 'Cow’s  Urine', 'Gaay पेशाब', NULL, NULL, NULL, '33000', 14, 1, '2019-06-12 11:23:49', '2019-08-31 11:23:55'),
(475, 'പശുവിന്‍ ചാണകം', 'Cow  Dung', 'गोबर', NULL, NULL, NULL, '50', 14, 1, '2019-06-12 11:23:49', '2019-08-31 11:24:08'),
(477, 'എരുമ നെയ്യ്', 'Buffalo’s Ghee', 'Bems ghee', '5d4bbd6aa46fc1565244778.jpg', NULL, NULL, '500', 15, 1, '2019-06-12 11:23:49', '2019-08-31 11:24:19'),
(478, 'എരുമ പാല്‍ (ലിറ്റര്‍)', 'Buffalo’s Milk', 'भैंस dhooth', '5d455c85e1a051564826757.jpeg', NULL, NULL, '25009500', 15, 1, '2019-06-12 11:23:49', '2019-08-31 11:24:30'),
(479, 'പശുവിൻ നെയ്യ്', 'Cow Ghee', 'Gaay ghee', '5d455c9617e671564826774.jpg', NULL, NULL, '38000', 15, 1, '2019-06-12 11:23:49', '2019-08-31 11:24:42'),
(480, 'പശുവിൻ പാൽ (ലിറ്റര്‍)', 'Cow\'s Milk', 'Go dhood', '5d455cb3343b71564826803.jpg', NULL, NULL, '640000', 15, 1, '2019-06-12 11:23:49', '2019-08-31 11:24:57'),
(481, 'പശുവിന്‍ മോര് (ലിറ്റര്‍)', 'Butter Milk', '', NULL, '', NULL, NULL, 15, 1, '2019-06-12 11:23:49', '2019-06-12 11:23:49'),
(483, 'അട്ടക്കരി', 'Soot', 'कालिख', '5d455cd5478461564826837.jpg', NULL, NULL, '300', 16, 1, '2019-06-12 11:23:49', '2019-08-31 11:25:22'),
(484, 'ചാണകവരളി (Nos)', NULL, NULL, '5d455cdfb39b51564826847.jpg', NULL, NULL, '10000', 16, 1, '2019-06-12 11:23:49', '2019-08-03 15:37:27'),
(485, 'നാടന്‍ കരി', NULL, '', NULL, '', NULL, '150', 16, 1, '2019-06-12 11:23:49', '2019-06-12 11:23:49'),
(486, 'പുളി വിറക് ഉണങ്ങിയത്', NULL, NULL, '5d4bf543156ad1565259075.jpg', NULL, NULL, '21000', 16, 1, '2019-06-12 11:23:49', '2019-08-08 15:41:15'),
(488, 'അഭ്ര ഭസ്മം', 'Abhra  Bhasmam', NULL, '5d455d21a24231564826913.png', NULL, NULL, '10', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:38:33'),
(489, 'കന്മദ ഭസ്മം', 'Kanmada  Bhasmam', NULL, '5d455d4e9f2491564826958.jpg', NULL, NULL, '25', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:39:18'),
(490, 'കാന്ത സിന്ദൂരം', 'Kantha  Sindhooram', NULL, '5d455d681dfcb1564826984.jpg', NULL, NULL, '2', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:39:44'),
(491, 'നാഗഭസ്മം', 'Naga  Bhasmam', NULL, '5d455d70b6a0a1564826992.jpg', NULL, NULL, '10', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:39:52'),
(492, 'പ്രവാള ഭസ്മം', 'Pravala  Bhasmam', NULL, '5d455d87db0fb1564827015.jpg', NULL, NULL, '35', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:40:15'),
(493, 'ലോഹ ഭസ്മം', 'Loha bhasmam', NULL, '5d455dff1e59c1564827135.jpg', NULL, NULL, '350', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:42:15'),
(494, 'സ്വര്‍ണ്ണമാക്ഷിക ഭസ്മം', 'Swarnamakshika  Bhasmam', NULL, '5d455e216ddfe1564827169.jpg', NULL, NULL, '5', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:42:49'),
(495, 'ഇലിപ്പ പിണ്ണാക്ക്', 'Oil cake of Mahua tree', NULL, '5d455e227e4ce1564827170.jpg', NULL, 'Oil cake of Madhuca longifolia', '25', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:42:50'),
(496, 'ഓപ്പിയം', 'Opium', NULL, '5d455e2d169df1564827181.jpg', NULL, NULL, '25', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:43:01'),
(497, 'ഓയില്‍ ഗ്രീന്‍ കളര്‍', 'Oil Green Color', NULL, '5d455e618f5a31564827233.jpg', NULL, NULL, '1Kg.', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:43:53'),
(498, 'ക്ളോവ് ഓയില്‍', 'Clove Oil', NULL, '5d455e60402371564827232.jpg', NULL, NULL, '5 Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:43:52'),
(499, 'കോക്കനട്ട് ഓയില്‍ പെര്‍ഫ്യും', 'Coconut  Oil  Perfume', NULL, '5d455e5a8587f1564827226.jpg', NULL, NULL, '---', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:43:46'),
(500, 'ഗ്ളിസറിന്‍ (ഐ.പി)', 'Glycerin (I. P.)', NULL, '5d455e65c84d41564827237.jpg', NULL, NULL, '40', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:43:57'),
(501, 'ചോക്കലേറ്റ് ഫ്ളേവര്‍', 'Chocolate  Flavour', 'चाकलेट पुट', '5d455e708e4631564827248.jpg', NULL, NULL, '5', 17, 1, '2019-06-12 11:23:49', '2019-08-31 11:26:17'),
(502, 'പെപ്പര്‍മിന്‍റ് ഫ്ളേവര്‍', 'Peppermint  Flavour', NULL, '5d455e947819b1564827284.jpg', NULL, NULL, '5  Ltr.', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:44:44'),
(503, 'പെര്‍ഫ്യും ഇറ്റേര്‍ണിറ്റി', 'Perfume  Eternity', NULL, '5d455e99c3eec1564827289.jpg', NULL, NULL, '5 Ltr.', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:44:49'),
(504, 'പെര്‍ഫ്യും ജാസ് ഡീലക്സ്/ജാസ്മിന്‍ 94 പെര്‍ഫ്യും', 'Perfume Jass (Deluxe)/Jasmin  94  Perfume', NULL, '5d455ea8c7a801564827304.jpg', NULL, NULL, '40Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:45:04'),
(505, 'പെര്‍ഫ്യും മസ്ക്ക്', 'Perfume  Musk', NULL, '5d455ebc23cc01564827324.jpg', NULL, NULL, '40Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:45:24'),
(506, 'ഫെന്നല്‍ ഫ്ളേവര്‍', 'Fennel Flavour', NULL, '5d4bf1295549a1565258025.jpg', NULL, NULL, '5 Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-08 15:23:45'),
(507, 'മെന്തോള്‍ ക്രസ്റ്റല്‍സ്(ഐ.പി)', 'Menthol Crystals   I. P.', NULL, '5d455eeb663671564827371.jpg', NULL, NULL, '150', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:46:11'),
(508, 'റെക്ടിഫൈഡ് സ്പിരിറ്റ്', 'Rectified  spirit', NULL, '5d455f05b6b881564827397.jpg', NULL, NULL, '70Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:46:37'),
(509, 'വാനില ഫ്ളേവര്‍', 'Vanila Flavour', NULL, '5d455f0d30ae21564827405.jpg', NULL, NULL, '5 Ltr', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:46:45'),
(510, 'വൈറ്റ് പെട്രോളിയം ജെല്ലി(ഐ.പി)', 'White Petroleum Jelly   I. P.', NULL, '5d455f185d2b81564827416.jpg', NULL, NULL, '3600', 17, 1, '2019-06-12 11:23:49', '2019-08-03 15:46:56'),
(512, 'ആവണക്കെണ്ണ ( ശുദ്ധി ചെയ്തത് )', 'Castor Oil (purified)', 'Erandi ka tel', '5d455f2a142591564827434.jpg', NULL, 'Ricinus communis', '6500', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:29:13'),
(513, 'ആവണക്കെണ്ണ (ORDINARY)', 'Castor Oil (ordinary)', 'Erandi ka tel', '5d47f79ba3b411564997531.jpg', NULL, 'Ricinus communis,Linn', '19000', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:29:31'),
(514, 'എണ്ണ നാടൻ ( To be supplied in Tankers)', 'Gingelly Oil ( To be supplied in Tankers)', 'Tel', '5d455f4d81f8e1564827469.jpg', NULL, 'Sesamum indicum', '260000', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:34:34'),
(515, 'കടുകെണ്ണ', 'Mustard Oil', 'sarso ka tel', '5d455f56741f61564827478.jpg', NULL, 'Brassica nigra', '1000', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:35:49'),
(516, 'മരോട്ടി എണ്ണ', 'Oil gungle Almond', 'Baadaam ka tel', '5d455f5f4f5491564827487.jpg', NULL, 'Hydno  carpuspetandra', '500', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:36:32'),
(517, 'വെര്‍ജിന്‍ വെളിച്ചെണ്ണ', 'Virgian Coconut Oil', 'Virgin nariyal ka thel', '5d455f69394d41564827497.jpg', NULL, NULL, '300', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:36:51'),
(518, 'വെളിച്ചെണ്ണ ( To be supplied in Tankers)', 'Coconut Oil ( To be supplied in Tankers)', 'Nariyal ka tel', '5d455f8d5edce1564827533.jpg', NULL, 'Cocos nucifera', '300000', 18, 1, '2019-06-12 11:23:49', '2019-08-31 11:37:16'),
(520, 'ആട്ടിൻമാംസം', 'Mutton', 'bhede ka maans', NULL, NULL, NULL, '3000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:37:32'),
(521, 'കൽക്കണ്ടം', 'Sugar Candy', 'mishri', '5d455fa771fef1564827559.jpg', NULL, NULL, '25000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:37:54'),
(522, 'തേൻ', 'Honey', 'Madhu', '5d455fb34b87b1564827571.jpg', NULL, 'Mel', '78000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:38:14'),
(523, 'നന്‍മുത്ത്', 'Pearl', 'moti', '5d455fb8e617e1564827576.jpg', NULL, NULL, '1', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:39:07'),
(524, 'പഞ്ചസാര', 'Sugar', 'cheeni', '5d455fe410d8c1564827620.jpg', NULL, NULL, '110000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:39:24'),
(525, 'പുല്‍തൈലം', 'Lemongrass oil', 'Lemon grass', '5d455febd61a31564827627.jpg', NULL, 'Cymbopogon citratus', '75', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:41:09'),
(526, 'ശർക്കര ഉണ്ട (1ST QUALITY)', 'Jaggery Ist quality', 'Guda', '5d455ff6444681564827638.jpg', NULL, NULL, '100000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:40:18'),
(527, 'ശർക്കര പൗഡർ (1ST QUALITY) See specification', 'Jaggery Powder (Ist quality)', 'Guda', '5d455fff304b71564827647.jpg', NULL, NULL, '500000', 19, 1, '2019-06-12 11:23:49', '2019-08-31 11:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_enquiries`
--

CREATE TABLE `raw_material_enquiries` (
  `enq_id` int(11) NOT NULL,
  `raw_material_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `message` longtext,
  `is_read` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raw_material_enquiries`
--

INSERT INTO `raw_material_enquiries` (`enq_id`, `raw_material_id`, `name`, `email`, `phone`, `message`, `is_read`, `date_added`, `date_modified`) VALUES
(1, 6, 'xcb', 'gfg@ghgj.uujh', NULL, 'fhdgfh', 0, '2019-04-27 12:05:55', '2019-04-27 12:05:55'),
(2, 6, 'Sam Alex', 'samalex@musicians.com', '6567876', 'zxcx', 0, '2019-04-27 12:11:09', '2019-04-27 12:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `raw_material_types`
--

CREATE TABLE `raw_material_types` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `raw_material_types`
--

INSERT INTO `raw_material_types` (`id`, `title`, `status`, `date_added`, `date_modified`) VALUES
(1, 'WHOLE PLANT (സമൂലം)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(2, 'ROOTS (വേരുകൾ)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(3, 'Rhizomes (കിഴങ്ങുകൾ)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(4, 'TWINNERS (വള്ളികള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(5, 'Leaves (പത്രങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(6, 'FLOWERS (പുഷ്പങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(7, 'FRUITS(ഫലങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(8, 'BARK(പട്ടകള്‍/തൊലികള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(9, 'Wood Matter (സാരങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(10, 'EXTRACTS (സത്വങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(11, 'Latex/Resins (നിര്യാസങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(12, 'Mineral Matter  (ധാതുവര്‍ഗ്ഗങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(13, 'Bones, Nail Etc. (അസ്ഥി, ദന്തം, ഖുരം, നഖം മുതലായവ)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(14, 'EXCRETA  (മൂത്രങ്ങള്‍, പുരീഷങ്ങള്‍ മുതലായവ ', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(15, 'Milk & Milk products (ക്ഷീരങ്ങള്‍, ഘൃതങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(16, 'Fire wood (വിറക്)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(17, 'Miscellaneous (പലവക)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(18, 'Oils (സ്നേഹ ദ്രവ്യങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00'),
(19, 'Other Items  (മറ്റു ഇനങ്ങള്‍)', 1, '2019-06-10 06:30:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `setting_id` int(11) NOT NULL,
  `code` varchar(128) NOT NULL,
  `value` text,
  `user_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`setting_id`, `code`, `value`, `user_id`, `date_added`) VALUES
(15, 'config_contact_email', 'mail@oushadhi.org', 1, '2019-08-01 14:55:17'),
(14, 'config_helpline_number', '0487 2459800', 1, '2019-08-01 14:55:17'),
(13, 'config_contact_number1', '0487 2459800', 1, '2019-08-01 14:55:17'),
(9, 'config_meta_title', 'Oushadhi', 1, '2019-08-01 14:55:17'),
(10, 'config_meta_description', 'The Pharmaceutical Corporation (IM)  Kerala Limited', 1, '2019-08-01 14:55:17'),
(11, 'config_address', 'The Pharmaceutical Corporation (IM)  Kerala Limited </br>\r\nKuttanellur, Thrissur', 1, '2019-08-01 14:55:17'),
(12, 'config_email', 'mail@oushadhi.org', 1, '2019-08-01 14:55:17'),
(19, 'config_facebook_page', 'https://www.facebook.com/oushadhi2020/', 1, '2019-08-01 14:55:17'),
(20, 'config_twitter_page', 'https://twitter.com/login?lang=en', 1, '2019-08-01 14:55:17'),
(21, 'config_instagram_page', 'https://www.instagram.com/accounts/login/?hl=en', 1, '2019-08-01 14:55:17'),
(24, 'config_default_per_page', '20', 1, '2019-08-01 14:55:17'),
(35, 'config_quotation_email', 'marketing@oushadhi.org', 1, '2019-08-01 14:55:17'),
(34, 'config_contact_number2', NULL, 1, '2019-08-01 14:55:17'),
(36, 'config_map_link', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3923.1061133077264!2d76.25379141428513!3d10.492300667244002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7f01c786d9dd3%3A0x6550f210ac42c95a!2sOushadhi!5e0!3m2!1sen!2sin!4v1564825889171!5m2!1sen!2sin', 1, '2019-08-01 14:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `tax_id` int(11) NOT NULL,
  `tax_name` varchar(50) DEFAULT NULL,
  `type` varchar(1) DEFAULT 'P',
  `tax_value` decimal(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`tax_id`, `tax_name`, `type`, `tax_value`, `status`, `date_added`, `date_modified`) VALUES
(1, 'VAT (5%)', 'P', 5.00, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'VAT (7%)', 'P', 7.00, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tenders`
--

CREATE TABLE `tenders` (
  `tender_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `last_date` date DEFAULT NULL,
  `last_time` varchar(255) DEFAULT NULL,
  `dept` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `tender_link` varchar(500) DEFAULT NULL COMMENT '1=normal,2=e-tender',
  `tender_type` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tenders`
--

INSERT INTO `tenders` (`tender_id`, `title`, `from_date`, `to_date`, `last_date`, `last_time`, `dept`, `contact`, `contact_email`, `tender_link`, `tender_type`, `status`, `date_added`, `date_modified`) VALUES
(10, 'E - Tender for supply of Raw materials for the period 01.10.2019 to 31.03.2020', '2019-10-01', '2020-03-31', '2019-08-29', '5:00 PM', 'Purchase', NULL, NULL, 'https://etenders.kerala.gov.in/nicgep/app?component=%24DirectLink&page=FrontEndTendersByOrganisation&service=direct&session=T&sp=Sc05xjcqsFuwuPjodZzLQ4tS0Fec7wUuNy1YFXyqSerE%3D', 2, 0, '2019-08-29 10:59:02', '2019-08-30 09:24:11'),
(11, 'E - Tender for Supply of Power Bank', NULL, NULL, '2019-09-23', '5:00 PM', 'Purchase', NULL, 'purchase@oushadhi.org', 'https://etenders.kerala.gov.in/nicgep/app?component=%24DirectLink&page=FrontEndTendersByOrganisation&service=direct&session=T&sp=Sc05xjcqsFuwuPjodZzLQ4tS0Fec7wUuNy1YFXyqSerE%3D', 2, 1, '2019-09-05 09:25:27', '2019-09-05 12:29:05'),
(12, 'Re E-Tender for supply of pallets', NULL, NULL, '2019-09-23', '5:00 PM', 'Purchase', NULL, 'purchase@oushadhi.org', 'https://etenders.kerala.gov.in/nicgep/app?component=%24DirectLink&page=FrontEndTendersByOrganisation&service=direct&session=T&sp=Sc05xjcqsFuwuPjodZzLQ4tS0Fec7wUuNy1YFXyqSerE%3D', 2, 1, '2019-09-05 12:23:44', '2019-09-05 12:29:27'),
(13, 'E - Tender for Supply of Oushadhi Diary 2020', NULL, NULL, '2019-09-30', '5:00 PM', 'Purchase', NULL, 'purchase@oushadhi.org', 'https://etenders.kerala.gov.in/nicgep/app?component=%24DirectLink&page=FrontEndTendersByOrganisation&service=direct&session=T&sp=Sc05xjcqsFuwuPjodZzLQ4tS0Fec7wUuNy1YFXyqSerE%3D', 2, 1, '2019-09-07 14:42:52', '2019-09-07 14:42:52'),
(14, 'Tender for Mezzanine Floor additional works – Structural Support for Monorail Hoist at Oushadhi Kuttanellur', NULL, NULL, '2019-09-25', '3:00 PM', 'Administration', NULL, 'administration@oushadhi.org', NULL, 1, 1, '2019-09-18 09:31:15', '2019-09-18 09:31:15'),
(15, 'Quotations are invited for cutting down old and non-using trees in oushadhi compound', NULL, NULL, '2019-10-10', '3:00 PM', 'Administration', '0487-2459800', 'administration@oushadhi.org', NULL, 1, 1, '2019-09-21 17:27:56', '2019-09-21 17:27:56');

-- --------------------------------------------------------

--
-- Table structure for table `tender_files`
--

CREATE TABLE `tender_files` (
  `file_id` int(11) NOT NULL,
  `tender_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `file_link` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tender_files`
--

INSERT INTO `tender_files` (`file_id`, `tender_id`, `title`, `file_link`) VALUES
(7, 4, 'Tender Notice', '5c7a64ec8cdcc1551525100-TN2.pdf'),
(8, 4, 'General and conditions', '5c7a64ec9a6c51551525100-GC.pdf'),
(13, 10, 'Intimation letter', '5d67629e1bac21567056542-intimation letter.pdf'),
(14, 10, 'E-Tender List', '5d67629e1c6cc1567056542-E-tender list.pdf'),
(15, 10, 'Terms and Conditions', '5d67629e1d07e1567056542-Terms and conditions.pdf'),
(16, 10, 'Documents to be submitted', '5d67629e1db891567056542-documents.pdf'),
(17, 10, 'A specificationn', '5d67629e1e7e91567056542-A.pdf'),
(18, 10, 'B specification', '5d67629e1f08f1567056542-B.pdf'),
(19, 10, 'C specification', '5d67629e1f72c1567056542-C.pdf'),
(20, 10, 'D specification', '5d67629e1fdc41567056542-D.pdf'),
(21, 11, 'Tender Notice', '5d70872fd37581567655727-TenderNotice.pdf'),
(22, 11, 'Documents to be submitted', '5d70872fd48dc1567655727-DocumentswithTender.pdf'),
(23, 11, 'General Conditions', '5d70872fd52081567655727-generalconditions.pdf'),
(24, 12, 'Tender Notice', '5d70b0f8cdd5c1567666424-TenderNoticePallets.pdf'),
(25, 12, 'General conditions', '5d70b0f8ce8d91567666424-generalconditions.pdf'),
(26, 12, 'Technical Specifications', '5d70b0f8cf3dd1567666424-Technical.pdf'),
(27, 13, 'Tender Notice', '5d737494c4c781567847572-TenderNoticeDiary.pdf'),
(28, 13, 'Preliminary Agreement', '5d737494c57841567847572-PreliminaryAgreement.pdf'),
(29, 13, 'Tender Document', '5d737494c5f4e1567847572-DocumentswithTender.pdf'),
(30, 13, 'General Conditions', '5d737494c74861567847572-generalconditions.pdf'),
(31, 14, 'Tender Document', '5d81ac0ba175b1568779275-Mezzanine Floor- additional work .pdf'),
(32, 15, 'Quotation Notice', '5d861044315f11569067076-ക്വട്ടേഷന്_ നോട്ടീസ്.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `name` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimonial_id`, `title`, `description`, `name`, `designation`, `image`, `status`, `date_added`, `date_modified`) VALUES
(2, 'testimonial1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since.', 'Ravi R Menon', 'CEO odfgn', NULL, 0, '2019-03-01 09:33:49', '2019-08-03 12:30:56'),
(3, 'testimonial2', 'Contrary to popular belief, Lorem Ipsum Contrary to popular belief, Lorem Ipsum Contrary to popular belief, Lorem Ipsum Contrary to popular belief, Lorem Ipsum Contrary to popular belief, Lorem IpsumContrary to popular belief, Lorem Ipsum', 'K R Varma', 'Founder KK f', NULL, 0, '2019-03-01 09:35:29', '2019-08-03 12:30:50'),
(4, 'Testimonial3', 'imply dummy text of the , Lorem Ipsum is not simply r imply dummy text of the , Lorem Ipsum is not simply r imply dummy text of the , Lorem Ipsum is not simply r imply dummy text of the , Lorem Ipsum is not simply r', 'Sugatha S Pillai', 'CEO sdf', NULL, 0, '2019-03-01 09:36:43', '2019-08-03 12:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `cus_token` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1=Doctor,2=Dealer',
  `is_verified` tinyint(1) NOT NULL,
  `is_admin_verified` int(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile`, `phone`, `district`, `state`, `address`, `cus_token`, `status`, `user_type`, `is_verified`, `is_admin_verified`, `remember_token`, `created_at`, `updated_at`) VALUES
(8, 'anju', 'anju@netstager.in', '$2y$10$TpuLMqNsTmg5ZtuYnhsJseGxO754mB0vnBNtZvGO1NBv4ujF/PoGm', '9874563210', '123456235', 'calicut', 'aaqsxz', 'asdas\r\ndfgd,sdfsd,dsg,dfv', '30c22bf04674e7d255b56705397d0f88', 1, 1, 1, 1, 'wRtDORhQ60qktz9SNPQYf8Bsb11RiMWkt9lWX3lCWcRIhG8f3hGSIo8vHhFD', '2019-03-07 04:12:06', '2019-08-03 07:04:44'),
(16, 'Dr.Sapna Deshmukh', 'sapna22.2011@rediffmail.com', '$2y$10$OYkc159pSOJulsL4uqybFOuRl3H40t1aS2tQxm9EmUWH4ZeAOuKUi', '9404100123', '9423100123', 'Jalna', 'Maharashtra', 'Dr.sapna\'s Deshmukh Ayurveda, vidyut nagar,old jalna, jalna, Maharashtra.', '1dea8b2d9a50dfc707a7b1dad3dd43f7', 1, 1, 0, 2, NULL, '2019-08-30 16:16:44', '2019-09-07 08:54:18'),
(17, 'Dr.Sapna Deshmukh', 'sapnardeshmukh7@gmail.com', '$2y$10$BYqRZPJFQnGX/.nheWBIJ.4VF.GedHeLG4nY63ZS5KJnI..I45iYm', '9404100123', '9423100123', 'Jalna', 'Maharashtra', 'Dr.Sapna\'s Deshmukh Ayurveda, vidyut nagar, old jalna,Japan, Maharashtra.', '3180e2453163836ee48d73fe8fd13abf', 1, 1, 0, 2, NULL, '2019-08-30 16:22:29', '2019-09-07 08:54:12'),
(18, 'Dr Prerana Patil', 'preranapishtepatil@gmail.com', '$2y$10$5CAS3LLBZXjS1k0eCekGWeOmUG2g2sjFjhZPUfpggh9DPRiXmlIsG', '9665220092', '9665220092', 'Kolhapur', 'Maharashtra', '704,Sai malhar residency, behind kedarling bakery\r\nAptenagar,Sane guruji vasahat, Kolhapur', '89e28521c0d192dab5690bc594de3034', 1, 1, 1, 2, 'B26e5iaMthzjYlZPUnTR1iej3sxTCsuSrIw9Tsg18SZXCdaa4ORLTEY9KjVo', '2019-08-31 00:03:14', '2019-09-07 08:54:06'),
(19, 'DanSpasp', 'dave@stripdesk.com', '$2y$10$ihbpLNXYQTVyU07DP2E8yOgURDOXPuMpvQqnZGP3k6Gt1peRd5K22', '81597511162', '84451767499', 'orca88.com/bitcoin casino', 'bitcoins gambling free', 'https://online-crypto-casino.ru/casino-deposit-bonuses.html', '4a99e12db1fe758a474ea9f5e3656135', 1, 1, 0, 2, NULL, '2019-08-31 07:08:34', '2019-09-07 08:54:01'),
(20, 'RomanosOG', 'romanilinin88@mail.ru', '$2y$10$6Xgp3ITBe4hFJELU0/QDI.r3EOg7G4geJKXaWVEg9vfCTmEceCH4u', '82256454675', '88996995367', 'Лучшие идеи бизнеса и заработка', 'Inюaat mьh.', 'http://p-business.ru/', '2a50b66dfa65f5b292dc79d63c3708b9', 1, 1, 0, 2, NULL, '2019-09-02 16:46:59', '2019-09-07 08:53:56'),
(21, 'Yadav Mohan', 'yadavkmohan@gmail.com', '$2y$10$EE4kIw5q8bQEp0cn/MeW7ugfVQHRwqjU7JOi87HRQx4vklBpZm5m6', '09809418393', '04872360744', 'Thrissur', 'KERALA', 'Kummali House, Puthurkkara\r\nAyyanthole P O', '78858c11f2308435c088a46953903b22', 1, 1, 1, 2, 'pgWJf6wSsZBxoEeX1hEspyKNuW1dtYRunOjVsWQlFTZCzOcXrE01zX8XBG0x', '2019-09-03 06:38:06', '2019-09-07 08:53:51'),
(22, 'Arun', 'arunasok31@gmail.com', '$2y$10$eBi0jUM4ATr/DrVGGQ5MiuM8Ea.pJN8BvvhX72KA/vVoieVQ6/v/C', '9746369092', '04742514372', 'Kollam', 'Kerala', 'Bunglowil house\r\nPuthenkulam (p.o)\r\nKollam', 'ec59b394cd71b69476c2498cd4cccecc', 1, 2, 1, 2, '69RF9u0DaoFbXXzCQJMMwulIVXPqRaMWOpmMuSI1HwEtlE39tTKCN6cLxKlB', '2019-09-04 05:28:14', '2019-09-07 08:53:46'),
(23, 'Mituh', 'ernest3u14mc@mail.com', '$2y$10$mtrI.sSudmd74DzTop1HTOwsWBcwAzBq3bjw.H20/EMO5JG0elLt6', '83185245886', '84726949818', 'Information On Trying Out A Fresh Activity', 'France', 'http://protegelinks.info/', '30b6437693d2fe445792ff0acccdfb24', 1, 1, 0, 2, NULL, '2019-09-05 17:30:33', '2019-09-07 08:53:41'),
(24, 'Dr Prerana Patil', 'vdprerana@gmail.com', '$2y$10$rC2AAZTghnciGEeHNbXFdex6tsfKNSa2lHiZDhHQT/Y6Pyz8I3FHy', '9665220092', '9665220092', 'Kolhapur', 'Maharashtra', '704,Sai malhar residency, behind kedarling bakery\r\nAptenagar,Sane guruji vasahat, Kolhapur', '303b4441e9eeabd565df772f7a749f4a', 1, 1, 1, 2, '0LtgIygTSOrAgSBB38PKfFyayGYTwjcpWYGvzPvfZ6K2TBOhDoaM7xujAVpa', '2019-09-06 01:08:55', '2019-09-07 08:53:37'),
(25, 'rardhourn', 'mitelite@eclipso.eu', '$2y$10$gqTewEf/GTNx.u6z1bpBbeYwVkEEzX6cJQ4bF8EX.eoml1SySrW7a', '88446286876', '87941559557', 'Test, just a test', 'Lebanon', 'Hukuk müţaviri', 'ce293df7a74c3b0805c338aa9c5f195a', 1, 1, 0, 2, NULL, '2019-09-07 03:26:04', '2019-09-07 08:53:32'),
(26, 'Prabeesh', 'prabeesh@netstager.com', '$2y$10$B0VBqiDcfZ8jlrw4MW4HLeOEs.2AUFjg83Ia/ZqILxs9oppi2SraS', '9633147805', '9633147805', 'Kozhikode', 'UL Cyberpark', 'Netstager technologies\r\n1st Floor UL Cyberpark', '5cec9db9c05c6eb6086d3d13f0fe8c60', 1, 1, 0, 2, NULL, '2019-09-07 08:43:51', '2019-09-07 08:47:01'),
(27, 'Garbi', 'garbiushus@gmail.com', '$2y$10$68AR8g1b2DhnV6xOx1Jg2.mG6n8ltJj6KJv4ZI8HgW7xpWlbwroLu', '9072511812', '04792472912', 'Alappuzha', 'Kerala', 'Ushus house\r\nMuthukulam.po \r\nAlappuzha \r\nKerala \r\nPin-690506', 'a780f7c4398fc9a061bcff7cfa19afaf', 1, 1, 1, 0, 'FBroHCWdQ5NrKlsRBThzvEOwBPWBxhwqsJwiW8NrquUNOiqHK15lN39rhNqJ', '2019-09-09 04:03:26', '2019-09-09 04:06:33'),
(28, 'Dr Dileep Kumar chaudhary', 'dileep2050@gmail.com', '$2y$10$vfpG5CWNPNSMazCK0NsneOGbwb0fISjbTcM.WC7QFNRUfAQuvABJ6', '9792944915', '9792944915', 'Allahabad', 'Uttar Pradesh', '154/1D/1E/1 bhola ka purva sainik Colony Dhumanganj prayagraj (allahabad) 211001', '6c32bdeb1f893b79ca6f4586d76cab23', 1, 1, 1, 2, 'o8a2D3F68Ms4BZBdVTtJrLjUfcSwADUQQMCsjJIjyHTGAuS9UxaLlgr3Q49B', '2019-09-09 07:44:09', '2019-09-18 04:10:04'),
(29, 'Namitha Jacob', 'namithajake7@gmail.com', '$2y$10$V3RQUsKdOE7N8CmZfpwweO1GW3/wBcxkz5K4kZgNwSZvzBN2UnPgi', '9539523030', '9539420309', 'Wayanad', 'Kerala', 'Thoppil House \r\nPerumthuruthy \r\nThiruvalla \r\nPathanamthitta', 'b052100564baa9ed937df69e64c02139', 1, 1, 1, 0, 'b3tDQpW5SR8l6QKHxe7cJ1gG3vMlEE5mAeTG9s6UKc1cMEjG3QpyncUYB5SH', '2019-09-09 08:30:27', '2019-09-09 08:31:06'),
(30, 'Sharafudeen', 'shashe721@gmail.com', '$2y$10$XJB5QaKZe24W9OQFzu46R.7t6zDW61ploEv3..MqCUuzLgBgYRfJS', '9400332491', '04712619528', 'Tvm', 'Kerala', 'Aramam, Thonnakkal. P. O, Trivendrum 695317', '4c64b89086603e7d01f1ead174f6d08f', 1, 1, 1, 0, 'psjBQRp1PkmU3ZqE63y0Df0faZ8krxGBbszw0wvHgGj23VsHVUER2X7rASuj', '2019-09-09 12:50:54', '2019-09-09 15:49:42'),
(31, 'DR BINI VARGHESE', 'drbinimd@gmail.com', '$2y$10$/wR7TVm0HkxubF55A3IGBO2DFBOuJSLFxgZbtvNXEpJj8TlXrnpHW', '9539548548', '9539548548', 'ERNAKULAM', 'KERALA', 'GAH KKLM', 'e3588ad6d3eaf17d88d145b78cf8ae1a', 1, 1, 1, 0, '1yfLaT508Lgifxz8FDMs7KbxfCKdEL6b6pImkFwUOzXTp3SXgiE3xlnqxkb7', '2019-09-13 08:45:50', '2019-09-13 08:47:56'),
(32, 'aravindakashan', 'aravindan2030@gmail.com', '$2y$10$DKd3fa5PIJRcWpM1rn/Op.vskB8G5ZwJQLpMzB76MTfE3EnDEZCiK', '9447510275', '04742792899', 'kollam', 'kerala', 'tra 46thevally kollam 691009', '78613e96f39219a3a271e56727e82bd1', 1, 1, 1, 0, 'Khq2BrcqaIWEgvZPXKdePndRFuoZCAEf9VD12Z82nTIZ0sPeM5uNo3JSOTiq', '2019-09-13 13:41:04', '2019-09-15 13:30:09'),
(33, 'vijayachandran Pillai K N   64', 'vijayachandranpillai1954@gmail.com', '$2y$10$Oq.vaiAMdsggfEkMORTH2OHQzkk56b1QPi6xaD2tjUMFziLaO.q0.', '9497667294', '09497667294', 'KOTTAYAM', 'Kerala', 'Ampazhathinal house, Mannarakayam PO, KANJIRAPALLY', 'bef4a549446596d100efb96c6b39ee2a', 1, 2, 1, 0, 'z8MGfuvNBpKEUoVJkUb01WamJJCztrok2vM9qXs2pXQIBh1D1zTfRhem3Dnz', '2019-09-14 14:50:19', '2019-09-14 15:06:32'),
(34, 'Anjana A Vijayan', 'anjanaavijayan2277@gmail.com', '$2y$10$0iN9ZciiS01qpQ30F0Pe7.vtxXCpfwg.2bjH5oH3ht48WAr1j6Ko6', '8289851760', '8289851760', 'KOTTAYAM', 'Kerala', 'MANNARAKAYAM P O \r\nPONKUNNAM\r\nKOTTAYAM', '3fd1db9e818b8d0d86499056d6c2ff3e', 1, 1, 0, 0, NULL, '2019-09-14 14:58:34', '2019-09-14 14:58:34'),
(35, 'Olegkaw', 'v.i.tal.i.ys.e.r.gee.v.0.2.20.1979@gmail.com', '$2y$10$w/z7B3cRnnHVZWrBQPtOvex1Y1FFVPBIUrSqTSlyDouWQq4GjHOwq', '84796252389', '83725265451', 'Регистрация', 'Россия', 'ьniversite mezunu', 'b501df91f79d87b93729e70b2b0638f3', 1, 1, 0, 2, NULL, '2019-09-14 16:01:35', '2019-09-18 04:09:41'),
(36, 'Brandontup', 'villirobert@yandex.ru', '$2y$10$v.cFJQi6PqkKAUmlPydG4uMBH17VazJtrCUDwSFdC1laxhCcARkfC', '84281923251', '86454612689', 'eHarmony VIP status for free.', 'Fiji', 'http://взгляд-51.рф', '8d5b5e570d5e89290c8ff8e505b08db1', 1, 1, 0, 2, NULL, '2019-09-15 09:10:08', '2019-09-18 04:09:36'),
(37, 'Jamesmep', 'dani1.n730@yandex.ru', '$2y$10$S9UmmUPsuQSlAoI8BUbt5uScKJmP4nifcs529oai80s6LpvZqPQvO', '83895382758', '87274659752', 'Рыбалка на Руси', 'Jamaica', 'http://nmp1.ru', '5a637cac6cf1fc91bd5cb5661855a3b7', 1, 1, 0, 2, NULL, '2019-09-15 10:48:39', '2019-09-18 04:09:30'),
(38, 'GeorgePioth', 'marinochka-26-04@mail.ru', '$2y$10$3/Y2gHEx33nnoRgwzCyYFOk0kqdA.GxnBjQklWpX.kJvV4vGxqPPm', '87997725778', '84783582724', 'Endьstri mьhendisi', 'Russia', 'https://ofis-kreslo.ru/', 'd3d2033bc20986fea21b0cc9c54f34a1', 1, 1, 0, 2, NULL, '2019-09-16 02:04:00', '2019-09-18 04:09:23'),
(39, 'kbqmew', 'jdresk@gmail.com', '$2y$10$nU8RkzuDcZVNUto2Sst5zesfTr1tXJr7RCV85XmXHsvH/7aa1GQP2', '87367739148', '85546152142', 'Test, just a test', 'MЬDЬR YRD', 'Tekniker', '2c6f80df71f8050a2ce4bc2a74ac09a8', 1, 1, 0, 0, NULL, '2019-09-18 05:27:41', '2019-09-18 05:27:41'),
(40, 'Binoy Babu', 'binoybabu74@gmail.com', '$2y$10$CgSY37Uc0q4QCd8kJ57m9./WTJ407IKir1/s1oDHGzWrKlY1T9siK', '9633534106', '2355407', 'Pathanapuram', 'Kerala', 'Pathanapuram', '759bd92cf03b8230550dd133a0be612d', 1, 2, 0, 0, NULL, '2019-09-18 14:25:26', '2019-09-18 14:25:26'),
(41, 'RAVI s', 'ravisattikuppe@gmail.com', '$2y$10$A9nY.yD2VWRlwt7/Yym3pO..CDGHcw2slvp8qN1oKGqc5LOzo.YKG', '8792235182', '8792235182', 'Mysuru', 'Karnataka', 'RAVI S  S/O SHIVALINGAIAH ATTIKUPPE VILLAGE CHILKUNDA POST HUNSUR TALUK MYSORE DIST KARNATAKA PIN-571105', 'b089fb35daf0e4b887f9f846657b4829', 1, 1, 0, 0, NULL, '2019-09-19 13:38:14', '2019-09-19 13:38:14'),
(42, 'Dr sajith', 'uusajith@gmail.com', '$2y$10$kuR/W5lKoFTb4JiSuFPgs.pr/gXNSmOiWFIzmtj2RoyMHMiHsKn3i', '9446235238', '04962588530', 'Calicut', 'Kerala', 'Medical officer\r\nGad nadu poyil\r\nKuttiady \r\nCalicut\r\n673507 pin', '14fa2774318cd67d855a8ba0b99a271d', 1, 1, 1, 0, 'LLcvyoG7pYUwtQ9VmIamDDOd0QCWvbaWCVnWQYW3x9TQSJcoclfuSO1qsOKM', '2019-09-20 07:16:23', '2019-09-20 07:16:47'),
(43, 'Dr Narasimha konapur', 'drnsk8891@gmail.com', '$2y$10$JNEIv/6hGvBKLWkcJ.8Ym.2QcrBReLbr3tmDm9Tv1V.1iGQfAhvoi', '9036813612', '9036813612', 'bengaluru', 'Karnataka', 'Assistant professor, Kaumarabritya department, Government Ayurveda Medical College, Dhanwantari road, Bengaluru 09', '4fa24b5125488dff8f12f056decd0d8d', 1, 1, 0, 0, NULL, '2019-09-21 07:20:32', '2019-09-21 07:20:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `creator` int(10) NOT NULL,
  `is_reserved` tinyint(1) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `role_name`, `created_at`, `creator`, `is_reserved`, `updated_at`) VALUES
(1, 'SuperAdmin', '2018-05-01 08:34:18', 0, 1, '2018-05-04 01:28:05'),
(2, 'Admin', '2018-09-23 18:30:00', 1, 1, '2018-09-23 18:30:00'),
(3, 'Sales', '2019-08-03 05:12:09', 1, 0, '2019-08-03 05:12:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `banner_images`
--
ALTER TABLE `banner_images`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `cancelled_quotations`
--
ALTER TABLE `cancelled_quotations`
  ADD PRIMARY KEY (`cancel_id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `career_files`
--
ALTER TABLE `career_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `career_form`
--
ALTER TABLE `career_form`
  ADD PRIMARY KEY (`career_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `contact_form`
--
ALTER TABLE `contact_form`
  ADD PRIMARY KEY (`contact_form_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_files`
--
ALTER TABLE `download_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `factory_images`
--
ALTER TABLE `factory_images`
  ADD PRIMARY KEY (`factory_id`);

--
-- Indexes for table `home_page_cms`
--
ALTER TABLE `home_page_cms`
  ADD PRIMARY KEY (`h_id`);

--
-- Indexes for table `measurement_units`
--
ALTER TABLE `measurement_units`
  ADD PRIMARY KEY (`mu_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `media_images`
--
ALTER TABLE `media_images`
  ADD PRIMARY KEY (`media_image_id`);

--
-- Indexes for table `media_videos`
--
ALTER TABLE `media_videos`
  ADD PRIMARY KEY (`media_video_id`);

--
-- Indexes for table `menu_master`
--
ALTER TABLE `menu_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permission_map`
--
ALTER TABLE `permission_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`additional_image_id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `product_review`
--
ALTER TABLE `product_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `quotations`
--
ALTER TABLE `quotations`
  ADD PRIMARY KEY (`quotation_id`);

--
-- Indexes for table `quotations_copy`
--
ALTER TABLE `quotations_copy`
  ADD PRIMARY KEY (`quotation_id`);

--
-- Indexes for table `quotation_details`
--
ALTER TABLE `quotation_details`
  ADD PRIMARY KEY (`details_id`);

--
-- Indexes for table `quotation_details_copy`
--
ALTER TABLE `quotation_details_copy`
  ADD PRIMARY KEY (`details_id`);

--
-- Indexes for table `quotation_history`
--
ALTER TABLE `quotation_history`
  ADD PRIMARY KEY (`quotation_history_id`);

--
-- Indexes for table `quotation_status`
--
ALTER TABLE `quotation_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `raw_materials`
--
ALTER TABLE `raw_materials`
  ADD PRIMARY KEY (`raw_material_id`);

--
-- Indexes for table `raw_material_enquiries`
--
ALTER TABLE `raw_material_enquiries`
  ADD PRIMARY KEY (`enq_id`);

--
-- Indexes for table `raw_material_types`
--
ALTER TABLE `raw_material_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`tax_id`);

--
-- Indexes for table `tenders`
--
ALTER TABLE `tenders`
  ADD PRIMARY KEY (`tender_id`);

--
-- Indexes for table `tender_files`
--
ALTER TABLE `tender_files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner_images`
--
ALTER TABLE `banner_images`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cancelled_quotations`
--
ALTER TABLE `cancelled_quotations`
  MODIFY `cancel_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `career_files`
--
ALTER TABLE `career_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `career_form`
--
ALTER TABLE `career_form`
  MODIFY `career_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `contact_form`
--
ALTER TABLE `contact_form`
  MODIFY `contact_form_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `download_files`
--
ALTER TABLE `download_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `factory_images`
--
ALTER TABLE `factory_images`
  MODIFY `factory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_page_cms`
--
ALTER TABLE `home_page_cms`
  MODIFY `h_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `measurement_units`
--
ALTER TABLE `measurement_units`
  MODIFY `mu_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `media_images`
--
ALTER TABLE `media_images`
  MODIFY `media_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `media_videos`
--
ALTER TABLE `media_videos`
  MODIFY `media_video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu_master`
--
ALTER TABLE `menu_master`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permission_map`
--
ALTER TABLE `permission_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=590;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `additional_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_price`
--
ALTER TABLE `product_price`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=855;

--
-- AUTO_INCREMENT for table `product_review`
--
ALTER TABLE `product_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotations`
--
ALTER TABLE `quotations`
  MODIFY `quotation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_details`
--
ALTER TABLE `quotation_details`
  MODIFY `details_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_history`
--
ALTER TABLE `quotation_history`
  MODIFY `quotation_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_status`
--
ALTER TABLE `quotation_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `raw_materials`
--
ALTER TABLE `raw_materials`
  MODIFY `raw_material_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=528;

--
-- AUTO_INCREMENT for table `raw_material_enquiries`
--
ALTER TABLE `raw_material_enquiries`
  MODIFY `enq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `raw_material_types`
--
ALTER TABLE `raw_material_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `tax_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tenders`
--
ALTER TABLE `tenders`
  MODIFY `tender_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tender_files`
--
ALTER TABLE `tender_files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
