<?php

namespace App\Http\Controllers\Admin;

use App\models\Activity;
use App\models\RawMaterial;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Excel;
use File;

class RawMaterialsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = RawMaterial::orderBy('raw_material_id', 'ASC')->paginate($per_page);
            return view('admin.catalog.raw-materials.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Raw Materials Required";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            $data['raw_types'] = DB::table('raw_material_types')->select('id','title')->orderby('title','ASC')->get();

            return view('admin.catalog.raw-materials.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "quantity" => "required"
        ], [
            'title.required' => 'This field is required',
            'quantity.required' => 'This field is required'
        ]);
        $changed_fileName = '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/raw-materials';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'title_en' => $request->title_en,
            'title_hn' => $request->title_hn,
            'image' => $changed_fileName,
            'size' => $request->size,
            'raw_type'=>(int)$request->raw_type,
            'botanical_name' => $request->botanical_name,
            'quantity' => $request->quantity,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $raw_material_id = RawMaterial::insertGetId($insertValues);


        if ($raw_material_id)
            $request->session()->flash('success', 'Success: Raw Materials Required Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/raw-materials');
    }

    public function edit($id)
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Raw Materials Required";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = RawMaterial::where('raw_material_id', $id)->first();
            $data['edit_id'] = $id;
            $data['raw_types'] = DB::table('raw_material_types')->select('id','title')->orderby('title','ASC')->get();

            return view('admin.catalog.raw-materials.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $raw_material_id)
    {
        $request->validate([
            "title" => "required",
            "quantity" => "required",
        ], [
            'title.required' => 'This field is required',
            'quantity.required' => 'This field is required',
        ]);

        $insertValues = array(
            'title' => $request->title,
            'title_en' => $request->title_en,
            'title_hn' => $request->title_hn,
            'size' => $request->size,
            'botanical_name' => $request->botanical_name,
            'raw_type'=>(int)$request->raw_type,
            'quantity' => $request->quantity,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = RawMaterial::where('raw_material_id', $raw_material_id)->update($insertValues);
        // remove existed file
        $image = RawMaterial::where("raw_material_id", $raw_material_id)->get(['image']);
        $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/raw-materials';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/raw-materials/' . $image_path)))
                    unlink(public_path('/uploads/raw-materials/' . $image_path));

            $up_status = RawMaterial::where('raw_material_id', $raw_material_id)->update(['image' => $changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Raw Materials Required Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/raw-materials');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = RawMaterial::where("raw_material_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

            $delete_status = RawMaterial::where('raw_material_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Raw Materials Required removed successfully');

                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/raw-materials/' . $image_path)))
                        unlink(public_path('/uploads/raw-materials/' . $image_path));
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show(Request $request, $id)
    {
        $data['page_title'] = "Raw Materials";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Raw Materials Required";

        $data['page_data'] = RawMaterial::where('raw_material_id', $id)->first();
        if ($data['page_data']) {
            $data['enquiry_data'] = DB::table('raw_material_enquiries')
                ->where('raw_material_id', (int)$id)
                ->orderby('enq_id', 'DESC')
                ->paginate(20);


            return view('admin.catalog.raw-materials.show', $data);
        } else
            $request->session()->flash('error', 'Raw material Required not found');

        return Redirect()->back();

    }

    public function importData(Request $request)
    {
        $data['page_title'] = "Raw Materials";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Raw Materials Required";


        return view('admin.catalog.raw-materials.import', $data);


    }

    public function importDataPost(Request $request)
    {
        $data['page_title'] = "Raw Materials";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Raw Materials Required";

        //validate the xls file
        $this->validate($request, array(
            'file' => 'required'
        ));

        if ($request->hasFile('file')) {
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function ($reader) {
                })->get();
                if (!empty($data) && $data->count()) {

                    $type = 1;
                    foreach ($data as $key => $value) {
                        if ($value->slno == 2222)
                            $type = 2;
                        if ($value->slno == 3333)
                            $type = 3;
                        if ($value->slno == 4444)
                            $type = 4;
                        if ($value->slno == 5555)
                            $type = 5;
                        if ($value->slno == 6666)
                            $type = 6;
                        if ($value->slno == 7777)
                            $type = 7;
                        if ($value->slno == 8888)
                            $type = 8;
                        if ($value->slno == 9999)
                            $type = 9;
                        if ($value->slno == 99991)
                            $type = 10;
                        if ($value->slno == 99992)
                            $type = 11;
                        if ($value->slno == 99993)
                            $type = 12;
                        if ($value->slno == 99994)
                            $type = 13;
                        if ($value->slno == 99995)
                            $type = 14;
                        if ($value->slno == 99996)
                            $type = 15;
                        if ($value->slno == 99997)
                            $type = 16;
                        if ($value->slno == 99998)
                            $type = 17;
                        if ($value->slno == 99999)
                            $type = 18;
                        if ($value->slno == 99990)
                            $type = 19;
                        $raws[] = array(
                            'title' => $value->title,
                            'title_en' => $value->title_en,
                            'title_hn' => '',
                            'botanical_name' => $value->title_bot,
                            'size' => '',
                            'quantity' => $value->quantity,
                            'raw_type' => $type,
                            'status' => 1,
                            'date_added' => now(),
                            'date_modified' => now(),
                        );
                    }

                    if (!empty($raws)) {

                        $insertData = RawMaterial::insert($raws);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        } else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            } else {
                Session::flash('error', 'File is a ' . $extension . ' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }


        return redirect()->back();


    }

    public function importDataPostold(Request $request)
    {
        $data['page_title'] = "Raw Materials";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Raw Materials Required";


        $request->validate([
            'file' => 'required|mimes:csv,xlsx,txt'
        ]);

        $file = $request->file('file');
        // if (strtolower($file->getClientOriginalExtension()) == 'csv') {


        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $changed_fileName = 'RawMat' . '-' . time() . '.' . $extension;

        // File upload location
        $file_saving_path = public_path() . '/uploads/imports';
        $file->move($file_saving_path, $changed_fileName);

        // Import CSV to Database
        $filepath = public_path('uploads/imports/' . $changed_fileName);
        // Reading file
        $file = fopen($filepath, "r");
        $i = 0;
        $products = array();
        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);

            // Skip first row header
            if ($i < 3) {
                $i++;
                continue;
            }
            if (!empty($filedata[1])) {
                $raws[] = array(
                    'title' => $filedata[1],
                    'title_en' => $filedata[2],
                    'title_hn' => '',
                    'botanical_name' => $filedata[3],
                    'size' => $filedata[4],
                    'quantity' => $filedata[5]
                );
            }
            $i++;
        }
        fclose($file);
        if (count($raws)) {
            foreach ($raws as $product) {
                RawMaterial::insert(array(
                    'title' => $product['title'],
                    'title_en' => $product['title_en'],
                    'title_hn' => $product['title_hn'],
                    'botanical_name' => $product['botanical_name'],
                    'size' => $product['size'],
                    'quantity' => $product['quantity'],
                    'status' => 1,
                    'date_added' => now(),
                    'date_modified' => now(),
                ));

            }

            $request->session()->flash('success', 'Success: Order Cart updated');
            return redirect(Utils::getUrlRoute() . '/raw-materials');
        } else {
            $request->session()->flash('error', 'Sorry: Invalid data');

        }

        return redirect()->back();


    }
}
