<?php

namespace App\Http\Controllers\Admin;

use App\models\Activity;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class AddressController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Address";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('addresses')->orderBy('id', 'DESC')->paginate($per_page);
            return view('admin.catalog.address.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Address";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Address";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.address.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $changed_fileName = '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/address';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'address' => $request->address,
            'phone' => $request->phone,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'image' => $changed_fileName,
            'status' => $request->status,
            'map_link'=>$request->map_link,
            'sort_order'=>(int)$request->sort_order,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $id = DB::table('addresses')->insertGetId($insertValues);


        if ($id)
            $request->session()->flash('success', 'Success: Address Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/addresses');
    }

    public function edit($id)
    {
        $data['page_title'] = "Address";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Address";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('addresses')->where('id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.address.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);

        $insertValues = array(
            'title' => $request->title,
            'address' => $request->address,
            'phone' => $request->phone,
            'mobile' => $request->mobile,
            'email' => $request->email,
            'status' => $request->status,
            'map_link'=>$request->map_link,
            'sort_order'=>(int)$request->sort_order,
            'date_modified' => now(),
        );
        $up_status = DB::table('addresses')->where('id', $id)->update($insertValues);
        // remove existed file
        $image = DB::table('addresses')->where("id", $id)->get(['image']);
        $image_path = (isset($image[0]->image))?$image[0]->image:'';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/address';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/address/' . $image_path)))
                    unlink(public_path('/uploads/address/' . $image_path));

            $up_status = DB::table('addresses')->where('id', $id)->update(['image'=>$changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Address Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/addresses');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = DB::table('addresses')->where("id", $id)->get(['image']);
            $image_path = (isset($image[0]->image))?$image[0]->image:'';

            $delete_status = DB::table('addresses')->where('id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Address removed successfully');

                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/address/' . $image_path)))
                        unlink(public_path('/uploads/address/' . $image_path));
            }else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
