<?php

namespace App\Http\Controllers\Admin;

use App\models\Testimonial;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestimonialsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Testimonial";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Testimonial::orderBy('testimonial_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.testimonials.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Testimonial";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Testimonial";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.testimonials.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "description" => "required",
            "name" => "required",
        ], [
            'title.required' => 'This field is required',
            'description.required' => 'This field is required',
            'name.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'description' => $request->description,
            'name' => $request->name,
            'designation' => $request->designation,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $testimonial_id = Testimonial::insertGetId($insertValues);


        if ($testimonial_id)
            $request->session()->flash('success', 'Success: Testimonial Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/testimonials');
    }

    public function edit($id)
    {
        $data['page_title'] = "Testimonial";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Testimonial";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = Testimonial::where('testimonial_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.testimonials.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $testimonial_id)
    {
        $this->validate($request, [
            "title" => "required",
            "description" => "required",
            "name" => "required",
        ], [
            'title.required' => 'This field is required',
            'description.required' => 'This field is required',
            'name.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'description' => $request->description,
            'name' => $request->name,
            'designation' => $request->designation,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = Testimonial::where('testimonial_id', $testimonial_id)->update($insertValues);

        if ($up_status)
            $request->session()->flash('success', 'Success: Testimonial Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/testimonials');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $delete_status = Testimonial::where('testimonial_id', $id)->delete();
            if ($delete_status)
                $request->session()->flash('success', 'Testimonial removed successfully');
            else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
