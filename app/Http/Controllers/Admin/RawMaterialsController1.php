<?php

namespace App\Http\Controllers\Admin;

use App\models\Activity;
use App\models\RawMaterial;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class RawMaterialsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = RawMaterial::orderBy('raw_material_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.raw-materials.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Raw Materials Required";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.raw-materials.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
            "common_name" => "required",
            "quantity" => "required",
            "contact_person" => "required",
            "contact_number" => "required",
            "contact_email" => "required",
        ], [
            'title.required' => 'This field is required',
            'common_name.required' => 'This field is required',
            'quantity.required' => 'This field is required',
            'contact_person.required' => 'This field is required',
            'contact_number.required' => 'This field is required',
            'contact_email.required' => 'This field is required',
        ]);
        $changed_fileName = '';
        $page_slug = Utils::craeteUniqueSlugForTable('raw_materials', 'slug', $request->title, 'raw_material_id', 0);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/raw-materials';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'slug' => $page_slug,
            'description' => $request->description,
            'image' => $changed_fileName,
            'common_name' => $request->common_name,
            'scientific_name' => $request->scientific_name,
            'botanical_name' => $request->botanical_name,
            'quantity' => $request->quantity,
            'contact_person' => $request->contact_person,
            'contact_number' => $request->contact_number,
            'contact_email' => $request->contact_email,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $raw_material_id = RawMaterial::insertGetId($insertValues);


        if ($raw_material_id)
            $request->session()->flash('success', 'Success: Raw Materials Required Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/raw-materials');
    }

    public function edit($id)
    {
        $data['page_title'] = "Raw Materials Required";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Raw Materials Required";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = RawMaterial::where('raw_material_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.raw-materials.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $raw_material_id)
    {
        $request->validate([
            "title" => "required",
            "common_name" => "required",
            "quantity" => "required",
            "contact_person" => "required",
            "contact_number" => "required",
            "contact_email" => "required",
        ], [
            'title.required' => 'This field is required',
            'common_name.required' => 'This field is required',
            'quantity.required' => 'This field is required',
            'contact_person.required' => 'This field is required',
            'contact_number.required' => 'This field is required',
            'contact_email.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('raw_materials', 'slug', $request->title, 'raw_material_id', $raw_material_id);

        $insertValues = array(
            'title' => $request->title,
            'slug' => $page_slug,
            'description' => $request->description,
            'common_name' => $request->common_name,
            'scientific_name' => $request->scientific_name,
            'botanical_name' => $request->botanical_name,
            'quantity' => $request->quantity,
            'contact_person' => $request->contact_person,
            'contact_number' => $request->contact_number,
            'contact_email' => $request->contact_email,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = RawMaterial::where('raw_material_id', $raw_material_id)->update($insertValues);
        // remove existed file
        $image = RawMaterial::where("raw_material_id", $raw_material_id)->get(['image']);
        $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/raw-materials';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/raw-materials/' . $image_path)))
                    unlink(public_path('/uploads/raw-materials/' . $image_path));

            $up_status = RawMaterial::where('raw_material_id', $raw_material_id)->update(['image' => $changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Raw Materials Required Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/raw-materials');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = RawMaterial::where("raw_material_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image)) ? $image[0]->image : '';

            $delete_status = RawMaterial::where('raw_material_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Raw Materials Required removed successfully');

                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/raw-materials/' . $image_path)))
                        unlink(public_path('/uploads/raw-materials/' . $image_path));
            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show(Request $request, $id)
    {
        $data['page_title'] = "Raw Materials";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Raw Materials Required";

        $data['page_data'] = RawMaterial::where('raw_material_id', $id)->first();
        if ($data['page_data']) {
            $data['enquiry_data']=DB::table('raw_material_enquiries')
                ->where('raw_material_id',(int)$id)
                ->orderby('enq_id','DESC')
                ->paginate(20);


            return view('admin.catalog.raw-materials.show', $data);
        } else
            $request->session()->flash('error', 'Raw material Required not found');

        return Redirect()->back();

    }
}
