<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DownloadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Downloads";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('downloads')->orderBy('id', 'DESC')->paginate($per_page);
            return view('admin.catalog.downloads.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Downloads";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Downloads";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.downloads.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $id = DB::table('downloads')->insertGetId($insertValues);
        if (isset($request->download_file)) {
            foreach ($request->download_file as $tf) {
                if(isset($tf['name']))
                $im = $tf['name'];
                else
                    $im='';
                if ($im != '') {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/downloads';
                    $im->move($file_saving_path, $changed_fileName);

                    DB::table('download_files')->insert(array('download_id' => (int)$id, 'title' => $tf['title'], 'file_link' => $changed_fileName));


                }
            }

        }
        if ($id)
            $request->session()->flash('success', 'Success: Downloads Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/downloads');
    }

    public function edit($id)
    {
        $data['page_title'] = "Downloads";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Downloads";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('downloads')->where('id', $id)->first();
            $data['download_files'] = DB::table('download_files')->where('download_id', $id)->get();
            $data['edit_id'] = $id;
            return view('admin.catalog.downloads.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $insertValues = array(
            'title' => $request->title,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = DB::table('downloads')->where('id', $id)->update($insertValues);
        DB::table('download_files')->where('download_id', $id)->delete();
        if (isset($request->download_file)) {
            foreach ($request->download_file as $tf) {
                $im = isset($tf['name']) ? $tf['name'] : '';
                if ($im != '') {
                    $extension = $im->getClientOriginalExtension();
                    $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                    $file_saving_path = public_path() . '/uploads/downloads';
                    $im->move($file_saving_path, $changed_fileName);
                    if (isset($tf['file_id']))
                        DB::table('download_files')->insert(array('file_id' => $tf['file_id'], 'download_id' => (int)$id, 'title' => $tf['title'], 'file_link' => $changed_fileName));
                    else
                        DB::table('download_files')->insert(array('download_id' => (int)$id, 'title' => $tf['title'], 'file_link' => $changed_fileName));


                } else {
                    if (isset($tf['file_id']))
                        DB::table('download_files')->insert(array('file_id' => $tf['file_id'], 'download_id' => (int)$id, 'title' => $tf['title'], 'file_link' => $tf['file_link']));
                    else
                        DB::table('download_files')->insert(array('download_id' => (int)$id, 'title' => $tf['title'], 'file_link' => $tf['file_link']));


                }
            }
        }
        if ($up_status)
            $request->session()->flash('success', 'Success: Downloads Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/downloads');
    }


    public
    function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {


            $delete_status = DB::table('downloads')->where('id', $id)->delete();
            $trr = DB::table('download_files')->where('download_id', (int)$id)->get();
            if (isset($trr))
                foreach ($trr as $trr_f) {
                    if (!empty($trr_f->file_link) && $trr_f->file_link != null)
                        if (file_exists(public_path('/uploads/downloads/' . $trr_f->file_link)))
                            unlink(public_path('/uploads/downloads/' . $trr_f->file_link));
                    DB::table('download_files')->where('file_id', (int)$trr_f->file_id)->delete();
                }
            if ($delete_status) {
                $request->session()->flash('success', 'Downloads removed successfully');

            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public
    function show()
    {
    }
}
