<?php

namespace App\Http\Controllers\Admin;

use App\Mails\QuotationStatus;
use App\models\Common;
use App\models\Notifications;
use App\models\Product;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Mail;

class QuotationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        //
        $data['page_title'] = "Performa Invoice";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('quotations as q')
                ->join('users as u', 'u.id', '=', 'q.user_id')
                ->where('q.status', 1);

            if ($request->quotation_id != "")
                $data['page_data'] = $data['page_data']->where('quotation_id', $request->quotation_id);

            if ($request->status != "")
                $data['page_data'] = $data['page_data']->where('quotation_status', $request->status);

            if ($request->from_date != "")
                $data['page_data'] = $data['page_data']->whereDate('q.date_added', '>=', $request->from_date);

            if ($request->to_date != "")
                $data['page_data'] = $data['page_data']->whereDate('q.date_added', '<=', $request->to_date);

            if ($request->user_type != "")
                $data['page_data'] = $data['page_data']->where('u.user_type', '=', (int)$request->user_type);

            if ($request->dealer != "")
                $data['page_data'] = $data['page_data']->where('u.name', 'LIKE', "%$request->dealer%");

            $data['page_data'] = $data['page_data']->orderBy('quotation_id', 'DESC')
                ->paginate($per_page);

            $data['quotation_status'] = DB::table('quotation_status')->where('status', 1)->get();

            return view('admin.quotations.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($quotation_id)
    {
        //

        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->first();
        if (isset($quotation)) {
            $data['crud_permissions'] = Utils::crudPermissions();
            if (in_array(2, $data['crud_permissions'])) {
                $data['page_title'] = "Performa Invoice #" . $quotation_id;
                $data['page_subtitle'] = "";
                $data['page_data'] = $quotation;
                $data['user_details'] = User::getUserDetails($quotation->user_id);
                $data['quotation_details'] = array();
                $data['quotation_status'] = DB::table('quotation_status')->where('status', 1)->get();
                $data['quotation_histories'] = DB::table('quotation_history as h')
                    ->join('quotation_status as s', 'h.status_id', '=', 's.status_id')
                    ->select('h.date_added', 'h.status_id', 'h.comment', 's.name')
                    ->where('h.quotation_id', (int)$quotation_id)
                    ->orderBy('quotation_history_id', 'ASC')
                    ->get();
                $data['quotation_details'] = $quotation_details = DB::table('quotation_details as q')
                    ->select('q.*',DB::raw('(SELECT p.model FROM product p WHERE p.product_id = q.product_id) as product_code'))
                    ->where('q.quotation_id', (int)$quotation_id)->where('q.is_deleted', 0)->get();
                return view('admin.quotations.show', $data);
            } else {
                return redirect('access-denied');
            }

        } else
            return redirect('access-denied');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $quotation_id)
    {
        $quotation = DB::table('quotations as q')
            ->join('users as u', 'u.id', '=', 'q.user_id')
            ->select('q.*','u.user_type')
            ->where('q.quotation_id', (int)$quotation_id)
            ->where('q.status', 1)
            ->first();
        if (isset($quotation)) {
            if ($quotation->quotation_status != 1) {
                $data['crud_permissions'] = Utils::crudPermissions();
                if (in_array(3, $data['crud_permissions'])) {
                    $data['page_title'] = "Performa Invoice #" . $quotation_id;
                    $data['page_subtitle'] = "Modify Performa Invoice";
                    $data['page_data'] = $quotation;
                    $data['edit_id'] = (int)$quotation_id;
                    $data['quotation_details'] = array();
                    $data['quotation_details'] = $quotation_details = DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->where('is_deleted', 0)->get();

                    return view('admin.quotations.edit', $data);
                } else {
                    return redirect('access-denied');
                }
            } else {
                $request->session()->flash('error', 'Sorry: Performa Invoice still in pending status.Change status to reviewed before edit');
                return redirect(Utils::getUrlRoute() . '/quotations');

            }

        } else {
            $request->session()->flash('error', 'Sorry: Performa Invoice not found');
            return redirect(Utils::getUrlRoute() . '/quotations');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $quotation_id)
    {

        if (isset($request->products)) {
            $total = 0;
            DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->delete();
            foreach ($request->products as $product) {
                if (isset($product['product_id'])) {
                    if (isset($product['desc']))
                        foreach ($product['desc'] as $price_det) {
                            if (isset($product['details_id'])) {
                                $details_id = DB::table('quotation_details')->insertGetId(
                                    array('details_id' => (int)$product['details_id'],
                                        'quotation_id' => (int)$quotation_id,
                                        'product_id' => (int)$product['product_id'],
                                        'product_name' => $product['name'],
                                        'qty_original' => $price_det['qty_original'],
                                        'qty' => $price_det['qty'],
                                        'price' => $price_det['price'],
                                        'uprice'=>$price_det['uprice'],
                                        'utax'=>$price_det['utax'],
                                        'total_original' => ($price_det['price'] * $price_det['qty_original']),
                                        'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                        'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                        'total' => ($price_det['price'] * $price_det['qty']),
                                        'admin_id' => 0,
                                        'unit_value' => $price_det['unit_value'],
                                        'is_deleted' => $product['status'],
                                        'date_added' => now(),
                                        'date_modified' => now())
                                );
                                if (!$product['status'])
                                    $total += ($price_det['price'] * $price_det['qty']);
                            } else {
                                if ((int)$price_det['qty'] > 0 && (int)$price_det['price'] > 0) {
                                    DB::table('quotation_details')->insertGetId(
                                        array('quotation_id' => (int)$quotation_id,
                                            'product_id' => (int)$product['product_id'],
                                            'product_name' => $product['name'],
                                            'qty_original' => $price_det['qty'],
                                            'qty' => $price_det['qty'],
                                            'price' => $price_det['price'],
                                            'uprice'=>$price_det['uprice'],
                                            'utax'=>$price_det['utax'],
                                            'total_original' => ($price_det['price'] * $price_det['qty']),
                                            'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                            'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                            'total' => ($price_det['price'] * $price_det['qty']),
                                            'unit_value' => $price_det['unit_value'],
                                            'is_deleted' => 0,
                                            'admin_id' => 0,
                                            'date_added' => now(),
                                            'date_modified' => now())
                                    );
                                    $total += ($price_det['price'] * $price_det['qty']);
                                }
                            }
                        }
                }
            }
            $up_status = DB::table('quotations')->where('quotation_id', (int)$quotation_id)->update([
                'total' => $total, 'date_modified' => now()
            ]);

            if ($up_status)
                $request->session()->flash('success', 'Success: Performa Invoice updated');
            else
                $request->session()->flash('error', 'Sorry: Performa Invoice updation failed. Please try again');

        } else {
            //no product selected

            $request->session()->flash('error', 'Sorry: You haven\'t selected any product');
        }


        return redirect(Utils::getUrlRoute() . '/quotations');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateStatus(Request $request, $quotation_id = 0)
    {

        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->first();
        if (isset($quotation)) {
            if ((int)$quotation->quotation_status != (int)$request->quotation_status) {
                if (((int)$quotation->quotation_status) < ((int)$request->quotation_status)) {
                    $history_id = DB::table('quotation_history')->insertGetId(
                        array('quotation_id' => (int)$quotation_id, 'status_id' => (int)$request->quotation_status,
                            'comment' => $request->comment, 'date_added' => now())
                    );
                    $up_id = DB::table('quotations')->where('quotation_id', (int)$quotation_id)->update(
                        array('quotation_status' => $request->quotation_status, 'date_modified' => now())
                    );
                    if ($request->quotation_status == 5) {
                        $cl_quot = DB::table('cancelled_quotations')->insert([
                            'quotation_id' => (int)$quotation_id,
                            'user_id' => Auth::user()->id, 'type' => 'admin',
                            'reason' => 'Admin cancelled Performa Invoice. Reason described: ' . $request->comment, 'date_added' => now()
                        ]);
                    }

                    $user_email = User::getUserDetails($quotation->user_id);
                    if (isset($user_email->email) && !empty($user_email->email)) {
                        $quot_data['status'] = Common::getQuotationStatusName((int)$request->quotation_status);
                        $quot_data['user_name'] = $user_email->name;
                        $quot_data['quotation_id'] = (int)$quotation_id;
                        $quot_data['quotation'] = Common::getQuotationDetails((int)$quotation_id);

                        $mail_data = array('data' => $quot_data, 'comment' => $request->comment);
                        Mail::to($user_email->email)->send(new QuotationStatus($mail_data));
                    }

                    if ($history_id) {
                        $request->session()->flash('success', 'Success: Performa Invoice status added');
                        $mg = 'Quotation #' . $quotation_id . ' Status changed to `' . Common::getQuotationStatusName((int)$request->quotation_status) . '`';
                        Notifications::addNotification($quotation->user_id, 'quotation', 'user', (int)$quotation_id, $mg, '');

                    } else
                        $request->session()->flash('error', 'Sorry: request failed. Please try again');
                } else
                    $request->session()->flash('error', 'Sorry: You cant change to this status');
            }
            return redirect(Utils::getUrlRoute() . '/quotations/' . $quotation_id . '#tab-status');
        } else {
            $request->session()->flash('error', 'Sorry: Performa Invoice not found');
            return redirect(Utils::getUrlRoute() . '/quotations');
        }
    }

    public function cancelledQuotations()
    {
        $data['page_title'] = "Cancelled Performa Invoice";
        $data['page_subtitle'] = "";

        $data['page_data'] = DB::table('quotations as q')
            ->join('cancelled_quotations as cq', 'cq.quotation_id', '=', 'q.quotation_id')
            ->select('q.*', 'cq.reason', 'cq.date_added as date_cancel')
            ->orderBy('cancel_id', 'DESC')
            ->paginate(20);
        return view('admin.quotations.cancel_index', $data);
    }

    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('product')
                ->select('product_id', 'title')
                ->where('status', '1')
                ->where('title', 'LIKE', "%{$query}%")
                ->get();

            $json = array();
            foreach ($data as $result) {
                $json[] = array(
                    'product_id' => $result->product_id,
                    'name' => $result->title
                );
            }

            return response()->json($json, 200);;
        }
    }

    function fetchPrice(Request $request)
    {
        $user_type=User::getUserType((int)$request->get('user_id'));
        if ($request->get('product_id')) {
            $product_id = (int)$request->get('product_id');
            $data = DB::table('product_price')
                ->where('product_id', $product_id)
                ->get();

            $tax_array = Product::getProductTaxOptions((int)$request->get('product_id'));

            $json = array();
            foreach ($data as $result) {
                $price = $result->price;
                if ($user_type == 2)
                    $price = $result->dealer_price;

                $tax = 0;
                if ($tax_array['type'] == 'P')
                    $tax = round((($price * $tax_array['value']) / 100), 2);
                elseif ($tax_array['type'] == 'F')
                    $tax = $tax_array['value'];

                $json[] = array(
                    'unit_value' => $result->unit_value,
                    'price' => round(($price + $tax), 2),
                    'uprice' => $price,
                    'utax' => round($tax, 2),
                );
            }

            return response()->json($json, 200);;
        }
    }

    public function QuotationReport(Request $request)
    {

        $data['page_title'] = "Performa Invoice Report";
        $data['page_subtitle'] = "";

        $past_month_data = DB::table('quotations as q')
            ->join('quotation_details as qd', 'qd.quotation_id', '=', 'q.quotation_id')
            ->join('users as u', 'u.id', '=', 'q.user_id')
            ->select(DB::raw('u.name,q.quotation_id,count(distinct qd.product_id) as total_products,sum(qd.total) as total_price,sum(qd.qty) as total_qty'))
            ->where('q.status', 1)->where('qd.is_deleted', 0);


        if ($request->date_from != "" && $request->date_to != "") {
            $past_month_data = $past_month_data->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
            if (strtotime($request->date_to))
                $past_month_data = $past_month_data->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

        }
        if ($request->search != "")
            $past_month_data = $past_month_data->where('u.name', 'LIKE', "%$request->search%");

        $past_month_data = $past_month_data
            ->groupby('qd.quotation_id')
            ->orderby('q.quotation_id', 'DESC')
            ->paginate(20);

        $data['quot_products'] = $past_month_data;
        //dd($past_month_data);
        return view('admin.reports.quotation', $data);
    }

    public function productWiseReport(Request $request)
    {

        $data['page_title'] = "Performa Invoice-Product Report";
        $data['page_subtitle'] = "";

        $past_month_data = DB::table('product as p')
            ->join('quotation_details as qd', 'qd.product_id', '=', 'p.product_id')
            ->join('quotations as q', 'q.quotation_id', '=', 'qd.quotation_id')
            ->select(DB::raw('p.product_id as product_id,p.title as title,q.quotation_id,(SELECT sum(qdd.total) FROM quotation_details qdd WHERE qdd.product_id = p.product_id GROUP BY qdd.product_id) as sum_total_price,
            (SELECT sum(qdd.qty)  FROM quotation_details qdd WHERE qdd.product_id = p.product_id GROUP BY qdd.product_id) as sum_total_qty'))
            ->where('q.status', 1)->where('p.status', 1)->where('qd.is_deleted', 0);

        if ($request->date_from != "" && $request->date_to != "") {
            $past_month_data = $past_month_data->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
            if (strtotime($request->date_to))
                $past_month_data = $past_month_data->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

        }
        if ($request->search != "")
            $past_month_data = $past_month_data->where('p.title', 'LIKE', "%$request->search%");

        $past_month_data = $past_month_data->groupby('qd.product_id')->orderby('p.title', 'ASC')
            ->paginate(20);
        $data['quot_products'] = $past_month_data;

        return view('admin.reports.quot_products', $data);
    }


    public function dealerWiseReport(Request $request)
    {

        $data['page_title'] = "Performa Invoice-Product Report";
        $data['page_subtitle'] = "";

        $past_month_data = DB::table('users as u')
            ->join('quotations as q', 'q.user_id', '=', 'u.id')
            ->select(DB::raw('u.id as user_id,u.name as name,u.email as email,(SELECT count(qdd.quotation_id) FROM quotations qdd WHERE qdd.user_id = u.id GROUP BY qdd.user_id) as total_quotation,
            (SELECT sum(qdd.total) FROM quotations qdd WHERE qdd.user_id = u.id GROUP BY qdd.user_id) as total_price'))
            ->where('q.status', 1);

        if ($request->date_from != "" && $request->date_to != "") {
            $past_month_data = $past_month_data->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
            if (strtotime($request->date_to))
                $past_month_data = $past_month_data->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

        }
        if ($request->search != "")
            $past_month_data = $past_month_data->where('u.name', 'LIKE', "%$request->search%");

        $past_month_data = $past_month_data->groupby('u.name')->orderby('u.name', 'ASC')
            ->paginate(20);
        $data['quot_products'] = $past_month_data;

        return view('admin.reports.dealer', $data);
    }

    public function categoryWiseReport(Request $request)
    {

        $data['page_title'] = "Performa Invoice-Category Report";
        $data['page_subtitle'] = "";

        $past_month_data = DB::table('category as c')
            ->join('product as p', 'p.category_id', '=', 'c.category_id')
            ->join('quotation_details as qd', 'qd.product_id', '=', 'p.product_id')
            ->join('quotations as q', 'q.quotation_id', '=', 'qd.quotation_id')
            ->select(DB::raw('c.title as title,count(distinct qd.quotation_id) as total_quotation,sum(qd.total) as total_price,sum(qd.qty) as total_qty'))
            ->where('q.status', 1)
            ->where('p.status', 1)->where('qd.is_deleted', 0)->where('c.status', 1);


        if ($request->date_from != "" && $request->date_to != "") {
            $past_month_data = $past_month_data->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
            if (strtotime($request->date_to))
                $past_month_data = $past_month_data->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

        }
        if ($request->search != "")
            $past_month_data = $past_month_data->where('c.title', 'LIKE', "%$request->search%");

        $past_month_data = $past_month_data
            ->groupby('c.category_id')
            ->orderby('c.title', 'ASC')
            ->paginate(20);

        $data['quot_products'] = $past_month_data;
        //dd($past_month_data);
        return view('admin.reports.category', $data);
    }


    public function PerformanceReport(Request $request)
    {
        $data['page_title'] = "Performance Report";
        $data['page_subtitle'] = "";

        $past_month_data = DB::table('quotation_status as qs')
            ->leftJoin('quotations as q', function ($join) use ($request) {
                $join->on('q.quotation_status', '=', 'qs.status_id')
                    ->where('q.status', 1);
                if ($request->date_from != "" && $request->date_to != "") {
                    $join->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
                    if (strtotime($request->date_to))
                        $join->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

                }

            })
            ->leftJoin('quotation_details as qd', function ($join) {
                $join->on('qd.quotation_id', '=', 'q.quotation_id')->where('qd.is_deleted', 0);
            })
            ->select(DB::raw('qs.status_id as status_id,qs.name as status_title,qs.color_code as color,count(distinct q.quotation_id) as total_quotations'));


        $past_month_data = $past_month_data
            ->groupby('qs.status_id')
            ->orderby('q.quotation_id', 'ASC')
            ->get();

        $total_quot = 0;
        if ($past_month_data)
            foreach ($past_month_data as $dt)
                $total_quot += $dt->total_quotations;


        $total_quot_data = DB::table('quotations as q')
            ->leftJoin('quotation_details as qd', function ($join) {
                $join->on('qd.quotation_id', '=', 'q.quotation_id')->where('qd.is_deleted', 0);
            })
            ->select(DB::raw('sum(qd.total) as total_amount,sum(qd.qty) as total_qty,count(distinct product_id) as total_products'))
            ->where('q.status', 1);

        if ($request->date_from != "" && $request->date_to != "") {
            $total_quot_data=$total_quot_data->whereDate('q.date_added', '>=', date('Y-m-d', strtotime($request->date_from)));
            if (strtotime($request->date_to))
                $total_quot_data=$total_quot_data->whereDate('q.date_added', '<=', date('Y-m-d', strtotime($request->date_to)));

        }
        $total_quot_data=$total_quot_data->first();

            $data['status_data'] = $past_month_data;
            $data['total_quot'] = $total_quot;
            $data['quotation_data'] = $total_quot_data;

        return view('admin.reports.performance', $data);
        }

    }
