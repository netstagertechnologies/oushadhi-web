<?php

namespace App\Http\Controllers\Admin;

use App\models\Blog;
use App\models\FactoryImage;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactoryImageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Factory Image";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = FactoryImage::orderBy('factory_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.factory-images.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Factory Image";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Factory Image";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.factory-images.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $changed_fileName = '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/factory-images';
            $file->move($file_saving_path, $changed_fileName);
        }
        $insertValues = array(
            'title' => $request->title,
            'image' => $changed_fileName,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $factory_id = FactoryImage::insertGetId($insertValues);


        if ($factory_id)
            $request->session()->flash('success', 'Success: Factory Image Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/factory-images');
    }

    public function edit($id)
    {
        $data['page_title'] = "Factory Image";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Factory Image";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = FactoryImage::where('factory_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.catalog.factory-images.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $factory_id)
    {
        $this->validate($request, [
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);

        $insertValues = array(
            'title' => $request->title,
            'date_modified' => now(),
        );
        $up_status = FactoryImage::where('factory_id', $factory_id)->update($insertValues);
        // remove existed file
        $image = FactoryImage::where("factory_id", $factory_id)->get(['image']);
        $image_path = (isset($image[0]->image))?$image[0]->image:'';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $changed_fileName = uniqid() . time() . "." . $extension;
            $file_saving_path = public_path() . '/uploads/factory-images';
            $file->move($file_saving_path, $changed_fileName);
            if (!empty($image_path) && $image_path != null)
                if (file_exists(public_path('/uploads/factory-images/' . $image_path)))
                    unlink(public_path('/uploads/factory-images/' . $image_path));

            $up_status = FactoryImage::where('factory_id', $factory_id)->update(['image'=>$changed_fileName]);
        }

        if ($up_status)
            $request->session()->flash('success', 'Success: Factory Image Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/factory-images');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $image = FactoryImage::where("factory_id", $id)->get(['image']);
            $image_path = (isset($image[0]->image))?$image[0]->image:'';


            $delete_status = FactoryImage::where('factory_id', $id)->delete();
            if ($delete_status) {
                $request->session()->flash('success', 'Factory Image removed successfully');
                if (!empty($image_path) && $image_path != null)
                    if (file_exists(public_path('/uploads/factory-images/' . $image_path)))
                        unlink(public_path('/uploads/factory-images/' . $image_path));
            }else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
