<?php

namespace App\Http\Controllers\Admin;

use App\models\Category;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Category";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = Category::orderBy('category_id', 'DESC')->paginate($per_page);
            return view('admin.manage_product.category.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Category";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Category";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.manage_product.category.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

        $request->validate([
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('category', 'slug', $request->title, 'category_id', 0);

        $insertValues = array(
            'title' => $request->title,
            'slug'=>$page_slug,
            'sort_order' => (int)$request->sort_order,
            'parent_id' => 0,
            'product_type' => (int)$request->product_type,
            'status' => $request->status,
            'date_added' => now(),
            'date_modified' => now(),
        );
        $category_id = Category::insertGetId($insertValues);


        if ($category_id)
            $request->session()->flash('success', 'Success: Category Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/category');
    }

    public function edit($id)
    {
        $data['page_title'] = "Category";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify Category";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = Category::where('category_id', $id)->first();
            $data['edit_id'] = $id;
            return view('admin.manage_product.category.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $category_id)
    {
        $this->validate($request, [
            "title" => "required",
        ], [
            'title.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('category', 'slug', $request->title, 'category_id', $category_id);

        $insertValues = array(
            'title' => $request->title,
            'slug'=>$page_slug,
            'product_type' => (int)$request->product_type,
            'sort_order' => (int)$request->sort_order,
            'status' => $request->status,
            'date_modified' => now(),
        );
        $up_status = Category::where('category_id', $category_id)->update($insertValues);

        if ($up_status)
            $request->session()->flash('success', 'Success: Category Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/category');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {

            $delete_status = Category::where('category_id', $id)->delete();
            if ($delete_status)
                $request->session()->flash('success', 'Category removed successfully');
            else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
