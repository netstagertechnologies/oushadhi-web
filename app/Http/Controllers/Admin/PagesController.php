<?php

namespace App\Http\Controllers\admin;

use App\models\Page;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\UserRole;
use App\MenuMaster;
use App\Utils;
use App\Permissions;
use Auth;
use Session;
use Redirect;
use DB;
use View;

class PagesController extends Controller
{
    public function __construct()
    {
        // for the authentication user and permissions
        $this->middleware('auth:admin');
    }

    /*
        * routed for memebers or admin
    */

    public function index(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 15;

        if (!empty($keyword)) {
            $pages = Page::where('title', 'LIKE', "%$keyword%")->where('status', 1)
                ->paginate($perPage);
        } else {
            $pages = Page::where('status', 1)->paginate($perPage);
        }

        $page_title = 'Page';
        $page_description = 'records';
        return view('admin.cms.page-cms.page_index', compact('get_permissions', 'page_title', 'page_description', 'pages'));
    }


    /*
        * edit page details
    */

    public function edit(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(3, $crud_permissions)) {
            $is_admin = Auth::user()->is_admin;
            $user_id = Auth::user()->id;
            $user_role = Auth::user()->user_role;
            $page = Page::where("page_id", $id)->get();
            if ($is_admin) {
                $get_permissions = Permissions::user_permissions();
            } else {
                $get_permissions = Permissions::user_permissions($user_role);
            }
            $page_title = 'Page';
            $page_description = 'edits';
            return view('admin.cms.page-cms.edit', compact('get_permissions', 'page_title', 'page_description', 'page'));
        } else {
            $request->session()->flash('info', 'Invalid permission access');
            return Redirect::back();
        }
    }

    /*
        * update city details
    */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "title" => "required",
            "page_content" => "required",
            "banner" => "required",
            "metaTitle" => "required",
            "metaDescription" => "required",
            "metaKeyword" => "required",
        ], [
            'title.required' => 'This field is required',
            'content.required' => 'This field is required',
            'banner.required' => 'This field is required',
            'metaTitle.required' => 'This field is required',
            'metaDescription.required' => 'This field is required',
            'metaKeyword.required' => 'This field is required',
        ]);
        $page_slug = Utils::craeteUniqueSlugForTable('pages', 'slug', $request->title, 'page_id', $id);
        $updateValues = array(
            'title' => $request->title,
            'page_content' => $request->page_content,
            'slug' => $page_slug,
            'metaTitle' => $request->metaTitle,
            'metaDescription' => $request->metaDescription,
            'metaKeyword' => $request->metaKeyword,
            'banner' => $request->banner,
        );
        Page::where('page_id', $id)->update($updateValues);
        $request->session()->flash('success', 'Page updated successfully');
        return Redirect::back();
    }
}
