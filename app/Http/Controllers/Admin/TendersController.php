<?php

namespace App\Http\Controllers\Admin;

use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class TendersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(Request $request)
    {
        $data['page_title'] = "Tenders";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "";
        $per_page = 20;
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(2, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('tenders')->orderBy('tender_id', 'DESC')->paginate($per_page);
            return view('admin.catalog.tenders.index', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function create()
    {
        $data['page_title'] = "Tenders";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Add Tenders";

        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(1, $data['crud_permissions'])) {
            return view('admin.catalog.tenders.create', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function store(Request $request)
    {

            $request->validate([
                "title" => "required",
                "last_date" => "required",
                "last_time" => "required",
                "tender_type" => "required",
            ], [
                'title.required' => 'This field is required',
                'last_date.required' => 'please enter last date and time',
                'last_time.required' => 'please enter last date and time ',
                'tender_type.required' => 'This field is required',
            ]);
       // if ($request->tender_type == 2) {
           /* $insertValues = array(
                'title' => $request->title,
                'tender_type' => $request->tender_type,
                'tender_link' => $request->tender_link,
                'status' => $request->status,
                'date_added' => now(),
                'date_modified' => now(),
            );
            $tender_id = DB::table('tenders')->insertGetId($insertValues);

        } else {*/
            $insertValues = array(
                'title' => $request->title,
                /*'from_date' => $request->from_date,
                'to_date' => $request->to_date,*/
                'last_date' => $request->last_date,
                'last_time' => $request->last_time,
                'dept' => $request->dept,
                'contact' => $request->contact,
                'contact_email' => $request->contact_email,
                'tender_type' => $request->tender_type,
                'status' => $request->status,
                'date_added' => now(),
                'tender_link' => $request->tender_link,
                'date_modified' => now(),
            );
            $tender_id = DB::table('tenders')->insertGetId($insertValues);

            if (isset($request->tender_file)) {
                foreach ($request->tender_file as $tf) {
                    $im = (isset($tf['name'])&&!empty($tf['name'])) ? $tf['name'] : '';
                    if (!empty($im)) {
                        $extension = $im->getClientOriginalExtension();
                        $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                        $file_saving_path = public_path() . '/uploads/tenders';
                        $im->move($file_saving_path, $changed_fileName);

                        DB::table('tender_files')->insert(array('tender_id' => (int)$tender_id, 'title' => $tf['title'], 'file_link' => $changed_fileName));


                    }
                }
            }
        //}
        if ($tender_id)
            $request->session()->flash('success', 'Success: tenders Added');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/tenders');
    }

    public function edit($id)
    {
        $data['page_title'] = "tenders";
        $data['page_subtitle'] = "";
        $data['page_subtitle_desc'] = "Modify tenders";
        $data['crud_permissions'] = Utils::crudPermissions();
        if (in_array(3, $data['crud_permissions'])) {
            $data['page_data'] = DB::table('tenders')->where('tender_id', $id)->first();
            $data['tender_files'] = DB::table('tender_files')->where('tender_id', $id)->get();
            $data['edit_id'] = $id;
            return view('admin.catalog.tenders.edit', $data);
        } else {
            return redirect('access-denied');
        }
    }

    public function update(Request $request, $tender_id)
    {


            $request->validate([
                "title" => "required",
                "last_date" => "required",
                "last_time" => "required",
                "tender_type" => "required",
            ], [
                'title.required' => 'This field is required',
                'last_date.required' => 'please enter last date and time',
                'last_time.required' => 'please enter last date and time ',
                'tender_type.required' => 'This field is required',
            ]);
        /*if ($request->tender_type == 2) {
            $insertValues = array(
                'title' => $request->title,
                'tender_type' => $request->tender_type,
                'tender_link' => $request->tender_link,
                'status' => $request->status,
                'date_modified' => now(),
            );
            $up_status = DB::table('tenders')->where('tender_id', $tender_id)->update($insertValues);

        } else {*/
            $insertValues = array(
                'title' => $request->title,
                /*'from_date' => $request->from_date,
                'to_date' => $request->to_date,*/
                'last_date' => $request->last_date,
                'last_time' => $request->last_time,
                'dept' => $request->dept,
                'contact' => $request->contact,
                'contact_email' => $request->contact_email,
                'tender_type' => $request->tender_type,
                'tender_link' => $request->tender_link,
                'status' => $request->status,
                'date_modified' => now(),
            );
            $up_status = DB::table('tenders')->where('tender_id', $tender_id)->update($insertValues);


            DB::table('tender_files')->where('tender_id', $tender_id)->delete();
            if (isset($request->tender_file)) {
                foreach ($request->tender_file as $tf) {
                    $im = (isset($tf['name'])&&!empty($tf['name'])) ? $tf['name'] : '';
                    if (!empty($im)) {
                        $extension = $im->getClientOriginalExtension();
                        $changed_fileName = uniqid() . time() . '-' . $im->getClientOriginalName();
                        $file_saving_path = public_path() . '/uploads/tenders';
                        $im->move($file_saving_path, $changed_fileName);
                        if (isset($tf['file_id']))
                            DB::table('tender_files')->insert(array('file_id' => $tf['file_id'], 'tender_id' => (int)$tender_id, 'title' => $tf['title'], 'file_link' => $changed_fileName));
                        else
                            DB::table('tender_files')->insert(array('tender_id' => (int)$tender_id, 'title' => $tf['title'], 'file_link' => $changed_fileName));


                    } else {
                        if (isset($tf['file_id']))
                            DB::table('tender_files')->insert(array('file_id' => $tf['file_id'], 'tender_id' => (int)$tender_id, 'title' => $tf['title'], 'file_link' => $tf['file_link']));
                        else
                            DB::table('tender_files')->insert(array('tender_id' => (int)$tender_id, 'title' => $tf['title'], 'file_link' => ''));

                    }
                }
            }
       // }
        if ($up_status)
            $request->session()->flash('success', 'Success: tenders Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUrlRoute() . '/tenders');
    }


    public function destroy(Request $request, $id)
    {
        $crud_permissions = Utils::crudPermissions();
        if (in_array(4, $crud_permissions)) {


            $delete_status = DB::table('tenders')->where('tender_id', $id)->delete();
            $trr = DB::table('tender_files')->where('tender_id', (int)$id)->get();
            if (isset($trr))
                foreach ($trr as $trr_f) {
                    if (!empty($trr_f->file_link) && $trr_f->file_link != null)
                        if (file_exists(public_path('/uploads/tenders/' . $trr_f->file_link)))
                            unlink(public_path('/uploads/tenders/' . $trr_f->file_link));
                    DB::table('tender_files')->where('file_id', (int)$trr_f->file_id)->delete();
                }
            if ($delete_status) {
                $request->session()->flash('success', 'tenders removed successfully');

            } else
                $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

            return Redirect()->back();
        } else {
            return redirect('access-denied');
        }
    }

    public function show()
    {
    }
}
