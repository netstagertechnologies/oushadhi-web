<?php

namespace App\Http\Controllers\Auth;

use App\Mails\UserRegistration;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|min:4|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'mobile' => 'required|numeric|digits_between:9,12',
            'phone' => 'required|numeric|digits_between:6,12',
            'district' => 'required|string|min:3|max:50',
            'state' => 'required|string|min:3|max:50',
            'address' => 'required|min:5'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $cus_token = md5(rand());
        return User::create([
            'name' => $data['name'],
            'email' => strtolower($data['email']),
            'password' => Hash::make($data['password']),
            'mobile' => $data['mobile'],
            'phone' => $data['phone'],
            'user_type'=>(isset($data['utype'])&&$data['utype']==2)?2:1,
            'district' => $data['district'],
            'state' => $data['state'],
            'address' => $data['address'],
            'cus_token' => $cus_token,
            'status' => 1,
            'is_verified' => 0,
            'is_admin_verified' => 0
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());

        //$this->guard()->login($user);
        /* $user_id = Auth::user()->id;
         $user_role = Auth::user()->user_role;
         $is_admin = Auth::user()->is_admin;
         Session::put('user_id', $user_id);
         Session::put('user_role',$user_role);
         Session::put('is_admin',$is_admin);*/
            $mail_data = array('user' => $request->name,'user_id'=>Crypt::encrypt($user->id), 'cus_token' => $user->cus_token);
            Mail::to(strtolower($request->email))->send(new UserRegistration($mail_data));
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('status', 'Verification Email has been sent to your registered email id');
    }
}
