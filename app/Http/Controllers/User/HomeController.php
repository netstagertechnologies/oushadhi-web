<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;

use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tabs'] = array('active_li' => 'dashboard-li', 'active_li_item' => 'dashboard-li');
        $data['pending_count'] = DB::table('quotations')->where('status', 1)->where('user_id', Auth::user()->id)->where('quotation_status', 1)->count();
        $data['review_count'] = DB::table('quotations')->where('status', 1)->where('user_id', Auth::user()->id)->where('quotation_status', 2)->count();
        $data['processing_count'] = DB::table('quotations')->where('status', 1)->where('user_id', Auth::user()->id)->where('quotation_status', 3)->count();
        $data['delivered_count'] = DB::table('quotations')->where('status', 1)->where('user_id', Auth::user()->id)->where('quotation_status', 4)->count();
        $data['cancelled_count'] = DB::table('quotations')->where('status', 1)->where('user_id', Auth::user()->id)->where('quotation_status', 5)->count();
        //$data['pending_count']= DB::table('quotations')->where('status', 1)->where('quotation_status',1)->count();
        $data['latest_quotations'] = DB::table('quotations as q')
            ->where('q.user_id', Auth::user()->id)
            ->where('q.status', 1)
            ->orderBy('q.quotation_id', 'DESC')
            ->limit(8)
            ->get();


        return view('user.home', $data);
    }

    public function profile()
    {
        $data['tabs'] = array('active_li' => 'profile-li', 'active_li_item' => 'profile-li-item');
        $data['page_title'] = "Change Profile";
        $data['page_subtitle'] = "";
        $data['page_data'] = User::where('id', (int)Auth::user()->id)->first();
        return view('user.profile.index', $data);
    }

    public function profileSubmit(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'mobile' => 'required|numeric|digits_between:9,12',
            'phone' => 'required|numeric|digits_between:6,12',
            'district' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'address' => 'required'
        ]);
        $up_status = User::where('id', (int)Auth::user()->id)->update([
            'name' => $request->name,
            'mobile' => $request->mobile,
            'phone' => $request->phone,
            'district' => $request->district,
            'state' => $request->state,
            'address' => $request->address,
        ]);
        if ($up_status)
            $request->session()->flash('success', 'Success: banner Modified');
        else
            $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

        return redirect(Utils::getUserUrlRoute() . '/profile');
    }

    public function password()
    {
        $data['tabs'] = array('active_li' => 'profile-li', 'active_li_item' => 'password-li-item');
        $data['page_title'] = "Change Password";
        $data['page_subtitle'] = "";
        return view('user.profile.password', $data);
    }

    public function passwordSubmit(Request $request)
    {
        $request->validate([
            'old_password' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'cpassword' => 'required|string|same:password',
        ]);
        $user_data = User::where('id', (int)Auth::user()->id)->first();
        if ($user_data->password) {
            if (Hash::check( $request->old_password,$user_data->password)) {
                $up_status = User::where('id', (int)Auth::user()->id)->update([
                    'password' => Hash::make($request->password)]);
                if ($up_status)
                    $request->session()->flash('success', 'Success: Password changed');
                else
                    $request->session()->flash('warning', 'Unable to perform requested operation.Please try again');

            } else {
                $request->session()->flash('warning', 'Error: Old password not matched.');
                return redirect(Utils::getUserUrlRoute() . '/password');
            }
        } else {
            $request->session()->flash('warning', 'Error: Invalid User.');
            return redirect(Utils::getUserUrlRoute() . '/password');
        }

        return redirect(Utils::getUserUrlRoute() . '/password');
    }
}
