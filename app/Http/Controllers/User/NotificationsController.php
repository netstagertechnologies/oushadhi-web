<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use Validator;

class NotificationsController extends Controller
{

    public function index()
    {
        $data['tabs'] = array('notification_li' => 'quotation-li', 'active_li_item' => 'notification-li-item');
        $data['page_title'] = "Notifications";
        $data['page_subtitle'] = "";
        $data['notifications'] = array();
        $data['notifications'] = DB::table('notifications')->select('*')->where('user_id', (int)Auth::user()->id)->orderby('date_added', 'DESC')
            ->paginate(20);

        return view('user.notifications', $data);

    }

    public function show($not_id = 0)
    {
        $nots = DB::table('notifications')->select('*')
            ->where('id', (int)$not_id)
            ->first();
        if ($nots) {
            $link = url(\App\Utils::getUserUrlRoute());
            if ($nots->not_type == 'quotation')
                $link = url(\App\Utils::getUserUrlRoute() . '/quotations/' . $nots->not_ref_id);
            DB::table('notifications')
                ->where('id', (int)$not_id)->update(array('read_status' => 1));
            return redirect($link);
        } else
            return redirect(Utils::getUserUrlRoute() . '/notifications');
    }
}
