<?php

namespace App\Http\Controllers\User;

use App\Mails\QuotationAdded;
use App\Mails\QuotationAddedAdmin;
use App\models\Common;
use App\models\Notifications;
use App\models\Product;
use App\models\Settings;
use App\Utils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Mail;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['tabs'] = array('active_li' => 'quotation-li', 'active_li_item' => 'quotation-li-item');
        $data['page_title'] = "Performa Invoices";
        $data['page_subtitle'] = "";
        $data['quotation_status'] = DB::table('quotation_status')->where('status', 1)->get();
        $data['page_data'] = DB::table('quotations')
            ->where('status', 1)
            ->where('user_id', Auth::user()->id)
            ->whereNotIn('quotation_status', [5]);

        if ($request->quotation_id != "")
            $data['page_data'] = $data['page_data']->where('quotation_id', $request->quotation_id);

        if ($request->status != "")
            $data['page_data'] = $data['page_data']->where('quotation_status', $request->status);

        if ($request->from_date != "")
            $data['page_data'] = $data['page_data']->whereDate('date_added', '>=', $request->from_date);

        if ($request->to_date != "")
            $data['page_data'] = $data['page_data']->whereDate('.date_added', '<=', $request->to_date);

        $data['page_data'] = $data['page_data']->orderBy('quotation_id', 'DESC')
            ->paginate(20);
        return view('user.quotations.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['tabs'] = array('active_li' => 'quotation-li', 'active_li_item' => 'quotation-li-item');
        $data['page_title'] = "Performa Invoice";
        $data['page_subtitle'] = "Add Performa Invoice";

        return view('user.quotations.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->products)) {
            $quotation_id = DB::table('quotations')->insertGetId(
                array('user_id' => (int)Auth::user()->id,
                    'total_original' => 0,
                    'total' => 0,
                    'status' => 1,
                    'quotation_status' => 1,
                    'date_added' => now(),
                    'date_modified' => now())
            );
            DB::table('quotations_copy')->insertGetId(
                array('user_id' => (int)Auth::user()->id,
                    'quotation_id' => (int)$quotation_id,
                    'total_original' => 0,
                    'total' => 0,
                    'status' => 1,
                    'quotation_status' => 1,
                    'date_added' => now(),
                    'date_modified' => now())
            );
            $total = 0;
            foreach ($request->products as $product) {
                if (isset($product['product_id'])) {
                    // Removes duplicates
                    // DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->where('user_id', (int)Auth::user()->id)->delete();
                    if (isset($product['desc']))
                        foreach ($product['desc'] as $price_det) {
                            if ((int)$price_det['qty'] > 0 && (int)$price_det['price'] > 0) {
                                $details_id = DB::table('quotation_details')->insertGetId(
                                    array('quotation_id' => (int)$quotation_id,
                                        'product_id' => (int)$product['product_id'],
                                        'product_name' => $product['name'],
                                        'qty_original' => $price_det['qty'],
                                        'qty' => $price_det['qty'],
                                        'uprice'=>$price_det['uprice'],
                                        'utax'=>$price_det['utax'],
                                        'price' => $price_det['price'],
                                        'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                        'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                        'total_original' => ($price_det['price'] * $price_det['qty']),
                                        'total' => ($price_det['price'] * $price_det['qty']),
                                        'admin_id' => 0,
                                        'unit_value' => $price_det['unit_value'],
                                        'is_deleted' => 0,
                                        'date_added' => now(),
                                        'date_modified' => now())
                                );
                                DB::table('quotation_details_copy')->insert(
                                    array('details_id' => $details_id, 'quotation_id' => (int)$quotation_id,
                                        'product_id' => (int)$product['product_id'],
                                        'product_name' => $product['name'],
                                        'qty_original' => $price_det['qty'],
                                        'qty' => $price_det['qty'],
                                        'uprice'=>$price_det['uprice'],
                                        'utax'=>$price_det['utax'],
                                        'price' => $price_det['price'],
                                        'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                        'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                        'total_original' => ($price_det['price'] * $price_det['qty']),
                                        'total' => ($price_det['price'] * $price_det['qty']),
                                        'admin_id' => 0,
                                        'unit_value' => $price_det['unit_value'],
                                        'is_deleted' => 0,
                                        'date_added' => now(),
                                        'date_modified' => now())
                                );
                                $total += ($price_det['price'] * $price_det['qty']);
                            }
                        }
                }
            }
            DB::table('quotation_history')->insert([
                'quotation_id' => (int)$quotation_id, 'status_id' => 1, 'comment' => 'Performa Invoice created', 'date_added' => now()
            ]);
            DB::table('quotations')->where('quotation_id', (int)$quotation_id)->update([
                'total_original' => $total, 'total' => $total
            ]);
            DB::table('quotations_copy')->where('quotation_id', (int)$quotation_id)->update([
                'total_original' => $total, 'total' => $total
            ]);

            $user_email = Auth::user()->email;
            if (isset($user_email) && !empty($user_email)) {
                $quot_data['status'] = Common::getQuotationStatusName(1);
                $quot_data['user_name'] = Auth::user()->name;
                $quot_data['quotation_id'] = (int)$quotation_id;
                $quot_data['quotation'] = Common::getQuotationDetails((int)$quotation_id);

                $mail_data = array('data' => $quot_data, 'comment' => 'New Performa Invoice added');
                Mail::to($user_email)->send(new QuotationAdded($mail_data));
                Mail::to(Settings::getQuotationEmailSettings())->send(new QuotationAddedAdmin($mail_data));
            }

            if ($quotation_id) {
                $request->session()->flash('success', 'Success: Performa Invoice added');
                $mg = 'Customer `' . Auth::user()->name . '` added a new Performa Invoice `Performa Invoice#' . $quotation_id . '`';
                Notifications::addNotification(1, 'quotation', 'admin', (int)$quotation_id, $mg, '');

            } else
                $request->session()->flash('error', 'Sorry: Performa Invoice failed. Please try again');

        } else {
            //no product selected

            $request->session()->flash('error', 'Sorry: You haven\'t selected any product');
        }


        return redirect(Utils::getUserUrlRoute() . '/quotations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $quotation_id)
    {
        //
        $data['tabs'] = array('active_li' => 'quotation-li', 'active_li_item' => 'quotation-li-item');
        $data['page_title'] = "Performa Invoice #" . $quotation_id;
        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->where('user_id', Auth::user()->id)
            ->first();
        if (isset($quotation)) {
            $data['page_data'] = $quotation;
            $data['quotation_details'] = array();
            $data['quotation_status'] = DB::table('quotation_status')->where('status', 1)->get();
            $data['quotation_histories'] = DB::table('quotation_history as h')
                ->join('quotation_status as s', 'h.status_id', '=', 's.status_id')
                ->select('h.date_added', 'h.status_id', 'h.comment', 's.name')
                ->where('h.quotation_id', (int)$quotation_id)
                ->orderBy('quotation_history_id', 'ASC')
                ->get();
            $data['quotation_details'] = $quotation_details = DB::table('quotation_details as q')
                ->select('q.*',DB::raw('(SELECT p.model FROM product p WHERE p.product_id = q.product_id) as product_code'))->where('q.quotation_id', (int)$quotation_id)->where('q.is_deleted', 0)->get();
            return view('user.quotations.view', $data);
        } else {
            $request->session()->flash('error', 'Sorry: Performa Invoice failed. Please try again');

            return redirect(Utils::getUserUrlRoute() . '/quotations');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $quotation_id)
    {
        //
        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->where('user_id', (int)Auth::user()->id)
            ->first();
        if (isset($quotation)) {
            if ($quotation->quotation_status == 1) {
                $data['tabs'] = array('active_li' => 'quotation-li', 'active_li_item' => 'quotation-li-item');
                $data['page_title'] = "Performa Invoice";
                $data['page_subtitle'] = "Modify Performa Invoice";
                $data['page_data'] = $quotation;
                $data['edit_id'] = (int)$quotation_id;
                $data['quotation_details'] = array();
                $data['quotation_details'] = $quotation_details = DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->where('is_deleted', 0)->get();

                return view('user.quotations.edit', $data);
            } else {
                $request->session()->flash('error', 'Sorry: You are not allowed to edit this Performa Invoice');

                return redirect(Utils::getUserUrlRoute() . '/quotations');
            }
        } else {
            $request->session()->flash('error', 'Sorry: You are not allowed to edit this Performa Invoice');

            return redirect(Utils::getUserUrlRoute() . '/quotations');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $quotation_id)
    {

        if (isset($request->products)) {
            $total = 0;
            DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->delete();
            foreach ($request->products as $product) {
                if (isset($product['product_id'])) {
                    if (isset($product['desc']))
                        foreach ($product['desc'] as $price_det) {
                            if (isset($product['details_id'])) {
                                $details_id = DB::table('quotation_details')->insertGetId(
                                    array('details_id' => (int)$product['details_id'],
                                        'quotation_id' => (int)$quotation_id,
                                        'product_id' => (int)$product['product_id'],
                                        'product_name' => $product['name'],
                                        'qty_original' => $price_det['qty'],
                                        'qty' => $price_det['qty'],
                                        'price' => $price_det['price'],
                                        'uprice'=>$price_det['uprice'],
                                        'utax'=>$price_det['utax'],
                                        'total_original' => ($price_det['price'] * $price_det['qty']),
                                        'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                        'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                        'total' => ($price_det['price'] * $price_det['qty']),
                                        'admin_id' => 0,
                                        'unit_value' => $price_det['unit_value'],
                                        'is_deleted' => $product['status'],
                                        'date_added' => now(),
                                        'date_modified' => now())
                                );
                                if (!$product['status'])
                                    $total += ($price_det['price'] * $price_det['qty']);
                            } else {
                                if ((int)$price_det['qty'] > 0 && (int)$price_det['price'] > 0) {
                                    DB::table('quotation_details')->insertGetId(
                                        array('quotation_id' => (int)$quotation_id,
                                            'product_id' => (int)$product['product_id'],
                                            'product_name' => $product['name'],
                                            'qty_original' => $price_det['qty'],
                                            'qty' => $price_det['qty'],
                                            'price' => $price_det['price'],
                                            'uprice'=>$price_det['uprice'],
                                            'utax'=>$price_det['utax'],
                                            'total_original' => ($price_det['price'] * $price_det['qty']),
                                            'total_uprice'=>($price_det['uprice']* $price_det['qty']),
                                            'total_utax'=>($price_det['utax'] * $price_det['qty']),
                                            'total' => ($price_det['price'] * $price_det['qty']),
                                            'unit_value' => $price_det['unit_value'],
                                            'is_deleted' => 0,
                                            'admin_id' => 0,
                                            'date_added' => now(),
                                            'date_modified' => now())
                                    );
                                    $total += ($price_det['price'] * $price_det['qty']);
                                }
                            }
                        }
                }
            }
            $up_status = DB::table('quotations')->where('quotation_id', (int)$quotation_id)->update([
                'total_original' => $total, 'total' => $total, 'date_modified' => now()
            ]);

            if ($up_status)
                $request->session()->flash('success', 'Success: Performa Invoice updated');
            else
                $request->session()->flash('error', 'Sorry: Performa Invoice updation failed. Please try again');

        } else {
            //no product selected

            DB::table('quotations_copy')->where('quotation_id', (int)$quotation_id)->delete();
            DB::table('quotation_details_copy')->where('quotation_id', (int)$quotation_id)->delete();
            DB::select("INSERT into quotations_copy (SELECT * FROM quotations where quotation_id='" . (int)$quotation_id . "'");
            DB::select("INSERT into quotation_details_copy (SELECT * FROM quotation_details where quotation_id='" . (int)$quotation_id . "'");
            $request->session()->flash('error', 'Sorry: You haven\'t selected any product');
        }


        return redirect(Utils::getUserUrlRoute() . '/quotations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $quotation_id)
    {
        //
        $delete_status = DB::table("quotations")->where('quotation_id', $quotation_id)->update(['status' => 0]);
        if ($delete_status) {
            $request->session()->flash('success', 'Performa Invoice removed successfully');
        } else
            $request->session()->flash('error', 'Unable to perform requested operation.Please try again');

        return Redirect()->back();
    }

    function fetch(Request $request)
    {
        if ($request->get('query')) {
            $query = $request->get('query');
            $data = DB::table('product')
                ->select('product_id', 'title')
                ->where('status', '1')
                ->where('title', 'LIKE', "%{$query}%")
                ->get();

            $json = array();
            foreach ($data as $result) {
                $json[] = array(
                    'product_id' => $result->product_id,
                    'name' => $result->title
                );
            }

            return response()->json($json, 200);;
        }
    }

    function fetchPrice(Request $request)
    {
        if ($request->get('product_id')) {
            $product_id = (int)$request->get('product_id');
            $data = DB::table('product_price')
                ->where('product_id', $product_id)
                ->get();
            $tax_array = Product::getProductTaxOptions((int)$request->get('product_id'));
            $json = array();
            foreach ($data as $result) {
                $price = $result->price;
                if (Auth::user()->user_type == 2)
                    $price = $result->dealer_price;

                $tax = 0;
                if ($tax_array['type'] == 'P')
                    $tax = round((($price * $tax_array['value']) / 100), 2);
                elseif ($tax_array['type'] == 'F')
                    $tax = $tax_array['value'];

                $json[] = array(
                    'unit_value' => $result->unit_value,
                    'price' => round(($price + $tax), 2),
                    'uprice' => $price,
                    'utax' => round($tax, 2),
                );
            }

            return response()->json($json, 200);;
        }
    }

    function cancelQuotation(Request $request)
    {
        $quotation_id = (isset($request->quotation_id)) ? (int)$request->quotation_id : 0;
        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->where('user_id', Auth::user()->id)
            ->whereNotIn('quotation_status', [4])
            ->first();
        if (isset($quotation)) {
            $cl_quot = DB::table('cancelled_quotations')->insert([
                'quotation_id' => (int)$quotation_id,
                'user_id' => Auth::user()->id, 'type' => 'user',
                'reason' => $request->reason, 'date_added' => now()
            ]);
            $up_status = DB::table('quotations')->where('quotation_id', (int)$quotation_id)->update([
                'quotation_status' => 5, 'date_modified' => now()
            ]);
            $history_id = DB::table('quotation_history')->insertGetId(
                array('quotation_id' => (int)$quotation_id, 'status_id' => 5,
                    'comment' => 'User cancelled Performa Invoice. Reason described: ' . $request->reason, 'date_added' => now())
            );
            if ($up_status) {
                $request->session()->flash('success', 'Success: Performa Invoice Cancelled');
                $mg = 'quotation#' . $quotation_id . ' has been cancelled.';
                Notifications::addNotification(1, 'quotation', 'admin', (int)$quotation_id, $mg, '');

                return redirect(Utils::getUserUrlRoute() . '/cancelled-quotations/');

            } else
                $request->session()->flash('error', 'Sorry: Performa Invoice cancellation failed. Please try again');

        } else
            $request->session()->flash('error', 'Sorry: you can\'t cancel this Performa Invoice');


        return redirect(Utils::getUserUrlRoute() . '/quotations/');
    }

    public function cancelledQuotations()
    {
        $data['tabs'] = array('cancelled-quot_li' => 'quotation-li', 'active_li_item' => 'cancelled-quot-li-item');
        $data['page_title'] = "Cancelled Performa Invoice";
        $data['page_subtitle'] = "";

        $data['page_data'] = DB::table('quotations as q')
            ->join('cancelled_quotations as cq', 'cq.quotation_id', '=', 'q.quotation_id')
            ->select('q.*', 'cq.reason', 'cq.date_added as date_cancel')
            ->orderBy('cancel_id', 'DESC')
            ->paginate(20);
        return view('user.quotations.cancel_index', $data);
    }

}
