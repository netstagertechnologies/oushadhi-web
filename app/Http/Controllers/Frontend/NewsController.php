<?php

namespace App\Http\Controllers\Frontend;

use App\models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));


        $data['news'] = News::where('status', 1)->get();
        $data['breadcrumbs'][] = array('name' => 'News', 'link' => '#');
        $data['page_title'] = 'Media';
        return view('frontend.news.index', $data);
    }

    public function details(Request $request, $slug = '')
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'News', 'link' => URL::to('/') . '/news');

        $medias = News::where('slug', $slug)->first();
        if ($medias) {
            $data['breadcrumbs'][] = array('name' => $medias->title, 'link' => '#');
            $data['page_title'] = $medias->title;
            $data['page_data'] = $medias;
            return view('frontend.news.details', $data);
        } else
            return redirect('page-not-found');

        return view('frontend.news.details', $data);
    }
}
