<?php

namespace App\Http\Controllers\Frontend;

use App\models\Category;
use App\models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use DB;

class ProductController extends Controller
{
    //
    public function index(Request $request, $slug = null, $featured = null)
    {
        $data['pdt_type']=0;
        $data['form_action'] = $request->url();
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));

        $data['categories'] = Category::where('status', 1)->where('product_type',0)->orderBy('sort_order', 'ASC')->get();
        $data['products'] = DB::table('product as p')
            ->join('category as c','c.category_id','=','p.category_id')
            ->select('p.*')
            ->where('p.status', 1)->where('c.product_type',0);
        $data['category_selected'] = $slug;
        $search_name = (!empty($request->search)) ? $request->search : '';
        $sort = (!empty($request->sort)) ? $request->sort : '';
        if (!empty($search_name))
            $data['products'] = $data['products']->where('p.title', 'LIKE', "%$search_name%");
        if ($featured == 'featured')
            $data['products'] = $data['products']->where('p.featured', 1);

        if ($slug == null) {
            $data['breadcrumbs'][] = array('name' => 'Products', 'link' => '#');
            $data['page_title'] = "Products";
            //$data['form_action'] = URL::to('/') . '/products';


        } else {
            if ($slug == 'featured') {
                $data['page_title'] = 'Featured Products';
                //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                $data['products'] = $data['products']->where('p.featured', 1);

            } else {
                $category_data = Category::where('slug', $slug)->first();
                if ($category_data) {
                    $data['breadcrumbs'][] = array('name' => $category_data->title, 'link' => '#');
                    $data['page_title'] = $category_data->title;
                    //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                    $data['products'] = $data['products']->where('p.category_id', (int)$category_data->category_id);
                } else
                    return redirect('page-not-found');
            }
        }
        if ($sort == 'name-az')
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        elseif ($sort == 'name-za')
            $data['products'] = $data['products']->orderBy('p.title', 'DESC');
        else
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        $data['products'] = $data['products']->paginate(40);
        return view('frontend.products.products', $data);
    }

    public function details(Request $request, $slug = null)
    {
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));
        $data['breadcrumbs'][] = array('name' => 'Products', 'link' => URL::to('/') . '/products');

        $data['page_title'] = "Products";
        if ($slug != null) {
            $product_data = Product::where('slug', $slug)->first();
            if ($product_data) {
                $data['breadcrumbs'][] = array('name' => $product_data->title, 'link' => '#');
                $data['page_title'] = $product_data->title;
                $data['product'] = $product_data;
                if(isset($data['product']->product_id))
                Product::where('product_id',(int)$data['product']->product_id)->increment('views',1);
                $data['product_prices'] = DB::table('product_price')->where('product_id', (int)$product_data->product_id)->orderBy('price_id', 'ASC')->get();
                $data['product_images'] = DB::table('product_images')->where('product_id', (int)$product_data->product_id)->orderBy('sort_order', 'ASC')->get();

            } else
                return redirect('page-not-found');

            return view('frontend.products.details', $data);
        } else
            return redirect('page-not-found');

    }

    public function sProductsCategory(Request $request, $slug = null, $featured = null)
    {
        $data['pdt_type']=1;
        $data['form_action'] = $request->url();
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));

        $data['categories'] = Category::where('status', 1)->where('product_type',1)->orderBy('sort_order', 'ASC')->get();
        $data['products'] = DB::table('product as p')
            ->join('category as c','c.category_id','=','p.category_id')
            ->select('p.*')
            ->where('p.status', 1)->where('c.product_type',1);
        $data['category_selected'] = $slug;
        $search_name = (!empty($request->search)) ? $request->search : '';
        $sort = (!empty($request->sort)) ? $request->sort : '';
        if (!empty($search_name))
            $data['products'] = $data['products']->where('p.title', 'LIKE', "%$search_name%");
        if ($featured == 'featured')
            $data['products'] = $data['products']->where('p.featured', 1);

        if ($slug == null) {
            $data['breadcrumbs'][] = array('name' => 'Products', 'link' => '#');
            $data['page_title'] = "Products";
            //$data['form_action'] = URL::to('/') . '/products';


        } else {
            if ($slug == 'featured') {
                $data['page_title'] = 'Featured Products';
                //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                $data['products'] = $data['products']->where('p.featured', 1);

            } else {
                $category_data = Category::where('slug', $slug)->first();
                if ($category_data) {
                    $data['breadcrumbs'][] = array('name' => $category_data->title, 'link' => '#');
                    $data['page_title'] = $category_data->title;
                    //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                    $data['products'] = $data['products']->where('p.category_id', (int)$category_data->category_id);
                } else
                    return redirect('page-not-found');
            }
        }
        if ($sort == 'name-az')
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        elseif ($sort == 'name-za')
            $data['products'] = $data['products']->orderBy('p.title', 'DESC');
        else
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        $data['products'] = $data['products']->paginate(40);
        return view('frontend.products.products', $data);
    }

    public function sProducts(Request $request, $slug = null, $featured = null)
    {
        $data['form_action'] = $request->url();
        $data['breadcrumbs'][] = array('name' => 'Home', 'link' => URL::to('/'));

        $data['categories'] = Category::where('status', 1)->where('product_type',1)->orderBy('sort_order', 'ASC')->get();
        $data['products'] = DB::table('product as p')
            ->join('category as c','c.category_id','=','p.category_id')
            ->select('p.*')
            ->where('p.status', 1)->where('c.product_type',1);
        $data['category_selected'] = $slug;
        $search_name = (!empty($request->search)) ? $request->search : '';
        $sort = (!empty($request->sort)) ? $request->sort : '';
        if (!empty($search_name))
            $data['products'] = $data['products']->where('p.title', 'LIKE', "%$search_name%");
        if ($featured == 'featured')
            $data['products'] = $data['products']->where('p.featured', 1);

        if ($slug == null) {
            $data['breadcrumbs'][] = array('name' => 'Siddha Products', 'link' => '#');
            $data['page_title'] = "Siddha Products";
            //$data['form_action'] = URL::to('/') . '/products';


        } else {
            if ($slug == 'featured') {
                $data['page_title'] = 'Featured Products';
                //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                $data['products'] = $data['products']->where('p.featured', 1);

            } else {
                $category_data = Category::where('slug', $slug)->first();
                if ($category_data) {
                    $data['breadcrumbs'][] = array('name' => $category_data->title, 'link' => '#');
                    $data['page_title'] = $category_data->title;
                    //$data['form_action'] = URL::to('/') . '/c/'.$slug;
                    $data['products'] = $data['products']->where('p.category_id', (int)$category_data->category_id);
                } else
                    return redirect('page-not-found');
            }
        }
        if ($sort == 'name-az')
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        elseif ($sort == 'name-za')
            $data['products'] = $data['products']->orderBy('p.title', 'DESC');
        else
            $data['products'] = $data['products']->orderBy('p.title', 'ASC');
        $data['products'] = $data['products']->paginate(40);
        return view('frontend.products.s-products', $data);
    }

}
