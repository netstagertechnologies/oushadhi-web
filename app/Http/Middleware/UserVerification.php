<?php

namespace App\Http\Middleware;

use App\models\Organisation;
use Closure;
use Auth;
use Session;
class UserVerification
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_type == 1 || Auth::user()->user_type == 2) {
            if ((Auth::user()->is_admin_verified)==0) {
                Auth::logout();
                $request->session()->flash('warning', 'Your account is not verified . Please contact Admin.');
                return redirect(route('login'));
            }elseif ((Auth::user()->is_admin_verified)==2) {
                Auth::logout();
                $request->session()->flash('warning', 'Your account rejected . Please contact Admin.');
                return redirect(route('login'));
            }

                return $next($request);
        } else {
            Auth::logout();
            $request->session()->invalidate();
            $request->session()->flash('warning', 'Access denied');
            return redirect(route('login'));
        }
    }

}
