<?php

namespace App\Http\ViewComposers;

use App\models\Settings;
use Illuminate\Contracts\View\View;
use DB;

class GeneralViewComposer
{

    public function compose(View $view)
    {
       $site_settings=Settings::getSettings();


        $view->with('site_settings', $site_settings);


    }
}

?>
