<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuMaster extends Model
{

  //table names

  protected $table = 'menu_master';


  protected $fillable = [
      'menu_name', 'menu_path', 'menu_parent', 'menu_icon' , 'status'
  ];


}
