<?php



namespace App;



use DB;



final class Permissions

{



    /*

       * get permissions

    */



    public static function user_permissions($role = null){
        if($role){
            $role_menu = UserPermissionMap::where('role_id' , $role)->get();

          $permitted_menu_id = [];
          foreach($role_menu as $row){
            $permitted_menu_id[] = $row -> menu_id;
          }
          $foobar = new Permissions;
          $permissons = $foobar->get_permission_roles($permitted_menu_id ,$role);
        }else{
          $permissons = MenuMaster::where('status',1)->orderBy('menu_order', 'asc')->get();
        }
        return $permissons;



    }



    /*

        * get user permission role

    */



    public function get_permission_roles($array_id = null,  $role = null){

      $menus_childs = DB::table('menu_master as master')

              ->join('permission_map as map' , 'master.id', '=' , 'map.menu_id')

              ->where('map.role_id' , $role )

              ->whereIn('master.id' , $array_id)

              ->get();



      $menus_child_parent_id = DB::table('menu_master as tbl1')

              ->whereIn('tbl1.id' , $array_id)->get(['menu_parent']);

      $parent_id = [];

      $in = 0;

      foreach($menus_child_parent_id as $row){

          $parent_id[$in] = $row -> menu_parent;

          $in++;

      }

      $menus_parents = DB::table('menu_master as tbl2')

              ->whereIn('tbl2.id' , $parent_id)->orderBy('menu_order', 'asc')->get();



      $array_result = array_merge($menus_parents->toArray(), $menus_childs->toArray());

      return $array_result;

    }

}

