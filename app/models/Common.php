<?php

namespace App\models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;

class Common extends Model
{
    //

    public static function getQuotationStatusName($status_id = 0)
    {
        $status_name = 'NA';
        if ((int)$status_id) {
            $quot = DB::table('quotation_status')->where('status_id', (int)$status_id)->where('status', 1)->first();
            if ($quot)
                return $quot->name;
        }
        return $status_name;
    }

    public static function getQuotationDetails($quotation_id = 0)
    {
        $quot = array();

        $quotation = DB::table('quotations')
            ->where('quotation_id', (int)$quotation_id)
            ->where('status', 1)
            ->first();
        if (isset($quotation)) {
            $quot['quotation_id']=$quotation->quotation_id;
            $quot['user_id']=$quotation->user_id;
            $quot['total']=$quotation->total;
            $quot['status_id']=$quotation->quotation_status;
            $quot['status']=Common::getQuotationStatusName((int)$quotation->quotation_status);
            $quot['date_added']=$quotation->date_added;
            $quot['date_modified']=$quotation->date_modified;
            $quot['user_details'] = User::getUserDetails($quotation->user_id);
            $quot['quotation_details'] = DB::table('quotation_details')->where('quotation_id', (int)$quotation_id)->where('is_deleted', 0)->get();

        }
        return $quot;
    }
}
