<?php

namespace App\models;


use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $primaryKey = 'page_id';

    protected $fillable = [
        'title' , 'page_content' ,'slug', 'banner' , 'metaTitle' , 'metaDescription' , 'metaKeyword' , 'createdDate' , 'status'
    ];
}
