<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blog';

    protected $primaryKey = 'blog_id';
    public $timestamps = false;
    protected $fillable = [
        'title' , 'description' ,'slug', 'short_description', 'image' , 'status' , 'date_added' , 'date_modified',
    ];
}
