<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model
{
    protected $table = 'product';

    protected $primaryKey = 'product_id';
    public $timestamps = false;
    protected $fillable = [
        'title', 'description', 'slug', 'category_id', 'image', 'status', 'date_added', 'date_modified', 'meta_title',
        'meta_description', 'keywords', 'model', 'featured', 'show_home','total_ingradients', 'main_ingradients', 'indication', 'dosage', 'views',
    ];

    public static function getAllTaxes()
    {
        $taxes = array();
        $taxes_qurys = DB::table('taxes')->where('status', 1)->get();
        if (isset($taxes_qurys) && count($taxes_qurys))
            foreach ($taxes_qurys as $taxes_qury)
                $taxes[] = array('tax_id' => $taxes_qury->tax_id,
                    'name' => $taxes_qury->tax_name,
                    'type' => $taxes_qury->type,
                    'value' => $taxes_qury->tax_value);

        return $taxes;
    }

    public static function getProductTaxOptions($product_id){

        $tax_array=array('type'=>'F','value'=>0);
        $product_taxes= DB::table('product as p')->join('taxes as t','t.tax_id','=','p.tax')
            ->where('t.status',1)->where('p.product_id',(int)$product_id)->first();
        if($product_taxes)
            $tax_array=array('type'=>$product_taxes->type,'value'=>$product_taxes->tax_value);
        return $tax_array;
    }
}
