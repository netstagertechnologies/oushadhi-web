<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class FactoryImage extends Model
{
    protected $table = 'factory_images';

    protected $primaryKey = 'factory_id';
    public $timestamps = false;
    protected $fillable = [
        'title' ,  'image' , 'date_added' , 'date_modified',
    ];
}
