<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $primaryKey = 'category_id';
    public $timestamps = false;
    protected $fillable = [
        'title' , 'slug' , 'image' ,'parent_id','sort_order','product_type', 'status' , 'date_added' , 'date_modified',
    ];
}
