<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';

    protected $primaryKey = 'activity_id';
    public $timestamps = false;
    protected $fillable = [
        'title' , 'description' ,'slug',  'image' , 'status' , 'date_added' , 'date_modified',
    ];
}
