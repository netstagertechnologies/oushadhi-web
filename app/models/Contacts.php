<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $primaryKey = 'contact_id';
    public $timestamps = false;
    protected $fillable = [
        'name' , 'res_no' ,'mob_no',  'designation' ,'email', 'status' ,'contact_type','sort_order', 'date_added' , 'date_modified',
    ];
}
