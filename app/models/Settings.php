<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Settings extends Model
{
    public static function getSettings()
    {
        $settings=array();
        $query = DB::table('settings')->get();
        if($query)
            foreach ($query as $row)
                $settings[$row->code]=$row->value;
        return $settings;

    }

    public static function getEmailSettings()
    {
        $query = DB::table('settings')->where('code','config_contact_email')->first();
        if(isset($query->value)&&$query->value!='')
            return $query->value;
        return 'info@oushadhi.com';

    }
    public static function getQuotationEmailSettings()
    {
        $query = DB::table('settings')->where('code','config_quotation_email')->first();
        if(isset($query->value)&&$query->value!='')
            return $query->value;
        return 'info@oushadhi.com';

    }

    public static function editSettings($data)
    {
        foreach ($data as $key => $value) {

            if (!self::checkExistanceofRow($key)) {
                DB::table('settings')
                    ->insert(array('code' => $key, 'value' => $value, 'user_id' => Auth::user()->id, 'date_added' => now()));
            } else {
                DB::table('settings')->whereRaw("`code` LIKE '" . $key . "'")
                    ->update(array('value' => $value, 'user_id' => Auth::user()->id, 'date_added' => now()));
            }
        }


    }

    public static function checkExistanceofRow($key)
    {
        $query = DB::table('settings')->whereRaw("`code` LIKE '" . $key . "'")->get();
        return sizeof($query);

    }
}
