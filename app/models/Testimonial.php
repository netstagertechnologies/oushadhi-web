<?php

namespace App\models;


use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $table = 'testimonials';

    protected $primaryKey = 'testimonial_id';
    public $timestamps = false;
    protected $fillable = [
        'title' , 'description' ,'name', 'designation' , 'image' , 'status' , 'date_added' , 'date_modified',
    ];
}
