<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $primaryKey = 'news_id';
    public $timestamps = false;
    protected $fillable = [
        'title' , 'description' ,'slug',  'image' , 'status' , 'date_added' , 'date_modified',
    ];
}
