<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermissionMap extends Model
{
    protected $table = 'permission_map';


    protected $fillable = [
        'role_id', 'menu_id', 'permissions'
    ];

    public $timestamps = false;
}
